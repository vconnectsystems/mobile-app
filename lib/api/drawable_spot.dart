class DrawableSpot{

  final String name;
  final List<double> x;
  final List<double> y;
//  final double x;
//  final double y;
  final String svg;
  final bool clickable;
  final String key;
  final int id;
  final bool booked;
  final double price;
  final double boatLength;
  final double boatWidth;
  final double boatDepth;
  final bool multi;

  DrawableSpot({
    this.name,
    this.x,
    this.y,
    this.svg,
    this.clickable,
    this.key,
    this.id,
    this.booked,
    this.price,
    this.boatLength,
    this.boatWidth,
    this.boatDepth,
    this.multi
  });

}
