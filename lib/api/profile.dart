class Profile {

  String name;
  String mail;
  String phone;
  int countryId;
  String home;
  String bName;
  int bType;
  String bModel;
  double bLength;
  double bWidth;
  double bDepth;
  String bPhoto;

  Profile({
    this.name,
    this.mail,
    this.phone,
    this.countryId,
    this.home,
    this.bName,
    this.bType,
    this.bModel,
    this.bLength,
    this.bWidth,
    this.bDepth,
    this.bPhoto,
  });

  factory Profile.fromJson(Map<String, dynamic> data) {
    return Profile(
      name: data['name'],
      mail: data['email'],
      phone: data['phone'],
      countryId: data['country_id'],
      home: data['home_harbour'],
      bName: data['boat_name'],
      bLength: data['boat_length'],
      bWidth: data['boat_width'],
      bDepth: data['boat_depth'],
      bPhoto: data['boat_picture'],
    );
  }

}

