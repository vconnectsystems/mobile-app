class BoatType {

  final int id;
  final String name;
  final int ind;

  BoatType({
    this.id,
    this.name,
    this.ind
  });

}
