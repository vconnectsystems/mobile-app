class Country {

  final int id;
  final String iso;
  final String name;
  final int ind;

  Country({
    this.id,
    this.iso,
    this.name,
    this.ind
  });

}
