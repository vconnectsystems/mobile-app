import 'campaign.dart';

class Harbour {

  final int id;
  final int ind;
  final String name;
  final String locationNorth;
  final String locationEast;
  final double latitude;
  final double longitude;
  final String master;
  final String address;
  final String address2;
  final String country;
  final String phone;
  final String map;
  final String subscriptionType;
  final int arrival;
  final int departure;
  final bool serviceRestaurant;
  final bool serviceToilet;
  final bool serviceWashing;
  final bool serviceElectricity;
  final bool serviceFuel;
  final List<String> gallery;
  final List<Campaign> campaigns;

  Harbour({
    this.id,
    this.ind,
    this.name,
    this.locationNorth,
    this.locationEast,
    this.latitude,
    this.longitude,
    this.master,
    this.address,
    this.address2,
    this.country,
    this.phone,
    this.map,
    this.subscriptionType,
    this.arrival,
    this.departure,
    this.serviceRestaurant,
    this.serviceToilet,
    this.serviceWashing,
    this.serviceElectricity,
    this.serviceFuel,
    this.gallery,
    this.campaigns
  });

//  factory Harbour.fromJson(Map<String, dynamic> data) {
//    return Harbour(
//        id: data['id'],
//        ind: data['ind'],
//        name: data['name'],
//        locationNorth: data['locationNorth'],
//        locationEast: data['locationEast'],
//        latitude: data['latitude'],
//        longitude: data['longitude'],
//        master: data['master'],
//        address: data['address'],
//        address2: data['address2'],
//        country: data['country'],
//        phone: data['phone'],
//        map: data['map'],
//        subscriptionType: data['subscription_type'],
//        arrival: data['arrival'],
//        departure: data['departure'],
//        serviceRestaurant: data['serviceRestaurant'],
//        serviceToilet: data['serviceToilet'],
//        serviceWashing: data['serviceWashing'],
//        serviceElectricity: data['serviceElectricity'],
//        serviceFuel: data['serviceFuel'],
//        gallery: data['gallery']
//    );
//  }

}

