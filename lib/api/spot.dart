class Spot {

  final String key;
  final int id;
  final bool booked;
  final double price;
  final double boatWidth;
  final double boatLength;
  final double boatDepth;
  final bool multi;
  final String serialNumber;

  Spot({
    this.key,
    this.id,
    this.booked,
    this.price,
    this.boatWidth,
    this.boatLength,
    this.boatDepth,
    this.multi,
    this.serialNumber
  });

//  factory Spot.fromJson(Map<String, dynamic> data) {
//    return Spot(
//      key: data['key'],
//      id: data['id'],
//      booked: data['booked'],
//      price: data['price'],
//      boatWidth: data['width'],
//      boatLength: data['length'],
//      boatDepth: data['depth'],
//      multi: data['is_multiple_booking']
//    );
//  }

}