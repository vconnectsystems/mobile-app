import '../bloc/global_state.dart';

class Message {

  final int id;
  final int harbourId;
  final String harbourName;
  final String username;
  final bool read;
  final String content;
  final DateTime createdOn;
  final DateTime updatedOn;

  Message({
    this.id,
    this.harbourId,
    this.harbourName,
    this.username,
    this.read,
    this.content,
    this.createdOn,
    this.updatedOn
  });

  factory Message.fromJson(Map<String, dynamic> data) {

    final GlobalState _store = GlobalState.instance;

    String getHarbourName(int harbourId){
      int harbourInd;
      for(int i = 0; i<_store.harbours.length; i++){
        if(_store.harbours[i].id == harbourId){
          harbourInd = i;
          break;
        }
      }
      return _store.harbours[harbourInd].name;
    }

    return Message(
      id: data['id'],
      harbourId: data['harbour_id'],
      harbourName: getHarbourName(data['harbour_id']),
      username: data['username'],
      read: data['read'] == 1 ? true : false,
      content: data['content'],
      createdOn: DateTime.parse(data['created_at']),//.toString().substring(0,4)
      updatedOn: DateTime.parse(data['updated_at'])
      //DateTime.parse(data['created_at']),
    );
  }

}