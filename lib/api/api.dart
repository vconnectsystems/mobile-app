import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:collection';
import 'package:http/http.dart';
import 'package:flutter/foundation.dart';
//import 'package:path_provider/path_provider.dart';
import 'harbour.dart';
import 'campaign.dart';
import 'profile.dart';
import 'country.dart';
import 'boat_type.dart';
import 'spot.dart';
import 'booking.dart';
import 'master_booking.dart';
import 'message.dart';
import 'message_read.dart';
//import 'language.dart';
import '../bloc/global_state.dart';

class Api {
  final GlobalState _store = GlobalState.instance;
  final HttpClient client = new HttpClient();

  final String base = "https://api.dock.vconnect.systems/api"; // DEV
  final String mapsFtpUrl = 'http://dock.vconnect.systems/harbour-maps/app_maps';
  // final String base = "https://api.docksideapp.dk/api"; // LIVE
  // final String mapsFtpUrl = 'https://marina.docksideapp.dk/harbour-maps/app_maps';

  final String loginUrl = 'login';
  final String checkEmailUrl = 'check-email';
  final String registerUrl = 'register';
  final String resetPassUrl = 'request-new-password';
  final String harbourListUrl = 'harbour/getList';
  final String spotsUrl = 'harbour-map/get-boat-spots';
  final String newPasswordUrl = 'user/change-password';

  final String getProfileUrl = 'user/profile';
  final String updateProfileUrl = 'user/update-profile';
  final String uploadBoatPhotoUrl = 'user/upload-boat-picture';
  final String setOccupiedUrl = 'user/is-occupied';
  final String getCountriesUrl = 'get-countries';

  final String addBookingUrl = 'booking/add';
  final String addShortBookingUrl = 'booking/add-short';
  final String cancelBookingUrl = 'booking/cancel';
  final String deleteBookingUrl = 'booking/remove';
  final String getBookingUrl = 'booking/get-bookings';
  final String rentBookingUrl = 'booking/rent';
  final String setReturnDateUrl = 'booking/set-return-date';
  final String getPaymentLinkUrl = 'payment/get-link';
  final String checkPaymentUrl = 'payment/check-booking';
  final String updateBookingUrl = 'booking/update';

  final String getMessagesUrl = 'message/inbox';
  final String readMessageUrl = 'message/read';
  final String delMessageUrl = 'message/delete';
  final String sendMessageUrl = 'message/send';
  final String replyMessageUrl = 'message/reply';
  final String newMessageCountUrl = 'message/new-messages';

  final String masterBookingsUrl = 'bookings/get-active-bookings';
  final String masterBookingDetailsUrl = 'bookings/get-booking-details';

  Api() {
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
  }

  Future<bool> login(String mail, String pass) async {
    print('_store.devToken: ${_store.devToken}');
    Map map = {
      "email": mail,
      "password": pass,
      "device_token": _store.devToken
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$loginUrl'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String str = await response.transform(utf8.decoder).join();
    bool success = true;
    if(str.contains('error') || str.contains('<html>')){
      success = false;
    } else {
      var parsedJson = json.decode(str);
      _store.apiToken = 'Bearer '+parsedJson['api_token'];
      _store.isMaster = parsedJson['is_master'];
      if(parsedJson['is_permanent'] == 1){
        _store.permanent = true;
        _store.occupied = (parsedJson['is_occupied'] == 1);
        if(parsedJson['rent_date_end'] != 0 && parsedJson['rent_date_end'] != '0' && parsedJson['rent_date_end'] != null){
          _store.availableTill = DateTime.parse(parsedJson['rent_date_end']);
          _store.permanentTill = DateTime.parse(parsedJson['date_end']);
        } else {
          _store.availableTill = null;
        }
      }
    }
    return success;
  }

  Future checkEmail(String mail) async {
    Map map = {
      "email": mail,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$checkEmailUrl'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future register(String name, String mail, int countryId, String bModel, double bLength, double bWidth, double bDepth, String pass) async {
    Map map = {
      "name": name,
      "email": mail,
      "country_id": countryId,
      "boat_model": bModel,
      "boat_length": bLength,
      "boat_width": bWidth,
      "boat_depth": bDepth,
      "password": pass
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$registerUrl'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future resetPassword(String mail) async {
    Map map = {
      "email": mail,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$resetPassUrl'));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future changePassword(String pass, String token) async {
    Map map = {
      "password": pass,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$newPasswordUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future sendMessage(int harbourId, String body, String token) async {
    Map map = {"harbour_id": harbourId, "content": body};
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$sendMessageUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future replyMessage(int messId, String body, String token) async {
    Map map = {
      "content": body,
      "message_id": messId,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$replyMessageUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future<int> getNewMessagesCount(String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$newMessageCountUrl'), headers: headers);
    return json.decode(response.body)['new_messages_count'];
  }

  Future updateProfile(Profile profile, String token) async {
    Map map = {
      "name": profile.name,
      "email": profile.mail,
      "phone": profile.phone,
      "country_id": profile.countryId,
      "home_harbour": profile.home,
      "boat_name": profile.bName,
      "boat_type": profile.bType,
      "boat_model": profile.bModel,
      "boat_length": profile.bLength,
      "boat_width": profile.bWidth,
      "boat_depth": profile.bDepth,
      "boat_picture": profile.bPhoto
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$updateProfileUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  Future uploadBoatPhoto(String base64Image, String token) async {
    Map map = {
      "boat_picture": base64Image,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$uploadBoatPhotoUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    return await response.transform(utf8.decoder).join();
  }

  //String notes,
  Future<int> addBooking(String start, String end, int spotId, int harbourId, String token) async {
    Map map = {
      "date_start": start,
      "date_end": end,
      "boat_spot_id": spotId,
      "harbour_id": harbourId,
      "note": '' //notes
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$addBookingUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    var parsedJson = json.decode(value);
    print('addBooking, parsedJson: $parsedJson');
    // print('addBooking, parsedJson[bookingId]: ${parsedJson['bookingId']}');
    _store.bookingId = parsedJson['bookingId'];
    print('_store.bookingId: ${_store.bookingId}');
    return parsedJson['bookingId'];
  }

  Future<int> addShortBooking(int spotId, int harbourId, String token) async {
    Map map = {"boat_spot_id": spotId, "harbour_id": harbourId, "note": ''};
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$addShortBookingUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    var parsedJson = json.decode(value);
    _store.bookingId = parsedJson['bookingId'];
    return parsedJson['bookingId'];
  }

  Future getPaymentLink(int bookingId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    // print('getPaymentLink bookingId: $bookingId');
    // print('getPaymentLink token: $token');
    Response response = await get(Uri.parse('$base/$getPaymentLinkUrl/$bookingId'), headers: headers);
    print('getPaymentLink response.body: ${response.body}');
    final parsedJson = json.decode(response.body);
    // 'error: could not create payment link
    if(parsedJson.toString().contains('payment_link')){
      _store.bookingPaymentLink = parsedJson['payment_link'];
    } else {
      _store.bookingPaymentLink = '';
    }
    // print('parsedJson[no_payment_required]: ${parsedJson['no_payment_required']}');
    // if(parsedJson['no_payment_required'] != null){}
    // _store.bookingPaymentLink = parsedJson['payment_link'];
    // print('parsedJson: $parsedJson');
  }

  Future checkPayment(int bookingId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$checkPaymentUrl/$bookingId'), headers: headers);
    final parsedJson = json.decode(response.body);
    return parsedJson['status_id'];
  }

  Future cancelBooking(int bookingId, String token) async {
    Map map = {"booking_id": bookingId, "status_id": 2};
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$cancelBookingUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    var parsedJson = json.decode(value);
    return parsedJson;
  }

  Future updateBooking(int bookingId, String start, String end, String token) async {
    print('updateBooking, bookingId: $bookingId');
    print('updateBooking, start: $start');
    print('updateBooking, end: $end');
    print('updateBooking, token: $token');
    Map map = {"date_start": start, "date_end": end};
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$updateBookingUrl/$bookingId'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    return value;
  }

  Future setOccupied(String token) async {
    Map map = {
      "is_occupied": 1,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$setOccupiedUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    String parsedJson = json.decode(value);
    return parsedJson;
  }

  Future rentBooking(String dateEnd, String token) async {
    Map map = {
      "date_end": dateEnd,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$rentBookingUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    //var parsedJson = json.decode(value);
    //print('rentBooking response: $parsedJson');
    return value;
  }

  Future setReturnDate(String dateEnd, String token) async {
    Map map = {
      "rent_date_end": dateEnd,
    };
    HttpClientRequest request = await client.postUrl(Uri.parse('$base/$setReturnDateUrl'));
    request.headers.set(HttpHeaders.contentTypeHeader, 'application/json');
    request.headers.set(HttpHeaders.authorizationHeader, token);
    request.add(utf8.encode(json.encode(map)));
    HttpClientResponse response = await request.close();
    String value = await response.transform(utf8.decoder).join();
    //var parsedJson = json.decode(value);
    //print('rentBooking response: $parsedJson');
    return value;
  }

  Future deleteBooking(int bookingId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$deleteBookingUrl/$bookingId'), headers: headers);
    final parsedJson = json.decode(response.body);
    return parsedJson;
  }

  Future<bool> getProfile(String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$getProfileUrl'), headers: headers);
    final responseJson = json.decode(response.body);
    // Types
    LinkedHashMap<String, dynamic> types = responseJson['boat_type']['types'];
    List<BoatType> typesList = [];
    int counter = 0;
    _store.boatTypeIndex = -1;
    types.forEach((k, v) {
      BoatType bt = new BoatType(id: int.parse(k), name: v, ind: counter);
      typesList.add(bt);
      if (int.parse(k) == responseJson['boat_type']['selected']) _store.boatTypeIndex = counter;
      counter++;
    });
    _store.boatTypes = typesList;
    //
    Profile profile = new Profile(
      name: responseJson['name'] == null ? '' : responseJson['name'],
      mail: responseJson['email'] == null ? '' : responseJson['email'],
      phone: responseJson['phone'] == null ? '' : responseJson['phone'],
      countryId: responseJson['country_id'] == null ? -1 : responseJson['country_id'],
      home: responseJson['home_harbour'] == null ? '' : responseJson['home_harbour'],
      bName: responseJson['boat_name'] == null ? '' : responseJson['boat_name'],
      bType: responseJson['boat_type']['selected'] == null ? -1 : responseJson['boat_type']['selected'],
      bModel: responseJson['boat_model'] == null ? '' : responseJson['boat_model'],
      bLength: responseJson['boat_length'] == null ? 0.0 : double.parse((responseJson['boat_length']).toString()),
      bWidth: responseJson['boat_width'] == null ? 0.0 : double.parse((responseJson['boat_width']).toString()),
      bDepth: responseJson['boat_depth'] == null ? 0.0 : double.parse((responseJson['boat_depth']).toString()),
      bPhoto: responseJson['boat_picture'] == null ? '' : responseJson['boat_picture'],
    );
    _store.profile = profile;
    return true;
  }

  Future<bool> getCountries(String lang) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
    };
    Response response = await get(Uri.parse('$base/$getCountriesUrl?language=$lang'), headers: headers);
    final responseJson = json.decode(response.body);
    List<dynamic> list = responseJson;
    List<Country> countries = [];
    for (int i = 0; i < list.length; i++) {
      Country country = new Country(id: list[i]['id'], iso: list[i]['iso'], name: list[i]['name'], ind: i);
      countries.add(country);
    }
    _store.countries = countries;
    _store.countryIndex = getCountryIndexFromId(_store.profile.countryId);
    return true;
  }

  int getCountryIndexFromId(int id) {
    int ind = -1;
    for (int i = 0; i < _store.countries.length; i++) {
      if (_store.countries[i].id == id) {
        ind = i;
        break;
      }
    }
    return ind;
  }

  Future<bool> getSpots(int harbourId, String startDate, String endDate, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    String newUrl = '$base/$spotsUrl?harbour_id=$harbourId&date_start=$startDate&date_end=$endDate';
    Response response = await get(Uri.parse(newUrl), headers: headers);
    final responseJson = json.decode(response.body);
    // print('getSpots.responseJson: $responseJson');
    LinkedHashMap<String, dynamic> spots = responseJson;
    List<Spot> spotsList = [];
    spots.forEach((k, v) {
      // if(k.contains('ZONE') || k.contains('G')){
      //   print('KEY: $k');
      // }
      RawSpot rs = RawSpot.fromJson(v);
      Spot sp = new Spot(
        id: rs.id,
        booked: rs.booked,
        price: rs.price,
        boatWidth: rs.boatWidth,
        boatLength: rs.boatLength,
        boatDepth: rs.boatDepth,
        multi: rs.multi,
        key: k,
        serialNumber: rs.serialNumber
      );
      spotsList.add(sp);
    });
    _store.spotsList = spotsList;
    _store.serverSpots = spots;
    return true;
    // return spots;
  }

//  int getHarbourIdFromName(String hName){
//    int hId = -1;
//    for(int i = 0; i<_store.harbours.length; i++){
//      if(_store.harbours[i].name == hName){
//        hId = _store.harbours[i].id;
//        break;
//      }
//    }
//    return hId;
//  }

  Future<List<Booking>> getBookings(String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$getBookingUrl'), headers: headers);
    print('getBookings, response.body: ${response.body}');
    final responseJson = json.decode(response.body);
    List<dynamic> list = responseJson;
    List<Booking> bookings = [];
    for (int i = 0; i < list.length; i++) {
      int harbourId = list[i]['harbour_id'];
      int harbourInd;
      for (int j = 0; j < _store.harbours.length; j++) {
        if (_store.harbours[j].id == harbourId) {
          harbourInd = j;
          break;
        }
      }
      Booking booking = new Booking(
        id: list[i]['id'],
        statusId: list[i]['status_id'],
        boatSpot: list[i]['boat_spot_name'],
        harbourId: list[i]['harbour_id'],
        harbourInd: harbourInd,
        harbourName: list[i]['harbour_name'],
        startDate: DateTime.parse(list[i]['start_date']),
        endDate: DateTime.parse(list[i]['end_date']),
        updatedAt: DateTime.parse(list[i]['updated_at'] != null ? list[i]['updated_at'] : DateTime.now().toString()),
        price: double.parse((list[i]['price']).toString()),
        totalPrice: double.parse((list[i]['total_price']).toString()),
        link: list[i]['link'].toString().length > 10 ? list[i]['link'] : ""
      );
      bookings.add(booking);
    }
    return bookings;
  }

  Future<bool> getMasterBookings(String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$masterBookingsUrl'), headers: headers);
    final responseJson = json.decode(response.body);
    List<dynamic> list = responseJson;
    List<MasterBooking> bookings = [];
    for (int i = 0; i < list.length; i++) {
      MasterBooking booking = new MasterBooking(
        id: list[i]['id'],
        boatSpot: list[i]['boat_spot_number'],
        userName: list[i]['user_name'],
        boatName: list[i]['boat_name'],
        startDate: DateTime.parse(list[i]['date_start']),
        endDate: DateTime.parse(list[i]['date_end']),
        totalPrice: int.parse((list[i]['total_price']).toString()));
      bookings.add(booking);
    }
    bookings.sort((a,b) {
      var aDate = a.boatSpot;
      var bDate = b.boatSpot;
      return aDate.compareTo(bDate);
    });
    _store.masterBookings = bookings;
    return true;
  }

  Future getMasterBookingDetails(int bookingId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$masterBookingDetailsUrl/$bookingId'), headers: headers);
    Map<String, dynamic> details = json.decode(response.body);
    return details;
  }

  Future<List<Message>> getMessages(String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$getMessagesUrl'), headers: headers);
    final responseJson = json.decode(response.body);
    List<dynamic> list = responseJson;
    List<Message> messages = [];
    for (int i = 0; i < list.length; i++) {
      Message message = Message.fromJson(list[i]);
      messages.add(message);
    }
    return messages;
  }

  Future<MessageReadObj> readMessage(int messId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json", // or whatever
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$readMessageUrl/$messId'), headers: headers);
    final responseJson = json.decode(response.body);
    Map<String, dynamic> map = responseJson;
    List<dynamic> list = map['contents'];
    List<SingleMessage> messList = [];
    for (int i = 0; i < list.length; i++) {
      SingleMessage single = SingleMessage.fromJson(list[i]);
      messList.add(single);
    }
    MessageReadObj message = new MessageReadObj(id: map['id'], harbourId: map['harbourId'], messages: messList);
    return message;
  }

  Future<String> delMessage(int messId, String token) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await get(Uri.parse('$base/$delMessageUrl/$messId'), headers: headers);
    return response.body;
  }

  Future<String> getSVG(String fileName) async {
    String url = '$mapsFtpUrl/$fileName';
    var request = await client.getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    return utf8.decode(bytes.buffer.asUint8List());
  }

  Future<void> getHarbours() async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: "application/json",
    };
    Response response = await get(Uri.parse('$base/$harbourListUrl'), headers: headers);
    print('response.body: ${response.body}');
    final responseJson = json.decode(response.body);
    List<dynamic> list = responseJson;
    List<Harbour> harbours = [];
    for (int i = 0; i < list.length; i++) {
      print('list[i]: ${list[i]}');
      RawHarbour rh = RawHarbour.fromJson(list[i]);
      List<dynamic> gal = rh.gallery;
      List<String> gallery = [];
      for (int j = 0; j < gal.length; j++) gallery.add(gal[j]);
      print('- - - - - - - - - - - - - - - - - -');
      print('rh.name: ${rh.name}');
      print('rh.subscriptionType: ${rh.subscriptionType}');
      print('rh.latitude: ${rh.latitude}');
      print('rh.longitude: ${rh.longitude}');
      Harbour harbour = new Harbour(
        id: rh.id,
        ind: i,
        name: rh.name,
        locationNorth: rh.locationNorth,
        locationEast: rh.locationEast,
        latitude: rh.latitude,
        longitude: rh.longitude,
        master: rh.master,
        address: rh.address,
        address2: rh.address2,
        country: rh.country,
        phone: rh.phone.toString().replaceAll('+', '00'),
        map: rh.map,
        subscriptionType: rh.subscriptionType,
        arrival: rh.arrival,
        departure: rh.departure,
        serviceRestaurant: rh.serviceRestaurant,
        serviceToilet: rh.serviceToilet,
        serviceWashing: rh.serviceWashing,
        serviceElectricity: rh.serviceElectricity,
        serviceFuel: rh.serviceFuel,
        gallery: gallery,
        campaigns: rh.campaigns,
      );
      harbours.add(harbour);
    }
    _store.harbours = harbours;
  }
}

class RawHarbour {
  final int id;
  final String name;
  final String locationNorth;
  final String locationEast;
  final double latitude;
  final double longitude;
  final String master;
  final String address;
  final String address2;
  final String country;
  final String phone;
  final String map;
  final String subscriptionType;
  final int arrival;
  final int departure;
  final bool serviceRestaurant;
  final bool serviceToilet;
  final bool serviceWashing;
  final bool serviceElectricity;
  final bool serviceFuel;
  final List<dynamic> gallery;
  final List<Campaign> campaigns;

  RawHarbour({
    this.id,
    this.name,
    this.locationNorth,
    this.locationEast,
    this.latitude,
    this.longitude,
    this.master,
    this.address,
    this.address2,
    this.country,
    this.phone,
    this.map,
    this.subscriptionType,
    this.arrival,
    this.departure,
    this.serviceRestaurant,
    this.serviceToilet,
    this.serviceWashing,
    this.serviceElectricity,
    this.serviceFuel,
    this.gallery,
    this.campaigns
  });

  factory RawHarbour.fromJson(Map<String, dynamic> data) {
    List<Campaign> _campaigns = [];
    if (data['campaigns'] != []) {
      for (int i = 0; i < data['campaigns'].length; i++) {
        Campaign _c = new Campaign.fromJson(data['campaigns'][i]);
        _campaigns.add(_c);
      }
    }
    // print('data[latitude]: ${data['latitude']}');
    // print('data[longitude]: ${data['longitude']}');
    return RawHarbour(
      id: data['id'],
      name: data['name'],
      locationNorth: data['locationNorth'],
      locationEast: data['locationEast'],
      latitude: double.parse(data['latitude'] ?? '0'),
      longitude: double.parse(data['longitude'] ?? '0'),
      master: data['harbour_master'],
      address: data['address'],
      address2: data['address2'],
      country: data['country'],
      phone: data['phone'],
      map: data['map'],
      subscriptionType: data['subscription_type'],
      arrival: 10,
      departure: 10,
      serviceRestaurant: data['serviceRestaurant'],
      serviceToilet: data['serviceToilet'],
      serviceWashing: data['serviceWashing'],
      serviceElectricity: data['serviceElectricity'],
      serviceFuel: data['serviceFuel'],
      gallery: data['gallery'],
      campaigns: _campaigns,
    );
  }
}

class RawSpot {
  final int id;
  final bool booked;
  final double price;
  final double boatWidth;
  final double boatLength;
  final double boatDepth;
  final bool multi;
  final String serialNumber;

  RawSpot({
    this.id,
    this.booked,
    this.price,
    this.boatWidth,
    this.boatLength,
    this.boatDepth,
    this.multi,
    this.serialNumber
  });

  factory RawSpot.fromJson(Map<String, dynamic> data) {
    return RawSpot(
      id: data['id'],
      booked: data['booked'],
      price: double.parse(data['price'].toString()),
      boatWidth: double.parse(data['width'].toString()),
      boatLength: double.parse(data['length'].toString()),
      boatDepth: double.parse(data['depth'].toString()),
      multi: data['is_multiple_booking'],
      serialNumber: data['serial_number'],
    );
  }
}
