class MessageReadObj {

  final int id;
  final int harbourId;
  final List<SingleMessage> messages;

  MessageReadObj({
    this.id,
    this.harbourId,
    this.messages
  });

}

class SingleMessage {

  final String content;
  final String sender;
  final DateTime date;

  SingleMessage({
    this.content,
    this.sender,
    this.date
  });

  factory SingleMessage.fromJson(Map<String, dynamic> data) {
    return SingleMessage(
        content: data['content'],
        sender: data['sender'],
        date: DateTime.parse(data['date'])
    );
  }

}