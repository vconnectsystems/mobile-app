class Booking {

  final int id;
  final statusId;
  final String boatSpot;
  final int harbourId;
  final int harbourInd;
  final String harbourName;
  final DateTime startDate;
  final DateTime endDate;
  final DateTime updatedAt;
  final double price;
  final double totalPrice;
  final String link;

  Booking({
    this.id,
    this.statusId,
    this.boatSpot,
    this.harbourId,
    this.harbourInd,
    this.harbourName,
    this.startDate,
    this.endDate,
    this.updatedAt,
    this.price,
    this.totalPrice,
    this.link
  });

//  factory Booking.fromJson(Map<String, dynamic> data) {
//    return Booking(
//      id: data['id'],
//      statusId: data['status_id'],
//      boatSpot: data['boat_spot_name'],
//      harbourId: data['harbour_id'],
//      harbourName: data['harbour_name'],
//      startDate: DateTime.parse(data['start_date']),
//      endDate: DateTime.parse(data['end_date']),
//      price: double.parse(data['price']),
//      updatedAt: DateTime.parse(data['updated_at']),
//      link: data['link'].toString().length > 10 ? data['link'] : ""
//    );
//  }

}