class MasterBooking {

  final int id;
  final String boatSpot;
  final DateTime startDate;
  final DateTime endDate;
  final String userName;
  final String boatName;
  final int totalPrice;

  MasterBooking({
    this.id,
    this.boatSpot,
    this.startDate,
    this.endDate,
    this.userName,
    this.boatName,
    this.totalPrice
  });

}