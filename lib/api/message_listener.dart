import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/pages/common/notification_dialog.dart';
import 'package:dockside/pages/common/device_token_dialog.dart';
import 'package:dockside/pages/common/page_obj.dart';
import 'package:dockside/pages/dashboard/dashboard_data.dart';
import 'package:dockside/pages/dashboard/dashboard_item_obj.dart';
import 'package:dockside/pages/dashboard/dashboard_transition_route.dart';
import 'package:dockside/pages/dashboard/page_wrap.dart';

class MessageListener extends ChangeNotifier {

  static const _testTopic = 'TestTopic';
  static const List<String> _testEmailUsers = ['Chavdar', 'Chavdar', 'Chavdar', 'Phillip', 'Phillip', 'Phillip', 'Stoyan'];
  static const List<String> _testEmails = [
    'chavdardonchev@gmail.com',
    'joevaldon@icloud.com',
    'joedonchev@icloud.com',
    'pb@dockgroup.dk',
    'pbodum@me.com',
    'pb@phillipbodum.com',
    's.todorov@vconnect.dk'
  ];
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  final GlobalState _store = GlobalState.instance;
  final DashboardData _dashData = new DashboardData();
  BuildContext context;
  Bloc _bloc;
  Language _lang;

  Future<void> getPermission() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  Future<bool> getDeviceToken(String email) async {
    _messaging.getToken().then((String value){
      _store.devToken = value;
      for(int i = 0; i < _testEmails.length; i++){
        if(email == _testEmails[i]){
          FirebaseMessaging.instance.subscribeToTopic(_testTopic);
          _store.testUserIndex = i;
          _store.showDeviceToken = false;
          break;
        }
      }
    });
    return true;
  }

  void showDeviceToken(){
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black54,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return DeviceTokenDialog(title: 'Hello ${_testEmailUsers[_store.testUserIndex]}!', body: _store.devToken);
      },
    );
  }

  void _showNotification(RemoteMessage message){
    if (message.data != null) {
      showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black54,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
          bool _isBooking = (message.data['type'] == 'booking_expiring');
          return NotificationDialog(
            isBooking: _isBooking,
            title: message.notification.title,
            body: message.notification.body,
            onTap: (){
              Navigator.of(context).pop();
              _bloc = BlocProvider.of(context);
              _lang = Language.of(context);
              DashboardItemObj obj = _dashData.boxes[_isBooking ? 5 : 2];
              _bloc.changePage(obj.ind);
              _bloc.changePageLoaded(false);
              PageObj pageObj = new PageObj(
                title: _lang.dashTitles[obj.ind],
                subTitle: _lang.dashSubTitles[obj.ind],
                pageId: obj.pageId,
                pageWidget: obj.pageWidget,
                initIndex: _isBooking ? 1 : 0
              );
              Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
            },
          );
        },
      );
    }
  }

  void listenToNotifications(){
    if(!_store.notificationListening){
      _store.notificationListening = true;
      FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage message) {});
      FirebaseMessaging.onMessage.listen(_showNotification);
      FirebaseMessaging.onMessageOpenedApp.listen(_showNotification);
    }
  }

}
