import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class Language {

  final Locale locale;

  Language(this.locale);

  static Language of(BuildContext context) {
    return Localizations.of<Language>(context, Language);
  }

  static Map<String, Map<String, List<String>>> _dashboardValues = {
    'da': {
      'labels':    ['QR BETALING', 'KORT',  'BESKEDER', 'LAV BOOKING', 'PROFIL'], //'SØG HAVN',
      'titles':    ['QR',          'HAVNE', 'HAVNE',    'LAV',         'PROFIL'], //'SØG',
      'subTitles': ['BETALING',    'KORT',  'BESKEDER', 'BOOKING',     'name']    //'HAVNE',
    },
    'en': {
      // 'labels':    ['SEARCH',   'MAP',     'MESSAGES', 'BOOKING', 'PROFILE'],
      'labels':    ['QR PAYMENT', 'MAP',     'MESSAGES', 'BOOKING', 'PROFILE'], //'SEARCH',
      'titles':    ['QR',         'MARINAS', 'MARINAS',  'QUICK',   'PROFILE'], //'SEARCH',
      'subTitles': ['PAYMENT',    'MAP',     'MESSAGES', 'BOOKING', 'name'] //'MARINAS',
    }
  };

  static Map<String, Map<String, String>> _localizedValues = {
    'da': {
      'login_logo': 'assets/images/loginlogo_dk.png',
      'welcome': 'VELKOMMEN',
      'forgot_pass': 'Glemt koden?',
      'as_guest': 'Klik for at se features',//'Fortsæt som gæst',
      'get_profile': 'Opret profil',//'Få en konto'
      'not_member': 'Ikke medlem?',
      'password': 'Kodeord',
      'email': 'E-mail',
      'invalid_email': 'Indtast en gyldig email addresse',
      'invalid_pass': 'Indtast gyldigt password',
      'incorrect_login': 'Forkert email eller kodeord',
      'profile_created': 'Super, din konto er nu oprettet!',
      'profile_created2': 'Klik videre for at logge ind',
      'enter_email': 'Indtast email addresse',
      'enter_name': 'Indtast navn',
      'at_least3': 'Brug mindst 3 tegn',
      'pass_match': 'De to kodeord skal være identiske',
      'pass8': 'Dit kodeord skal indeholde mindst 8 symboler',
      'reg_problem': 'Vi kunne ikke gemme din profil',
      'enter_pass': 'Indtast kodeord...',
      'enter_pass2': 'Indtast kodeord igen...',
      'forgot_complete1': 'Der er blevet sendt et midlertidigt',
      'forgot_complete2': 'kodeord til din indbakke',
      'no_email': 'Ingen profil med den emailaddresse',
      'reset_problem': 'Der var et problem med at nulstille dit kodeord',
      'enter_email2': 'Indtast email addresse...',
      'search': 'Søg',
      'harbour': 'Havn',
      'harbours': 'Havne',
      'call_error': 'Kunne ikke ringe op',
      'harbour_master': 'Havnefoged',
      'tel': 'Tel:',
      'quick': 'Lav',
      'booking': 'Booking',
      'bookings': 'Bookinger',
      'profile': 'Profil',
      'save_profile_error': 'Vi kunne ikke gemme din profil',
      'save_profile_success': 'Din profil blev gemt',
      'pr_name': 'Navn...',
      'pr_phone': '+00 12 34 56 78...',
      'pr_email': 'E-mail...',
      'pr_home': 'Hjemhavn...',
      'pr_boat_name': 'Bådnavn...',
      'pr_boat_type': 'Bådtype...',
      'pr_boat_model': 'Båd model...',
      'pr_Length': 'Bådlængde...',
      'pr_width': 'Bådbredde...',
      'pr_depth': 'Båddybde...',
      'pr_Length2': 'Bådlængde: ',
      'pr_width2': 'Bådbredde: ',
      'pr_depth2': 'Båddybde: ',
      'no_bookings': 'Ingen bookinger...',
      'message': 'Beskeder',
      'messages': 'Beskeder',
      'no_messages': 'Ingen beskeder...',
      'del_message': 'Sikker på at du vil slette din besked?',
      'to': 'Til: ',
      'mess_send_error': 'Ups, beskeden kunne ikke sendes',
      'mess_send_success': 'Din besked blev sendt!',
      'cancel': 'Annuller',
      'send': 'Send',
      'del_mess': 'Slet besked',
      'delete': 'Slet',
      'spots_for_light': 'Når du har lagt til på en plads, skal du vælge pladsnummeret på kortet og derefter betale for overnatningen',
      'spots_for_period1': 'Der er ',
      'spots_for_period2': ' ledige pladser som passer til din bådstørrelse i den valgte periode',
      'max_size': 'Max størrelse: ',
      'available_for': 'Ledig på dine valgte dage',
      'total_price': 'Total Pris: ',
      'loading': 'Arbejder...',
      'ok': 'OK',
      'go_back': 'Tilbage',
      'book': 'Book',
      'cancelled': 'Booking er blevet annulleret',
      'deleted': 'Bookingen er blevet fjernet',
      'cancel_error': 'Kunne ikke annullere booking',
      'delete_error': 'Der var et problem med at fjerne bookingen',
      'payment_error': 'Betaling kunne ikke gennemføres',
      'pay_for': 'Betal for',
      'select_arrival': 'Vælg ankomst tid',
      'select_departure': 'Vælg afrejse tid',
      'hint': 'Husk: ',
      'arrival_note': 'der kan altid afbestilles op til én time før, derefter koster det en dagsleje...',
      'departure_note': 'Se mere info og redigér din booking under din Profil',
      'book_email_error': 'Venligst indtast email addresse',
      'book_boat_name_error': 'Venligst, oplys dit bådnavn', // TODO
      'book_phone_error': 'Havnen kræver dit telefonnummer',
      'book_depth_error': 'Venligst gå til din profil og indtast bådens dybgang i m',
      'book_width_error': 'Venligst gå til din profil og indtast bådens længde i m',
      'book_length_error': 'Indtast bådens længde i m',
      'book_arrival_error': 'Vælg ankomstdato',
      'book_departure_error': 'Vælg afgangsdato',
      'book_harbour_error': 'Vælg havn',
      'select_harbour': 'Vælg havn...',
      'dear': 'Kære ',
      'thank_you1': 'Mange tak for din booking.',
      'details': 'Detaljer:',
      'spot': 'Plads ',
      'from': 'Fra: ',
      'date': 'Dato',
      'orig_message': 'original besked:',
      'more_info1': 'Se mere info og redigér din booking under "',
      'more_info2': '" i din profil.',
      'cancel_policy': '',
//      'cancel_policy': 'Anullerings politk: ',
      'thank_you2': 'TAK FOR DIN BOOKING',
      //'cancel_policy': 'Afbestillings-/anulleringspolitik: ',
      'confirmation': 'BEKRÆFTIGELSE',
      'select': 'Vælg',
      'available': 'LEDIG',
      'occupied': 'OPTAGET',
      'logout': 'Log ud',
      'cancel_booking': 'Sikker på at du vil annullerer din booking?',
      'delete_booking': 'Er du sikker på at du vil fjerne denne booking?',
      'price_per_day': 'Pris pr. dag: ',
      'yes': 'Ja',
      'no': 'Nej',
      'cancelled_word': 'Annulleret',
      'status': 'Status: ',
      'ended': 'Sluttede',
      'booked': 'Booked',
      'no_available' : 'Vi har desværre ikke nogen pladser i systemet til din bådstørrelse. Kontakt venligst havnefogeden.',
      'cancel_policy_body': '',
//      'cancel_policy_body': 'Annullerings politk:\nLorem ipsum dolor sit amet, consectetur adipiscing elit. '
//          'Aliquam semper at turpis id tristique. '
//          'Proin pellentesque ornare ipsum, eget scelerisque nulla consectetur at. '
//          'Curabitur a elit odio. Nullam maximus risus sed fermentum posuere.',
      'more_days_plus' : 'Du har valgt yderligere [1] til din booking.\n'
          'Din nuværende booking vil blive anulleret og refunderet automatisk. '
          'Prisen har ændret sig til [2] DKK[3] for den nye booking.',
      'day' : 'dag',
      'days' : 'dage',
      'more_days_minus' : 'Du har valgt [1] mindre til din booking.\n'
          'Din nuværende booking vil blive anulleret og refunderet automatisk. '
          'Prisen har ændret sig til [2] DKK[3] for den nye booking.',
      'no_changes' : 'Du har ingen ændringer lavet i din booking.',
      'per_day' : 'per dag',
      'same_days_count' : 'Du har valgt samme antal dage for din booking.\nTryk "bekræft" for a gemme ændringerne.',
      'sorry_booked' : 'Beklager men plads #[1] i [2] er ikke ledig på de valgt datoer.\n'
          'Du kan enten vælge andre datoer eller gå tilbage og tjekke andre pladser.',
      'confirm_btn' : 'Bekræft',
      'confirm' : 'BEKRÆFT',
      'already_booked' : 'Beklager, men pladsen du har valgt er lige blevet booket af en anden sejler.\nVenligst vælg en anden plads eller andre datoer.',
      'available_ttl' : 'Hvornår kommer du retur?',
      'available_hint' : 'jo tidligere du notificere om din retur dato - jo nemmere er det for havnefogeden at organisere havnen!',
      'conditions_text1' : 'Ved oprettelse af profil, accepterer du hermed ',
      'conditions_text2' : 'DOCKSIDE\'s generelle ',
      'conditions_btn' : 'handelsbetingelser',
      'conditions_link' : 'https://www.docksideapp.dk/handelsbetingelser',
      'guest_message' : 'For at lave en booking, kræver det at du opretter en profil først.',
      'register' : 'Registrér',
      'light_harbour_ttl' : 'BETAL PR. DAG',
      'light_harbour_mess' : 'For denne havn skal du betale pr. dag. '
          'Pladsen gælder til kl 10:00 dagen efter. Hvis du ikke ønsker at blive, '
          'skal du forlade pladsen inden kl 12:00',
      'you_selected': 'Du har valgt plads:',
      'nationality': 'Vælg nationalitet',
      'nationality_hint': 'Vælg nationalitet...',
      'nationality_error': 'Venligst, vælg en nationalitet',
      'pending': 'Afventende',
      'already_registered': 'Emailen er allerede i systemet',
      'only_today': 'Denne plads kan kunnes bookes for dagen i dag',
      'isGreenTtl': 'Er skiltet på pladsen grønt?',
      'isGreenTxt': 'Skiltet på pladsen skal være grønt før du må lægge til og inden du betaler for din overnatning eller anløb.',
      'select_boat_type': 'Vælg bådtype',
      'model_error': 'Skriv venligst minimum 5 bogstaver for din bådmodel',
      'boat_length_error': 'Venligst angiv din båds længde',
      'boat_width_error': 'Venligst angiv din båds bredde',
      'boat_depth_error': 'Venligst angiv din båds dybde',
      'phone_error': 'Beklager men dit telefonnummer er indtastet forkert',
      'photo_add': 'Tilføj',
      'photo_change': 'Skift',
      'photo_profile': 'Tryk for at tilføje et profilbillede',
      'photo_boat': 'Tryk for at vælge et båd foto',
      'profile_photo': 'Båd foto...',
      'boat_photo': 'Båd foto...',
      'all': 'Alle',
      //
      'forgot_pass_mess_1': 'Indtast din email adresse, så sender vi dig et midlertidigt kodeord:',
      'forgot_pass_mess_2': 'Vi har sendt dig et midlertidigt kodeord. Skriv det her:',
      'pass_code': 'Indtast kodeord...',
      'incorrect_code': 'Forkert kodeord',
      'forgot_pass_mess_3': 'Du kan nu logge ind med dit nye kodeord',
      'overnight': 'OVERNAT',
      'midnight': 'ANLØB',
      'midnightNote': 'Ved anløb betales der for nuværende tidspunkt og frem til kl 18:00. '
          'Ved ankomst efter kl 18:00 skal der betales for en overnatning og ikke anløb. '
          'Bemærk at der kan forekomme kontrol.'
    },
    'en': {
      'login_logo': 'assets/images/loginlogo.png',
      'welcome': 'WELCOME',
      'forgot_pass': 'Forgot password?',
      'as_guest': 'Click to see a sneak peak',//'Continue as guest',
      'get_profile': 'Get an account',
      'not_member': 'Not a member?',
      'password': 'Password',
      'email': 'E-mail',
      'invalid_email': 'Please, enter a valid email',
      'invalid_pass': 'Please, enter a valid password',
      'incorrect_login': 'Incorrect email or password',
      'profile_created': 'Great, your account is now created!',
      'profile_created2': 'Tap to continue',
      'enter_email': 'Enter your email',
      'enter_name': 'Enter your name',
      'at_least3': 'Enter your name',
      'pass_match': 'Your password and confirmation password must match',
      'pass8': 'Your password must be at least 8 symbols',
      'reg_problem': 'There was a problem while trying to register your account',
      'enter_pass': 'Enter password',
      'enter_pass2': 'Re-enter password',
      'forgot_complete1': 'We have sent a temporary password',
      'forgot_complete2': 'to your email address',
      'no_email': "Sorry, we couldn't find an account with this email",
      'reset_problem': 'Sorry, there was a problem while trying to reset your password',
      'enter_email2': 'Enter your email...',
      'search': 'Search',
      'harbour': 'Marina',
      'harbours': 'Marinas',
      'call_error': 'Couldn\'t call',
      'harbour_master': 'Harbour master',
      'tel': 'Tel:',
      'quick': 'Quick',
      'booking': 'Booking',
      'bookings': 'Bookings',
      'profile': 'Profile',
      'save_profile_error': 'There was a problem while saving your profile',
      'save_profile_success': 'Your profile was saved successfully!',
      'pr_name': 'Name...',
      'pr_phone': '+00 12 34 56 78...',
      'pr_email': 'E-mail...',
      'pr_home': 'Home marina...',
      'pr_boat_name': 'Boat name...',
      'pr_boat_type': 'Boat type...',
      'pr_boat_model': 'Boat model...',
      'pr_Length': 'Boat lenght...',
      'pr_width': 'Boat width...',
      'pr_depth': 'Boat draft...',
      'pr_Length2': 'Boat lenght: ',
      'pr_width2': 'Boat width: ',
      'pr_depth2': 'Boat draft: ',
      'no_bookings': 'No bookings...',
      'message': 'Message',
      'messages': 'Messages',
      'no_messages': 'No messages...',
      'del_message': 'Are you sure you want to delete this message?',
      'to': 'To: ',
      'mess_send_error': 'There was a problem while sending the message',
      'mess_send_success': 'Your message was sent successfully!',
      'cancel': 'Cancel',
      'send': 'Send',
      'del_mess': 'Delete message',
      'delete': 'Delete',
      'spots_for_light': 'When you have docked your boat, please pick the spot on the map and pay for the night',
      'spots_for_period1': 'There are ',
      'spots_for_period2': ' available spots that fit your boat size for the selected period',
      'max_size': 'Max size: ',
      'available_for': 'Available for the selected period',
      'total_price': 'Total price: ',
      'loading': 'Loading...',
      'ok': 'OK',
      'go_back': 'Go back',
      'book': 'Book',
      'cancelled': 'The booking has been cancelled',
      'deleted': 'The booking was removed',
      'cancel_error': 'There was an error while trying to cancel your booking',
      'delete_error': 'There was a problem while trying to remove the booking',
      'payment_error': 'Sorry, the payment was not successful. Please, try again',
      'pay_for': 'Payment for',
      'select_arrival': 'Select time of arrival',
      'select_departure': 'Select time of departure',
      'hint': 'Note: ',
      'arrival_note': 'You can always cancel at no cost up to an hour before. Less than 1 hour marina will charge you for one night',
      'departure_note': 'See more info and edit your booking under Profile',
      'book_email_error': 'Please, enter your email',
      'book_boat_name_error': 'Please, enter the name of your boat',
      'book_phone_error': 'The harbour requires your phone number',
      'book_depth_error': 'Please, go to your profile and enter the boat\'s draft',
      'book_width_error': 'Please, go to your profile and enter the boat\'s width',
      'book_length_error': 'Please, go to your profile and enter the boat\'s length',
      'book_arrival_error': 'Please, select arrival date',
      'book_departure_error': 'Please, select departure date',
      'book_harbour_error': 'Please select a marina',
      'select_harbour': 'Select marina...',
      'dear': 'Dear ',
      'thank_you1': 'Thank you very much for your booking!',
      'details': 'Details:',
      'spot': 'Spot ',
      'from': 'From: ',
      'date': 'Date',
      'orig_message': 'original message:',
      'more_info1': 'See more info and edit your booking at "',
      'more_info2': '" in your profile.',
      'cancel_policy': '',
//      'cancel_policy': 'Cancellation policy: ',
      'thank_you2': 'THANK YOU FOR BOOKING',
      'confirmation': 'Confirmation',
      'select': 'Select',
      'available': 'AVAILABLE',
      'occupied': 'OCCUPIED',
      'logout': 'Logout',
      'cancel_booking': 'Are you sure you want to cancel this booking?',
      'delete_booking': 'Are you sure you want to remove this booking?',
      'price_per_day': 'Price per day: ',
      'yes': 'Yes',
      'no': 'No',
      'cancelled_word': 'Cancelled',
      'status': 'Status: ',
      'ended': 'Ended',
      'booked': 'Booked',
      'no_available' : 'Unfortunately, we do not have any spots in the system for the selected dates and/or your boat size. Please contact the marina master.',
      'cancel_policy_body': '',
//      'cancel_policy_body': 'Cancellation policy:\nLorem ipsum dolor sit amet, consectetur adipiscing elit. '
//          'Aliquam semper at turpis id tristique. '
//          'Proin pellentesque ornare ipsum, eget scelerisque nulla consectetur at. '
//          'Curabitur a elit odio. Nullam maximus risus sed fermentum posuere.',
      'more_days_plus' : 'You have selected additional [1] to your booking.\n'
          'Your current booking will be cancelled and refunded automatically, '
          'then you will be asked to pay a total of [2] DKK[3] for this new booking.',
      'day' : 'day',
      'days' : 'days',
      'more_days_minus' : 'You have selected less [1] for your booking.\n'
          'Your current booking will be cancelled and refunded automatically, '
          'then you will be asked to pay a total of [2] DKK[3] for this new booking.',
      'no_changes' : 'You haven\'t made any changes to your booking',
      'per_day' : 'per day',
      'same_days_count' : 'You have selected the same number of days for your booking.\nPress "Confirm" to save the changes.',
      'sorry_booked' : 'Sorry, the spot #[1] at [2] is currently not available for the selected dates.\n'
          'You can either choose other dates, or cancel this booking and make a new booking with another spot.',
      'confirm_btn' : 'Confirm',
      'confirm' : 'CONFIRM',
      'already_booked' : 'Sorry, the spot you\'ve selected was just booked by another customer.\nPlease, select another spot or other days.',
      'available_ttl' : 'When will you return?',
      'available_hint' : 'the earlier you notify of your return date - the easier it is for the port manager to organize the port!',
      'conditions_text1' : 'By creating a profile, you hereby agree to the ',
      'conditions_text2' : 'DOCKSIDE\'s general ',
      'conditions_btn' : 'terms and conditions',
      'conditions_link' : 'https://www.docksideapp.dk/handelsbetingelser',
      'guest_message' : 'To make a booking, please make a profile first',
      'register' : 'Register',
      'light_harbour_ttl' : 'PAY PER NIGHT',
      'light_harbour_mess' : 'For this harbour you pay per night. '
          'The cutoff time is next day at 10:00 am, with a 2 hr buffer - If you don’t intend to stay, '
          'then departure needs to be before 12:00 pm',
      'you_selected': 'You have selected spot:',
      'nationality': 'Select nationality',
      'nationality_hint': 'Select nationality...',
      'nationality_error': 'Please, select your nationality',
      'pending': 'Pending',
      'already_registered': 'This email was already registered',
      'only_today': 'This spot can be booked only for today',
      'isGreenTtl': 'Is the spot green?',
      'isGreenTxt': 'The sign on the pier must be green before you can dock here and before you pay the harbour fee',
      'select_boat_type': 'Select boat type',
      'model_error': 'Please, enter at least 5 letters for your boat model',
      'boat_length_error': 'Please, specify the length of your boat',
      'boat_width_error': 'Please, specify the width of your boat',
      'boat_depth_error': 'Please, specify the draft of your boat',
      'phone_error': 'Sorry, there\'s an issue with your phone number. Make sure you have entered it correctly',
      'photo_add': 'Add',
      'photo_change': 'Change',
      'photo_profile': 'Tap to select a profile photo',
      'photo_boat': 'Tap to select a boat photo',
      'profile_photo': 'Profile photo...',
      'boat_photo': 'Boat photo...',
      'all': 'All',
      //
      'forgot_pass_mess_1': 'Please, enter your email address and we will send you a temporary password:',
      'forgot_pass_mess_2': 'We have sent a temporary password to your email. Please, enter it here:',
      'pass_code': 'Enter code...',
      'incorrect_code': 'Incorrect code',
      'forgot_pass_mess_3': 'You can now login with your new password',
      'overnight': 'OVERNIGHT',
      'midnight': 'QUICK',
      'midnightNote': 'A quick payment counts from current time and until 6:00 pm. '
          'Please pay for a full day if arrival occurs after 6:00 pm. '
          'Please note, payment inspection will occur.'
    }
  };

  String get loginLogo {return _localizedValues[locale.languageCode]['login_logo'];}
  String get welcome {return _localizedValues[locale.languageCode]['welcome'];}
  String get forgotPass {return _localizedValues[locale.languageCode]['forgot_pass'];}
  String get asGuest {return _localizedValues[locale.languageCode]['as_guest'];}
  String get getProfile {return _localizedValues[locale.languageCode]['get_profile'];}
  String get notAMember {return _localizedValues[locale.languageCode]['not_member'];}
  String get password {return _localizedValues[locale.languageCode]['password'];}
  String get email {return _localizedValues[locale.languageCode]['email'];}
  String get invalidEmail {return _localizedValues[locale.languageCode]['invalid_email'];}
  String get invalidPass {return _localizedValues[locale.languageCode]['invalid_pass'];}
  String get incorrectLogin {return _localizedValues[locale.languageCode]['incorrect_login'];}
  String get profileCreated {return _localizedValues[locale.languageCode]['profile_created'];}
  String get profileCreated2 {return _localizedValues[locale.languageCode]['profile_created2'];}
  String get enterEmail {return _localizedValues[locale.languageCode]['enter_email'];}
  String get enterName {return _localizedValues[locale.languageCode]['enter_name'];}
  String get atLeast3 {return _localizedValues[locale.languageCode]['at_least3'];}
  String get passMatch {return _localizedValues[locale.languageCode]['pass_match'];}
  String get pass8 {return _localizedValues[locale.languageCode]['pass8'];}
  String get regProblem {return _localizedValues[locale.languageCode]['reg_problem'];}
  String get enterPass {return _localizedValues[locale.languageCode]['enter_pass'];}
  String get enterPass2 {return _localizedValues[locale.languageCode]['enter_pass2'];}
  String get forgotComplete1 {return _localizedValues[locale.languageCode]['forgot_complete1'];}
  String get forgotComplete2 {return _localizedValues[locale.languageCode]['forgot_complete2'];}
  String get noEmail {return _localizedValues[locale.languageCode]['no_email'];}
  String get resetProblem {return _localizedValues[locale.languageCode]['reset_problem'];}
  String get enterEmail2 {return _localizedValues[locale.languageCode]['enter_email2'];}
  String get search {return _localizedValues[locale.languageCode]['search'];}
  String get harbour {return _localizedValues[locale.languageCode]['harbour'];}
  String get harbours {return _localizedValues[locale.languageCode]['harbours'];}
  String get callError {return _localizedValues[locale.languageCode]['call_error'];}
  String get harbourMaster {return _localizedValues[locale.languageCode]['harbour_master'];}
  String get tel {return _localizedValues[locale.languageCode]['tel'];}
  String get quick {return _localizedValues[locale.languageCode]['quick'];}
  String get booking {return _localizedValues[locale.languageCode]['booking'];}
  String get bookings {return _localizedValues[locale.languageCode]['bookings'];}
  String get profile {return _localizedValues[locale.languageCode]['profile'];}
  String get saveProfileError {return _localizedValues[locale.languageCode]['save_profile_error'];}
  String get saveProfileSuccess {return _localizedValues[locale.languageCode]['save_profile_success'];}
  String get profileName {return _localizedValues[locale.languageCode]['pr_name'];}
  String get profilePhone {return _localizedValues[locale.languageCode]['pr_phone'];}
  String get profileEmail {return _localizedValues[locale.languageCode]['pr_email'];}
  String get profileHome {return _localizedValues[locale.languageCode]['pr_home'];}
  String get profileBoatName {return _localizedValues[locale.languageCode]['pr_boat_name'];}
  String get profileBoatType {return _localizedValues[locale.languageCode]['pr_boat_type'];}
  String get profileBoatModel {return _localizedValues[locale.languageCode]['pr_boat_model'];}
  String get profileBoatLength {return _localizedValues[locale.languageCode]['pr_Length'];}
  String get profileBoatWidth {return _localizedValues[locale.languageCode]['pr_width'];}
  String get profileBoatDepth {return _localizedValues[locale.languageCode]['pr_depth'];}
  String get profileBoatLength2 {return _localizedValues[locale.languageCode]['pr_Length2'];}
  String get profileBoatWidth2 {return _localizedValues[locale.languageCode]['pr_width2'];}
  String get profileBoatDepth2 {return _localizedValues[locale.languageCode]['pr_depth2'];}
  String get noBookings {return _localizedValues[locale.languageCode]['no_bookings'];}
  String get message {return _localizedValues[locale.languageCode]['message'];}
  String get messages {return _localizedValues[locale.languageCode]['messages'];}
  String get noMessages {return _localizedValues[locale.languageCode]['no_messages'];}
  String get delMessage {return _localizedValues[locale.languageCode]['del_message'];}
  String get to {return _localizedValues[locale.languageCode]['to'];}
  String get messSendError {return _localizedValues[locale.languageCode]['mess_send_error'];}
  String get messSendSuccess {return _localizedValues[locale.languageCode]['mess_send_success'];}
  String get cancel {return _localizedValues[locale.languageCode]['cancel'];}
  String get send {return _localizedValues[locale.languageCode]['send'];}
  String get delMess {return _localizedValues[locale.languageCode]['del_mess'];}
  String get delete {return _localizedValues[locale.languageCode]['delete'];}
  String get spotsForLight {return _localizedValues[locale.languageCode]['spots_for_light'];}
  String get spotsForPeriod1 {return _localizedValues[locale.languageCode]['spots_for_period1'];}
  String get spotsForPeriod2 {return _localizedValues[locale.languageCode]['spots_for_period2'];}
  String get maxSize {return _localizedValues[locale.languageCode]['max_size'];}
  String get availableFor {return _localizedValues[locale.languageCode]['available_for'];}
  String get totalPrice {return _localizedValues[locale.languageCode]['total_price'];}
  String get loading {return _localizedValues[locale.languageCode]['loading'];}
  String get ok {return _localizedValues[locale.languageCode]['ok'];}
  String get goBack {return _localizedValues[locale.languageCode]['go_back'];}
  String get book {return _localizedValues[locale.languageCode]['book'];}
  String get cancelled {return _localizedValues[locale.languageCode]['cancelled'];}
  String get deleted {return _localizedValues[locale.languageCode]['deleted'];}
  String get cancelError {return _localizedValues[locale.languageCode]['cancel_error'];}
  String get deleteError {return _localizedValues[locale.languageCode]['delete_error'];}
  String get paymentError {return _localizedValues[locale.languageCode]['payment_error'];}
  String get payFor {return _localizedValues[locale.languageCode]['pay_for'];}
  String get selectArrival {return _localizedValues[locale.languageCode]['select_arrival'];}
  String get selectDeparture {return _localizedValues[locale.languageCode]['select_departure'];}
  String get hint {return _localizedValues[locale.languageCode]['hint'];}
  String get arrivalNote {return _localizedValues[locale.languageCode]['arrival_note'];}
  String get departureNote {return _localizedValues[locale.languageCode]['departure_note'];}
  String get bookEmailError {return _localizedValues[locale.languageCode]['book_email_error'];}
  String get bookPhoneError {return _localizedValues[locale.languageCode]['book_phone_error'];}
  String get bookBoatNameError {return _localizedValues[locale.languageCode]['book_boat_name_error'];}
  String get bookDepthError {return _localizedValues[locale.languageCode]['book_depth_error'];}
  String get bookWidthError {return _localizedValues[locale.languageCode]['book_width_error'];}
  String get bookLengthError {return _localizedValues[locale.languageCode]['book_length_error'];}
  String get bookArrivalError {return _localizedValues[locale.languageCode]['book_arrival_error'];}
  String get bookDepartureError {return _localizedValues[locale.languageCode]['book_departure_error'];}
  String get bookHarbourError {return _localizedValues[locale.languageCode]['book_harbour_error'];}
  String get selectHarbour {return _localizedValues[locale.languageCode]['select_harbour'];}
  String get dear {return _localizedValues[locale.languageCode]['dear'];}
  String get thankYou1 {return _localizedValues[locale.languageCode]['thank_you1'];}
  String get details {return _localizedValues[locale.languageCode]['details'];}
  String get spot {return _localizedValues[locale.languageCode]['spot'];}
  String get from {return _localizedValues[locale.languageCode]['from'];}
  String get date {return _localizedValues[locale.languageCode]['date'];}
  String get origMessage {return _localizedValues[locale.languageCode]['orig_message'];}
  String get moreInfo1 {return _localizedValues[locale.languageCode]['more_info1'];}
  String get moreInfo2 {return _localizedValues[locale.languageCode]['more_info2'];}
  String get cancelPolicy {return _localizedValues[locale.languageCode]['cancel_policy'];}
  String get thankYou2 {return _localizedValues[locale.languageCode]['thank_you2'];}
  String get confirmation {return _localizedValues[locale.languageCode]['confirmation'];}
  String get select {return _localizedValues[locale.languageCode]['select'];}
  String get available {return _localizedValues[locale.languageCode]['available'];}
  String get occupied {return _localizedValues[locale.languageCode]['occupied'];}
  String get logout {return _localizedValues[locale.languageCode]['logout'];}
  String get cancelBooking {return _localizedValues[locale.languageCode]['cancel_booking'];}
  String get deleteBooking {return _localizedValues[locale.languageCode]['delete_booking'];}
  String get pricePerDay {return _localizedValues[locale.languageCode]['price_per_day'];}
  String get yes {return _localizedValues[locale.languageCode]['yes'];}
  String get no {return _localizedValues[locale.languageCode]['no'];}
  String get cancelledWord {return _localizedValues[locale.languageCode]['cancelled_word'];}
  String get status {return _localizedValues[locale.languageCode]['status'];}
  String get ended {return _localizedValues[locale.languageCode]['ended'];}
  String get booked {return _localizedValues[locale.languageCode]['booked'];}
  String get noAvailable {return _localizedValues[locale.languageCode]['no_available'];}
  String get cancelPolicyBody {return _localizedValues[locale.languageCode]['cancel_policy_body'];}
  String get moreDaysPlus {return _localizedValues[locale.languageCode]['more_days_plus'];}
  String get day {return _localizedValues[locale.languageCode]['day'];}
  String get days {return _localizedValues[locale.languageCode]['days'];}
  String get moreDaysMinus {return _localizedValues[locale.languageCode]['more_days_minus'];}
  String get noChanges {return _localizedValues[locale.languageCode]['no_changes'];}
  String get perDay {return _localizedValues[locale.languageCode]['per_day'];}
  String get sameDaysCount {return _localizedValues[locale.languageCode]['same_days_count'];}
  String get sorryBooked {return _localizedValues[locale.languageCode]['sorry_booked'];}
  String get confirmBtn {return _localizedValues[locale.languageCode]['confirm_btn'];}
  String get confirm {return _localizedValues[locale.languageCode]['confirm'];}
  String get alreadyBooked {return _localizedValues[locale.languageCode]['already_booked'];}
  String get availableTitle {return _localizedValues[locale.languageCode]['available_ttl'];}
  String get availableHint {return _localizedValues[locale.languageCode]['available_hint'];}
  String get conditionsText1 {return _localizedValues[locale.languageCode]['conditions_text1'];}
  String get conditionsText2 {return _localizedValues[locale.languageCode]['conditions_text2'];}
  String get conditionsBtn {return _localizedValues[locale.languageCode]['conditions_btn'];}
  String get conditionsLink {return _localizedValues[locale.languageCode]['conditions_link'];}
  String get guestMessage {return _localizedValues[locale.languageCode]['guest_message'];}
  String get register {return _localizedValues[locale.languageCode]['register'];}
  String get lightHarbourTtl {return _localizedValues[locale.languageCode]['light_harbour_ttl'];}
  String get lightHarbourMess {return _localizedValues[locale.languageCode]['light_harbour_mess'];}
  String get youSelected {return _localizedValues[locale.languageCode]['you_selected'];}
  String get nationality {return _localizedValues[locale.languageCode]['nationality'];}
  String get nationalityHint {return _localizedValues[locale.languageCode]['nationality_hint'];}
  String get nationalityError {return _localizedValues[locale.languageCode]['nationality_error'];}
  String get pending {return _localizedValues[locale.languageCode]['pending'];}
  String get alreadyRegistered {return _localizedValues[locale.languageCode]['already_registered'];}
  String get onlyToday {return _localizedValues[locale.languageCode]['only_today'];}
  String get isGreenTtl {return _localizedValues[locale.languageCode]['isGreenTtl'];}
  String get isGreenTxt {return _localizedValues[locale.languageCode]['isGreenTxt'];}
  String get selectBoatType {return _localizedValues[locale.languageCode]['select_boat_type'];}
  String get modelError {return _localizedValues[locale.languageCode]['model_error'];}
  String get boatLengthError {return _localizedValues[locale.languageCode]['boat_length_error'];}
  String get boatWidthError {return _localizedValues[locale.languageCode]['boat_width_error'];}
  String get boatDepthError {return _localizedValues[locale.languageCode]['boat_depth_error'];}
  String get phoneError {return _localizedValues[locale.languageCode]['phone_error'];}
  String get photoAdd {return _localizedValues[locale.languageCode]['photo_add'];}
  String get photoChange {return _localizedValues[locale.languageCode]['photo_change'];}
  String get photoProfile {return _localizedValues[locale.languageCode]['photo_profile'];}
  String get photoBoat {return _localizedValues[locale.languageCode]['photo_boat'];}
  String get profilePhoto {return _localizedValues[locale.languageCode]['profile_photo'];}
  String get boatPhoto {return _localizedValues[locale.languageCode]['boat_photo'];}
  String get all {return _localizedValues[locale.languageCode]['all'];}
  //
  String get forgotPassMess1 {return _localizedValues[locale.languageCode]['forgot_pass_mess_1'];}
  String get forgotPassMess2 {return _localizedValues[locale.languageCode]['forgot_pass_mess_2'];}
  String get passCode {return _localizedValues[locale.languageCode]['pass_code'];}
  String get incorrectCode {return _localizedValues[locale.languageCode]['incorrect_code'];}
  String get forgotPassMess3 {return _localizedValues[locale.languageCode]['forgot_pass_mess_3'];}
  String get overnight {return _localizedValues[locale.languageCode]['overnight'];}
  String get midnight {return _localizedValues[locale.languageCode]['midnight'];}
  String get midnightNote {return _localizedValues[locale.languageCode]['midnightNote'];}

  List<String> get dashLabels {
    return _dashboardValues[locale.languageCode]['labels'];
  }
  List<String> get dashTitles {
    return _dashboardValues[locale.languageCode]['titles'];
  }
  List<String> get dashSubTitles {
    return _dashboardValues[locale.languageCode]['subTitles'];
  }

  String get lang { return locale.languageCode; }

}

class LanguageLocalizationsDelegate extends LocalizationsDelegate<Language> {
  const LanguageLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'da'].contains(locale.languageCode);

  @override
  Future<Language> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of Language.
    return SynchronousFuture<Language>(Language(locale));
  }

  @override
  bool shouldReload(LanguageLocalizationsDelegate old) => false;

}