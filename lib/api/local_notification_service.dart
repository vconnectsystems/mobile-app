import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:flutter/material.dart' show BuildContext;

class LocalNotificationService{

  static final FlutterLocalNotificationsPlugin _notificationPlugin = FlutterLocalNotificationsPlugin();
  // BuildContext _context;
  //BuildContext context

  Future<void> initialize() async {
    // _context = context;
    final InitializationSettings initializationSettings = InitializationSettings(
      android: AndroidInitializationSettings("@mipmap/ic_launcher"),
      iOS: IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification,
        requestAlertPermission: true, requestBadgePermission: true, requestSoundPermission: true
      )
    );
    await _notificationPlugin.initialize(initializationSettings, onSelectNotification: (String payload) async {
      if(payload != null){
        print('payload: $payload');
      }
    });
  }

  Future<void> display(RemoteMessage message) async {
    try {
      final int _id  = DateTime.now().millisecondsSinceEpoch ~/ 4;
      final NotificationDetails _details = NotificationDetails(
        android: AndroidNotificationDetails(
          'dockside', 'docksideChannel', 'docksideNotificationChannel',
          importance: Importance.max, priority: Priority.high
        )
      );
      await _notificationPlugin.show(
        _id,
        message.notification.title,
        message.notification.body,
        _details,
        payload: message.data.values.first // TODO
      );
    } on Exception catch (e) {
      print(e);
    }
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {
    //_context
    // display a dialog with the notification details, tap ok to go to another page
    // showDialog(
    //   context: context,
    //   builder: (BuildContext context) => CupertinoAlertDialog(
    //     title: Text(title),
    //     content: Text(body),
    //     actions: [
    //       CupertinoDialogAction(
    //         isDefaultAction: true,
    //         child: Text('Ok'),
    //         onPressed: () async {
    //           Navigator.of(context, rootNavigator: true).pop();
    //           await Navigator.push(
    //             context,
    //             MaterialPageRoute(
    //               builder: (context) => SecondScreen(payload),
    //             ),
    //           );
    //         },
    //       )
    //     ],
    //   ),
    // );
  }

}