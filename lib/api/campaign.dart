
class Campaign{

  int id;
  // String name;
  // String text;
  String image;

  Campaign({
    this.id,
    // this.name,
    // this.text,
    this.image
  });

  factory Campaign.fromJson(Map<String, dynamic> data) {
    return Campaign(
      id: data['id'],
      // name: data['name'],
      // text: data['text'],
      image: data['image'],
    );
  }

}