import 'dart:async';
import 'package:flutter/material.dart' show Offset;
//import '../pages/common/harbour_obj.dart';
import '../api/harbour.dart';
import '../api/country.dart';
//import '../api/api.dart';
//import '../bloc/global_state.dart';

class Bloc {

//  final Api api = new Api();
//  final GlobalState _store = GlobalState.instance;

  final _pageController = StreamController<int>.broadcast();
  Stream<int> get page => _pageController.stream;
  Function(int) get changePage => _pageController.sink.add;

  ////

  final _pageLoadController = StreamController<bool>.broadcast();
  Stream<bool> get pageLoaded => _pageLoadController.stream;
  Function(bool) get changePageLoaded => _pageLoadController.sink.add;

  ////

  final _messSubPageController = StreamController<int>.broadcast();
  Stream<int> get messSubPage => _messSubPageController.stream;
  Function(int) get changeMessSubPage => _messSubPageController.sink.add;

  ////

  final _messHomeController = StreamController<bool>.broadcast();
  Stream<bool> get messHome => _messHomeController.stream;
  Function(bool) get changeMessHome => _messHomeController.sink.add;

  ////

  final _messIdController = StreamController<int>.broadcast();
  Stream<int> get messId => _messIdController.stream;
  Function(int) get changeMessId => _messIdController.sink.add;

  ////

  final _profileSubPageController = StreamController<int>.broadcast();
  Stream<int> get profileSubPage => _profileSubPageController.stream;
  Function(int) get changeProfileSubPage => _profileSubPageController.sink.add;

  ////

  final _searchResultsController = StreamController<List<Harbour>>.broadcast();
  Stream<List<Harbour>> get searchResults => _searchResultsController.stream;
  Function(List<Harbour>) get updateSearchResults => _searchResultsController.sink.add;

  ////

  final _searchHarbourController = StreamController<int>.broadcast();
  Stream<int> get searchHarbour => _searchHarbourController.stream;
  Function(int) get updateSearchHarbour => _searchHarbourController.sink.add;

  ////

  final _bookingHarbourController = StreamController<int>.broadcast();
  Stream<int> get bookingHarbour => _bookingHarbourController.stream;
  Function(int) get updateBookingHarbour => _bookingHarbourController.sink.add;

  ////

  final _selectCheckInController = StreamController<DateTime>.broadcast();
  Stream<DateTime> get selectCheckIn => _selectCheckInController.stream;
  Function(DateTime) get updateCheckIn => _selectCheckInController.sink.add;

  ////

  final _selectCheckOutController = StreamController<DateTime>.broadcast();
  Stream<DateTime> get selectCheckOut => _selectCheckOutController.stream;
  Function(DateTime) get updateCheckOut => _selectCheckOutController.sink.add;

  ////

  final _boatLengthController = StreamController<String>.broadcast();
  Stream<String> get boatLength => _boatLengthController.stream;
  Function(String) get updateBoatLength => _boatLengthController.sink.add;

  ////

  final _boatWidthController = StreamController<String>.broadcast();
  Stream<String> get boatWidth => _boatWidthController.stream;
  Function(String) get updateBoatWidth => _boatWidthController.sink.add;

  ////

  final _boatDepthController = StreamController<String>.broadcast();
  Stream<String> get boatDepth => _boatDepthController.stream;
  Function(String) get updateBoatDepth => _boatDepthController.sink.add;

  ////

  final _scaleController = StreamController<double>.broadcast();
  Stream<double> get scale => _scaleController.stream;
  Function(double) get setScale => _scaleController.sink.add;

  ////

  final _offsetController = StreamController<Offset>.broadcast();
  Stream<Offset> get offset => _offsetController.stream;
  Function(Offset) get setOffset => _offsetController.sink.add;

  ////

  final _loadingController = StreamController<bool>.broadcast();
  Stream<bool> get loading => _loadingController.stream;
  Function(bool) get setLoading => _loadingController.sink.add;

  ////

  final _spotClickedController = StreamController<String>.broadcast();
  Stream<String> get spotClick => _spotClickedController.stream;
  Function(String) get clickSpot => _spotClickedController.sink.add;

  ////

  final _nameChangeController = StreamController<String>.broadcast();
  Stream<String> get nameChange => _nameChangeController.stream;
  Function(String) get changeName => _nameChangeController.sink.add;

  ////

  final _idleSwitchController = StreamController<bool>.broadcast();
  Stream<bool> get idleSwitch => _idleSwitchController.stream;
  Function(bool) get switchIdle => _idleSwitchController.sink.add;

  ////

  final _readMessController = StreamController<bool>.broadcast();
  Stream<bool> get messRead => _readMessController.stream;
  Function(bool) get readMess => _readMessController.sink.add;

  ////

  final _updateMessController = StreamController<bool>.broadcast();
  Stream<bool> get messUpdate => _updateMessController.stream;
  Function(bool) get updateMess => _updateMessController.sink.add;

  ////

  final _updateBookController = StreamController<bool>.broadcast();
  Stream<bool> get bookUpdate => _updateBookController.stream;
  Function(bool) get updateBook => _updateBookController.sink.add;

  ////

  final _updateUnreadController = StreamController<int>.broadcast();
  Stream<int> get unreadUpdate => _updateUnreadController.stream;
  Function(int) get updateUnread => _updateUnreadController.sink.add;

  ////

  final _reloadMessController = StreamController<bool>.broadcast();
  Stream<bool> get messReload => _reloadMessController.stream;
  Function(bool) get reloadMess => _reloadMessController.sink.add;

  ////

  final _reloadBookController = StreamController<bool>.broadcast();
  Stream<bool> get bookReload => _reloadBookController.stream;
  Function(bool) get reloadBook => _reloadBookController.sink.add;

  ////

  final _spotsPopupController = StreamController<int>.broadcast();
  Stream<int> get spotsPopupShow => _spotsPopupController.stream;
  Function(int) get showSpotsPopup => _spotsPopupController.sink.add;

  ////

  final _spotsPopupRungstedController = StreamController<int>.broadcast();
  Stream<int> get spotsPopupRungstedShow => _spotsPopupRungstedController.stream;
  Function(int) get showSpotsPopupRungsted => _spotsPopupRungstedController.sink.add;

  ////

  final _nationalityController = StreamController<int>.broadcast();
  Stream<int> get nationalityUpdate => _nationalityController.stream;
  Function(int) get updateNationality => _nationalityController.sink.add;

  ////

  final _boatTypeController = StreamController<int>.broadcast();
  Stream<int> get boatTypeUpdate => _boatTypeController.stream;
  Function(int) get updateBoatType => _boatTypeController.sink.add;

  ////

  final _countryController = StreamController<Country>.broadcast();
  Stream<Country> get countryUpdate => _countryController.stream;
  Function(Country) get updateCountry => _countryController.sink.add;

  ////

  final _proPhotoController = StreamController<String>.broadcast();
  Stream<String> get proPhotoUpdate => _proPhotoController.stream;
  Function(String) get updateProPhoto => _proPhotoController.sink.add;

  ////

  final _boatPhotoController = StreamController<String>.broadcast();
  Stream<String> get boatPhotoUpdate => _boatPhotoController.stream;
  Function(String) get updateBoatPhoto => _boatPhotoController.sink.add;

  ////

  final _updateReturnController = StreamController<bool>.broadcast();
  Stream<bool> get returnUpdate => _updateReturnController.stream;
  Function(bool) get updateReturn => _updateReturnController.sink.add;

  ////

  final _searchingController = StreamController<bool>.broadcast();
  Stream<bool> get searching => _searchingController.stream;
  Function(bool) get isSearching => _searchingController.sink.add;

  ////

  final _masterSearchController = StreamController<bool>.broadcast();
  Stream<bool> get masterSearchResults => _masterSearchController.stream;
  Function(bool) get updateMasterSearchResults => _masterSearchController.sink.add;

  ////

//  final _loadBookingsController = StreamController<String>.broadcast();
//  Stream<String> get bookingsLoad => _loadBookingsController.stream;
//  Future<String> loadBookings() async {
//    String bookingsModel = await api.getBookings(_store.apiToken);
//    _loadBookingsController.sink.add(bookingsModel);
//    return bookingsModel;
//  }

  //////////////////////////////////////////////////

  void dispose (){
    _pageController.close();
    _pageLoadController.close();
    _messSubPageController.close();
    _messHomeController.close();
    _messIdController.close();
    _profileSubPageController.close();
    _searchResultsController.close();
    _searchHarbourController.close();
    _selectCheckInController.close();
    _selectCheckOutController.close();
    _boatLengthController.close();
    _boatWidthController.close();
    _boatDepthController.close();
    _scaleController.close();
    _offsetController.close();
    _loadingController.close();
    _bookingHarbourController.close();
    _spotClickedController.close();
    _nameChangeController.close();
    _idleSwitchController.close();
    _readMessController.close();
    _updateMessController.close();
    _updateBookController.close();
    _updateUnreadController.close();
    _spotsPopupController.close();
    _spotsPopupRungstedController.close();
    _nationalityController.close();
    _boatTypeController.close();
    _countryController.close();
    _reloadMessController.close();
    _reloadBookController.close();
    _proPhotoController.close();
    _boatPhotoController.close();
    _updateReturnController.close();
    _searchingController.close();
    _masterSearchController.close();
  }

////

}