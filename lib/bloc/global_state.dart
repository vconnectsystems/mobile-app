import '../api/harbour.dart';
import '../api/profile.dart';
import '../api/country.dart';
import '../api/boat_type.dart';
import '../api/spot.dart';
import '../api/drawable_spot.dart';
import '../api/booking.dart';
import '../api/master_booking.dart';
import 'dart:collection';

class GlobalState {
  //
  static GlobalState instance = new GlobalState._();
  GlobalState._();
  //
  int messId = 0;
  bool guest = true;
  String apiToken;
  String devToken = '';
  bool permanent = false;
  bool occupied;
  DateTime availableTill;
  DateTime permanentTill;
  List<Harbour> harbours;
  List<Country> countries = [];
  List<BoatType> boatTypes = [];
  LinkedHashMap<String, dynamic> boatTypesMap;
  int countryIndex;
  int boatTypeIndex;
  Profile profile;
  //
  // BOOKING
  String bookingDsCode;
  int bookingHarbourIndex = -1;
  int bookingHarbourId;
  String bookingHarbourType = "gold";
  int editHarbourIndex = -1;
  String editHarbourName = '';
  DateTime bookingCheckIn;
  DateTime bookingCheckOut;
  DateTime editCheckIn;
  DateTime editCheckOut;
  bool bookingIsForToday;
  String bookingBoatName;
  double bookingBoatLength = 0.0;
  double bookingBoatWidth = 0.0;
  double bookingBoatDepth = 0.0;
  String bookingEmail;
  String bookingPhone;
  Spot bookingSpot;
  String bookingTotalPrice;
  String editSpotCode;
  bool bookingShort = false;
  String svg;
  double svgW;
  double svgH;
  bool svgLoaded = false;
  LinkedHashMap<String, dynamic> serverSpots;
  List<DrawableSpot> dSpots;
  List<Spot> spotsList;
  int bookingId;
  String bookingPaymentLink;
  int oldBookingId;
  List<Booking> bookings;
  bool isMaster = false;
  List<MasterBooking> masterBookings;
  Booking editBooking;
//  List<Message> messages;
//  bool messNoMore = false;
  int unreadMess = 0;
  bool needNewPass = false;
  Map<String, dynamic> message;
  bool notificationListening = false;
  bool showDeviceToken = false;
  int testUserIndex;

}