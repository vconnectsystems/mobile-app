import 'package:flutter/material.dart';
import 'bloc.dart';

class BlocProvider extends InheritedWidget {
  final bloc = Bloc();
  bool updateShouldNotify(_) => true;
  BlocProvider({Key key, Widget child}):super(key: key, child: child);
  static Bloc of(BuildContext context){
//    return (context.inheritFromWidgetOfExactType(Provider) as Provider).bloc;
    return (context.dependOnInheritedWidgetOfExactType<BlocProvider>()).bloc;
  }
}