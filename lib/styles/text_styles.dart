import 'package:flutter/material.dart';

class MyStyles {

  static final Color _commonColor = Color(0xFF1A3B62);
  final Color commonColor = Color(0xFF1A3B62);
  static final Color _transparent = Colors.transparent;
  static final Color _lineColor = Color(0x591A3B62);// 59 = 35%
  final BorderRadius dialogBorderRadius = BorderRadius.circular(16.0);

  static final BorderRadius _buttonRadius = BorderRadius.circular(8.0);
  final BorderRadius buttonRadius = BorderRadius.circular(8.0);

  final Color dialogBackgroundColor = Colors.white.withOpacity(.9);

  final Color permanentRed = Color(0xFFFF0000).withOpacity(.45);
  final Color permanentGreen = Color(0xFF00FF07).withOpacity(.35);

  final int toastGravity = 2;
  final int toastDuration = 3;

  final TextStyle pageTitleLight = new TextStyle(
    fontFamily: "Neutra2Light",
    fontWeight: FontWeight.w200,
    color: _commonColor,
    fontSize: 26.0,
  );

  final TextStyle pageTitleBold = new TextStyle(
    fontFamily: "Neutra2Demi",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 24.0,
  );

  final TextStyle dashBoxStyle = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 14.0,
  );

  final TextStyle textInputStyle = new TextStyle(
    fontFamily: "Neutra2Book",
    fontWeight: FontWeight.w400,
    color: _commonColor,//0xFFE0E0E2
    fontSize: 22.0,
  );

  final TextStyle textInputLabelStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w400,
    color: Colors.black87,
    fontSize: 22.0,
  );

  final TextStyle textInputHintStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w400,
    color: Colors.black26,
    fontSize: 22.0,
  );

  final TextStyle formInputStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,//0xFFE0E0E2
    fontSize: 19.0,
  );

  final TextStyle masterListStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,//0xFFE0E0E2
    fontSize: 18.0,
  );

  final TextStyle formInputLabelStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: Colors.black87,
    fontSize: 19.0,
  );

  final TextStyle formInputHintStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: Colors.black26,
    fontSize: 19.0,
  );

  final TextStyle listItemStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 22.0,
  );

  final TextStyle pageText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 17.0,
  );

  final TextStyle pageTextMedium = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 19.0,
  );

  final TextStyle pageTextMediumHeavy = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 19.0,
  );

  final TextStyle pageTextBig = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 22.0,
  );

  final TextStyle pageLineText = new TextStyle(
    fontFamily: "AvenirLight",
    fontWeight: FontWeight.w200,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle pageInTitleText = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 18.0
  );

  final TextStyle pageInSubTitleText = new TextStyle(
    fontFamily: "AvenirLight",
    fontWeight: FontWeight.w200,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle boxInputText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 18.0,
  );

  final TextStyle messTitleText = new TextStyle(
      fontFamily: "AvenirHeavy",
      fontWeight: FontWeight.w600,
      color: _commonColor,
      fontSize: 18.0
  );

  final TextStyle messSubTitleText = new TextStyle(
    fontFamily: "AvenirLight",
    fontWeight: FontWeight.w200,
    color: _commonColor,
    fontSize: 18.0,
  );

  final TextStyle modalTitleText = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 22.0,
  );

  final TextStyle modalBodyText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 16.0,
  );

  final TextStyle modalBiggerText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 21.0,
  );

  final TextStyle tabBarSelected = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w500,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle tabBarDeSelected = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle switchSelected = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w500,
    color: Colors.white.withOpacity(.9),
    fontSize: 16.5,
  );

  final TextStyle switchDeSelected = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor.withOpacity(.7),
    fontSize: 16.5,
  );

  final TextStyle dialogButtonText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle dialogNumButtonText = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w500,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle loginMessageText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 16.0,
  );

  final TextStyle loginLittleText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: Colors.white70,
    fontSize: 14.0,
  );

  final TextStyle loginLittleLink = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: Colors.white,
    fontSize: 14.0,
  );

  final TextStyle loginMediumText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: Colors.white,
    fontSize: 16.0,
  );

  final TextStyle loginInputHintText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 22.0,
  );

  final TextStyle appleDateText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle popupHintText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 14.0,
  );

  final TextStyle popupHintBoldText = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 14.0,
  );

  final TextStyle photoHintText = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 14.0,
  );

  final TextStyle textPageTitle = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w500,
    color: _commonColor,
    fontSize: 20.0,
  );

  final TextStyle textPageSubTitle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 18.0,
  );

  final TextStyle textPageBody = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 16.0,
  );

  final TextStyle textPageBodyBold = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w500,
    color: _commonColor,
    fontSize: 16.0,
  );

  final TextStyle noContentStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w400,
    color: Colors.black26,
    fontSize: 18.0,
  );

  final TextStyle badgeStyle = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w400,
    color: Colors.white,
    fontSize: 14.0,
  );

  final TextStyle messageReadTitle = new TextStyle(
      fontFamily: "AvenirHeavy",
      fontWeight: FontWeight.w600,
      color: _commonColor,
      fontSize: 20.0
  );

  final TextStyle messageReadBold = new TextStyle(
    fontFamily: "AvenirHeavy",
    fontWeight: FontWeight.w600,
    color: _commonColor,
    fontSize: 18.0
  );

  final TextStyle messageReadCapital = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 18.0
  );

  final TextStyle messageReadBody = new TextStyle(
    fontFamily: "AvenirBook",
    fontWeight: FontWeight.w300,
    color: _commonColor,
    fontSize: 16.0
  );

  /////////////////////////////////////////////////////////////////////////////

  final BoxDecoration dialogButtonDecoration = new BoxDecoration(
    color: _transparent,
    borderRadius: _buttonRadius,
    border: Border.all(color: _commonColor, width: 1.0)
  );

  final BoxDecoration dialogNumButtonDecoration = new BoxDecoration(
    color: _transparent,
  );

  final BoxDecoration placeholderDecoration = new BoxDecoration(
    color: _lineColor,
  );

  final BoxShadow dialogShadow = new CustomBoxShadow(
    blurStyle: BlurStyle.outer,
    color: Colors.black26,
    offset: new Offset(0.0, 0.0),
    blurRadius: 9.0,
  );

  static final BoxShadow loginBoxShadow = new CustomBoxShadow(
    blurStyle: BlurStyle.outer,
    color: Colors.black38,
    offset: new Offset(0.0, 0.0),
    blurRadius: 15.0,
  );


  final BoxDecoration loginBoxDecoration = new BoxDecoration(
    color: Colors.white70,
    borderRadius: BorderRadius.circular(10),
    boxShadow: [loginBoxShadow],
  );

  final BoxDecoration lineDecoration = BoxDecoration(
    color: _lineColor,
  );

  final BoxDecoration bgDecoration = new BoxDecoration(
    image: new DecorationImage(
      image: new AssetImage("assets/images/unnamed.jpg"),
      fit: BoxFit.cover
    ),
  );

  final BoxDecoration pageDecoration = BoxDecoration(
    color: Colors.white70,
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(24),
      topRight: Radius.circular(24),
    ),
    boxShadow: [
      new BoxShadow(
        color: Colors.black26,
        offset: new Offset(0.0, 0.0),
        blurRadius: 12.0,
      )
    ],
  );

  final BoxDecoration messageBoxDecoration = new BoxDecoration(
    color: _commonColor.withOpacity(0.1),//Colors.white38,
    borderRadius: BorderRadius.circular(10),
  );

  MyStyles();

}


class CustomBoxShadow extends BoxShadow {

  final BlurStyle blurStyle;

  const CustomBoxShadow({
    Color color = const Color(0xFF000000),
    Offset offset = Offset.zero,
    double blurRadius = 0.0,
    this.blurStyle = BlurStyle.normal,
  }) : super(color: color, offset: offset, blurRadius: blurRadius);

  @override
  Paint toPaint() {
    final Paint result = Paint()
      ..color = color
      ..maskFilter = MaskFilter.blur(this.blurStyle, blurSigma);
    assert(() {
      if (debugDisableShadows)
        result.maskFilter = null;
      return true;
    }());
    return result;
  }
}