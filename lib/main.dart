import 'package:dockside/pages/booking/booking_complete.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
// import 'package:flutter/foundation.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'bloc/bloc_provider.dart';
import 'pages/login/login_page.dart';
import 'pages/dashboard/dashboard_page.dart';
import 'pages/master/master_page.dart';
import 'api/language.dart';
import 'api/message_listener.dart';
// import 'api/local_notification_service.dart';

Map<int, Color> color = {
  50:Color.fromRGBO(136,14,79, .1),
  100:Color.fromRGBO(136,14,79, .2),
  200:Color.fromRGBO(136,14,79, .3),
  300:Color.fromRGBO(136,14,79, .4),
  400:Color.fromRGBO(136,14,79, .5),
  500:Color.fromRGBO(136,14,79, .6),
  600:Color.fromRGBO(136,14,79, .7),
  700:Color.fromRGBO(136,14,79, .8),
  800:Color.fromRGBO(136,14,79, .9),
  900:Color.fromRGBO(136,14,79, 1),
};

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  RemoteNotification notification = message.notification;
  if (notification != null) { // && android != null && !kIsWeb
    print('_firebaseMessagingBackgroundHandler.message.notification.title: ${message.notification.title}'); //2419200
    print('_firebaseMessagingBackgroundHandler.message.notification.body: ${message.notification.body}'); //2419200
    if (message.data != null) {
      print('onMessage, message.data: ${message.data}');
    }
  }
  print('Handling a background message ${message.messageId}');//0:1625581025075738%6e2566536e256653
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  //
  runApp(BlocProvider(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<MessageListener>(
              create: (_) => MessageListener()
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Dockside',
          localizationsDelegates: [
            const LanguageLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en'), // English
            const Locale('da'), // Danish
          ],
          theme: ThemeData(
            // fontFamily: 'Neutra2Book',
              scaffoldBackgroundColor: Colors.white,//Color(0xFFB6BFC6),
              primarySwatch: MaterialColor(0xFF1A3B62, color),//Color(0xFF1A3B62);//Colors.deepPurple,
              accentColor: MaterialColor(0xFF1A3B62, color),
              buttonColor: Color(0xFF1A3B62),
              disabledColor: Color(0x661A3B62),//Color(0xFF8191FC),B985FC
              backgroundColor: Colors.white,//Color(0xFF50525F)//161620
              textTheme: TextTheme(
                headline5: TextStyle(fontSize: 26.0,
                  fontFamily: "Neutra2Light",
                  fontWeight: FontWeight.w200,
                  color: Color(0xFF1A3B62)
                ),
                headline6: TextStyle(
                  fontSize: 24.0,
                  fontFamily: "Neutra2Demi",
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF1A3B62)
                ),
                subtitle2: TextStyle(
                  fontSize: 22.0,
                  fontFamily: "Neutra2Book",
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF1A3B62)
                ),
                bodyText2: TextStyle(
                  fontSize: 22.0,
                  fontFamily: "Neutra2Book",
                  color: Color(0xFF1A3B62),//0xFFE0E0E2
                  fontWeight: FontWeight.w400
                ),
                bodyText1: TextStyle(
                  fontSize: 22.0,
                  fontFamily: "Neutra2Book",
                  color: Color(0xFF1A3B62),
                  fontWeight: FontWeight.w400
                ),
              )
          ),
          routes: <String, WidgetBuilder>{
            '/LoginPage': (BuildContext context) => LoginPage(),
            '/Dashboard': (BuildContext context) => DashboardPage(),
            '/Master': (BuildContext context) => MasterPage(),
            '/Completed': (BuildContext context) => BookingCompletePage(),
          },
          home: LoginPage(),
        ),
      )
  ));
}
