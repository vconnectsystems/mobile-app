import 'package:dockside/pages/common/toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../api/profile.dart';
import '../common/dialog_button.dart';

class BoatPhotoDialog extends StatelessWidget {

  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final Api _api = new Api();
//  final double _numBtnHeight = 41.0;
  final double _numBtnSpace = 18.0;
  final ImagePicker _picker = ImagePicker();

  BoatPhotoDialog({Key key}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final double _dialogWidth = MediaQuery.of(context).size.width-52;
    final double _imgW = _dialogWidth - (2 * _numBtnSpace);
    final double _imgH = _imgW - (_imgW/3);
    final double _dialogHeight = _imgH+98;
    PickedFile _file;
    dynamic _pickImageError;

    Widget imagePlaceholder() {
      if(_file != null){
        return Image.file(File(_file.path), fit: BoxFit.cover);
      } else if(_pickImageError != null){
        return Container(
          decoration: _styles.placeholderDecoration,
          alignment: Alignment.center,
          child: Text('Error: $_pickImageError', style: _styles.popupHintText),
        );
      } else {
        if(_store.profile.bPhoto != ''){
          print('_store.profile.bPhoto: ${_store.profile.bPhoto}');
          return Image.network('http://${_store.profile.bPhoto}', fit: BoxFit.cover);
        } else {
          return Container(
            decoration: _styles.placeholderDecoration,
            alignment: Alignment.center,
            child: Text(_lang.photoBoat, style: _styles.popupHintText),
          );
        }
//        return Container(
//          decoration: _styles.placeholderDecoration,
//          alignment: Alignment.center,
//          child: Text(_lang.photoBoat, style: _styles.popupHintText),
//        );
      }
    }

    void getImage() async {
      try {
        _file = await _picker.getImage(source: ImageSource.gallery);
        _bloc.updateBoatPhoto(_file.path);
      } catch (e) {
        _pickImageError = e;
      }
    }

    void _showToast(String error, BuildContext context){
      Toast.show(
        error.toString(), context,
        backgroundColor: Colors.red,
        duration: _styles.toastDuration,
        gravity: _styles.toastGravity
      );
    }
    void _dataError(dynamic error, BuildContext context){
      _bloc.setLoading(false);
      if(error.runtimeType == SocketException){
        if(error.message.contains('Failed host lookup')){
          _showToast('Please, check your internet connectivity', context);
        } else {
          _showToast(error.message, context);
        }
      } else {
        _showToast(error.toString(), context);
      }
    }

    void _profileSuccess(Profile profile, BuildContext context){
      _store.profile = profile;
      _bloc.setLoading(false);
      _bloc.updateBoatPhoto('');
      Navigator.of(context).pop();
    }
    void _updateSuccess(String value, BuildContext context){
      print('SUCCESS! : $value');
      Future result = _api.getProfile(_store.apiToken);
      result.then((value) => _profileSuccess(value, context)).catchError((error) => _dataError(error, context));
    }

    void uploadImage(){
      _bloc.setLoading(true);
      _file.path;
      File _fl = File(_file.path);
      String base64Image = base64Encode(_fl.readAsBytesSync());
      print('base64Image: $base64Image');
      Future result = _api.uploadBoatPhoto(base64Image, _store.apiToken);
      result.then((value) => _updateSuccess(value, context)).catchError((error) => _dataError(error, context));
    }

    return Center(
      child: Container(
        width: _dialogWidth,
        height: _dialogHeight,
        decoration: BoxDecoration(
          color: _styles.dialogBackgroundColor,
          borderRadius: _styles.dialogBorderRadius,
          boxShadow: [_styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Container(
              width: _imgW,
              height: _imgH,
              child: Material(
                child: InkWell(
                  child: StreamBuilder<Object>(
                    stream: _bloc.boatPhotoUpdate,
                    builder: (context, snapshot) {
                      return imagePlaceholder();
                    }
                  ),
                  onTap: getImage,
                )
              ),
            ),
            Spacer(),
            Row(
              children: <Widget>[
                SizedBox(width: _numBtnSpace),
                DialogButton(
                  ttl: _lang.cancel,
                  onPressed: (){
                    _bloc.updateBoatPhoto('');
                    Navigator.of(context).pop();
                  },
                ),
                Spacer(),
//                Expanded(child: SizedBox(width: 1)),
                StreamBuilder<String>(
                    stream: _bloc.boatPhotoUpdate,
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data != ''){
                        return StreamBuilder(
                          stream: _bloc.loading,
                          builder: (context, shot){
                            if(shot.hasData && shot.data){
                              return Container(
                                width: 48, height: 48,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(5),
                                child: CircularProgressIndicator(
                                  strokeWidth: 3.5,
                                  valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
                                ),
                              );
                            } else {
                              return DialogButton(
                                ttl: _lang.confirmBtn,
                                onPressed: (){
                                  uploadImage();
                                },
                              );
                            }
                          },
                        );
                      } else {
                        return DialogButton(
//                          ttl: _file != null ? _lang.photoChange : _lang.photoAdd,
                          ttl: _lang.photoChange,
                          onPressed: (){
                            getImage();
//                            Navigator.of(context).pop();
                          },
                        );
                      }
                    }
                ),
                SizedBox(width: _numBtnSpace),
              ],
            ),
            SizedBox(height: _numBtnSpace),
          ],
        ),
      ),
    );
  }
}
