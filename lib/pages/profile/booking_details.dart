import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import '../../api/api.dart';
import '../../api/booking.dart';
import '../../api/language.dart';
// import '../../bloc/bloc.dart';
// import '../../bloc/provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
// import '../common/toast.dart';
// import '../common/progress_dialog.dart';
import '../common/icon_font_icons.dart';
import '../common/circle_button.dart';
import 'booking_edit_new.dart';
// import 'booking_delete_dialog.dart';
import 'booking_edit_payment_new.dart';

class BookingDetails extends StatelessWidget {

  final Booking obj;
  final MyStyles styles = new MyStyles();
  final double space = 10.0;
  final double bigSpace = 20.0;
  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;

  BookingDetails({Key key, @required this.obj}) : super(key: key);

  @override
  Widget build(BuildContext context){

    // final Bloc _bloc = Provider.of(context);
    final Language lang = Language.of(context);
//    final DateFormat dateFormat = DateFormat('dd.MM', lang.lang);// - hh:mm
    final DateFormat dateFormatYear = DateFormat('dd.MM.yyyy', lang.lang);// - hh:mm
//    final DateFormat dateTimeFormat = DateFormat('dd.MM.yyyy - hh:mm', lang.lang);//
    final DateFormat dateTimeFormatYear = DateFormat('dd.MM.yyyy - HH:mm', lang.lang);//
//    final DateFormat timeFormat = DateFormat('hh:mm');

    String statusText;
    Color statusColor;
    Row bottomRow;

    // void showProgress(){
    //   showGeneralDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    //     barrierColor: Colors.white70,
    //     transitionDuration: const Duration(milliseconds: 200),
    //     pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation) {
    //       return ProgressDialog();
    //     },
    //   );
    // }

    // void _dataError(Error error, BuildContext context){
    //   _bloc.setLoading(false);
    //   print('error: ${error.toString()}');
    //   Toast.show(error.toString(), context, backgroundColor: Colors.red);
    // }

    // Delete Booking
    // void _deleteSuccess(value, BuildContext context){
    //   _bloc.setLoading(false);
    //   if(value.toString().contains('success')){
    //     Toast.show(lang.deleted, context, backgroundColor: Colors.green);
    //     Navigator.pop(context); // Popup
    //     Navigator.pop(context, 1); // Page
    //   }else{
    //     Toast.show(lang.deleteError, context, backgroundColor: Colors.red);
    //   }
    // }
    // void deleteBooking(){
    //   Navigator.pop(context);
    //   showProgress();
    //   Future result = api.deleteBooking(obj.id, _store.apiToken);
    //   result.then((value) => _deleteSuccess(value, context)).catchError((error) => _dataError(error, context));
    // }

    // Cancel Booking
    // void _cancelSuccess(value, BuildContext context){
    //   _bloc.setLoading(false);
    //   if(value.toString().contains('success')){
    //     Toast.show(lang.cancelled, context, backgroundColor: Colors.green);
    //     Navigator.pop(context); // Popup
    //     Navigator.pop(context, 1); // Page
    //   }else{
    //     Toast.show(lang.cancelError, context, backgroundColor: Colors.red);
    //   }
    // }
    // void cancelBooking(){
    //   Navigator.pop(context);
    //   showProgress();
    //   Future result = api.cancelBooking(obj.id, _store.apiToken);
    //   result.then((value) => _cancelSuccess(value, context)).catchError((error) => _dataError(error, context));
    // }

    // void showDeleteDialog() {
    //   showGeneralDialog(
    //     context: context,
    //     barrierDismissible: true,
    //     barrierLabel: MaterialLocalizations
    //         .of(context)
    //         .modalBarrierDismissLabel,
    //     barrierColor: null,
    //     transitionDuration: Duration.zero,
    //     pageBuilder: (BuildContext cont, Animation animation,
    //         Animation secondaryAnimation) {
    //       return DeleteDialog(
    //         deleteMessage: obj.statusId == 2 ? lang.deleteBooking : lang.cancelBooking,
    //         onPressed: () {
    //           obj.statusId == 2 ? deleteBooking() : cancelBooking();
    //           //Delete the message...
    //           //Navigator.of(context).pop();
    //         },
    //       );
    //     },
    //   );
    // }

    void editBooking (){
      _store.editCheckIn = obj.startDate;
      _store.editCheckOut = obj.endDate;
      Navigator.push(context,
        CupertinoPageRoute(builder: (context) => BookingEdit(booking: obj)),
      );
    }
    void goBack(){
      Navigator.pop(context, 1);
    }

    void payPending(){
      Navigator.push(context,
        CupertinoPageRoute(builder: (context) => BookingEditPaymentPage(paymentLink: obj.link)),
      );
    }

//    CircleButton(icon: IconFont.goback2, onPressed: goBack),
//    Spacer(),
//    (obj.statusId == 2 || obj.endDate.isBefore(DateTime.now())) ? SizedBox(width: 0) :
//    CircleButton(icon: IconFont.edit2, onPressed: editBooking),
//    (obj.statusId == 2 || obj.endDate.isBefore(DateTime.now())) ? SizedBox(width: 0) : Spacer(),
//    //(obj.statusId == 2 || obj.endDate.isBefore(DateTime.now())) ? SizedBox(width: 0) :
//    CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),

    switch (obj.statusId){
      case 1:
      // Booked
        if(obj.endDate.isBefore(DateTime.now()) || _store.harbours[obj.harbourInd].subscriptionType == "light"){
          // Ended
          statusText = lang.ended.toUpperCase();
          statusColor = Color(0xFF1A3B62);
          bottomRow = new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleButton(icon: IconFont.goback2, onPressed: goBack),
              Spacer(),
//              CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
            ],
          );
        } else {
          // Future
          statusText = lang.booked.toUpperCase();
          statusColor = Color(0xFF009640);
          bottomRow = new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleButton(icon: IconFont.goback2, onPressed: goBack),
              Spacer(),
              CircleButton(icon: IconFont.edit2, onPressed: editBooking),
//              Spacer(),
//              CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
            ],
          );
        }
        break;
      case 2:
      // Cancelled
        statusText = lang.cancelledWord.toUpperCase();
        statusColor = Color(0xFFBE1622);
        bottomRow = new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleButton(icon: IconFont.goback2, onPressed: goBack),
            Spacer(),
//            CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
          ],
        );
        break;
      case 3:
      // Pending
        statusText = lang.pending.toUpperCase();
        statusColor = Color(0xFFD78026);// 0xFFEC8D2B
        if(obj.link.startsWith("https")){
          bottomRow = new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleButton(icon: IconFont.goback2, onPressed: goBack),
              Spacer(),
//              CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
//              Spacer(),
              CircleButton(icon: IconFont.arrow_icon, onPressed: payPending),
            ],
          );
        } else {
          if(_store.harbours[obj.harbourInd].subscriptionType != "light"){
            bottomRow = new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleButton(icon: IconFont.goback2, onPressed: goBack),
                Spacer(),
                CircleButton(icon: IconFont.edit2, onPressed: editBooking),
//                Spacer(),
//                CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
              ],
            );
          } else {
            bottomRow = new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleButton(icon: IconFont.goback2, onPressed: goBack),
                Spacer(),
//                CircleButton(icon: IconFont.trash2, onPressed: showDeleteDialog),
              ],
            );
          }

        }
        break;
      default:
        bottomRow = new Row(
          children: <Widget>[
            SizedBox(width: 0)
          ]
        );
        break;
    }

    // int getDays(DateTime from, DateTime to){
    //   int days = to.difference(from).inDays;
    //   return days;
    // }

    StringBuffer bodySb = new StringBuffer();
    bodySb.write('${lang.details}\n');
    bodySb.write('${lang.spot}#${obj.boatSpot}\n');
    bodySb.write('${lang.from}${
//        DateTime.now().year == obj.startDate.year ?
//        dateFormat.format(obj.startDate) :
        dateFormatYear.format(obj.startDate)
    }\n');
    bodySb.write('${lang.to}${
//        DateTime.now().year == obj.endDate.year ?
//        dateFormat.format(obj.endDate) :
        dateFormatYear.format(obj.endDate)
    }\n');
    bodySb.write('${lang.pricePerDay}${(obj.price)}\n');
    bodySb.write('${lang.totalPrice}${obj.totalPrice}\n\n');
//    bodySb.write('${lang.totalPrice}${(obj.totalPrice * getDays(obj.startDate, obj.endDate))}\n\n');
//    bodySb.write((obj.endDate.isBefore(DateTime.now()) || obj.statusId > 1) ? '' : lang.cancelPolicyBody);
    String bodyStr = bodySb.toString();

//    TextSpan cancelledSpan = TextSpan(text: '${lang.cancelledWord.toUpperCase()}\n\n', style: styles.textPageBodyBold);
    TextSpan dateSpan = TextSpan(text:
//    DateTime.now().year == obj.startDate.year ?
//    '${dateTimeFormat.format(obj.updatedAt)}\n\n'
//        : '${dateTimeFormatYear.format(obj.updatedAt)}\n\n',
      '${dateTimeFormatYear.format(obj.updatedAt)}\n\n',
      style: styles.textPageSubTitle);
    TextSpan statusSpan1 = TextSpan(text: '${lang.status.toUpperCase()}', style: styles.textPageBody);
    TextSpan statusSpan = TextSpan(
        text:'$statusText\n\n',
        style: TextStyle(
          color: statusColor,
          fontFamily: "AvenirHeavy",
          fontWeight: FontWeight.w600,
          fontSize: 16.0,
        ));
    TextSpan bodySpan = TextSpan(text: bodyStr, style: styles.textPageBody);

    return WillPopScope(
      onWillPop: (){
        goBack();
        return;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Header(
                  xVis: true,
                  mainTitle: lang.booking.toUpperCase(),
                  subTitle: obj.harbourName.toUpperCase(),
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(height: 16),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 26, right: 26, top: 26, bottom: 26),
                  decoration: styles.pageDecoration,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: SelectableText.rich(
                          TextSpan(
                            style: styles.popupHintText,
                            children: <TextSpan>[
                              dateSpan,
                              statusSpan1,
                              statusSpan,
                              bodySpan
                            ]
                          ),
                          textAlign: TextAlign.left,
                          maxLines: null,
                        )
                      ),
                      SizedBox(height: 20),
                      bottomRow,
                    ]
                  ),
                )
              ),
            ]
          ),
        )
      ),
    );
  }
}

