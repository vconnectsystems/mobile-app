import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
// import '../../api/language.dart';
// import '../../bloc/bloc.dart';
// import '../../bloc/provider.dart';
import '../../styles/text_styles.dart';

class SelectPhotoPage extends StatelessWidget {

  final MyStyles styles = new MyStyles();
  // final GlobalState _store = GlobalState.instance;
  final double space = 10.0;
  final double bigSpace = 20.0;
  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();
  // final ImagePicker _picker = ImagePicker();

  SelectPhotoPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    // final Language _lang = Language.of(context);
    // final Bloc _bloc = Provider.of(context);
    double _imgW = MediaQuery.of(context).size.width - (2 * 26);
    PickedFile _file;
    // dynamic _pickImageError;

    Widget imagePlaceholder() {
      if(_file != null){
        return Image.file(File(_file.path), fit: BoxFit.cover);
      } else {
        return Container(
          //decoration: ,
        );
      }
    }

    // void getImage() async {
    //   _file = await _picker.getImage(source: ImageSource.gallery);
    // }

    // void goBack(){
    //   Navigator.pop(context, 'photo...');
    // }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        width: _imgW,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: _imgW,
              height: _imgW,
              child: imagePlaceholder(),
            ),
            Row(
              children: <Widget>[
                //
              ],
            )
          ],
        ),
      ),
    );
  }

}