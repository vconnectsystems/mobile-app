import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
// import 'dart:io';
//import 'dart:convert';
//import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../api/profile.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import '../common/nationality_dialog.dart';
import '../common/boat_type_dialog.dart';
import '../booking/apple_number_dialog.dart';
import 'boat_photo_dialog.dart';

class ProfilePageInfo extends StatefulWidget {

  final GestureTapCallback backToDashboard;

  ProfilePageInfo({Key key, this.backToDashboard}) : super (key: key);

  @override
  _ProfilePageInfo createState() => _ProfilePageInfo();

}

class _ProfilePageInfo extends State<ProfilePageInfo> {

  static const double _spaceBetween = 1.0;
  static const double _spaceBottom = 12.0;
  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final Api _api = new Api();
//  final ImagePicker _picker = ImagePicker();
  SharedPreferences _prefs;
  //static const double _proPhotoIconW = 36;
  static const double _boatPhotoIconW = 50;
  static const double _boatPhotoIconH = 36;

  void _getSharedPreferences() async {
    _prefs = await SharedPreferences.getInstance();
  }

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _mailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _homeController = TextEditingController();
  final TextEditingController _bNameController = TextEditingController();
  final TextEditingController _bModelController = TextEditingController();
  FocusNode _nameFocusNode;
  FocusNode _mailFocusNode;
  FocusNode _phoneFocusNode;
  FocusNode _homeFocusNode;
  FocusNode _bNameFocusNode;
  FocusNode _bModelFocusNode;

  Language _lang;
  Bloc _bloc;
  Future<bool> _initFuture;
  Profile _newProfile;

  int _countryInd;
  int _boatTypeInd;
  List<String> _boatTypeNames;
  double _bLength;
  double _bWidth;
  double _bDepth;
  //String _boatPhotoFilePath = '';
  String _bPhoto;
  //
//  PickedFile _file;
//  dynamic _pickImageError;

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: _styles.toastDuration,
      gravity: _styles.toastGravity
    );
  }

  Widget imagePlaceholderBoat() {
    if(_store.profile.bPhoto != ''){
      print('_store.profile.bPhoto: ${_store.profile.bPhoto}');
      return Image.network('http://${_store.profile.bPhoto}', fit: BoxFit.cover);
    } else {
      return Container(decoration: _styles.placeholderDecoration);
    }
  }

  String _camelName (String nm){
    List<String> arr = nm.split(' ');
    List<String> camelArr = [];
    for(int i = 0; i<arr.length; i++){
      String str = arr[i];
      camelArr.add('${str[0].toUpperCase()}${str.substring(1)}');
    }
    return camelArr.join(' ');
  }

  void _updateBoatTypeNames(){
    _boatTypeNames = [];
    for(int i=0; i<_store.boatTypes.length; i++){
      List<String> li = (_store.boatTypes[i].name).split('|');
      String nm = _lang.lang == 'da'? li[0] : li[1];
      _boatTypeNames.add(nm);
    }
  }

//  void getImage() async {
//    try {
//      _file = await _picker.getImage(source: ImageSource.gallery);
//      _bloc.updateBoatPhoto(_file.path);
//    } catch (e) {
//      _pickImageError = e;
//      print(_pickImageError.toString());
//    }
//  }

  @override
  void initState() {
    super.initState();
    _getSharedPreferences();
    _initFuture = Future<bool>.delayed(Duration.zero,() {
      _lang = Language.of(context);
      _bloc = BlocProvider.of(context);
      _updateBoatTypeNames();
      _nameController.text = _camelName(_store.profile.name);//_store.profile.name;
      _mailController.text = _store.profile.mail;
      _phoneController.text = _store.profile.phone;
      _homeController.text = _store.profile.home;
      _bNameController.text = _store.profile.bName;
      _bModelController.text = _store.profile.bModel;
      _nameFocusNode = FocusNode();
      _mailFocusNode = FocusNode();
      _phoneFocusNode = FocusNode();
      _homeFocusNode = FocusNode();
      _bNameFocusNode = FocusNode();
      _bModelFocusNode = FocusNode();
      _countryInd = _store.countryIndex;
      _boatTypeInd = _store.boatTypeIndex;
      _bLength = _store.profile.bLength;
      _bWidth = _store.profile.bWidth;
      _bDepth = _store.profile.bDepth;
      _bPhoto = _store.profile.bPhoto;
      print('_bPhoto: $_bPhoto');
      return true;
    });
  }

  void killFocus(){
    _nameFocusNode.unfocus();
    _mailFocusNode.unfocus();
    _phoneFocusNode.unfocus();
    _homeFocusNode.unfocus();
    _bNameFocusNode.unfocus();
    _bModelFocusNode.unfocus();
  }

  @override
  void dispose (){
    killFocus();
    _nameController.dispose();
    _mailController.dispose();
    _phoneController.dispose();
    _homeController.dispose();
    _bNameController.dispose();
    _bModelController.dispose();
    super.dispose();
  }

  TextFormField _myTextField(TextEditingController ctrl, FocusNode focus, String hint, {TextInputType keyboardType: TextInputType.text}) {
    return TextFormField(
      controller: ctrl,
      focusNode: focus,
      autofocus: false,
      // autovalidate: false,
      keyboardType: ctrl == _mailController ? TextInputType.emailAddress : keyboardType,
      textInputAction: TextInputAction.done,
      textCapitalization: TextCapitalization.sentences,
      autocorrect: false,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: hint,
        hintStyle: _styles.formInputHintStyle,
        labelStyle: _styles.formInputLabelStyle,
      ),
      onChanged: (text){
        if(ctrl == _nameController){
          _bloc.changeName(text);
        }
      },
      style: _styles.formInputStyle,
    );
  }

  void _selectNationality(){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return NationalityDialog(
          initInd: _countryInd,
          ttl: _lang.nationality,
          list: _store.countries,
          hint: '',
        );
      },
    );
  }

  void _selectBoatType(){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return BoatTypeDialog(
          initInd: _boatTypeInd,
          ttl: _lang.selectBoatType,
          names: _boatTypeNames,
          hint: '',
        );
      },
    );
  }

  void _showNumPad(String type, String ttl, String initStr){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleNumberDialog(
          initSize: initStr,
          ttl: ttl,
          type: type,
          hint: '',
        );
      },
    );
  }

//  void boatPhotoChanged(){
//    //
//  }

  void _selectBoatPhoto() async {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return BoatPhotoDialog();
      },
    );
  }

  void _dataError(dynamic error, BuildContext context){
    _bloc.setLoading(false);
    if(error.runtimeType == SocketException){
      SocketException _err = error;
      if(_err.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(_err.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }

  void _updateSuccess(String value, BuildContext context){
    _bloc.setLoading(false);
    if(value.contains('error') || value.contains('<html>')){
      _showToast(_lang.saveProfileError, context);
    }else{
      _showToast(_lang.saveProfileSuccess, context, green: true);
      _store.profile = _newProfile;
      _store.countryIndex = _countryInd;
      _store.boatTypeIndex = _boatTypeInd;
      _store.bookingEmail = _mailController.text;
      _store.bookingBoatLength = _bLength;
      _store.bookingBoatWidth = _bWidth;
      _store.bookingBoatDepth = _bDepth;
      widget.backToDashboard();
    }
  }

  bool _validateName(String value){
    bool valid = false;
    if (value.length > 2 && value.isNotEmpty) {
      valid = true;
    }
    return valid;
  }
  bool _validateMail(String value){
    bool valid = false;
    if (value.length > 6 && value.isNotEmpty && value.contains('@') && value.contains('.') && !value.contains(' ')) {
      valid = true;
    }
    return valid;
  }
  bool _validatePhone(String value){
    bool valid = true;
    if (value.isNotEmpty) {
      if(value.length < 10){
        valid = false;
      }
    }
    return valid;
  }
  bool _validateBoatModel(String value){
    bool valid = false;
    if (value.length > 2 && value.isNotEmpty) {
      valid = true;
    }
    return valid;
  }
  void _updateProfile(BuildContext context){
    _nameFocusNode.unfocus();
    _mailFocusNode.unfocus();
    _phoneFocusNode.unfocus();
    _homeFocusNode.unfocus();
    _bNameFocusNode.unfocus();
    _bModelFocusNode.unfocus();
    if(_validateName(_nameController.text)){
      if(_validateMail(_mailController.text)){
        if(_validatePhone(_phoneController.text)){
          if(_validateBoatModel(_bModelController.text)){
            if(_bLength > 0){
              if(_bWidth > 0){
                _newProfile = new Profile(
                  name: _nameController.text,
                  mail: _mailController.text,
                  phone: _phoneController.text,
                  countryId: _store.countries[_countryInd].id,
                  home: _homeController.text,
                  bName: _bNameController.text,
                  bType: _store.boatTypes[_boatTypeInd].id,
                  bModel: _bModelController.text,
                  bLength: _bLength,
                  bWidth: _bWidth,
                  bDepth: _bDepth,
                  bPhoto: _bPhoto
                );
                _bloc.setLoading(true);
                //
//                Future result;
//                if(_boatPhotoFilePath != ''){
//                  File _fl = File(_boatPhotoFilePath);
//                  String base64Image = base64Encode(_fl.readAsBytesSync());
//                  result = _api.updateProfile(_newProfile, base64Image, _store.apiToken);
//                } else {
//                  print('No new image');
//                  result = _api.updateProfile(_newProfile, _bPhoto, _store.apiToken);
//                }
                Future result = _api.updateProfile(_newProfile, _store.apiToken);
                //
                result.then((value) => _updateSuccess(value, context)).catchError((error) => _dataError(error, context));
              } else {
                _showToast(_lang.boatWidthError, context);
              }
            } else {
              _showToast(_lang.boatLengthError, context);
            }
          } else {
            _showToast(_lang.modelError, context);
          }
        } else {
          _showToast(_lang.phoneError, context);
        }
      } else {
        _showToast(_lang.invalidEmail, context);
      }
    } else{
      _showToast(_lang.atLeast3, context);
    }
  }

  @override
  Widget build(BuildContext context){

    return FutureBuilder<Object>(
      future: _initFuture,
      builder: (context, snapshot) {
        if(snapshot.hasData && snapshot.data){
          return ListView(
            padding: EdgeInsets.only(top: 12.0),
            children: <Widget>[
              ////////////////////////////////////////////////////////////////
              ProfileInputRow(
                icon: IconFont.profile_icon,
                iconSize: 20,
                widget: _myTextField(_nameController, _nameFocusNode, _lang.profileName),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.email,
                iconSize: 20,
                widget: _myTextField(_mailController, _mailFocusNode, _lang.profileEmail),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.phone,
                iconSize: 20,
                widget: _myTextField(_phoneController, _phoneFocusNode, _lang.profilePhone, keyboardType: TextInputType.phone),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.geography,
                iconSize: 20,
                widget: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 6, bottom: 5, right: 6),
                    child: Row(
                      children: <Widget>[
                        StreamBuilder<Object>(
                          stream: _bloc.nationalityUpdate,
                          builder: (context, snapshot) {
                            if(snapshot.hasData){
                              _countryInd = snapshot.data;
                            }
                            return Text(
                              snapshot.hasData ?
                              (_countryInd != -1 ? _store.countries[_countryInd].name : _lang.nationalityHint) :
                              (_countryInd == -1 ? _lang.nationalityHint : _store.countries[_countryInd].name),
                              style: snapshot.hasData ?
                              (_countryInd != -1 ? _styles.formInputStyle : _styles.formInputHintStyle) :
                              (_countryInd == -1 ? _styles.formInputHintStyle : _styles.formInputStyle),
                              textAlign: TextAlign.left,
                            );
                          }
                        ),
                        Spacer(),
                        Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                      ],
                    ),
                  ),
                  onTap: (){
                    killFocus();
                    _selectNationality();
                  },
                ),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.map,
                iconSize: 20,
                widget: _myTextField(_homeController, _homeFocusNode, _lang.profileHome),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.boat,
                iconSize: 26,
                widget: _myTextField(_bNameController, _bNameFocusNode, _lang.profileBoatName),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.boat,
                iconSize: 26,
                widget: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 6, bottom: 5, right: 6),
                    child: StreamBuilder<String>(
                        stream: _bloc.boatPhotoUpdate,
                        builder: (context, photoShot) {
                          return Row(
                            children: <Widget>[
                              Text(
                                _lang.boatPhoto,
                                style: _styles.formInputStyle,
                                textAlign: TextAlign.left,
                              ),
                              Spacer(),
                              _store.profile.bPhoto != '' ? Container(
                                width: _boatPhotoIconW,
                                height: _boatPhotoIconH,
                                child: imagePlaceholderBoat()
                              ) : SizedBox(width: 0,),
                              SizedBox(width: (_store.profile.bPhoto != '' ? 6 : 0)),
                            ],
                          );
                        }
                    ),
                  ),
                  onTap: (){
                    killFocus();
                    //getImage();
                    _selectBoatPhoto();
                  },
                ),
              ),
              SizedBox(height: _spaceBetween),
              //////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////////////////////////////////////////////////////////
              ProfileInputRow(
                icon: IconFont.boat,
                iconSize: 26,
                widget: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 6, bottom: 5, right: 6),
                    child: Row(
                      children: <Widget>[
                        StreamBuilder<Object>(
                          stream: _bloc.boatTypeUpdate,
                          builder: (context, snapshot) {
                            if(snapshot.hasData){
                              _boatTypeInd = snapshot.data;
                            }
                            return Text(
                              snapshot.hasData ?
                              (_boatTypeInd != -1 ? _boatTypeNames[_boatTypeInd] : _lang.profileBoatType) :
                              (_boatTypeInd == -1 ? _lang.profileBoatType : _boatTypeNames[_boatTypeInd]),
                              style: snapshot.hasData ?
                              (_boatTypeInd != -1 ? _styles.formInputStyle : _styles.formInputHintStyle) :
                              (_boatTypeInd == -1 ? _styles.formInputHintStyle : _styles.formInputStyle),
                              textAlign: TextAlign.left,
                            );
                          }
                        ),
                        Spacer(),
                        Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                      ],
                    ),
                  ),
                  onTap: (){
                    killFocus();
                    _selectBoatType();
                  },
                ),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.boat,
                iconSize: 26,
                widget: _myTextField(_bModelController, _bModelFocusNode, _lang.profileBoatModel),
              ),
              //////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////////////////////////////////////////////////////////
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.sizeh,
                iconSize: 20,
                widget: StreamBuilder<String>(
                  stream: _bloc.boatLength,
                  builder: (context, snapshot) {
                    bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (_bLength != 0.0 ? false : true);
                    if(snapshot.hasData){
                      _bLength = double.parse(snapshot.data.toString());
                    }
                    return InkWell(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 6),
                        child: Row(
                          children: <Widget>[
                            Text(
                              snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatLength)
                                  : (_bLength != 0.0 ? '${_bLength.toString()} m' : _lang.profileBoatLength),
                              style: isEmpty ? _styles.formInputHintStyle : _styles.formInputStyle,
                              textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                          ],
                        ),
                      ),
                      onTap: (){
                        killFocus();
                        _showNumPad('length', _lang.profileBoatLength2, snapshot.hasData ? snapshot.data : _bLength.toString());
                      },
                    );
                  }
                ),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.sizew,
                iconSize: 20,
                widget: StreamBuilder<String>(
                  stream: _bloc.boatWidth,
                  builder: (context, snapshot) {
                    bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (_bWidth != 0.0 ? false : true);
                    if(snapshot.hasData){
                      _bWidth = double.parse(snapshot.data.toString());
                    }
                    return InkWell(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 6),
                        child: Row(
                          children: <Widget>[
                            Text(
                              snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatWidth)
                                  : (_bWidth != 0.0 ? '${_bWidth.toString()} m' : _lang.profileBoatWidth),
                              style: isEmpty ? _styles.formInputHintStyle : _styles.formInputStyle,
                              textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                          ],
                        ),
                      ),
                      onTap: (){
                        killFocus();
                        _showNumPad('height', _lang.profileBoatWidth2, snapshot.hasData ? snapshot.data : _bWidth.toString());
                      },
                    );
                  }
                ),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.sized,
                iconSize: 20,
                widget: StreamBuilder<Object>(
                  stream: _bloc.boatDepth,
                  builder: (context, snapshot) {
                    bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (_bDepth != 0.0 ? false : true);
                    if(snapshot.hasData){
                      _bDepth = double.parse(snapshot.data.toString());
                    }
                    return InkWell(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 6),
                        child: Row(
                          children: <Widget>[
                            Text(
                              snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatDepth)
                                  : (_bDepth != 0.0 ? '${_bDepth.toString()} m' : _lang.profileBoatDepth),
                              style: isEmpty ? _styles.formInputHintStyle : _styles.formInputStyle,
                              textAlign: TextAlign.left,
                            ),
                            Spacer(),
                            Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                          ],
                        ),
                      ),
                      onTap: (){
                        killFocus();
                        _showNumPad('depth', _lang.profileBoatDepth2, snapshot.hasData ? snapshot.data : _bDepth.toString());
                      },
                    );
                  }
                ),
              ),
              SizedBox(height: _spaceBetween),
              ProfileInputRow(
                icon: IconFont.profile_icon,
                iconSize: 20,
                widget: InkWell(
                  child: Text(
                    _lang.logout,
                    style: _styles.formInputStyle,
                    textAlign: TextAlign.left,
                  ),
                  onTap: (){
                    killFocus();
                    _prefs.setString('loginMail', '');
                    _prefs.setString('loginPass', '');
                    Navigator.popUntil(context, ModalRoute.withName('/'));
                  },
                )
              ),
              SizedBox(height: _spaceBottom),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  StreamBuilder(
                    stream: _bloc.loading,
                    builder: (context, snapshot){
                      if(snapshot.hasData && snapshot.data){
                        return Container(
                          width: 48, height: 48,
                          padding: EdgeInsets.all(5),
                          child: CircularProgressIndicator(
                            strokeWidth: 3.5,
                            valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
                          ),
                        );
                      } else {
                        return IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(IconFont.check22, size: 46, color: _styles.commonColor),
                          onPressed: (){
                            _updateProfile(context);
                          }
                        );
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: 26),
            ],
          );
        } else{
          return Container(
            child: Center(
              child: SizedBox(width: 1, height: 1,),
            )
          );
        }
      }
    );
  }
}

class ProfileInputRow extends StatelessWidget {

  final IconData icon;
  final double iconSize;
  final Widget widget;
  static const rowHeight = 50.0;

  ProfileInputRow({Key key, @required this.icon, @required this.iconSize, @required this.widget}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: iconSize == 20 ? 20 : 17, height: rowHeight),
          Icon(icon, color: Color(0xFF1A3B62), size: iconSize),
          SizedBox(width: iconSize == 20 ? 10 : 7),
          Container(width: 1, height: 36, color: Color(0xFF1A3B62)),
          SizedBox(width: 10),
          Expanded(child: widget),
          SizedBox(width: 12)
        ],
      ),
    );
  }
}
/*
class NationalityInput extends StatelessWidget {

  final IconData icon;
  final double iconSize;
  final Widget widget;
  static const rowHeight = 50.0;

  NationalityInput({Key key, @required this.icon, @required this.iconSize, @required this.widget}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: iconSize == 20 ? 20 : 17, height: rowHeight),
          Icon(icon,
              color: Color(0xFF1A3B62),
              size: iconSize
          ),
          SizedBox(width: iconSize == 20 ? 10 : 7),
          Container(width: 1,
              height: 36,
              color: Color(0xFF1A3B62)
          ),
          SizedBox(width: 10),
          Expanded(
            child: widget,
          ),
          SizedBox(width: 12)
        ],
      ),
    );
  }
}
*/
