// import 'package:flutter/material.dart';
// import '../../styles/text_styles.dart';
// import '../../api/language.dart';
// import '../common/dialog_button.dart';
//
// class DeleteDialog extends StatelessWidget {
//
//   final String deleteMessage;
//   final GestureTapCallback onPressed;
//
//   final double dialogHeight = 180;//160;
//   final double numBtnHeight = 41.0;
//   final double numBtnSpace = 18.0;
//
//   final MyStyles styles = new MyStyles();
//
//   DeleteDialog({Key key, @required this.deleteMessage, @required this.onPressed}): super(key:key);
//
//   @override
//   Widget build(BuildContext context){
//
//     final Language lang = Language.of(context);
//
//     return Center(
//       child: Container(
//         width: MediaQuery.of(context).size.width-52,
//         height: dialogHeight,
//         decoration: BoxDecoration(
//           color: styles.dialogBackgroundColor,
//           borderRadius: styles.dialogBorderRadius,
//           boxShadow: [styles.dialogShadow],
//         ),
//         child: Column(
//           children: <Widget>[
//             SizedBox(height: 16),
//             Expanded(
//               child: Material(
//                 color: Colors.transparent,
//                 type: MaterialType.transparency,
//                 child: Padding(
//                   padding: const EdgeInsets.only(left: 20, right: 20),
//                   child: SelectableText(
//                       deleteMessage,
//                       enableInteractiveSelection: true,
//                       showCursor: false,
//                       textAlign: TextAlign.left,
//                       style: styles.pageTextMedium
//                   ),
//                 ),
//               ),
//             ),
//             SizedBox(height: 12),
//             Row(
//               children: <Widget>[
//                 SizedBox(width: numBtnSpace),
//                 DialogButton(
//                   ttl: lang.no,
//                   onPressed: (){
//                     Navigator.of(context).pop();
//                   },
//                 ),
//                 Spacer(),
// //                Expanded(child: SizedBox(width: 1)),
//                 DialogButton(
//                   ttl: lang.yes,
//                   onPressed: onPressed,
//                 ),
//                 SizedBox(width: numBtnSpace),
//               ],
//             ),
//             SizedBox(height: numBtnSpace),
//           ],
//         ),
//       ),
//     );
//   }
// }
