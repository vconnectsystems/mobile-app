import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../../api/language.dart';
import '../common/dialog_button.dart';

class ProPhotoDialog extends StatelessWidget {

//  final GestureTapCallback onChange;

  final MyStyles _styles = new MyStyles();
//  final double _numBtnHeight = 41.0;
  final double _numBtnSpace = 18.0;
  final ImagePicker _picker = ImagePicker();

  ProPhotoDialog({Key key}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final double _dialogWidth = MediaQuery.of(context).size.width-52;
    final double _dialogHeight = MediaQuery.of(context).size.width+6;
    double _imgW = _dialogWidth - (2 * _numBtnSpace);
    PickedFile _file;
    dynamic _pickImageError;

    Widget imagePlaceholder() {
      if(_file != null){
        return Image.file(File(_file.path), fit: BoxFit.cover);
      } else if(_pickImageError != null){
        return Container(
          decoration: _styles.placeholderDecoration,
          alignment: Alignment.center,
          child: Text('Error: $_pickImageError', style: _styles.popupHintText),
        );
      } else {
      return Container(
        decoration: _styles.placeholderDecoration,
        alignment: Alignment.center,
        child: Text(_lang.photoProfile, style: _styles.popupHintText),
      );
      }
    }

    void getImage() async {
      print('GET IMAGE, GET IMAGE, GET IMAGE, GET IMAGE, GET IMAGE, GET IMAGE');
      //try {
        _file = await _picker.getImage(source: ImageSource.gallery);
        print('_file.path: ${_file.path}');
        _bloc.updateProPhoto(_file.path);
     // } catch (e) {
       // _pickImageError = e;
      //}
    }

    return Center(
      child: Container(
        width: _dialogWidth,
        height: _dialogHeight,
        decoration: BoxDecoration(
          color: _styles.dialogBackgroundColor,
          borderRadius: _styles.dialogBorderRadius,
          boxShadow: [_styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Container(
              width: _imgW,
              height: _imgW,
              child: Material(
                child: InkWell(
                  child: StreamBuilder<Object>(
                    stream: _bloc.proPhotoUpdate,
                    builder: (context, snapshot) {
                      return imagePlaceholder();
                    }
                  ),
                  onTap: getImage,
                )
              ),
            ),
            Spacer(),
            Row(
              children: <Widget>[
                SizedBox(width: _numBtnSpace),
                DialogButton(
                  ttl: _lang.cancel,
                  onPressed: (){
                    _bloc.updateBoatPhoto('');
                    Navigator.of(context).pop();
                  },
                ),
                Spacer(),
//                Expanded(child: SizedBox(width: 1)),
                StreamBuilder<String>(
                  stream: _bloc.proPhotoUpdate,
                  builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.data != ''){
                      return DialogButton(
                        ttl: _lang.confirmBtn,
                        onPressed: (){
                          Navigator.of(context).pop();
                        }
                      );
                    } else {
                      return DialogButton(
                        ttl: _file != null ? _lang.photoChange : _lang.photoAdd,
                        onPressed: getImage
                      );
                    }
                  }
                ),
                SizedBox(width: _numBtnSpace),
              ],
            ),
            SizedBox(height: _numBtnSpace),
          ],
        ),
      ),
    );
  }
}
