import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/api.dart';
import '../../api/booking.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/toast.dart';
import 'booking_list_item.dart';
import 'booking_details.dart';

class ProfilePageBookings extends StatefulWidget {
  ProfilePageBookings({Key key}) : super(key: key);
  @override
  _ProfilePageBookings createState() => _ProfilePageBookings();
}

class _ProfilePageBookings extends State<ProfilePageBookings> {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();

  Bloc bloc;
  Future<bool> initFuture;
  List<BookingListRow> listItems;
  Language lang;

  void itemTapped(Booking obj) async {
    int newIndex = await Navigator.of(context).push(
      CupertinoPageRoute(builder: (context) => BookingDetails(obj: obj)),
    );
    if(newIndex == 1){
      print('newIndex: $newIndex');
      updateBookings(context);
    }
  }

  void _dataError(dynamic error, BuildContext context){
    Navigator.of(context).pop();
    if(error.runtimeType == SocketException){
      SocketException _err = error;
      if(_err.message.contains('Failed host lookup')){
        Toast.show(
          'Please, check your internet connectivity',
          context,
          backgroundColor: Colors.red,
          duration: styles.toastDuration,
          gravity: styles.toastGravity
        );
      } else {
        Toast.show(
          _err.message,
          context,
          backgroundColor: Colors.red,
          duration: styles.toastDuration,
          gravity: styles.toastGravity
        );
      }
    } else {
      Toast.show(
        error.toString(),
        context,
        backgroundColor: Colors.red,
        duration: styles.toastDuration,
        gravity: styles.toastGravity
      );
    }
  }

  void buildList(List<Booking> bookings, BuildContext context){
    listItems = [];
    bookings.sort((a,b) {
      var aDate = a.updatedAt;
      var bDate = b.updatedAt;
      return aDate.compareTo(bDate);
    });
    int count = bookings.length-1;
    for(int j = 0; j<bookings.length; j++){
      if(bookings[count].statusId < 4){
        BookingListRow item = new BookingListRow(obj: bookings[count], onTap: itemTapped);
        listItems.add(item);
        count--;
      }
    }
    bloc.updateBook(true);
  }

  void updateBookings(BuildContext context){
    print('updateBookings...');
    Future result = api.getBookings(_store.apiToken);
    result.then((value) => buildList(value, context)).catchError((error) => _dataError(error, context));
  }

  @override
  void initState() {
    super.initState();
    initFuture = Future.delayed(Duration.zero,() {
      bloc = BlocProvider.of(context);
      lang = Language.of(context);
      return true;
    });
    updateBookings(context);
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<bool>(
      future: initFuture,
      builder: (context, AsyncSnapshot<bool> intiShot) {
        if(intiShot.hasData && intiShot.data){
          return StreamBuilder<bool>(
            stream: bloc.bookReload,
            builder: (context, reloadShot){
              if(reloadShot.hasData && reloadShot.data){
                print('RELOAD!!!');
                updateBookings(context);
                bloc.reloadBook(false);
              }
              return StreamBuilder<bool>(
                stream: bloc.bookUpdate,
                builder: (context, dataShot) {
                  if(dataShot.hasData && dataShot.data){
                    if(listItems.length > 0){
                      return ListView(
                        padding: EdgeInsets.only(top: 0),
                        children: listItems,
                      );
                    } else {
                      return Center(
                        child: Text(lang.noBookings, style: styles.noContentStyle)
                      );
                    }
                  }else {
                    return Center(
                      child: Container(
                        width: 48, height: 48,
                        padding: EdgeInsets.all(5),
                        child: CircularProgressIndicator(
                          strokeWidth: 3.5,
                          valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                        ),
                      )
                    );
                  }
                }
              );
            }
          );
        } else{
          return Center(
            child: Container(
              width: 48, height: 48,
              padding: EdgeInsets.all(5),
              child: CircularProgressIndicator(
                strokeWidth: 3.5,
                valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
              ),
            )
          );
        }
      }
    );
  }
}
