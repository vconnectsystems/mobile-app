import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../api/booking.dart';
import '../../api/language.dart';
import '../common/icon_font_icons.dart';

class BookingListRow extends StatelessWidget {

  final Booking obj;
  final TextEditingController controller = TextEditingController();
  final void Function(Booking) onTap;

  BookingListRow({Key key, @required this.obj, @required this.onTap}) : super (key : key);

  @override
  Widget build(BuildContext context){

    Language lang = Language.of(context);
//    final DateFormat dateFormat = DateFormat('dd MMM', lang.lang);
    final DateFormat dateFormatYear = DateFormat('dd.MM.yyyy', lang.lang);

    IconData icon;
    String titleFont;
    FontWeight titleWeight;
    TextDecoration titleDecoration;
    Color titleColor;
    String statusText;
    Color statusStringColor;
    Color statusColor;
    Color spotColor;
    Color dateColor;

    switch (obj.statusId){
      case 1:
        // Booked
        if(obj.endDate.isBefore(DateTime.now())){
          icon = IconFont.panorama_fish_eye;
          statusText = lang.ended.toUpperCase();
          statusColor = Color(0xFF1A3B62);
        } else {
          icon = IconFont.lens;
          statusText = lang.booked.toUpperCase();
          statusColor = Color(0xFF009640);
        }
        titleColor = Color(0xFF1A3B62);
        titleFont = "AvenirHeavy";
        titleWeight = FontWeight.w600;
        titleDecoration = TextDecoration.none;
        statusStringColor = Color(0xFF1A3B62);
        spotColor = Color(0xFF1A3B62);
        dateColor = Color(0xFF1A3B62);
        break;
      case 2:
        // Cancelled
        if(obj.endDate.isBefore(DateTime.now())){
          icon = IconFont.panorama_fish_eye;
        } else {
          icon = IconFont.lens;
        }
        titleColor = Colors.black38;
        statusText = lang.cancelledWord.toUpperCase();
        titleFont = "AvenirBook";
        titleWeight = FontWeight.w300;
        titleDecoration = TextDecoration.lineThrough;
        statusStringColor = Colors.black38;
        statusColor = Color(0xFFBE1622).withOpacity(0.38);
        spotColor = Colors.black38;
        dateColor = Colors.black38;
        break;
      case 3:
        // Pending
        icon = IconFont.lens;
        statusText = lang.pending.toUpperCase();
        titleColor = Color(0xFF1A3B62);
        titleFont = "AvenirHeavy";
        titleWeight = FontWeight.w600;
        titleDecoration = TextDecoration.none;
        statusStringColor = Color(0xFF1A3B62);
        statusColor = Color(0xFFD78026);
        spotColor = Color(0xFF1A3B62);
        dateColor = Color(0xFF1A3B62);
        break;
      default:
        if(obj.endDate.isBefore(DateTime.now())){
          icon = IconFont.panorama_fish_eye;
          statusText = lang.ended.toUpperCase();
          statusColor = Color(0xFF1A3B62);
        } else {
          icon = IconFont.lens;
          statusText = lang.booked.toUpperCase();
          statusColor = Color(0xFF009640);
        }
        titleColor = Color(0xFF1A3B62);
        titleFont = "AvenirHeavy";
        titleWeight = FontWeight.w600;
        titleDecoration = TextDecoration.none;
        statusStringColor = Color(0xFF1A3B62);
        spotColor = Color(0xFF1A3B62);
        dateColor = Color(0xFF1A3B62);
        break;
    }

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: InkWell(
        highlightColor: Colors.white.withOpacity(0),
        splashColor: Colors.white.withOpacity(.3),
        onTap: (){
          onTap(obj);
        },
        child: Column (
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 20),
                Icon(icon, size: 12),
                SizedBox(width: 6),
                Expanded(
                  child:
                  Container(
                    child: Text(obj.harbourName.toUpperCase(),
                      style: TextStyle(
                        color: titleColor,
                        fontFamily: titleFont,
                        fontWeight: titleWeight,
                        decoration: titleDecoration,
                        fontSize: 18.0
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 2),
            Row(
              children: <Widget>[
                SizedBox(width: 38),
                Text(
                  lang.status,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: statusStringColor,
                    fontFamily: "AvenirBook",
                    fontSize: 16.0,
                  ),
                ),
                //SizedBox(width: 2),
                Text(
                  statusText,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: statusColor,
                    fontFamily: "AvenirHeavy",
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 4),
            Row(
              children: <Widget>[
                SizedBox(width: 38),
                Text('#${obj.boatSpot}',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: spotColor,
                    fontFamily: "AvenirBook",
                    fontSize: 19.0,
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 4),
            Row(
              children: <Widget>[
                SizedBox(width: 38),
                Text(
//                  DateTime.now().year == obj.startDate.year ?
//                  '${dateFormat.format(obj.startDate)} - ${dateFormat.format(obj.endDate)}'
//                      : '${dateFormatYear.format(obj.startDate)} - ${dateFormatYear.format(obj.endDate)}' ,
                  '${dateFormatYear.format(obj.startDate)} - ${dateFormatYear.format(obj.endDate)}',
                  softWrap: true,
                  style: TextStyle(
                    color: dateColor,
                    fontFamily: "AvenirBook",
                    fontWeight: FontWeight.w300,
                    fontSize: 17.0
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 1,
                color: Color(0xFF1A3B62).withOpacity(.35)
              ),
            ),
          ],
        ),
      ),
    );
  }
}
