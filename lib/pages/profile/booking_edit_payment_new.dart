import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
// import 'booking_edit_complete.dart';
import 'package:meta/meta.dart';

class BookingEditPaymentPage extends StatefulWidget {

  final String paymentLink;

  BookingEditPaymentPage({Key key, @required this.paymentLink}) : super(key: key);

  @override
  _BookingEditPaymentPage createState() => _BookingEditPaymentPage();

}

class _BookingEditPaymentPage extends State<BookingEditPaymentPage> {

  @override
  void initState() {
    super.initState();
    final webView = FlutterWebviewPlugin();
    webView.onUrlChanged.listen((String url) {
      print(":::::::::::::::URL:$url::::::::::::::::::::::::");
      if (url.contains("payment-cancel-callback")) {
        Navigator.of(context).pop('cancel');
      } else {
        if ( url.contains("api.docksideapp.dk") || url.contains("api.dock.vconnect.systems")) {
          print(":::::::::::::::SUCCESS!!!::::::::::::::::::::::::");
          Navigator.of(context).pop('success');
          // Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingCompletePage()));
        }
      }
      // if ( url.endsWith("https://api.docksideapp.dk/")
      //     || url.endsWith("https://api.docksideapp.dk/harbour-map")
      //     || url.endsWith("http://dock.vconnect.systems/")
      //     || url.endsWith("http://dock.vconnect.systems/harbour-map") ) {
      //   print(":::::::::::::::SUCCESS!!!::::::::::::::::::::::::");
      //   Navigator.of(context).pop();
      //   Navigator.push(context,
      //     CupertinoPageRoute(builder: (context) => BookingCompletePage()),
      //   );
      // }
    });
  }

  @override
  Widget build(BuildContext context){
    return WebviewScaffold (
      withJavascript: true,
      appCacheEnabled: true,
      url: widget.paymentLink,
      withLocalStorage: true,
      hidden: true,
      initialChild: Center(
        child: Container(
          width: 48, height: 48,
          padding: EdgeInsets.all(5),
          child: CircularProgressIndicator(
            strokeWidth: 3.5,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1A3B62))
          ),
        ),
      ),
    );
  }
}

// class PopWithResults<T> {
//   /// poped from this page
//   final String fromPage;
//   /// pop until this page
//   final String toPage;
//   /// results
//   final Map<String, T> results;
//   /// constructor
//   PopWithResults({@required this.fromPage, @required this.toPage, this.results});
// }