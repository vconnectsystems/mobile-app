import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/dialog_button.dart';

class MoreDaysDialog extends StatelessWidget {

  final String message;
  final int statusCode;
  final GestureTapCallback onConfirm;
  final GestureTapCallback onCancel;

  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;

  final MyStyles styles = new MyStyles();

  MoreDaysDialog({Key key,
    @required this.message,
    @required this.statusCode,
    @required this.onConfirm,
    @required this.onCancel
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    double dialogHeight = statusCode == 0 ? 280 : 360;//0 ? 260 : 340;

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Expanded(
                child: Material(
                  color: Colors.transparent,
                  type: MaterialType.transparency,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: SelectableText(
                      message,
                      style: styles.pageTextMedium,
                      maxLines: null,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  SizedBox(width: numBtnSpace),
                  statusCode > 0 ? DialogButton(
                    flex: 3,
                    ttl: lang.goBack,
                    onPressed: onCancel
                  ) : SizedBox(width: 1),
                  Spacer(),
                  DialogButton(
                    flex: 3,
                    ttl: statusCode == 0 ? lang.ok : lang.confirmBtn,
                    onPressed: statusCode == 0 ? onCancel : onConfirm
                  ),
                  SizedBox(width: numBtnSpace),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}