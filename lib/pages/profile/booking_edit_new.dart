import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'dart:ui';
import 'dart:async' show Future;
import 'dart:convert';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../api/booking.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import '../booking/apple_date_dialog.dart';
// import '../booking/date_picker_theme.dart';
import 'more_days_dialog.dart';
// import 'booking_edit_payment_new.dart';
import '../booking/booking_payment_new.dart';
import 'booking_edit_complete.dart';

class BookingEdit extends StatefulWidget {

  final Booking booking;

  BookingEdit({Key key, @required this.booking}) : super (key : key);

  @override
  _BookingEditState createState() => _BookingEditState();
}

class _BookingEditState extends State<BookingEdit> {

  static const _spaceBetween = 4.0;
  static const _spaceBottom = 20.0;
  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final Api _api = new Api();
  final DateFormat _apiDateFormat = DateFormat('yyyy-MM-dd');
  Language _lang;
  Bloc _bloc;
  DateFormat _dateFormat;
  bool _sameDays = false;

  void selectCheckIn(){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleDateDialog(
          ttl: _lang.selectArrival,
          hint: _lang.arrivalNote,
          minDate: DateTime.now(),
          initDate: (_store.editCheckIn != null) ? _store.editCheckIn : DateTime.now(),
          onSelect: (DateTime newDate){
            if(newDate != null) {
              _store.editCheckIn = newDate;
              _bloc.updateCheckIn(newDate);
            } else {
              if(_store.editCheckIn == null){
                _store.editCheckIn = DateTime.now();
                _bloc.updateCheckIn(DateTime.now());
              }
            }
          },
        );
      },
    );
  }

  void selectCheckOut() {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleDateDialog(
          ttl: _lang.selectDeparture,
          hint: _lang.departureNote,
          minDate: _store.editCheckIn.add(Duration(days: 1)),
          initDate: (_store.editCheckOut != null) ?
          ((_store.editCheckOut.isBefore(_store.editCheckIn) ? _store.editCheckIn : _store.editCheckOut))
              : _store.editCheckIn.add(Duration(days: 1)),
          onSelect: (DateTime newDate){
            if(newDate != null) {
              _store.editCheckOut = newDate;
              _bloc.updateCheckOut(newDate);
            }
          },
        );
      },
    );
  }

  void _showToast(String error, BuildContext context){
    Toast.show(
        error.toString(), context,
        backgroundColor: Colors.red,
        duration: _styles.toastDuration,
        gravity: _styles.toastGravity
    );
  }
  void _dataError(dynamic error, BuildContext context){
    _bloc.setLoading(false);
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(error.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }

  void _paymentLinkSuccess(){
    _bloc.setLoading(false);
    print('_paymentLinkSuccess...');
    if(_store.bookingPaymentLink != ''){
      Future<dynamic> result = Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage()));
      result.then((status){
        print('status: $status');
        if(status == 'success'){
          Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingCompletePage()));
        } else {
          _showToast('The payment failed!', context);
        }
      });
    } else {
      _showToast('The payment failed!', context);
    }
  }

  void _updateSuccess(String value, BuildContext context){
    print('value: $value');
    if(value.toString().contains('success')){
      print('_sameDays: $_sameDays');
      if(_sameDays){
        Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingCompletePage()));
      } else {
        Future result = _api.getPaymentLink(widget.booking.id, _store.apiToken);
        result.then((value) => _paymentLinkSuccess()).catchError((error) => _dataError(error, context));
      }
    } else {
      _bloc.setLoading(false);
      var parsedJson = json.decode(value);
      _showToast(parsedJson['error'], context);
    }
  }

  void _updateBooking(BuildContext context){
    _bloc.setLoading(true);
    Future result = _api.updateBooking(
      widget.booking.id,
      _apiDateFormat.format(_store.editCheckIn),
      _apiDateFormat.format(_store.editCheckOut),
      _store.apiToken
    );
    result.then((value) => _updateSuccess(value, context)).catchError((error) => _dataError(error, context));
  }

  void showMoreDays(String message, int status){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return MoreDaysDialog(
          message: message,
          statusCode: status,
          onCancel: (){
            Navigator.pop(context);
          },
          onConfirm: (){
            Navigator.pop(context);
            _updateBooking(context);
          },
        );
      },
    );
  }

  void next (BuildContext context){
    if(_store.editCheckIn != null){
      if(_store.editCheckOut != null){
        int hoursBefore = widget.booking.endDate.difference(widget.booking.startDate).inHours;
        int daysBefore = (hoursBefore/24).ceil();
        int hoursNow = _store.editCheckOut.difference(_store.editCheckIn).inHours;
        int daysNow = (hoursNow/24).ceil();
        String perDayString = '';
        double newTotal = widget.booking.price * daysNow;
        if(daysNow > 1){
          perDayString = ' (${widget.booking.price} DKK ${_lang.perDay})';
        }
        if(daysNow > daysBefore){
          int moreDays = daysNow-daysBefore;
          String moreDaysStr = '$moreDays ${moreDays == 1 ? _lang.day : _lang.days }';
          String m0 = _lang.moreDaysPlus;
          String m1 = m0.replaceFirst('[1]', moreDaysStr);
          String m2 = m1.replaceFirst('[2]', newTotal.toString());
          String m3 = m2.replaceFirst('[3]', perDayString);
          showMoreDays(m3, 1);
        } else if(daysNow < daysBefore){
          int lessDays = daysBefore-daysNow;
          String lessDaysStr = '$lessDays ${lessDays == 1 ? _lang.day : _lang.days }';
          String m0 = _lang.moreDaysMinus;
          String m1 = m0.replaceFirst('[1]', lessDaysStr);
          String m2 = m1.replaceFirst('[2]', newTotal.toString());
          String m3 = m2.replaceFirst('[3]', perDayString);
          showMoreDays(m3, 1);
        } else if(_dateFormat.format(widget.booking.startDate) == _dateFormat.format(_store.editCheckIn)
            && _dateFormat.format(widget.booking.endDate) == _dateFormat.format(_store.editCheckOut) ){
          _sameDays = true;
          showMoreDays(_lang.noChanges, 0);
        }else{
          _sameDays = true;
          showMoreDays(_lang.sameDaysCount, 2);
        }
      } else{
        _showToast(_lang.bookDepartureError, context);
      }
    } else {
      _showToast(_lang.bookArrivalError, context);
    }
  }

  @override
  Widget build(BuildContext context){

    _lang = Language.of(context);
    _bloc = BlocProvider.of(context);
    _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);

    // Harbour
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: _styles.bgDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: _lang.booking.toUpperCase(),
                subTitle: widget.booking.harbourName.toUpperCase(),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: Container(
                decoration: _styles.pageDecoration,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.only(top: 16, bottom: 20),
                        children: <Widget>[
                          BookingInputRow(
                            icon: IconFont.location,
                            iconSize: 20,
                            widget: Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 5),
                              child: Text(
                                '${widget.booking.harbourName}, #${widget.booking.boatSpot}',
                                style: TextStyle(
                                  fontFamily: "AvenirBook",
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black54,
                                  fontSize: 19.0,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),

                          SizedBox(height: _spaceBetween),

                          // Check-in, Check-out
                          StreamBuilder<Object>(
                              stream: _bloc.selectCheckIn,
                              builder: (context, snapshot) {
                                if(snapshot.hasData){
                                  _store.editCheckIn = snapshot.data;
                                  if(_store.editCheckOut.isBefore(_store.editCheckIn.add(Duration(days: 1)))){
                                    _store.editCheckOut = _store.editCheckIn.add(Duration(days: 1));
                                  }
                                }
                                return Column(
                                  children: <Widget>[
                                    BookingInputRow(
                                        icon: IconFont.checkout,
                                        iconSize: 20,
                                        widget: InkWell(
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 6, bottom: 5),
                                            child: Text(
                                              _dateFormat.format(_store.editCheckIn),
                                              style: _styles.formInputStyle,
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                          onTap: selectCheckIn,
                                        )
                                    ),
                                    SizedBox(width: _spaceBetween),
                                    StreamBuilder<DateTime>(
                                        stream: _bloc.selectCheckOut,
                                        builder: (context, snapOut) {
                                          if(snapOut.hasData){
                                            _store.editCheckOut = snapOut.data.isBefore(_store.editCheckIn.add(Duration(days: 1))) ? _store.editCheckIn.add(Duration(days: 1)) : snapOut.data;
                                          }
                                          return BookingInputRow(
                                              icon: IconFont.checkin,
                                              iconSize: 20,
                                              widget: InkWell(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top: 6, bottom: 5),
                                                  child: Text(
                                                    _dateFormat.format(_store.editCheckOut),
                                                    style: _styles.formInputStyle,
                                                    textAlign: TextAlign.left,
                                                  ),
                                                ),
                                                onTap: selectCheckOut
                                              )
                                          );
                                        }
                                    ),
                                  ],
                                );
                              }
                          ),
                        ],
                      ),
                    ),

                    SizedBox(height: _spaceBottom),

                    //Next button
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
//                        SizedBox(width: 26),
//                        IconButton(
//                            padding: EdgeInsets.all(0),
//                            icon: Icon(IconFont.goback2,
//                              size: 46,
//                              color: styles.commonColor,
//                            ),
//                            onPressed: (){
//                              Navigator.pop(context);
//                            }
//                        ),
//                        Spacer(),
                        StreamBuilder(
                          stream: _bloc.loading,
                          builder: (context, snapshot){
                            if(snapshot.hasData && snapshot.data){
                              return Container(
                                width: 48, height: 48,
                                padding: EdgeInsets.all(5),
                                child: CircularProgressIndicator(
                                    strokeWidth: 3.5,
                                    valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
                                ),
                              );
                            } else {
                              return IconButton(
                                  padding: EdgeInsets.all(0),
                                  icon: Icon(IconFont.arrow_icon,
                                    size: 46,
                                    color: _styles.commonColor,
                                  ),
                                  onPressed: (){
                                    next(context);
                                  }
                              );
                            }
                          },
                        ),
//                        SizedBox(width: 26),
                      ],
                    ),
                    SizedBox(height: 26),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BookingInputRow extends StatelessWidget {

  final IconData icon;
  final double iconSize;
  final Widget widget;
  static const rowHeight = 50.0;

  BookingInputRow({Key key, @required this.icon, @required this.iconSize, @required this.widget}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: iconSize == 20 ? 20 : 17, height: rowHeight),
          Icon(icon,
              color: Color(0xFF1A3B62),
              size: iconSize
          ),
          SizedBox(width: iconSize == 20 ? 10 : 7),
          Container(width: 1,
              height: 36,
              color: Color(0xFF1A3B62)
          ),
          SizedBox(width: 10),
          Expanded(
            child: widget,
          ),
          SizedBox(width: 12)
        ],
      ),
    );
  }
}

class SvgButton {
  final String key;
  final double x;
  final double y;
  SvgButton({
    this.key,
    this.x,
    this.y,
  });
}