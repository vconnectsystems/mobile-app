import 'package:flutter/material.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import 'profile_tab_bar.dart';
import 'profile_page_info.dart';
import 'profile_page_bookings.dart';

class ProfilePage extends StatelessWidget {

  final int initIndex;
  final GestureTapCallback backToDashboard;
  final TextEditingController controller = TextEditingController();

  ProfilePage({Key key, this.initIndex, this.backToDashboard}) : super (key : key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);

    return Column(
      children: <Widget>[
        ProfileTabBar(initIndex: initIndex),
        Expanded(
          child: StreamBuilder(
            stream: _bloc.profileSubPage,
            builder: (context, snapshot) {
              return snapshot.hasData
                ? (
                    snapshot.data == 0
                    ? ProfilePageInfo(backToDashboard: backToDashboard)
                    : ProfilePageBookings()
                  )
                : (
                    initIndex != null
                    ? initIndex == 1 ? ProfilePageBookings() : ProfilePageInfo(backToDashboard: backToDashboard)
                    : ProfilePageInfo(backToDashboard: backToDashboard)
                  );
            }
          ),
        ),
      ],
    );
  }
}
