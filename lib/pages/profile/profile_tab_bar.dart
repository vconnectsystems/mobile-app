import 'package:flutter/material.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';

class ProfileTabBar extends StatefulWidget {

  final int initIndex;

  ProfileTabBar({Key key, this.initIndex}) : super (key : key);

  @override
  _ProfileTabBarState createState() => _ProfileTabBarState();
}

class _ProfileTabBarState extends State<ProfileTabBar> {

  final tabBarOuterMargin = 20.0;
  final tabBarMargin = 20.0;
  final MyStyles styles = new MyStyles();
  Bloc _bloc;
  Language _lang;
  Future<bool> _initFuture;
  double _screenWidth = 0;

  double _getLeft(int buttonIndex){
    double left;
    double scrWidth = _screenWidth;// > 480.0 ? 480.0 : screenWidth;
    if(buttonIndex == 0){
      left = tabBarOuterMargin+tabBarMargin;
    }else{
      double lineWidth = scrWidth-(tabBarOuterMargin*2);
      double halfLine = lineWidth/2;
      left = tabBarOuterMargin+halfLine+tabBarMargin;
    }
    return left;
  }

  double _getRight(int buttonIndex){
    double right;
    double scrWidth = _screenWidth;// > 480.0 ? 480.0 : screenWidth;
    if(buttonIndex == 0){
      double lineWidth = scrWidth-(tabBarOuterMargin*2);
      double halfLine = lineWidth/2;
      right = tabBarOuterMargin+halfLine+tabBarMargin;
    }else{
      right = tabBarOuterMargin+tabBarMargin;
    }
    return right;
  }

  @override
  void initState() {
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _bloc = BlocProvider.of(context);
      _lang = Language.of(context);
      _screenWidth = MediaQuery.of(context).size.width;
      Future<void>.delayed(Duration(milliseconds: 30),() async {
        if (widget.initIndex != null){
          _bloc.changeProfileSubPage(widget.initIndex);
        }
      });
      return true;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return FutureBuilder<bool>(
      future: _initFuture,
      builder: (BuildContext context, AsyncSnapshot initShot) {
        if(initShot.hasData && initShot.data){
          return Column(
            children: <Widget>[
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  SizedBox(width: 20),
                  Expanded(
                    child: MaterialButton(
                      highlightColor: Colors.white.withOpacity(0),
                      splashColor: Colors.white.withOpacity(.2),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      height: 44,
                      child: StreamBuilder<int>(
                        stream: _bloc.profileSubPage,
                        builder: (context, snapshot) {
                          return Text(_lang.profile,
                            style: snapshot.hasData ? (snapshot.data == 0 ? styles.tabBarSelected : styles.tabBarDeSelected) : styles.tabBarSelected
                          );
                        }
                      ),
                      onPressed: (){
                        _bloc.changeProfileSubPage(0);
                      },
                    ),
                  ),
                  Expanded(
                    child: MaterialButton(
                      highlightColor: Colors.white.withOpacity(0),
                      splashColor: Colors.white.withOpacity(.2),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      height: 44,
                      child: StreamBuilder<int>(
                        stream: _bloc.profileSubPage,
                        builder: (context, snapshot) {
                          return Text(_lang.bookings,
                              style: snapshot.hasData ? (snapshot.data == 0 ? styles.tabBarDeSelected : styles.tabBarSelected) : styles.tabBarDeSelected
                          );
                        }
                      ),
                      onPressed: (){
                        _bloc.changeProfileSubPage(1);
                      },
                    )
                  ),
                  SizedBox(width: 20),
                ],
              ),
              SizedBox(height: 4),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Stack(
                          children: <Widget>[
                            Positioned(
                              child: Container(
                                constraints: BoxConstraints.expand(height: 1),
                                margin: EdgeInsets.only(top: 1, bottom: 1),
                                decoration: BoxDecoration(
                                    color: Color(0xFF1A3B62).withOpacity(.35)
                                ),
                              ),
                            ),
                            StreamBuilder(
                                stream: _bloc.profileSubPage,
                                builder: (context, snapshot) {
                                  return AnimatedPositioned(
                                    curve: Curves.easeOut,
                                    duration: Duration(milliseconds: 300),
                                    left: _getLeft(snapshot.hasData ? snapshot.data : 0),
                                    //_getRedLeft(),
                                    right: _getRight(snapshot.hasData ? snapshot.data : 0),
                                    //_getRedRight(),
                                    top: 0,
                                    bottom: 0,
                                    child: DecoratedBox(
                                        decoration: BoxDecoration(
                                            color: Color(0xFF1A3B62),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(1)
                                            )
                                        )
                                    ),
                                  );
                                }
                            ),
                          ]
                      )
                  ),
                ],
              ),
            ],
          );
        } else {
          return Container();
        }
      }
    );
  }
}