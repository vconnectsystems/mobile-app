import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'dart:core';
import '../../api/api.dart';
import '../../api/message.dart';
import '../../api/message_read.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/dialog_button.dart';
import '../common/toast.dart';
import '../common/icon_font_icons.dart';
import '../common/progress_dialog.dart';
import 'messages_page_reply.dart';

class MessagesRead extends StatefulWidget {

  final Message messObj;
  final String ttl;
  final String subTtl;

  MessagesRead({Key key, @required this.messObj, @required this.ttl, @required this.subTtl}) : super (key : key);

  @override
  _MessagesRead createState() => _MessagesRead();

}

class _MessagesRead extends State<MessagesRead> {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();
  final double bigSpace = 20.0;
  final double space = 10.0;

  DateFormat dateFormat;
  Future<bool> initFuture;
  Future<MessageReadObj> objFuture;
  MessageReadObj obj;
  Language lang;
  String newBody;
  List<TextSpan> textSpans;
  List<Widget> messWidgets;

  void _showToast(String error, BuildContext context){
    Toast.show(
      error.toString(), context,
      backgroundColor: Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }
  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(error.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }

  @override
  void initState() {
    super.initState();
    objFuture = api.readMessage(widget.messObj.id, _store.apiToken).then((MessageReadObj message) {
      //
      lang = Language.of(context);
      dateFormat = DateFormat('dd.MM.yyyy - HH:mm', lang.lang);
      //
      messWidgets = [];
      //
      int messNum = message.messages.length;
      int count = message.messages.length-1;
      for(int i = 0; i < messNum; i++){
        String cleanStr1 = message.messages[count].content.replaceAll('<p>', '');
        String cleanStr2 = cleanStr1.replaceAll('</p>', '');
        MessageBox box = new MessageBox(
          date: dateFormat.format(message.messages[count].date),
          sender: message.messages[count].sender,
          content: cleanStr2,
        );
        messWidgets.add(box);
        count--;
      }
      //
      obj = message;//newMessage;
      //
      return obj;
      }, onError: (error) {_dataError(error, context);}
    );
  }

  void showProgress(){
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.white70,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation) {
        return ProgressDialog();
      },
    );
  }

  void _deleteSuccess(value, BuildContext context){
    Navigator.pop(context);
    Navigator.pop(context, 1);
  }

  void delMessage(){
    Navigator.of(context).pop();
    showProgress();
    Future result = api.delMessage(obj.id, _store.apiToken);
    result.then((value) => _deleteSuccess(value, context)).catchError((error) => _dataError(error, context));
  }

  void deleteMessage() {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations
        .of(context)
        .modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: Duration.zero,
      pageBuilder: (BuildContext cont, Animation animation,
          Animation secondaryAnimation) {
        return DeleteDialog(
          deleteMessage: lang.delMessage,
          onPressed: () {
            delMessage();
          },
        );
      },
    );
  }

  void replyMessage(BuildContext context) async {
    int newIndex = await Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => MessagesReply(
        messObj: widget.messObj,
        ttl: lang.harbours.toUpperCase(),
        subTtl: lang.messages.toUpperCase(),
        body: '',//obj.contents[0]
      )),
    );
    if(newIndex == 1){
      Navigator.pop(context, 1);
    }
  }

  void _moveToSignInScreen(BuildContext context){
    Navigator.pop(context, 1);
  }

  @override
  Widget build(BuildContext context){

    return WillPopScope(
      onWillPop: (){
        _moveToSignInScreen(context);
        return;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Header(
                  xVis: true,
                  mainTitle: widget.ttl,
                  subTitle: widget.subTtl,
                  onPressed: (){
                    Navigator.popUntil(context, ModalRoute.withName('/Dashboard'));
                  },
                ),
              ),
              SizedBox(height: 16),
              Expanded(
                child: Container(
                  decoration: styles.pageDecoration,
                  child: FutureBuilder<MessageReadObj>(
                    future: objFuture,
                    builder: (context, snapshot) {
                      if(snapshot.hasData){
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 6),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(width: 26),
                                Expanded(
                                  child: SizedBox(
                                    height: 50,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          top: 14,//top: 15,
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                widget.messObj.harbourName.toUpperCase(),
                                                textAlign: TextAlign.left,
                                                style: styles.messageReadTitle
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                      //alignment: cen,
                                    ),
                                  ),
                                ),
                                IconButton(
                                  icon: Icon(IconFont.closex,
                                    color: Color(0xFF1A3B62),
                                    size: 16
                                  ),
                                  onPressed: (){
                                    //goBack(context);
                                    Navigator.pop(context, 1);
                                  },
                                ),
                                SizedBox(width: 9),
                              ],
                            ),
                            SizedBox(height: 4),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        child: Container(
                                          constraints: BoxConstraints.expand(height: 1),
                                          margin: EdgeInsets.only(top: 1, bottom: 1),
                                          decoration: BoxDecoration(
                                            color: Color(0xFF1A3B62).withOpacity(.35)
                                          ),
                                        ),
                                      ),
                                    ]
                                  )
                                ),
                              ],
                            ),
                            Expanded(
                              child: ListView(
                                padding: EdgeInsets.only(left: 18, right: 18, top: 16, bottom: 20),
                                children: messWidgets,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(IconFont.reply_arrow,
                                      color: Color(0xFF1A3B62),
                                      size: 22
                                    ),
                                    onPressed: (){
                                      replyMessage(context);
                                    }
                                    //replyMessage,
                                  ),
                                  Expanded(
                                    child: SizedBox(height: 22,),
                                  ),
                                  IconButton(
                                    icon: Icon(IconFont.trash_light,
                                      color: Color(0xFF1A3B62),
                                      size: 26
                                    ),
                                    onPressed: deleteMessage,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      } else {
                        return Center(
                          child: Container(
                            width: 48, height: 48,
                            padding: EdgeInsets.all(5),
                            child: CircularProgressIndicator(
                              strokeWidth: 3.5,
                              valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                            ),
                          )
                        );
                      }
                    }
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MessageBox extends StatelessWidget {

  final String date;
  final String sender;
  final String content;

  final double space = 8.0;
  final MyStyles styles = new MyStyles();

  MessageBox({Key key, @required this.date, @required this.sender, @required this.content}): super(key:key);

  @override
  Widget build(BuildContext context){

//    final Language lang = Language.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(width: space),
            Text(
              sender.toUpperCase(),
              style: styles.textPageBodyBold
            ),
            SizedBox(width: space),
          ],
        ),
        Row(
          children: <Widget>[
            SizedBox(width: space),
            Text(
              date,
              style: styles.textPageBody,
            ),
            SizedBox(width: space),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: styles.messageBoxDecoration,
                padding: EdgeInsets.all(space),
                child: SelectableText(
                  content,
                  style: styles.messageReadBody,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 16,
        )
      ],
    );
  }

}

class DeleteDialog extends StatelessWidget {

  final String deleteMessage;
  final GestureTapCallback onPressed;

  final double dialogHeight = 160;
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;

  final MyStyles styles = new MyStyles();

  DeleteDialog({Key key, @required this.deleteMessage, @required this.onPressed}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Expanded(
              child: Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: SelectableText(
                    deleteMessage,
                    enableInteractiveSelection: true,
                    showCursor: false,
                    textAlign: TextAlign.left,
                    style: styles.pageTextMedium
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                DialogButton(
                  ttl: lang.cancel,
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                ),
                Expanded(child: SizedBox(width: 1)),
                DialogButton(
                  ttl: lang.delete,
                  onPressed: onPressed,
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}
