import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/api.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../api/message.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/toast.dart';
import 'message_list_item.dart';
import 'messages_page_read.dart';

class MessagesPageDate extends StatefulWidget {

  MessagesPageDate({Key key}) : super(key: key);

  @override
  _MessagesPageDate createState() => _MessagesPageDate();

}

class _MessagesPageDate extends State<MessagesPageDate> {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();
  Bloc _bloc;
  Future<bool> initFuture;
  List<MessageListRow> listItems;
  Future<List<MessageListRow>> listFuture;
  Language lang;

  void messageItemTapped(Message messObj) async {
    int newIndex = await Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (context) => MessagesRead(
          messObj: messObj,
          ttl: lang.harbours.toUpperCase(),
          subTtl: lang.messages.toUpperCase()
        )
      ),
    );
    if(newIndex == 1){
      print('newIndex: $newIndex');
      getMessages(context);
    }
  }

  void _showToast(String error, BuildContext context){
    Toast.show(
      error.toString(), context,
      backgroundColor: Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }
  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(error.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }

  void getMessages(BuildContext context){
    print('getMessages...');
    listItems = [];
    print('listItems.length: ${listItems.length}');
    listFuture = api.getMessages(_store.apiToken).then(
      (List<Message> messages) {
        print('then...');
        messages.sort((a,b) {
          var aDate = a.createdOn;
          var bDate = b.createdOn;
          return -aDate.compareTo(bDate);
        });
        _bloc = BlocProvider.of(context);
        lang = Language.of(context);
        int unreadCount = 0;
        for(int j = 0; j<messages.length; j++){
          MessageListRow item = new MessageListRow(obj: messages[j], onTap: messageItemTapped);
          listItems.add(item);
          if(messages[j].read == false){
            unreadCount++;
          }
        }
        _store.unreadMess = unreadCount;
        print('update................');
        _bloc.updateUnread(unreadCount);
        _bloc.updateMess(true);
        return listItems;
      },
      onError: (error) {
        _dataError(error, context);
      }
    );
  }

  @override
  void initState() {
    super.initState();
    print('init, init, init.......');
    getMessages(context);
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<List<MessageListRow>>(
      future: listFuture,
      builder: (context, AsyncSnapshot<List<MessageListRow>> snapshot) {
        if(snapshot.hasData){
          print(' snapshot.hasData snapshot.hasData snapshot.hasData snapshot.hasData ');
          if(snapshot.data.length > 0){
            return ListView(
              padding: EdgeInsets.only(top: 0),
              children: snapshot.data,
            );
          } else {
            return Center(
              child: Text(
                lang.noMessages,
                style: styles.noContentStyle
              )
            );
          }
        } else{
          return Center(
            child: Container(
              width: 48, height: 48,
              padding: EdgeInsets.all(5),
              child: CircularProgressIndicator(
                strokeWidth: 3.5,
                valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
              ),
            )
          );
        }
      }
    );
  }
}