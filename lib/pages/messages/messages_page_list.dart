import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/api.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../api/message.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/toast.dart';
import 'message_list_item.dart';
import 'messages_page_read.dart';

class MessagesPageList extends StatefulWidget {

  final bool byDate;

  MessagesPageList({Key key, @required this.byDate}) : super(key: key);

  @override
  _MessagesPageList createState() => _MessagesPageList();

}

class _MessagesPageList extends State<MessagesPageList> {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();

  Bloc bloc;
  List<MessageListRow> listItems;
  Future<bool> initFuture;
  Language lang;

  void messageItemTapped(Message messObj) async {
    int newIndex = await Navigator.of(context).push(
      CupertinoPageRoute(
        builder: (context) => MessagesRead(
          messObj: messObj,
          ttl: lang.harbours.toUpperCase(),
          subTtl: lang.messages.toUpperCase()
        )
      ),
    );
    if(newIndex == 1){
      print('newIndex: $newIndex');
      updateMessages(context);
    }
  }

  void _showToast(String error, BuildContext context){
    Toast.show(
      error.toString(), context,
      backgroundColor: Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }
  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(error.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }

  void buildList(List<Message> messages, BuildContext context){
    listItems = [];
    messages.sort((a,b) {
      if(widget.byDate){
        var aDate = a.createdOn;
        var bDate = b.createdOn;
        return -aDate.compareTo(bDate);
      } else {
        var aDate = a.harbourName;
        var bDate = b.harbourName;
        return -aDate.compareTo(bDate);
      }
    });
    int unreadCount = 0;
    for(int j = 0; j<messages.length; j++){
      MessageListRow item = new MessageListRow(obj: messages[j], onTap: messageItemTapped);
      listItems.add(item);
      if(messages[j].read == false){
        unreadCount++;
      }
    }
    _store.unreadMess = unreadCount;
    bloc.updateUnread(unreadCount);
    bloc.updateMess(true);
  }

  void updateMessages(BuildContext context){
    print('updateMessages...');
    Future result = api.getMessages(_store.apiToken);
    result.then((value) => buildList(value, context)).catchError((error) => _dataError(error, context));
  }

  @override
  void initState() {
    super.initState();
    print('initState...');
    initFuture = Future.delayed(Duration.zero,() {
      bloc = BlocProvider.of(context);
      lang = Language.of(context);
      return true;
    });
    updateMessages(context);
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<bool>(
      future: initFuture,
      builder: (context, AsyncSnapshot<bool> intiShot) {
        if(intiShot.hasData && intiShot.data){
          return StreamBuilder<bool>(
            stream: bloc.messReload,
            builder: (context, reloadShot){
              if(reloadShot.hasData && reloadShot.data){
                print('RELOAD!!!');
                updateMessages(context);
                bloc.reloadMess(false);
              }
              return StreamBuilder<bool>(
                stream: bloc.messUpdate,
                builder: (context, dataShot) {
                  if(dataShot.hasData && dataShot.data){
                    if(listItems.length > 0){
                      return ListView(
                        padding: EdgeInsets.only(top: 0),
                        children: listItems,
                      );
                    } else {
                      return Center(
                        child: Text(lang.noMessages, style: styles.noContentStyle)
                      );
                    }
                  }else {
                    return Center(
                      child: Container(
                        width: 48, height: 48,
                        padding: EdgeInsets.all(5),
                        child: CircularProgressIndicator(
                          strokeWidth: 3.5,
                          valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                        ),
                      )
                    );
                  }
                }
              );
            }
          );
        } else{
          return Center(
            child: Container(
              width: 48, height: 48,
              padding: EdgeInsets.all(5),
              child: CircularProgressIndicator(
                strokeWidth: 3.5,
                valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
              ),
            )
          );
        }
      }
    );
  }
}