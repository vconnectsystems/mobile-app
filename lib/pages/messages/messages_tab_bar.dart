import 'package:flutter/material.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';

class MessagesTabBar extends StatelessWidget {

  final tabBarOuterMargin = 20.0;
  final tabBarMargin = 30.0;

  MessagesTabBar({Key key}) : super (key : key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);
    final GlobalState _store = GlobalState.instance;
    final MyStyles styles = new MyStyles();

    double screenWidth = MediaQuery.of(context).size.width;

    double _getLeft(int buttonIndex){
      double left;
      double scrWidth = screenWidth;// > 480.0 ? 480.0 : screenWidth;
      if(buttonIndex == 0){
        left = tabBarOuterMargin+tabBarMargin;
      }else{
        double lineWidth = scrWidth-(tabBarOuterMargin*2);
        double halfLine = lineWidth/2;
        left = tabBarOuterMargin+halfLine+tabBarMargin;
      }
      return left;
    }
    double _getRight(int buttonIndex){
      double right;
      double scrWidth = screenWidth;// > 480.0 ? 480.0 : screenWidth;
      if(buttonIndex == 0){
        double lineWidth = scrWidth-(tabBarOuterMargin*2);
        double halfLine = lineWidth/2;
        right = tabBarOuterMargin+halfLine+tabBarMargin;
      }else{
        right = tabBarOuterMargin+tabBarMargin;
      }
      return right;
    }
    return Column(
      children: <Widget>[
        SizedBox(height: 10),
        Stack(
          children: <Widget>[
            Positioned(
              top: -1,
              left: 16,
              child: StreamBuilder<int>(
                stream: _bloc.unreadUpdate,
                builder: (context, snapshot) {
                  int count = (snapshot.hasData ? snapshot.data : _store.unreadMess);
                  if(count > 0){
                    return Chip(
                      backgroundColor: Colors.red,
                      padding: EdgeInsets.all(0),
                      labelPadding: EdgeInsets.only(left: 6, right: 6, top: -4.0, bottom: -4.0),
                      label: Text(
                          count.toString(),
                          style: styles.badgeStyle
                      ),
                    );
                  } else {
                    return SizedBox(width: 1, height: 1);
                  }
                }
              )
            ),
            Row(
              children: <Widget>[
                SizedBox(width: 20),
                Expanded(
                  child: MaterialButton(
                    highlightColor: Colors.white.withOpacity(0),
                    splashColor: Colors.white.withOpacity(.2),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    height: 44,
                    child: StreamBuilder<int>(
                        stream: _bloc.messSubPage,
                        builder: (context, snapshot) {
                          return Text("Dato",
                              style: TextStyle(
                                  color: Color(0xFF1A3B62),
                                  fontFamily: snapshot.hasData ? (snapshot.data == 0 ? "Neutra2Demi" : "Neutra2Book") : "Neutra2Demi",
                                  fontWeight: snapshot.hasData ? (snapshot.data == 0 ? FontWeight.w700 : FontWeight.w300) : FontWeight.w700,
                                  fontSize: 20.0
                              )
                          );
                        }
                    ),
                    onPressed: (){
                      _bloc.changeMessSubPage(0);
                    },
                  ),
                ),
                Expanded(
                    child: MaterialButton(
                      highlightColor: Colors.white.withOpacity(0),
                      splashColor: Colors.white.withOpacity(.2),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      height: 44,
                      child: StreamBuilder<int>(
                          stream: _bloc.messSubPage,
                          builder: (context, snapshot) {
                            return Text("Havne",
                                style: TextStyle(
                                    color: Color(0xFF1A3B62),
                                    fontFamily: snapshot.hasData ? (snapshot.data == 1 ? "Neutra2Demi" : "Neutra2Book") : "Neutra2Book",
                                    fontWeight: snapshot.hasData ? (snapshot.data == 1 ? FontWeight.w700 : FontWeight.w300) : FontWeight.w300,
                                    fontSize: 20.0
                                )
                            );
                          }
                      ),
                      onPressed: (){
                        _bloc.changeMessSubPage(1);
                      },
                    )
                ),
                SizedBox(width: 20),
              ],
            ),
          ],
        ),
        SizedBox(height: 4),
        Row(
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    child: Container(
                      constraints: BoxConstraints.expand(height: 1),
                      margin: EdgeInsets.only(top: 1, bottom: 1),
                      decoration: BoxDecoration(
                        color: Color(0xFF1A3B62).withOpacity(.35)
                      ),
                    ),
                  ),
                  StreamBuilder(
                    stream: _bloc.messSubPage,
                    builder: (context, snapshot) {
                      return AnimatedPositioned(
                        curve: Curves.easeOut,
                        duration: Duration(milliseconds: 300),
                        left: _getLeft(snapshot.hasData ? snapshot.data : 0),
                        //_getRedLeft(),
                        right: _getRight(snapshot.hasData ? snapshot.data : 0),
                        //_getRedRight(),
                        top: 0,
                        bottom: 0,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            color: Color(0xFF1A3B62),
                            borderRadius: BorderRadius.all(
                              Radius.circular(1)
                            )
                          )
                        ),
                      );
                    }
                  ),
                ]
              )
            ),
          ],
        ),
      ],
    );
  }
}