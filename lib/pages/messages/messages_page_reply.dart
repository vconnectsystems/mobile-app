import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:core';
import '../../api/api.dart';
import '../../api/message.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/toast.dart';
import '../common/icon_font_icons.dart';
import '../common/progress_dialog.dart';

class MessagesReply extends StatelessWidget {

  final Message messObj;
  final String ttl;
  final String subTtl;
  final String body;

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();
  final double bigSpace = 26.0;
  final double space = 10.0;
  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();

  MessagesReply({Key key, @required this.messObj, @required this.ttl, @required this.subTtl, @required this.body}) : super (key : key);

  // void _showToast(String mess, BuildContext context, {bool green = false}){
  //   Toast.show(
  //       mess,
  //       context,
  //       backgroundColor: green ? Colors.green : Colors.red,
  //       duration: styles.toastDuration,
  //       gravity: styles.toastGravity
  //   );
  // }

  String getHarbourMaster(int harbourId){
    int harbourInd;
    for(int i = 0; i<_store.harbours.length; i++){
      if(_store.harbours[i].id == harbourId){
        harbourInd = i;
        break;
      }
    }
    return _store.harbours[harbourInd].master;
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    //final DateFormat dateFormat = DateFormat('dd MMM yyyy - hh:mm', lang.lang);

    controller.selection = new TextSelection(
      baseOffset: 0,
      extentOffset: 0,
    );

    void showProgress(){
      focusNode.unfocus();
      showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.white70,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation) {
          return ProgressDialog();
        },
      );
    }

    void _showToast(String error, BuildContext context, bool green){
      Toast.show(
        error.toString(), context,
        backgroundColor: green ? Colors.green : Colors.red,
        duration: styles.toastDuration,
        gravity: styles.toastGravity
      );
    }
    void _dataError(dynamic error, BuildContext context){
      if(error.runtimeType == SocketException){
        if(error.message.contains('Failed host lookup')){
          _showToast('Please, check your internet connectivity', context, false);
        } else {
          _showToast(error.message, context, false);
        }
      } else {
        _showToast(error.toString(), context, false);
      }
    }

    void _replySuccess(value, BuildContext context){
      Navigator.pop(context);
      _showToast(lang.messSendSuccess, context, true);
      Navigator.pop(context, 1);
    }

    void replyMessage(BuildContext context){
      showProgress();
      Future result = api.replyMessage(messObj.id, controller.text, _store.apiToken);
      result.then((value) => _replySuccess(value, context)).catchError((error) => _dataError(error, context));
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: styles.bgDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: ttl,
                subTitle: subTtl,
                onPressed: (){
                  focusNode.unfocus();
                  Navigator.pop(context, -1);
                },
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: Container(
                decoration: styles.pageDecoration,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 7),
                    Row (
                      children: <Widget>[
                        SizedBox(width: bigSpace),
                        Expanded(
                          child: Text(
                            messObj.harbourName.toUpperCase(),
                            style: styles.pageLineText
                          ),
                        ),
                        SizedBox(width: space),
                        Container(
                          width: 1,
                          height: 46,
                        ),
                        IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(IconFont.sent2,
                            size: 24,
                            color: styles.commonColor,
                          ),
                          onPressed: (){
                            replyMessage(context);
                          }
                        ),
                        SizedBox(width: bigSpace),
                      ],
                    ),
                    SizedBox(height: 4),
                    Container(
                      height: 1,
                      decoration: styles.lineDecoration,
                    ),
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.only(top: 26, left: 26, right: 26),
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                lang.to,
                                style: styles.messSubTitleText,
                              ),
                              Expanded(
                                child:
                                Container(
                                  child: Text(
                                    getHarbourMaster(messObj.harbourId).toUpperCase(),
                                    style: styles.messTitleText,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                            child: TextField(
                              keyboardType: TextInputType.multiline,
                              textCapitalization: TextCapitalization.sentences,
                              controller: controller,
                              focusNode: focusNode,
                              autofocus: true,
                              decoration: new InputDecoration(
                                focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                    width: 0.0,
                                    color: Color(0x591A3B62)
                                  )
                                ),
                                border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                    width: 0.0,
                                    color: Color(0x591A3B62)
                                  )
                                )
                              ),
                              maxLines: null,
                              style: styles.boxInputText,
                            ),
                          )
                        ],
                      ),
                    )
                  ]
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
