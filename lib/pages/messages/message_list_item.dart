import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../api/message.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../common/icon_font_icons.dart';

class MessageListRow extends StatelessWidget {

  final Message obj;
  final TextEditingController controller = TextEditingController();

  final void Function(Message) onTap;

  MessageListRow({Key key, @required this.obj, @required this.onTap}) : super (key : key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);
    Language lang = Language.of(context);
    final DateFormat dateFormatTime = DateFormat('HH:mm', lang.lang);
    final DateFormat dateFormatDate = DateFormat('dd.MM.yyyy', lang.lang);

    String getTime(DateTime date){
      if(date.isAfter(DateTime.now().subtract(Duration(hours: DateTime.now().hour)))){
        return dateFormatTime.format(date);
      }else{
        return dateFormatDate.format(date);
      }
    }

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: InkWell(
        highlightColor: Colors.white.withOpacity(0),
        splashColor: Colors.white.withOpacity(.3),
        onTap: (){
          onTap(obj);
        },
        child: Column (
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10),
            StreamBuilder<bool>(
              stream: _bloc.messRead,
              builder: (context, snapshot) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 20),
                    Icon(
                      snapshot.hasData ? (snapshot.data ? IconFont.panorama_fish_eye : IconFont.lens) :
                      (obj.read? IconFont.panorama_fish_eye : IconFont.lens),
                      size: 12
                    ),
                    SizedBox(width: 6),
                    Expanded(
                      child:
                      Container(
                      //_store.harbours[harbourInd].name
                        child: Text(obj.harbourName.toUpperCase(),
                          style: TextStyle(
                            color: Color(0xFF1A3B62),
                            fontFamily: snapshot.hasData ? (snapshot.data ? "AvenirBook" : "AvenirHeavy") :
                            (obj.read? "AvenirBook" : "AvenirHeavy"),
                            fontWeight: snapshot.hasData ? (snapshot.data ? FontWeight.w300 : FontWeight.w600) :
                            (obj.read? FontWeight.w300 : FontWeight.w600),
                            fontSize: 18.0
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 56,
                      child: Text(getTime(obj.createdOn),
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          color: Color(0xFF1A3B62),
                          fontFamily: "AvenirBook",
                          fontWeight: FontWeight.w300,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                  ],
                );
              }
            ),
            SizedBox(height: 2),
            Row(
              children: <Widget>[
                SizedBox(width: 38),
                Text(obj.username.toUpperCase(), softWrap: true,
                  //textWidthBasis: TextWidthBasis.parent,
                  style: TextStyle(
                    color: Color(0xFF1A3B62),
                    fontFamily: "AvenirBook",
                    fontWeight: FontWeight.w300,
                    fontSize: 16.0
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 3),
            Row(
              children: <Widget>[
                SizedBox(width: 38),
                Expanded(
                  child: Container(
                    child: Text(obj.content,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Color(0xFF1A3B62),
                        fontFamily: "AvenirBook",
                        fontWeight: FontWeight.w300,
                        fontSize: 16.0
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20),
              ],
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 1,
                color: Color(0xFF1A3B62).withOpacity(.35)
              ),
            ),
          ],
        ),
      ),
    );
  }
}
