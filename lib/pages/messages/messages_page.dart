import 'package:flutter/material.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import 'messages_data.dart';
import 'messages_tab_bar.dart';
import 'messages_page_list.dart';

class MessagesPage extends StatelessWidget {

  final MessagesData data = new MessagesData();

  MessagesPage({Key key}) : super (key : key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);

    return StreamBuilder<bool>(
      stream: _bloc.messHome,
        builder: (context, snapshot) {
          return Stack(
            children: <Widget>[
              Opacity(
                opacity: snapshot.hasData? (snapshot.data? 1 : 0) : 1,
                child: Column(
                  children: <Widget>[
                    MessagesTabBar(),
                    Expanded(
                      child: StreamBuilder<int>(
                        stream: _bloc.messSubPage,
                        builder: (context, snapshot) {
                          return snapshot.hasData ?
                          (snapshot.data == 1 ? MessagesPageList(byDate: false) : MessagesPageList(byDate: true))
                          : MessagesPageList(byDate: true);
                        }
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        }
    );
  }
}
