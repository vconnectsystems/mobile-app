import 'package:flutter/material.dart' show IconData;
import 'package:flutter/material.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import 'package:dockside/pages/map/map_page.dart';
import 'package:dockside/pages/messages/messages_page.dart';
import 'package:dockside/pages/booking/booking_page.dart';
import 'package:dockside/pages/booking/qr_booking_page.dart';
import 'package:dockside/pages/profile/profile_page.dart';
import 'dashboard_item_obj.dart';
import 'pages_enum.dart';

class DashboardData{

  List<DashboardItemObj> _boxes = [];

  final List<int> _ids = [0, 1, 2, 3, 4];
  final List<IconData> _icons = [
    Icons.qr_code_scanner,
    IconFont.map,
    IconFont.mess_1,
    IconFont.clock,
    IconFont.profile_icon,
  ];
  final List<double> _icnSize = [38.0, 40.0, 36.0, 38.0, 38.0];
  final List<Pages> _pageId = [Pages.qrBooking, Pages.map, Pages.messages, Pages.booking, Pages.profile];
  final List<Widget> _pageWidgets = [QrBookingPage(), MapPage(harbourInd: -1), MessagesPage(), BookingPage(), ProfilePage()];

  DashboardData(){
    for(var i = 0; i<_ids.length; i++){
      DashboardItemObj obj = new DashboardItemObj(
        ind: _ids[i],
        pageId: _pageId[i],
        icon: _icons[i],
        iconSize: _icnSize[i],
        label: '',
        title: '',
        subTitle: '',
        pageWidget: _pageWidgets[i],
      );
      obj.ind = _ids[i];
      obj.pageId = _pageId[i];
      obj.icon = _icons[i];
      obj.iconSize = _icnSize[i];
      obj.label = '';
      obj.title = '';
      obj.subTitle = '';
      obj.pageWidget = _pageWidgets[i];
      _boxes.add(obj);
    }
  }
  List<DashboardItemObj> get boxes => _boxes;
}
