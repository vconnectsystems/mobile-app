import 'package:flutter/material.dart';
import 'pages_enum.dart';

class DashboardItemObj {

  int ind;
  Pages pageId;
  IconData icon;
  double iconSize;
  String label;
  String title;
  String subTitle;
  Widget pageWidget;

  DashboardItemObj({
    this.ind,
    this.pageId,
    this.icon,
    this.iconSize,
    this.label,
    this.title,
    this.subTitle,
    this.pageWidget
  });

}