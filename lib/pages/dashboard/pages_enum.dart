enum Pages {
  search,
  booking,
  map,
  messages,
  qrBooking,
  profile,
  settings,
  other
}