import 'dart:io' show SocketException;
import 'dart:async' show Timer;
import 'package:intl/intl.dart' show DateFormat;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show PlatformException;
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:dockside/api/api.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/api/message_listener.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/booking/apple_date_dialog.dart';
import 'package:dockside/pages/common/header.dart';
import 'package:dockside/pages/common/page_obj.dart';
import 'package:dockside/pages/common/focus_killer.dart';
import 'package:dockside/pages/common/toast.dart';
import 'package:dockside/pages/dashboard/dashboard_item_obj.dart';
import 'package:dockside/pages/dashboard/dash_box.dart';
import 'package:dockside/pages/dashboard/dashboard_data.dart';
import 'package:dockside/pages/dashboard/permanent_button.dart';
import 'package:dockside/pages/dashboard/page_wrap.dart';
import 'package:dockside/pages/dashboard/dashboard_transition_route.dart';

class DashboardPage extends StatefulWidget {
  final int initIndex;
  DashboardPage({Key key, this.initIndex}) : super(key: key);
  @override
  _DashboardPage createState() => _DashboardPage();
}

class _DashboardPage extends State<DashboardPage> {

  final DashboardData _dashData = new DashboardData();
  final GlobalState _store = GlobalState.instance;
  final MyStyles _styles = new MyStyles();
  final Api _api = new Api();
  final DateFormat _apiDateFormat = DateFormat('yyyy-MM-dd');
  Timer _timer;
  Bloc _bloc;
  Language _lang;
  DateTime _tempReturnDate;
  Future<bool> _initFuture;

  void _showToast(String error, BuildContext context) => Toast.show(
    error.toString(), context,
    backgroundColor: Colors.red,
    duration: _styles.toastDuration,
    gravity: _styles.toastGravity
  );

  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else _showToast(error.message, context);
    } else _showToast(error.toString(), context);
  }

  void _messCountSuccess(dynamic value){
    if (value > _store.unreadMess) _bloc.reloadMess(true);
    _store.unreadMess = value;
    _bloc.updateUnread(value);
  }
  Future<void> checkMessCount() async {
    await _api.getNewMessagesCount(_store.apiToken).then(_messCountSuccess).catchError((error) => _dataError(error, context));
  }

  void _rentSuccess(String value){
    if(value.contains('success')){
      _store.availableTill = _tempReturnDate;
      _store.occupied = true;
      _bloc.switchIdle(false);
    }
  }

  void selectComeBack() {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleDateDialog(
          ttl: _lang.availableTitle,
          hint: _lang.availableHint,
          minDate: DateTime.now().add(Duration(days: 1)),
          initDate: DateTime.now().add(Duration(days: 1)),
          maxDate: _store.permanentTill,
          onSelect: (DateTime newDate){
            if(newDate != null) {
              _tempReturnDate = newDate;
              String newDateStr = _apiDateFormat.format(newDate);
              Future result = _api.rentBooking(newDateStr, _store.apiToken);
              result.then((value) => _rentSuccess(value)).catchError((error) => _dataError(error, context));
            }
          },
        );
      },
    );
  }

  void _setReturnSuccess(String value){
    if(value.contains('success')){
      _store.availableTill = _tempReturnDate;
      if(_apiDateFormat.format(_tempReturnDate) == _apiDateFormat.format(DateTime.now())) _store.occupied = true;
      _bloc.updateReturn(true);
    }
  }

  void comeBack() {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleDateDialog(
          ttl: _lang.availableTitle,
          hint: _lang.availableHint,
          minDate: DateTime.now().add(Duration(hours: 1)),//.add(Duration(days: 1)),
          initDate: DateTime.now().add(Duration(hours: 1)),//.add(Duration(days: 1)),
          maxDate: _store.permanentTill,
          onSelect: (DateTime newDate){
            if(newDate != null) {
              _tempReturnDate = newDate;
              String newDateStr = _apiDateFormat.format(newDate);
              Future result = _api.setReturnDate(newDateStr, _store.apiToken);
              result.then((value) => _setReturnSuccess(value)).catchError((error) => _dataError(error, context));
            }
          },
        );
      },
    );
  }

//  void switchClicked(){
//    _bloc.switchIdle(!isIdle);
//    isIdle = !isIdle;
//    if(_store.occupied){
//      selectComeBack();
//    }else{
//      Future result = api.setOccupied(_store.apiToken);
//      result.then((value) => _setOccupiedSuccess(value)).catchError((error) => _dataError(error));
//    }
//  }

  Future<void> _scanBarcode() async {
    String barcodeScanRes;
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#1A3B62', _lang.cancel, false, ScanMode.QR);
      if(barcodeScanRes.contains('DS ')){
        _store.bookingDsCode = barcodeScanRes;
        DashboardItemObj obj = _dashData.boxes[0];
        _bloc.changePage(obj.ind);
        _bloc.changePageLoaded(false);
        PageObj pageObj = new PageObj(
          title: _lang.dashTitles[obj.ind],
          subTitle: _lang.dashSubTitles[obj.ind],
          pageId: obj.pageId,
          pageWidget: obj.pageWidget
        );
        Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
      }
    } on PlatformException {
      print('Failed to get platform version.');
    }
    if (!mounted) return;
  }

  @override
  void initState() {
    super.initState();
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _bloc = BlocProvider.of(context);
      _lang = Language.of(context);
      if(widget.initIndex != null){
        Future<void>.delayed(Duration(milliseconds: 200),() async {
          DashboardItemObj obj = _dashData.boxes[widget.initIndex];
          _bloc.changePage(obj.ind);
          _bloc.changePageLoaded(false);
          PageObj pageObj = new PageObj(
            title: _lang.dashTitles[obj.ind],
            subTitle: _lang.dashSubTitles[obj.ind],
            pageId: obj.pageId,
            pageWidget: obj.pageWidget,
            initIndex: 1
          );
          Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
        });
      } else if (_store.showDeviceToken) Provider.of<MessageListener>(context, listen: false).showDeviceToken();
      if (!_store.guest) _timer = Timer.periodic(Duration(seconds: 5), (timer) => checkMessCount());
      return true;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context){
    double _bottomPadding = MediaQuery.of(context).padding.bottom;
    return FocusKiller(
      child: Scaffold(
        body: DecoratedBox(
          decoration: _styles.bgDecoration,
          child: FutureBuilder<bool>(
            future: _initFuture,
            builder: (BuildContext context, AsyncSnapshot initShot) {
              if(initShot.hasData && initShot.data){
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Hero(
                      tag: 'header',
                      child: Material(
                        color: Colors.transparent,
                        type: MaterialType.transparency,
                        child: Header(
                          xVis: false,
                          mainTitle: _lang.welcome,
                          subTitle: _store.guest ? 'GUEST' : _store.profile.name.toUpperCase(),
                          onPressed: (){
                            if (Navigator.of(context).canPop()) Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 26.0, right: 26.0, bottom: (_bottomPadding+30.0)),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  DashBox(
                                    obj: _dashData.boxes[0],
                                    flexFactor: _store.permanent ? 9 : 6,
                                    enabled: true,
                                    onTap: _scanBarcode
                                  ),
                                  SizedBox(height: 12),
                                  DashBox(
                                    obj: _dashData.boxes[1],
                                    flexFactor: 9,
                                    enabled: true,
                                  ),
                                  SizedBox(height: 12),
                                  DashBox(
                                    obj: _dashData.boxes[2],
                                    flexFactor: _store.permanent ? 6 : 9,
                                    enabled: !_store.guest,
                                  )
                                ],
                              )
                            ),
                            SizedBox(width: 12),
                            Expanded(
                              child: Column(
                                children: <Widget>[
                                  _store.permanent ? PermanentButton(
                                    flexFactor: 6,
                                    onTap: selectComeBack,
                                    onTapReturn: comeBack,
                                  ) : SizedBox.shrink(),
                                  _store.permanent ? SizedBox(height: 12) : SizedBox.shrink(),
                                  DashBox(
                                    obj: _dashData.boxes[3],
                                    flexFactor: _store.permanent ? 9 : 6,
                                    enabled: true,
                                  ),
                                  SizedBox(height: 12),
                                  DashBox(
                                    obj: _dashData.boxes[4],
                                    flexFactor: _store.permanent ? 9 : 6,
                                    enabled: !_store.guest,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ),
                  ]
                );
              } else return Container(decoration: _styles.bgDecoration);
            }
          ),
        )
      ),
    );
  }
}
