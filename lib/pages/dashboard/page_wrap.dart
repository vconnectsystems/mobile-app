import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dockside/pages/common/page_obj.dart';
import 'package:dockside/pages/common/page_wrap_in.dart';
import 'package:dockside/pages/common/focus_killer.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';

class PageWrap extends StatelessWidget {
  final PageObj obj;
  PageWrap({Key key, @required this.obj}) : super(key: key);
  @override
  Widget build(BuildContext context){
    final Bloc _bloc = BlocProvider.of(context);
    return FocusKiller(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: PageWrapIn(
          obj: obj,
          onPressed: (){
            if (Navigator.canPop(context)) {
              _bloc.changePageLoaded(false);
              Navigator.popUntil(context, ModalRoute.withName('/Dashboard'));
            }
          }
        )
      ),
    );
  }
}
