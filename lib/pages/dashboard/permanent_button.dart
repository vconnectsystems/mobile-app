import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';

class PermanentButton extends StatelessWidget {

  final GestureTapCallback onTap;
  final GestureTapCallback onTapReturn;
  final int flexFactor;

  PermanentButton({Key key, @required this.flexFactor, @required this.onTap, @required this.onTapReturn}) : super (key : key);

  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();
  final DateFormat apiDateFormat = DateFormat('yyyy-MM-dd');

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);
    final Language lang = Language.of(context);
    final DateFormat dateFormat = DateFormat('dd.MM.yyyy', lang.lang);

    return Expanded(
      flex: flexFactor,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            new BoxShadow(
              color: Colors.black26,
              offset: new Offset(0.0, 0.0),
              blurRadius: 12.0,
            )
          ],
        ),
        child:  Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              StreamBuilder<bool>(
                stream: _bloc.idleSwitch,
                builder: (context, snapshot) {
                  return StreamBuilder<bool>(
                    stream: _bloc.returnUpdate,
                    builder: (context, snap) {
                      bool isGreen = false;
                      if(_store.permanent){
                        if(_store.availableTill != null){
                          DateTime tillDate = new DateTime(_store.availableTill.year, _store.availableTill.month, _store.availableTill.day);
                          DateTime today = new DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
                          isGreen = !(tillDate.isBefore(today) || tillDate.isAtSameMomentAs(today));
                        }
                      }
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: Center(
                              child: InkWell(
                                child: Stack(
                                  children: <Widget>[
                                    AnimatedContainer(
                                      duration: Duration(milliseconds: 300),
                                      width: 68,
                                      height: 36,
                                      decoration: BoxDecoration(
                                        color: isGreen ? styles.permanentGreen : styles.permanentRed,
                                        border: Border.all(color: styles.commonColor, width: 3),
                                        borderRadius: BorderRadius.all(Radius.circular(20)),
                                      ),
                                    ),
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 300),
                                      curve: Curves.easeOut,
                                      right: isGreen ? 4 : 36,
                                      top: 4,
                                      child: Container(
                                        width: 28,
                                        height: 28,
                                        decoration: BoxDecoration(
                                          color: styles.commonColor,
                                          borderRadius: BorderRadius.all(Radius.circular(17)),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                onTap: isGreen ? onTapReturn : onTap,
                              ),
                            ),
                          ),
                          SizedBox(height: 12),
                          Text(
                            isGreen ? lang.available.toUpperCase() : lang.occupied.toUpperCase(),
                            style: styles.dashBoxStyle.copyWith(color: styles.commonColor)
                          ),
                          isGreen ? SizedBox(height: 2) : SizedBox(height: 0),
                          isGreen ? Text(
                            dateFormat.format(_store.availableTill),
                            style: styles.dashBoxStyle.copyWith(
                              color: isGreen ? styles.commonColor.withOpacity(0.5) : styles.commonColor
                            )
                          ) : SizedBox(height: 0),
                        ]
                      );
                    }
                  );
                }
              )
            ]
          )
        ),
      ),
    );
  }
}
