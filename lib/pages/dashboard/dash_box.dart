import 'package:flutter/material.dart';
import 'dashboard_item_obj.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/page_obj.dart';
import 'dashboard_transition_route.dart';
import 'page_wrap.dart';

class DashBox extends StatelessWidget {

  final DashboardItemObj obj;
  final int flexFactor;
  final bool enabled;
  final GestureTapCallback onTap;

  DashBox({Key key,
    @required this.obj,
    @required this.flexFactor,
    @required this.enabled,
    this.onTap
  }): super(key:key);

  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);

    return Expanded(
      flex: flexFactor,
      child: Hero(
        tag: obj.pageId,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white70,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [BoxShadow(color: Colors.black26, offset: new Offset(0, 0), blurRadius: 12)],
          ),
          child:  Material(
            color: Colors.transparent,
            type: MaterialType.transparency,
            child: InkWell(
              onTap: enabled ? (onTap == null ? (){
                _bloc.changePage(obj.ind);
                _bloc.changePageLoaded(false);
                PageObj pageObj = new PageObj(
                  title: _lang.dashTitles[obj.ind],
                  subTitle: _lang.dashSubTitles[obj.ind],
                  pageId: obj.pageId,
                  pageWidget: obj.pageWidget
                );
                Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
              } : onTap) : null,
              borderRadius: BorderRadius.circular(10),
              splashColor: Colors.white,
              child: Stack(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(obj.icon,
                            size: obj.iconSize == null ? 46 : obj.iconSize,
                            color: enabled ? styles.commonColor : styles.commonColor.withOpacity(0.5)
                          ),
                          SizedBox(height: 12),
                          Text(
                            _lang.dashLabels[obj.ind],
                            style: enabled ? styles.dashBoxStyle : styles.dashBoxStyle.copyWith(color: styles.commonColor.withOpacity(0.5))
                          ),
                        ]
                      )
                    ]
                  ),
                  Positioned(
                    top: 2, right: 12,
                    child: obj.ind == 2 ? StreamBuilder<int>(
                      stream: _bloc.unreadUpdate,
                      builder: (context, snapshot) {
                        int count = (snapshot.hasData ? snapshot.data : _store.unreadMess);
                        if(count != null && count > 0){
                          return Chip(
                            backgroundColor: Colors.red,
                            padding: EdgeInsets.all(0),
                            labelPadding: EdgeInsets.only(left: 6, right: 6, top: -4.0, bottom: -4.0),
                            label: Text(count.toString(), style: styles.badgeStyle),
                          );
                        } else return SizedBox(width: 1, height: 1);
                      }
                    ) : SizedBox(width: 1, height: 1),
                  )
                ],
              )
            )
          ),
        )
      ),
    );
  }
}
