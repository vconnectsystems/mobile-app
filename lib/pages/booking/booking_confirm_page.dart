import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'dart:core';
import 'dart:collection';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../api/spot.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/toast.dart';
import '../common/icon_font_icons.dart';
//import '../common/progress_dialog.dart';
import 'booking_payment_new.dart';
import 'booking_admin.dart';

class BookingConfirmPage extends StatelessWidget {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();
  final double bigSpace = 26.0;
  final double space = 10.0;
  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();
  final DateFormat apiDateFormat = DateFormat('yyyy-MM-dd');

  BookingConfirmPage({Key key}) : super (key : key);

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final DateFormat dateFormat = DateFormat('dd.MM.yyyy', lang.lang);
    //final DateFormat dateFormat = DateFormat(lang.lang == 'da'? 'dd/MM/yyyy' : 'dd/MM/yyyy', lang.lang);
    final Bloc _bloc = BlocProvider.of(context);
    final BookingAdmin _admin = new BookingAdmin(context: context);

    controller.selection = new TextSelection(
      baseOffset: 0,
      extentOffset: 0,
    );

//    void showProgress(){
//      focusNode.unfocus();
//      showGeneralDialog(
//        context: context,
//        barrierDismissible: false,
//        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//        barrierColor: Colors.white70,
//        transitionDuration: const Duration(milliseconds: 200),
//        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation) {
//          return ProgressDialog();
//        },
//      );
//    }

    void _dataError(dynamic error, BuildContext context){
      Navigator.of(context).pop();
      if(error.runtimeType == SocketException){
        SocketException _err = error;
        if(_err.message.contains('Failed host lookup')){
          _showToast('Please, check your internet connectivity', context);
        } else {
          _showToast(_err.message, context);
        }
      } else {
        _showToast(error.toString(), context);
      }
    }
    void _paymentLinkSuccess(){
      if(_store.bookingPaymentLink != ''){
        Navigator.of(context).pop();
        Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage()));
      }
    }
    void _bookingIdSuccess(value, BuildContext context){
      _store.bookingId = value;
      Future result = api.getPaymentLink(value, _store.apiToken);
      result.then((value) => _paymentLinkSuccess()).catchError((error) => _dataError(error, context));
    }

    void _getBookingId(LinkedHashMap<String, dynamic> loadedSpots, BuildContext context){
      _store.serverSpots = loadedSpots;
      Spot sp = _admin.getSpotFromKey(_store.bookingSpot.key);
      if(!sp.booked) {
        Future result = api.addBooking(
          apiDateFormat.format(_store.bookingCheckIn),
          apiDateFormat.format(_store.bookingCheckOut),
          _store.bookingSpot.id,
          _store.harbours[_store.bookingHarbourIndex].id,
          //controller.text,
          _store.apiToken
        );
        result.then((value) => _bookingIdSuccess(value, context)).catchError((
            error) => _dataError(error, context));
      } else {
        _bloc.showSpotsPopup(4);
      }
    }

    void next(BuildContext context) {
      _bloc.setLoading(true);
      Future result = api.getSpots(
          _store.harbours[_store.bookingHarbourIndex].id,
          apiDateFormat.format(_store.bookingCheckIn),
          apiDateFormat.format(_store.bookingCheckOut),
          _store.apiToken
      );
      result.then((value) => _getBookingId(value, context)).catchError((error) => _dataError(error, context));
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        decoration: styles.bgDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: lang.confirm,
                subTitle: lang.booking.toUpperCase(),
                onPressed: (){
                  focusNode.unfocus();
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: Container(
                decoration: styles.pageDecoration,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.only(top: 24, left: 26, right: 26),
                        children: <Widget>[
                          Row (
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '${lang.booking.toUpperCase()} ${lang.details.toUpperCase()}',
                                  style: styles.pageInTitleText
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 12),
                          Row(
                            children: <Widget>[
                              Text(
                                '${lang.harbour}: ',
                                style: styles.pageText,
                              ),
                              Text(
                                _store.harbours[_store.bookingHarbourIndex].name,
                                style: styles.pageText,
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Row(
                            children: <Widget>[
                              Text(
                                '${lang.spot}: #',
                                style: styles.pageText,
                              ),
                              Text(
                                _store.bookingSpot.key,
                                style: styles.pageText,
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Row(
                            children: <Widget>[
                              Text(
                                lang.from,
                                style: styles.pageText,
                              ),
                              Text(
                                dateFormat.format(_store.bookingCheckIn),
                                style: styles.pageText,
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Row(
                            children: <Widget>[
                              Text(
                                lang.to,
                                style: styles.pageText,
                              ),
                              Text(
                                dateFormat.format(_store.bookingCheckOut),
                                style: styles.pageText,
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Row(
                            children: <Widget>[
                              Text(
                                lang.totalPrice,
                                style: styles.pageText,
                              ),
                              Text(
                                _store.bookingTotalPrice,
                                style: styles.pageText,
                              ),
                            ],
                          ),
                          SizedBox(height: 4),
                          Padding(
                            padding: const EdgeInsets.only(left: 0, right: 0, top: 16, bottom: 16),
                            child: TextField(
                              keyboardType: TextInputType.multiline,
                              textCapitalization: TextCapitalization.sentences,
                              controller: controller,
                              focusNode: focusNode,
                              autofocus: false,
                              decoration: new InputDecoration(
                                hintText: 'Tilføj note...',
                                hintStyle: styles.formInputHintStyle.copyWith(fontSize: 18),
                                contentPadding: EdgeInsets.only(left: 6, right: 6, top: 6, bottom: 6),
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(7.0),
                                  borderSide: new BorderSide(
                                    width: 0.0,
                                    color: Color(0x901A3B62)
                                  )
                                ),
                                focusedBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(7.0),
                                  borderSide: new BorderSide(
                                    width: 0.0,
                                    color: Color(0x901A3B62)
                                  )
                                ),
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(7.0),
                                  borderSide: new BorderSide(
                                    width: 0.0,
                                    color: Color(0x901A3B62)
                                  )
                                )
                              ),
                              maxLines: 4,
                              style: styles.boxInputText,
                            ),
                          ),
//                          SizedBox(height: 4),
//                          SelectableText(
//                            lang.cancelPolicyBody,
//                            style: styles.popupHintText,
//                            textAlign: TextAlign.left,
//                            maxLines: null,
//                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        StreamBuilder(
                          stream: _bloc.loading,
                          builder: (context, snapshot){
                            if(snapshot.hasData && snapshot.data){
                              return Container(
                                width: 48, height: 48,
                                padding: EdgeInsets.all(5),
                                child: CircularProgressIndicator(
                                  strokeWidth: 3.5,
                                  valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                                ),
                              );
                            } else {
                              return IconButton(
                                padding: EdgeInsets.all(0),
                                icon: Icon(IconFont.arrow_icon,
                                  size: 46,
                                  color: styles.commonColor,
                                ),
                                onPressed: (){
                                  next(context);
                                }
                              );
                            }
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 26),
                  ]
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
