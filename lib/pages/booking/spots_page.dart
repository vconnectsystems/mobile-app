// import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
// import 'dart:collection';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../api/spot.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/progress_dialog.dart';
import '../common/toast.dart';
import 'zoom_widget.dart';
import 'selection_dialog.dart';
//import 'booking_payment_page.dart';
import 'booking_payment_new.dart';
import 'available_dialog.dart';
import 'only_today_dialog.dart';
import 'green_dialog.dart';
import 'booking_admin.dart';

class SpotsPage extends StatefulWidget {

  final String svg;

  SpotsPage({Key key, @required this.svg}) : super(key: key);

  @override
  _SpotsPage createState() => _SpotsPage();

}

class _SpotsPage extends State<SpotsPage> {

  final Api _api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles _styles = new MyStyles();
  // final DateFormat _apiDateFormat = DateFormat('yyyy-MM-dd');
  double _stageW;
  double _stageH;
  // int _mapW;
  // int _mapH;
  Bloc _bloc;
  Future<List<Widget>> _stackFuture;
  Language _lang;
  DateFormat _dateFormat;
  // Init Popup
  String _initMessage;
  // Spot Popup
  String _spotPopTtl;
  String _spotPopMess;
  double _spotPopPrice;
  int _clickableButtonsCount = 0;
  BookingAdmin _admin;

  void _showToast(String mess, BuildContext context, {bool green = false}) => Toast.show(
    mess, context,
    backgroundColor: green ? Colors.green : Colors.red,
    duration: _styles.toastDuration,
    gravity: _styles.toastGravity
  );

  void _showInitMessage(){
    if(_clickableButtonsCount == 0){
      _initMessage = _lang.noAvailable;
    } else {
      if(_store.bookingHarbourType == 'light') {
        _initMessage = _lang.spotsForLight;
      } else {
        _initMessage = '${_lang.spotsForPeriod1}$_clickableButtonsCount${_lang.spotsForPeriod2}:\n'
          '${_dateFormat.format(_store.bookingCheckIn)} - ${_dateFormat.format(_store.bookingCheckOut)}';
      }
    }
    _bloc.showSpotsPopup(1);
  }

  // void _dataError(dynamic error, BuildContext context){
  //   Navigator.of(context).pop();
  //   if(error.runtimeType == SocketException){
  //     SocketException _err = error;
  //     if(_err.message.contains('Failed host lookup')){
  //       _showToast('Please, check your internet connectivity', context);
  //     } else _showToast(_err.message, context);
  //   } else _showToast(error.toString(), context);
  // }

  // void _paymentLinkSuccess(value, BuildContext context){
  //   print('_paymentLinkSuccess...');
  //   String paymentLink = value['payment_link'];
  //   Navigator.of(context).pop();
  //   Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage(paymentLink: paymentLink)));
  // }
  //
  // void _bookingIdSuccess(){
  //   print('_bookingIdSuccess...');
  //   Future result = _api.getPaymentLink(_store.bookingId, _store.apiToken);
  //   result.then((linkValue) => _paymentLinkSuccess(linkValue, context)).catchError((error) => _dataError(error, context));
  // }

  // void _getBookingId(){
  //   print('_getBookingId...');
  //   Spot sp = _admin.getSpotFromKey(_store.bookingSpot.key);
  //   if(!sp.booked) {
  //     Future result;
  //     if(_store.bookingShort){
  //       result = _api.addShortBooking(
  //         _store.bookingSpot.id,
  //         _store.harbours[_store.bookingHarbourIndex].id,
  //         _store.apiToken
  //       );
  //     } else {
  //       result = _api.addBooking(
  //         _apiDateFormat.format(_store.bookingCheckIn),
  //         _apiDateFormat.format(_store.bookingCheckOut),
  //         _store.bookingSpot.id,
  //         _store.harbours[_store.bookingHarbourIndex].id,
  //         _store.apiToken
  //       );
  //     }
  //     result.then((value) => _bookingIdSuccess()).catchError((error) => _dataError(error, context));
  //   } else {
  //     _bloc.showSpotsPopup(4);
  //   }
  // }
  Future<void> _doubleCheckSpot() async {
    _bloc.showSpotsPopup(3);
    await _admin.getSpots();
    Spot sp = _admin.getSpotFromKey(_store.bookingSpot.key);
    if(!sp.booked) {
      await _admin.getBookingId();
      await _admin.getPaymentLink();
      // Navigator.of(context).pop();
      _bloc.showSpotsPopup(0);
      if(_store.bookingPaymentLink != ''){
        Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage()));
      } else _showToast('Payment failed!', context);
    } else _bloc.showSpotsPopup(4);
    // Future result = _api.getSpots(
    //   _store.harbours[_store.bookingHarbourIndex].id,
    //   _apiDateFormat.format(_store.bookingCheckIn),
    //   _apiDateFormat.format(_store.bookingCheckOut),
    //   _store.apiToken
    // );
    // result.then((value) => _getBookingId()).catchError((error) => _dataError(error, context));
  }

  void _btnClick(int btnIndex, String btnCode, BuildContext context){
    _store.bookingSpot = _admin.getSpotFromKey(btnCode);
    _bloc.clickSpot(btnCode);
    _spotPopTtl = _store.bookingSpot.key;
    _spotPopMess = '${_lang.maxSize}${_store.bookingSpot.boatLength} x ${_store.bookingSpot.boatWidth} m';
    _spotPopPrice = _store.bookingSpot.price;//double.parse((sp.price).toString()) * days;
    _bloc.showSpotsPopup(_store.bookingHarbourIndex == 0 ? 6 : 2);
  }

  void _makeACall(String phoneNum) async{
    if(await UrlLauncher.canLaunch(phoneNum)){
      await UrlLauncher.launch(phoneNum);
    } else {
      throw _lang.callError;
    }
  }

  @override
  void initState() {

    super.initState();

    _stackFuture = Future<List<Widget>>.delayed(Duration.zero,() {
      _lang = Language.of(context);
      _admin = new BookingAdmin(context: context);
      _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);
      _bloc = BlocProvider.of(context);
      _stageW = MediaQuery.of(context).size.width;
      _stageH = MediaQuery.of(context).size.height;

      _bloc.showSpotsPopup(0);

      List<Widget> _stack = [];

      _stack.add(
        SvgPicture.network(widget.svg, placeholderBuilder: (BuildContext context) =>
          Container(
            child: Center(
              child: Text(
                _lang.loading,
                style: TextStyle(
                  fontFamily: "AvenirBook",
                  fontWeight: FontWeight.w300,
                  color: _styles.commonColor,
                  fontSize: 140.0,
                ),
              )
            )
          )
        )
      );

      for(int p = 0; p<_store.dSpots.length; p++){
        StreamBuilder<String> streamBuilder = new StreamBuilder<String>(
          stream: _bloc.spotClick,
          builder: (context, snapshot) {
            String svg = _store.dSpots[p].svg;
            if(snapshot.hasData){
              if(snapshot.data == _store.dSpots[p].key){
                if(_store.dSpots[p].multi && !_store.bookingIsForToday){
                  //
                } else{
                  svg = svg.replaceFirst('rgb(0,150,64)', 'rgb(255,153,0)');
                  svg = svg.replaceFirst('rgb(0,0,255)', 'rgb(255,153,1)');
                  //rgb(3,115,252)
                }
              } else if(snapshot.data == '-1'){
                svg = svg.replaceFirst('rgb(255,153,0)', 'rgb(0,150,64)');
                svg = svg.replaceFirst('rgb(255,153,1)', 'rgb(0,0,255)');
                //rgb(3,115,252)
              }
            }
            return SvgPicture.string(svg);
          }
        );
        _stack.add(streamBuilder);
      }

      for(int i = 0; i<_store.dSpots.length; i++){
        if(_store.dSpots[i].clickable){
          _clickableButtonsCount++;
          double btnSize = 8;
          double btnSizeHalf = 4;
          int multiplier = 6;
          for(int b = 0; b<_store.dSpots[i].x.length; b++ ){
            Widget _btn = Positioned(
              left: (_store.dSpots[i].x[b]-btnSizeHalf) * multiplier,
              top: (_store.dSpots[i].y[b]-btnSizeHalf) * multiplier,
              width: btnSize * multiplier,
              height: btnSize * multiplier,
              child: Material(
                color: Colors.transparent,
                type: MaterialType.circle,
                child: InkWell(
                  splashColor: Colors.white38,
                  borderRadius: BorderRadius.circular(btnSizeHalf * multiplier),
                  onTap: (){
                    if(_store.dSpots[i].multi && !_store.bookingIsForToday){
                      _bloc.showSpotsPopup(5);
                    } else{
                      _btnClick(i, _store.dSpots[i].key, context);
                    }
                  },
                ),
              ),
            );
            _stack.add(_btn);
          }
        }
      }

      return _stack;

    });

    if(!_store.bookingShort){
      Future.delayed(Duration(seconds: 1),() {
        _showInitMessage();
      });
    }

  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Color(0xFFDAEEF3),
      body: Container(
        width: _stageW,
        height: _stageH,
        decoration: BoxDecoration(color: Color(0xFFDAEEF3)),
        child: Material(
          color: Color(0xFFDAEEF3),
          type: MaterialType.canvas,
          child: Stack(
            children: <Widget>[
              FutureBuilder<List<Widget>>(
                future: _stackFuture,
                builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
                  if (snapshot.hasData) {
                    return Container(
                      width: _stageW,
                      height: _stageH,
                      child: Zoom(
                        width: _store.svgW,//6300,//8070
                        height: _store.svgH,//7800,//7746
                        canvasColor: Color(0xFFDAEEF3),
                        backgroundColor: Color(0xFFDAEEF3),
                        zoomSensibility: 20,
                        initZoom: 0,
                        child: Center(child: Stack(fit: StackFit.expand, children: snapshot.data)),
                      ),
                    );
                  } else return Container(child: Center(child: SizedBox(width: 1, height: 1)));
                }
              ),
              Positioned(
                top: 32,
                right: 10,
                child: IconButton(
                  icon: Icon(IconFont.closex, color: _styles.commonColor, size: 16),
                  onPressed: Navigator.of(context).pop,
                ),
              ),
              FutureBuilder<List<Widget>>(
                future: _stackFuture,
                builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
                  if (snapshot.hasData) {
                    return StreamBuilder<int>(
                      stream: _bloc.spotsPopupShow,
                      builder: (context, snapshot) {
                        if(snapshot.hasData && (((snapshot.data > 0) && (snapshot.data < 3)) || snapshot.data == 6)){
                          return GestureDetector(
                            onTap: (){
                              if(snapshot.data == 2) _bloc.clickSpot('-1');
                              _bloc.showSpotsPopup(0);
                            },
                            child: Container(color: Colors.white30),
                          );
                        } else return SizedBox(width: 1, height: 1);
                      }
                    );
                  } else return SizedBox(width: 1, height: 1);
                }
              ),
              FutureBuilder<List<Widget>>(
                future: _stackFuture,
                builder: (BuildContext context, AsyncSnapshot<List<Widget>> initShot) {
                  if (initShot.hasData) {
                    return StreamBuilder<int>(
                      stream: _bloc.spotsPopupShow,
                      builder: (context, snapshot) {
                        if(snapshot.hasData){
                          if(snapshot.data == 1){
                            return AvailableDialog(
                              message: _initMessage,
                              spotsNum: _clickableButtonsCount,
                              onCancel: () => _bloc.showSpotsPopup(0),
                              onCall: () => _makeACall('tel:${_store.harbours[_store.bookingHarbourIndex].phone}'),
                            );
                          } else if(snapshot.data == 2){
                            return SelectionDialog(
                              ttl: _spotPopTtl,
                              message1: _spotPopMess,
                              message2: _lang.availableFor,
                              priceStr: _lang.totalPrice,
                              totalPriceStr: _admin.priceToString(_spotPopPrice),
                              onConfirm: _doubleCheckSpot,
                              onCancel: (){
                                _bloc.clickSpot('-1');
                                _bloc.showSpotsPopup(0);
                              },
                            );
                          } else if(snapshot.data == 3){
                            return ProgressDialog();
                          } else if(snapshot.data == 4){
                            return AvailableDialog(
                              message: _lang.alreadyBooked,
                              spotsNum: -1,
                              onCancel: (){
                                _bloc.clickSpot('-1');
                                _bloc.showSpotsPopup(0);
                              },
                              onCall: (){},
                            );
                          } else if(snapshot.data == 5){
                            return OnlyTodayDialog(
                              message: _lang.onlyToday,
                              onOK: () => _bloc.showSpotsPopup(0),
                            );
                          } else if(snapshot.data == 6){
                            return GreenDialog(
                              onYes: () => _bloc.showSpotsPopup(2),
                              onNo: () => _bloc.showSpotsPopup(0),
                            );
                          } else return SizedBox(width: 1, height: 1);
                        } else return SizedBox(width: 1, height: 1);
                      }
                    );
                  } else return SizedBox(width: 1, height: 1);
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
