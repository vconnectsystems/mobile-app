// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:dockside/api/language.dart';
// import 'package:dockside/bloc/bloc.dart';
// import 'package:dockside/bloc/provider.dart';
// import 'package:dockside/bloc/global_state.dart';
// import 'package:dockside/styles/text_styles.dart';
// import 'package:dockside/pages/common/icon_font_icons.dart';
// import 'package:dockside/pages/common/booking_input_row.dart';
// import 'apple_date_dialog.dart';
//
//
// class BookingDates extends StatefulWidget {
//   @override
//   _BookingDatesState createState() => _BookingDatesState();
// }
//
// class _BookingDatesState extends State<BookingDates> {
//
//   final MyStyles _styles = new MyStyles();
//   final GlobalState _store = GlobalState.instance;
//   static const double _spaceBetween = 4.0;
//   DateFormat _dateFormat;
//   DateFormat _dateFormatLight;
//   int _arrival;
//   bool _isLight = false;
//   Language _lang;
//   Bloc _bloc;
//   Future<bool> _initFuture;
//   DateTime _minDateIn;
//   DateTime _initDateIn;
//   DateTime _minDateOut;
//   DateTime _initDateOut;
//
//   DateTime _toArrivalTime(DateTime date){
//     int hour = date.hour;
//     int diff = hour-_arrival;
//     DateTime newTime = date.subtract(Duration(hours: diff));
//     return newTime;
//   }
//
//   void _updateDates(DateTime newIn, DateTime newOut){
//     _isLight = false;
//     if(_store.bookingHarbourIndex != -1) {
//       _store.bookingHarbourType = _store.harbours[_store.bookingHarbourIndex].subscriptionType;
//       _isLight = (_store.bookingHarbourType == 'light');
//       _arrival = _store.harbours[_store.bookingHarbourIndex].arrival;
//     }
//     if (_isLight){
//       if(DateTime.now().hour < _arrival){
//         _store.bookingCheckIn = _toArrivalTime(DateTime.now().subtract(Duration(days: 1)));
//         _store.bookingCheckOut = _toArrivalTime(DateTime.now());
//       } else {
//         _store.bookingCheckIn = _toArrivalTime(DateTime.now());
//         _store.bookingCheckOut = _toArrivalTime(DateTime.now().add(Duration(days: 1)));
//       }
//     } else {
//       if(newIn != null){
//         _store.bookingCheckIn = newIn;
//       } else {
//         if(_store.bookingCheckIn == null){
//           _store.bookingCheckIn = DateTime.now();
//         }
//       }
//       _minDateIn = DateTime.now();
//       _initDateIn = _store.bookingCheckIn;
//       _minDateOut = _store.bookingCheckIn.add(Duration(days: 1));
//       if(newOut != null){
//         _store.bookingCheckOut = newOut;
//       } else {
//         if(_store.bookingCheckOut == null){
//           _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
//         } else {
//           if(_store.bookingCheckOut.isBefore(_store.bookingCheckIn.add(Duration(days: 1)))){
//             _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
//           }
//         }
//       }
//       _initDateOut = _store.bookingCheckOut;
//     }
//     //
//     _bloc.updateCheckIn(_store.bookingCheckIn);
//     _bloc.updateCheckOut(_store.bookingCheckOut);
//   }
//
//   void _selectCheckIn(){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: true,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return AppleDateDialog(
//           ttl: _lang.selectArrival,
//           hint: _lang.arrivalNote,
//           minDate: _minDateIn,
//           initDate: _initDateIn,
//           onSelect: (DateTime newDate){
//             _updateDates(newDate, null);
//           },
//         );
//       },
//     );
//   }
//
//   void _selectCheckOut() {
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: true,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return AppleDateDialog(
//           ttl: _lang.selectDeparture,
//           hint: _lang.departureNote,
//           minDate: _minDateOut,
//           initDate: _initDateOut,
//           onSelect: (DateTime newDate){
//             if(newDate != null) {
//               _updateDates(null, newDate);
//             }
//           },
//         );
//       },
//     );
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _initFuture = Future<bool>.delayed(Duration.zero,() async {
//       _lang = Language.of(context);
//       _bloc = Provider.of(context);
//       _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);
//       // _dateFormatLight = DateFormat('dd.MM.yyyy,', _lang.lang).add_j();
//       _dateFormatLight = DateFormat('dd.MM.yyyy', _lang.lang);
//       _updateDates(null, null);
//       return true;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<bool>(
//       future: _initFuture,
//       builder: (BuildContext context, AsyncSnapshot<bool> initShot) {
//         if(initShot.hasData && initShot.data){
//           return Column(
//             children: [
//               StreamBuilder<DateTime>(
//                 stream: _bloc.selectCheckIn,
//                 builder: (context, snapshot) {
//                   return Column(
//                     children: <Widget>[
//                       BookingInputRow(
//                         icon: IconFont.checkout,
//                         iconSize: 20,
//                         widget: InkWell(
//                           child: Padding(
//                             padding: const EdgeInsets.only(top: 6, bottom: 5),
//                             child: Text((_store.bookingHarbourType == "light")
//                               ? _dateFormatLight.format(_store.bookingCheckIn)
//                               : _dateFormat.format(_store.bookingCheckIn),
//                               style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                 ? _styles.formInputHintStyle
//                                 : _styles.formInputStyle,
//                               textAlign: TextAlign.left,
//                             ),
//                           ),
//                           onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                             ? null
//                             : _selectCheckIn,
//                         )
//                       ),
//                       SizedBox(width: _spaceBetween),
//                       StreamBuilder<DateTime>(
//                         stream: _bloc.selectCheckOut,
//                         builder: (context, snapOut) {
//                           return BookingInputRow(
//                             icon: IconFont.checkin,
//                             iconSize: 20,
//                             widget: InkWell(
//                               child: Padding(
//                                 padding: const EdgeInsets.only(top: 6, bottom: 5),
//                                 child: Text((_store.bookingHarbourType == "light")
//                                   ? _dateFormatLight.format(_store.bookingCheckOut)
//                                   : _dateFormat.format(_store.bookingCheckOut),
//                                   style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                     ? _styles.formInputHintStyle
//                                     : _styles.formInputStyle,
//                                   textAlign: TextAlign.left,
//                                 ),
//                               ),
//                               onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                 ? null
//                                 : _selectCheckOut
//                             )
//                           );
//                         }
//                       ),
//                     ],
//                   );
//                 }
//               ),
//             ],
//           );
//         } else {
//           return Container();
//         }
//       }
//     );
//   }
// }
