import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart' show CupertinoPageRoute;
import 'dart:io' show SocketException;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:xml2json/xml2json.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/api/api.dart';
import 'package:dockside/api/spot.dart';
import 'package:dockside/api/drawable_spot.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/common/toast.dart';
import 'guest_dialog.dart';
import 'light_dialog.dart';
import 'selection_dialog.dart';
import 'available_dialog.dart';
import 'package:dockside/pages/common/svg_button.dart';
import 'package:dockside/pages/login/register_name_page.dart';

class BookingAdmin {

  final BuildContext context;
  final GlobalState _store = GlobalState.instance;
  final Api _api = new Api();
  final MyStyles _styles = new MyStyles();
  final DateFormat _apiDateFormat = DateFormat('yyyy-MM-dd');
  Language _lang;
  Bloc _bloc;

  BookingAdmin({@required this.context}){
    _lang = Language.of(context);
    _bloc = BlocProvider.of(context);
  }

  void showToast(String mess, {bool green = false}){
    Toast.show(
      mess, context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: _styles.toastDuration,
      gravity: _styles.toastGravity
    );
  }

  void showLightDialog(){
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return LightDialog(onOk: Navigator.of(context).pop);
      },
    );
  }

  void showBookedDialog(){
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AvailableDialog(
          message: _lang.alreadyBooked,
          spotsNum: -1,
          onCancel: (){
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          onCall: (){},
        );
      },
    );
  }

  void showGuestDialog(){
    showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,//Colors.black87,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return GuestDialog(
          onCancel: (){
            Navigator.pop(context);
            Navigator.pop(context);
          },
          onConfirm: (){
            Navigator.of(context).push(CupertinoPageRoute(builder: (context) => RegisterNamePage()));
          },
        );
      },
    );
  }

  void dataError(dynamic error){
    _bloc.setLoading(false);
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        showToast('Please, check your internet connectivity');
      } else showToast(error.message);
    } else showToast(error.toString());
  }

  bool validatePhone(String value){
    return (value != null && value != '' && value.isNotEmpty && value.length >= 9 && (value.contains('+') || value.startsWith('00')));
  }

  DateTime toArrivalTime(DateTime date){
    int diff = date.hour - _store.harbours[_store.bookingHarbourIndex].arrival;
    return date.subtract(Duration(hours: diff));
  }

  bool validateBoatSize(double size) => size != null && size > 0;

  void setHarbourIdFromQrCode(String qr){
    String num = qr.substring(3);
    int numInt = int.parse(num);
    _store.bookingHarbourId = numInt <= 900 ? 1 : 13;
    for(int i = 0; i<_store.harbours.length; i++){
      if(_store.harbours[i].id == _store.bookingHarbourId){
        _store.bookingHarbourIndex = i;
        _store.bookingHarbourType = _store.harbours[i].subscriptionType;
        break;
      }
    }
  }

  Future<void> getBookingId() async {
    if(_store.bookingShort){
      _store.bookingId = await _api.addShortBooking(
        _store.bookingSpot.id,
        _store.bookingHarbourId,
        _store.apiToken
      );
    } else {
      // print('_apiDateFormat.format(_store.bookingCheckIn): ${_apiDateFormat.format(_store.bookingCheckIn)}');
      // print('_apiDateFormat.format(_store.bookingCheckOut): ${_apiDateFormat.format(_store.bookingCheckOut)}');
      // print('_store.bookingSpot.id: ${_store.bookingSpot.id}');
      // print('_store.harbours[_store.bookingHarbourIndex].id: ${_store.harbours[_store.bookingHarbourIndex].id}');
      _store.bookingId = await _api.addBooking(
        _apiDateFormat.format(_store.bookingCheckIn),
        _apiDateFormat.format(_store.bookingCheckOut),
        _store.bookingSpot.id,
        _store.harbours[_store.bookingHarbourIndex].id,
        _store.apiToken
      );
    }
  }

  Future<void> getPaymentLink() async {
    // print('getPaymentLink, _store.bookingId: ${_store.bookingId}');
    // print('getPaymentLink, _store.apiToken: ${_store.apiToken}');
    await _api.getPaymentLink(_store.bookingId, _store.apiToken);
  }

  Future<bool> processQrCode() async {
    await getSpots();
    _bloc.setLoading(false);
    if(_store.bookingSpot.booked) {
      showBookedDialog();
      return false;
    } else {
      bool confirmed = await showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: null,//Colors.black87,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
          return SelectionDialog(
            ttl: _store.bookingSpot.key,
            message1: '${_lang.maxSize}${_store.bookingSpot.boatLength} x ${_store.bookingSpot.boatWidth} m',
            message2: _lang.availableFor,
            priceStr: _lang.totalPrice,
            totalPriceStr: priceToString(_store.bookingSpot.price),
            onConfirm: () => Navigator.of(context).pop(true),
            onCancel: () => Navigator.of(context).pop(false),
          );
        },
      );
      if(confirmed){
        await getBookingId();
        await getPaymentLink();
        return true;
      } else {
        return false;
      }
    }
  }

  String priceToString(double price) {
    String str = price.toStringAsFixed(2);
    List<String> list = str.split('.');
    if (list[1] == '00') str = list[0];
    return str;
  }

  Spot getSpotFromKey(String num) {
    Map<String, dynamic> json = _store.serverSpots[num];
    // if (num == '1' || num == '2' || num == '3' || num == '4' || num == '5' || num == '6') print('json: $json');
    RawSpot rs = RawSpot.fromJson(json);
    Spot sp = new Spot(
      id: rs.id,
      booked: rs.booked,
      price: rs.price,
      boatWidth: rs.boatWidth,
      boatLength: rs.boatLength,
      boatDepth: rs.boatDepth,
      multi: rs.multi,
      key: num
    );
    return sp;
  }

  bool isForToday(){
    String from = _apiDateFormat.format(_store.bookingCheckIn);
    String to = _apiDateFormat.format(_store.bookingCheckOut);
    String today;
    String tomorrow;
    if(_store.bookingHarbourType == 'light' && DateTime.now().hour < _store.harbours[_store.bookingHarbourIndex].arrival){
      today = _apiDateFormat.format(DateTime.now().subtract(Duration(days: 1)));
    } else {
      today = _apiDateFormat.format(DateTime.now());
    }
    if(_store.bookingHarbourType == 'light' && DateTime.now().hour < _store.harbours[_store.bookingHarbourIndex].arrival){
      tomorrow = _apiDateFormat.format(DateTime.now());
    } else {
      tomorrow = _apiDateFormat.format(DateTime.now().add(Duration(days: 1)));
    }
    return (from == today && to == tomorrow);
  }

  Future<void> getSpots() async {
    await _api.getSpots(
      _store.bookingHarbourId,
      _apiDateFormat.format(_store.bookingCheckIn),
      _apiDateFormat.format(_store.bookingCheckOut),
      _store.apiToken
    ).then((bool value){}).catchError((error) => dataError(error));
  }

  Future<List<SvgButton>> _getSvgButtons() async {
    List<SvgButton> list = [];
    String svg = await _api.getSVG('h_${_store.bookingHarbourId}_buttons.svg');
    final Xml2Json _xml2json = new Xml2Json();
    _xml2json.parse(svg);
    String jsonString = _xml2json.toBadgerfish();
    Map<String, dynamic> map = json.decode(jsonString);
    int itemsCount = map['svg']['g']['circle'].length;
    for(int i=0; i<itemsCount; i++){
      SvgButton btn = new SvgButton(
        key: (map['svg']['g']['circle'][i]['@id']).toString(),
        x: double.parse((map['svg']['g']['circle'][i]['@cx']).toString()),
        y: double.parse((map['svg']['g']['circle'][i]['@cy']).toString()),
      );
      list.add(btn);
    }
    return list;
  }

  Future<void> getSvgSpots() async {
    List<SvgButton> buttons = await _getSvgButtons();
    String xmlString = await _api.getSVG('h_${_store.bookingHarbourId}_spots.svg');
    String trimmed = xmlString.trim();
    String trimmed2 = trimmed.replaceAll('>\n', '>');
    final RegExp regex = RegExp(r'>\s*<');
    String trimmed3 = trimmed2.replaceAll(regex, '><');
    String footer = '</svg>';
    String trimmed4 = trimmed3.replaceAll(footer, '');
    String splitter = '<g><';
    List<String> nodes = trimmed4.split(splitter);
    String header = nodes[0];
    Xml2Json xml2json = Xml2Json();
    xml2json.parse(xmlString);
    String jsonString = xml2json.toBadgerfish();
    Map<String, dynamic> map = json.decode(jsonString);
    int itemsCount = map['svg']['g'].length;
    String viewBox = map['svg']['@viewBox'].toString();
    List vbList = viewBox.split(' ');
    double _w = double.parse(vbList[2]);
    double _h = double.parse(vbList[3]);
    _store.svgW = _w*6;
    _store.svgH = _h*6;
    List<DrawableSpot> dSpots = [];
    for(int i=0; i<itemsCount; i++){
      Map<String, dynamic> thisMap = map['svg']['g'][i];
      String key;
      if(thisMap.containsKey('g')){
        key = thisMap['g']['text']['\$'];
      }else {
        key = thisMap['text']['\$'];
      }
      List<double> btnX = [];
      List<double> btnY = [];
      for(int j = 0; j<buttons.length; j++){
        if(buttons[j].key == key){
          btnX.add(buttons[j].x);
          btnY.add(buttons[j].y);
        }
      }
      Spot loadedSpot = getSpotFromKey(key);
      bool clickable = true;
      String newSvg = nodes[i+1];
      bool sizeFit = false;
      if(loadedSpot.boatWidth >= _store.bookingBoatWidth
          && loadedSpot.boatLength >= _store.bookingBoatLength
          && loadedSpot.boatDepth >= _store.bookingBoatDepth){
        sizeFit = true;
      }
      if(loadedSpot.booked || !sizeFit){
        clickable = false;
        newSvg = newSvg.replaceAll('rgb(0,150,64)', 'rgb(190,22,34)');
        newSvg = newSvg.replaceAll('rgb(0,0,255)', 'rgb(190,22,35)');
      } else if(loadedSpot.multi && !_store.bookingIsForToday){
        newSvg = newSvg.replaceAll('rgb(0,0,255)', 'rgb(130,180,230)');
      }
      ///////////////////////
      DrawableSpot drawableSpot = new DrawableSpot(
        key: loadedSpot.key,
        id: loadedSpot.id,
        booked: loadedSpot.booked,
        price: loadedSpot.price,
        boatLength: loadedSpot.boatLength,
        boatWidth: loadedSpot.boatWidth,
        boatDepth: loadedSpot.boatDepth,
        name: key,
        x: btnX,//button.x,
        y: btnY,//button.y,
        clickable: clickable,
        multi: loadedSpot.multi,
        svg: '$header$splitter$newSvg$footer',
      );
      dSpots.add(drawableSpot);
    }
    _store.dSpots = dSpots;
  }

  // Future<void> _validate() async {
  //   if(_store.bookingHarbourIndex != -1 && _store.bookingHarbourIndex != null){
  //     if(_store.bookingCheckIn != null){
  //       if(_store.bookingCheckOut != null){
  //         if(_store.bookingBoatLength != 0.0 && _store.bookingBoatLength != null && _store.bookingBoatLength != 0){
  //           if(_store.bookingBoatWidth != 0.0 && _store.bookingBoatWidth != null && _store.bookingBoatWidth != 0){
  //             if(_store.bookingBoatDepth != 0.0 && _store.bookingBoatDepth != null && _store.bookingBoatDepth != 0){
  //               if(_store.bookingEmail != '' && _store.bookingEmail != null){
  //                 if(_validatePhone(_phoneController.text)){
  //                   _store.bookingPhone = _phoneController.text;
  //                   bool loadingOn = false;
  //                   if(_store.profile.phone == null || _store.profile.phone == '' || _store.profile.phone != _store.bookingPhone){
  //                     _store.profile.phone = _store.bookingPhone;
  //                     _bloc.setLoading(true);
  //                     loadingOn = true;
  //                     await _api.updateProfile(_store.profile, _store.apiToken);
  //                   }
  //                   if(_bNameController.text != '' && _phoneController.text != null){
  //                     if(_store.profile.bName == null || _store.profile.bName == '' || _store.profile.bName != _store.bookingBoatName){
  //                       _store.profile.bName = _store.bookingBoatName;
  //                       if(!loadingOn) _bloc.setLoading(true);
  //                       loadingOn = true;
  //                       await _api.updateProfile(_store.profile, _store.apiToken);
  //                     }
  //                     if(!loadingOn) _bloc.setLoading(true);
  //                     _next(context);
  //                   } else {
  //                     if(loadingOn) _bloc.setLoading(false);
  //                     loadingOn = false;
  //                     _showToast(_lang.bookBoatNameError, context);
  //                   }
  //                 } else {
  //                   _showToast(_lang.bookPhoneError, context);
  //                 }
  //               } else{
  //                 _showToast(_lang.bookEmailError, context);
  //               }
  //             } else{
  //               _showToast(_lang.bookDepthError, context);
  //             }
  //           } else{
  //             _showToast(_lang.bookWidthError, context);
  //           }
  //         } else{
  //           _showToast(_lang.bookLengthError, context);
  //         }
  //       } else{
  //         _showToast(_lang.bookDepartureError, context);
  //       }
  //     } else {
  //       _showToast(_lang.bookArrivalError, context);
  //     }
  //   } else {
  //     _showToast(_lang.bookHarbourError, context);
  //   }
  // }



}