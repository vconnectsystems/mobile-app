import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
// import '../common/icon_font_icons.dart';
import '../common/dialog_button.dart';
// import '../common/dialog_icon_button.dart';

class GreenDialog extends StatelessWidget {

  final GestureTapCallback onYes;
  final GestureTapCallback onNo;

  final double dialogHeight = 320;//224
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;

  final MyStyles styles = new MyStyles();

  GreenDialog({Key key,
    @required this.onYes,
    @required this.onNo
  }): super(key:key);

  List<TextSpan> _getSpans(String ttl, String body){
    List<String> _grList = [];
    String _grWord = '';
    if(body.contains('green')){
      _grWord = 'green';
      _grList = body.split('green');
    } else if(body.contains('grønt')){
      _grWord = 'grønt';
      _grList = body.split('grønt');
    }
    List<TextSpan> _spList = [
      TextSpan(text: '$ttl\n', style: styles.modalTitleText.copyWith(height: 1.2)),
      TextSpan(text: '\n${_grList[0]}', style: styles.modalBiggerText),
      TextSpan(text: _grWord, style: styles.modalBiggerText.copyWith(color: Color.fromRGBO(0, 150, 64, 1))),
      TextSpan(text: _grList[1], style: styles.modalBiggerText)
    ];
    return _spList;
  }

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);

    final List<TextSpan> _spans = _getSpans(_lang.isGreenTtl, _lang.isGreenTxt);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Expanded(
              child: Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: SelectableText.rich(
                    TextSpan(
                      style: styles.popupHintText,
                      children: _spans
                    ),
                    textAlign: TextAlign.left,
                    style: styles.pageTextMedium,
                    maxLines: null,
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                DialogButton(
                  ttl: _lang.no,
                  onPressed: onNo
                ),
                Spacer(),
                DialogButton(
                  ttl: _lang.yes,
                  onPressed: onYes
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}
