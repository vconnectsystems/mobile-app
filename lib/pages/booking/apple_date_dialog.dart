import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';

class AppleDateDialog extends StatelessWidget {

  final String ttl;
  final String hint;
  final DateTime initDate;
  final DateTime minDate;
  final DateTime maxDate;
  final Function(DateTime dateTime) onSelect;

  final double dialogHeight = 432;
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 12.0;

  final MyStyles styles = new MyStyles();

  AppleDateDialog({Key key,
    @required this.ttl,
    @required this.hint,
    @required this.initDate,
    @required this.minDate,
    this.maxDate,
    @required this.onSelect}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    DateTime _selectedDateTime;
    bool _timeChanged = false;

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 16),
              Text(ttl, style: styles.pageLineText, textAlign: TextAlign.center),
              SizedBox(height: numBtnSpace),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              Expanded(
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(
                      dateTimePickerTextStyle: styles.appleDateText,
                    ),
                  ),
                  child: CupertinoDatePicker(
                    backgroundColor: Colors.white,
                    mode: CupertinoDatePickerMode.date,
                    initialDateTime: initDate,
                    minimumDate: new DateTime(minDate.year, minDate.month, minDate.day),
                    maximumDate: maxDate != null ? maxDate : minDate.add(Duration(days: 365)),
                    onDateTimeChanged: (DateTime newDate) {
                      _timeChanged = true;
                      _selectedDateTime = newDate;
                    },
                  ),
                ),
              ),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              SizedBox(height: 12),
              Container(
                height: 80,
                padding: EdgeInsets.only(left:16, right: 16),
                child: Center(
                  child: SelectableText.rich(
                    TextSpan(
                      style: styles.popupHintText,
                      children: <TextSpan>[
                        TextSpan(text: lang.hint, style: styles.popupHintBoldText),
                        TextSpan(text: hint)
                      ]
                    ),
                    textAlign: TextAlign.center,
                    maxLines: null,
                  ),
                )
              ),
              SizedBox(height: 12),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.exti2,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.check22,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          if(_timeChanged){
                            onSelect(_selectedDateTime);
                          } else {
                            onSelect(initDate);
                          }
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}