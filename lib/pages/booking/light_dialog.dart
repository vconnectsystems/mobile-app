import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/dialog_button.dart';

class LightDialog extends StatelessWidget {

  final double dialogHeight = 300;
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;
  final GestureTapCallback onOk;

  final MyStyles styles = new MyStyles();

  LightDialog({Key key,
    @required this.onOk
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Expanded(
              child: Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 8),
                      Text(
                        lang.lightHarbourTtl, style: styles.pageTextMedium,
                      ),
                      SizedBox(height: 16),
                      SelectableText(
                        lang.lightHarbourMess,
                        textAlign: TextAlign.left,
                        style: styles.pageText,
                        maxLines: null,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                Spacer(),
                DialogButton(
                    ttl: lang.ok,
                    onPressed: onOk
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}
