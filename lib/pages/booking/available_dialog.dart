import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/dialog_button.dart';
import '../common/dialog_icon_button.dart';

class AvailableDialog extends StatelessWidget {

  final String message;
  final int spotsNum;

  final double dialogHeight = 260;//224
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;
  final GestureTapCallback onCancel;
  final GestureTapCallback onCall;

  final MyStyles styles = new MyStyles();

  AvailableDialog({Key key,
    @required this.message,
    @required this.spotsNum,
    @required this.onCancel,
    @required this.onCall
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Expanded(
              child: Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: SelectableText(
                    message,
                    textAlign: TextAlign.left,
                    style: styles.pageTextMedium,
                    maxLines: null,
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                spotsNum == 0 ? DialogIconButton(
                  icon: IconFont.phone,
                  onPressed: onCall
                ) : SizedBox(width: 1),
//                Expanded(child: SizedBox(width: 1)),
//                Expanded(child: SizedBox(width: 1)),
                Spacer(),
                DialogButton(
                  ttl: lang.ok,
                  onPressed: onCancel
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}
