import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:dockside/api/api.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/api/campaign.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/common/header.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import 'package:dockside/pages/common/campaign_item.dart';

class BookingCompletePage extends StatelessWidget {

  final MyStyles styles = new MyStyles();
  final double space = 10.0;
  final double bigSpace = 20.0;
  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;

  BookingCompletePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final DateFormat dateFormat = DateFormat('dd.MM.yyyy', lang.lang);

    StringBuffer bodySb1 = new StringBuffer();
    bodySb1.write('${lang.dear}${_store.profile.name} -\n\n');
    bodySb1.write('${lang.thankYou1}\n\n');
    bodySb1.write('${lang.details}\n');
    bodySb1.write('${_store.harbours[_store.bookingHarbourIndex].name}\n');
    bodySb1.write('${lang.spot}#${_store.bookingSpot.key}\n');
    if(!_store.bookingShort){
      bodySb1.write('${lang.from}${dateFormat.format(_store.bookingCheckIn)}\n');
      bodySb1.write('${lang.to}${dateFormat.format(_store.bookingCheckOut)}\n\n');
    }
    bodySb1.write(lang.moreInfo1);
    String bodyStr1 = bodySb1.toString();

    StringBuffer bodySb2 = new StringBuffer();
    bodySb2.write('${lang.moreInfo2}');
//    bodySb2.write(lang.cancelPolicyBody);
    String bodyStr2 = bodySb2.toString();

    TextSpan textSpan1 = TextSpan(text: '${lang.thankYou2}\n', style: styles.textPageTitle);
    TextSpan textSpan2 = TextSpan(text: '${dateFormat.format(DateTime.now())}\n\n', style: styles.textPageSubTitle);
    TextSpan textSpan3 = TextSpan(text: bodyStr1, style: styles.textPageBody);
    TextSpan textSpan4 = TextSpan(text: lang.bookings, style: styles.textPageBodyBold);
    TextSpan textSpan5 = TextSpan(text: bodyStr2, style: styles.textPageBody);

    List<Campaign> _campaigns = [];
    if(_store.bookingHarbourIndex != null && _store.bookingHarbourIndex != -1){
      _campaigns = _store.harbours[_store.bookingHarbourIndex].campaigns;
    }
    bool _camp = _campaigns.length > 0;

    void doneBooking(){
      Navigator.popUntil(context, ModalRoute.withName('/Dashboard'));
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        decoration: styles.bgDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: lang.booking.toUpperCase(),
                subTitle: lang.confirmation.toUpperCase(),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 26, right: 26, top: 26, bottom: 26),
                decoration: styles.pageDecoration,
                child: ListView(
                  padding: EdgeInsets.all(0),
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SelectableText.rich(
                      TextSpan(
                          style: styles.popupHintText,
                          children: <TextSpan>[
                            textSpan1,
                            textSpan2,
                            textSpan3,
                            textSpan4,
                            textSpan5
                          ]
                      ),
                      textAlign: TextAlign.left,
                      maxLines: null,
                    ),
                    _camp ? SizedBox(height: 16) : SizedBox.shrink(),
                    _camp ? CampaignItem(
                      campaigns: _campaigns,
                      w: MediaQuery.of(context).size.width,
                      lines: false
                    ) : SizedBox.shrink(),
                    SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        StreamBuilder(
                          stream: _bloc.loading,
                          builder: (context, snapshot){
                            if(snapshot.hasData && snapshot.data){
                              return Container(
                                width: 48, height: 48,
                                padding: EdgeInsets.all(5),
                                child: CircularProgressIndicator(
                                  strokeWidth: 3.5,
                                  valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                                ),
                              );
                            } else {
                              return IconButton(
                                padding: EdgeInsets.all(0),
                                icon: Icon(IconFont.check22,
                                  size: 46,
                                  color: styles.commonColor,
                                ),
                                onPressed: (){
                                  doneBooking();
                                }
                              );
                            }
                          },
                        ),
                      ],
                    ),
                  ]
                ),
              )
            ),
          ]
        ),
      )
    );
  }
}
