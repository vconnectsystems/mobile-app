import 'package:flutter/material.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../common/dialog_button.dart';
import '../common/dialog_num_button.dart';
import '../common/dialog_num_icon_button.dart';
import '../common/icon_font_icons.dart';

class AppleNumPadDialog extends StatelessWidget {

  final String initSize;
  final String ttl;
  final String type;
  final Stream<String> stream;
  final IconData icon;

  final double dialogHeight = 354;
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 12.0;
  final MyStyles styles = new MyStyles();

  AppleNumPadDialog({Key key,
    @required this.initSize,
    @required this.ttl,
    @required this.type,
    @required this.stream,
    @required this.icon}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);

    String size = initSize;

    void updateValue(String val){
      switch(type){
        case 'length':
          _bloc.updateBoatLength(val);
          break;
        case 'height':
          _bloc.updateBoatWidth(val);
          break;
        case 'depth':
          _bloc.updateBoatDepth(val);
          break;
        default:
          break;
      }
    }
    void numClicked(String val){
      if(size == initSize){
        size = '0';
      }
      if(size == '0'){
        updateValue(val);
      }else{
        updateValue('$size$val');
      }
    }
    void dotClicked(){
      if(!size.contains('.')){
        if(size == initSize){
          size = '0';
        }
        if(size == '0'){
          updateValue('0.');
        }else{
          updateValue('$size.');
        }
      }
    }
    void zeroClicked(){
      if(size == initSize){
        size = '0';
      }
      if(size == '0'){
        updateValue('0');
      }else{
        updateValue('${size}0');
      }
    }
    void delClicked(){
      if(size != '0'){
        if(size.length == 1){
          updateValue('0');
        }else{
          updateValue(size.substring(0, size.length-1));
        }
      }
    }
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-62,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 16),
                  Text(ttl, style: styles.pageLineText),
                  StreamBuilder<Object>(
                    stream: stream,
                    builder: (context, snapshot) {
                      if(snapshot.hasData){
                        size = snapshot.data;
                      }
                      return Text(
                        snapshot.hasData? snapshot.data : size,
                        textAlign: TextAlign.left,
                        style: styles.dialogNumButtonText
                      );
                    }
                  ),
                  Text(
                    ' m',
                    style: styles.pageLineText
                  ),
                ],
              ),
            ),
            SizedBox(height: numBtnSpace),
            Container(
              height: 1,
              decoration: styles.lineDecoration,
            ),
            SizedBox(height: 8),
            Expanded(
              child: Column(
                children: <Widget>[
                  SizedBox(height: numBtnSpace),
                  Row(
                    children: <Widget>[
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '1',
                        onPressed: (){
                          numClicked('1');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '2',
                        onPressed: (){
                          numClicked('2');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '3',
                        onPressed: (){
                          numClicked('3');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                    ],
                  ),
                  SizedBox(height: numBtnSpace),
                  Row(
                    children: <Widget>[
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '4',
                        onPressed: (){
                          numClicked('4');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '5',
                        onPressed: (){
                          numClicked('5');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '6',
                        onPressed: (){
                          numClicked('6');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                    ],
                  ),
                  SizedBox(height: numBtnSpace),
                  Row(
                    children: <Widget>[
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '7',
                        onPressed: (){
                          numClicked('7');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '8',
                        onPressed: (){
                          numClicked('8');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '9',
                        onPressed: (){
                          numClicked('9');
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                    ],
                  ),
                  SizedBox(height: numBtnSpace),
                  Row(
                    children: <Widget>[
                      SizedBox(width: numBtnSpace),
                      DialogNumIconButton(
                        icon: IconFont.dot,
                        onPressed: (){
                          dotClicked();
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumButton(
                        ttl: '0',
                        onPressed: (){
                          zeroClicked();
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                      DialogNumIconButton(
                        icon: Icons.backspace,
                        onPressed: (){
                          delClicked();
                        },
                      ),
                      SizedBox(width: numBtnSpace),
                    ],
                  ),
                  SizedBox(height: numBtnSpace),
                ],
              )
            ),
            SizedBox(height: numBtnSpace),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                DialogButton(
                  ttl: lang.cancel,
                  onPressed: (){
                    updateValue(initSize);
                    Navigator.of(context).pop();
                  },
                ),
                Expanded(child: SizedBox(width: 1)),
                DialogButton(
                  ttl: lang.select,
                  onPressed: (){
                    if(size == '0.0'){
                      size = '0';
                      updateValue('0');
                    }
                    Navigator.of(context).pop();
                  },
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}

