import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/dialog_button.dart';

class OnlyTodayDialog extends StatelessWidget {

  final String message;

  final double dialogHeight = 156;
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;
  final GestureTapCallback onOK;

  final MyStyles styles = new MyStyles();

  OnlyTodayDialog({Key key,
    @required this.message,
    @required this.onOK
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 16),
            Expanded(
              child: Material(
                color: Colors.transparent,
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: SelectableText(
                    message,
                    textAlign: TextAlign.left,
                    style: styles.pageTextMedium,
                    maxLines: null,
                  ),
                ),
              ),
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(width: numBtnSpace),
                Spacer(),
                DialogButton(
                    ttl: lang.ok,
                    onPressed: onOK
                ),
                SizedBox(width: numBtnSpace),
              ],
            ),
            SizedBox(height: numBtnSpace),
          ],
        ),
      ),
    );
  }
}
