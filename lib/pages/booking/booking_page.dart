import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show PlatformException;
import 'package:flutter/cupertino.dart' show CupertinoPageRoute;
import 'package:intl/intl.dart' show DateFormat;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:dockside/api/api.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/api/campaign.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import 'package:dockside/pages/common/booking_input_row.dart';
import 'package:dockside/pages/common/campaign_item.dart';
import 'package:dockside/pages/common/short_switch.dart';
import 'booking_payment_new.dart';
import 'apple_date_dialog.dart';
import 'search_harbour_page.dart';
import 'spots_page.dart';
import 'booking_admin.dart';

class BookingPage extends StatefulWidget {
  BookingPage({Key key}) : super(key: key);
  @override
  _BookingPage createState() => _BookingPage();
}

class _BookingPage extends State<BookingPage> {

  final double _spaceBetween = 4.0;
  static const _spaceBottom = 20.0;
  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final TextEditingController _mailController = TextEditingController();
  final FocusNode _mailFocusNode = FocusNode();
  final TextEditingController _phoneController = TextEditingController();
  final FocusNode _phoneFocusNode = FocusNode();
  final TextEditingController _bNameController = TextEditingController();
  final FocusNode _bNameFocusNode = FocusNode();
  final Api _api = new Api();
  BookingAdmin _admin;
  DateFormat _dateFormat;
  DateFormat _dateFormatLight;
  Language _lang;
  Bloc _bloc;
  DateTime _minDateIn;
  DateTime _initDateIn;
  DateTime _minDateOut;
  DateTime _initDateOut;
  Future<bool> _initFuture;
  List<Campaign> _campaigns = [];

  Future<void> _scanBarcode() async {
    try {
      String scanResult = await FlutterBarcodeScanner.scanBarcode('#1A3B62', _lang.cancel, false, ScanMode.QR);
      if(scanResult.contains('DS ')){
        _store.bookingDsCode = scanResult;
        for(int i = 0; i < _store.spotsList.length; i++){
          if(_store.spotsList[i].serialNumber == _store.bookingDsCode){
            _store.bookingSpot = _store.spotsList[i];
            break;
          }
        }
        _bloc.setLoading(true);
        bool confirmed = await _admin.processQrCode();
        if(confirmed){
          Navigator.of(context).pop();
          Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage()));
        }
      }
    } on PlatformException {
      print('Failed to get platform version.');
    }
    if (!mounted) return;
  }

  Future<void> _next() async {
    _store.bookingIsForToday = _admin.isForToday();
    if(_store.bookingHarbourType == 'super-light'){
      _bloc.setLoading(false);
      _scanBarcode();
    } else {
      await _admin.getSpots();
      await _admin.getSvgSpots();
      _bloc.setLoading(false);
      Navigator.of(context).push(CupertinoPageRoute(builder: (context) => SpotsPage(
        svg: '${_api.mapsFtpUrl}/h_${_store.bookingHarbourId}_map.svg'
      )));
    }
  }

  Future<void> _validate(BuildContext context) async {
    if(_store.bookingHarbourIndex != -1 && _store.bookingHarbourIndex != null){
      if(_store.bookingCheckIn != null){
        if(_store.bookingCheckOut != null){
          if(_admin.validateBoatSize(_store.bookingBoatLength)){
            if(_admin.validateBoatSize(_store.bookingBoatWidth)){
              if(_admin.validateBoatSize(_store.bookingBoatDepth)){
                if(_store.bookingEmail != '' && _store.bookingEmail != null){
                  if(_admin.validatePhone(_store.bookingPhone)){
                    bool loadingOn = false;
                    if(_store.profile.phone == null || _store.profile.phone == '' || _store.profile.phone != _store.bookingPhone){
                      _store.profile.phone = _store.bookingPhone;
                      _bloc.setLoading(true);
                      loadingOn = true;
                      await _api.updateProfile(_store.profile, _store.apiToken);
                    }
                    if(_bNameController.text != '' && _phoneController.text != null){
                      if(_store.profile.bName == null || _store.profile.bName == '' || _store.profile.bName != _store.bookingBoatName){
                        _store.profile.bName = _store.bookingBoatName;
                        if(!loadingOn) _bloc.setLoading(true);
                        loadingOn = true;
                        await _api.updateProfile(_store.profile, _store.apiToken);
                      }
                      if(!loadingOn) _bloc.setLoading(true);
                      _next();
                    } else {
                      if(loadingOn) _bloc.setLoading(false);
                      loadingOn = false;
                      _admin.showToast(_lang.bookBoatNameError);
                    }
                  } else _admin.showToast(_lang.bookPhoneError);
                } else _admin.showToast(_lang.bookEmailError);
              } else _admin.showToast(_lang.bookDepthError);
            } else _admin.showToast(_lang.bookWidthError);
          } else _admin.showToast(_lang.bookLengthError);
        } else _admin.showToast(_lang.bookDepartureError);
      } else _admin.showToast(_lang.bookArrivalError);
    } else _admin.showToast(_lang.bookHarbourError);
  }

  _searchHarbour() async {
    int newIndex = await Navigator.of(context).push(CupertinoPageRoute(builder: (context) => SearchHarbourPage()));
    if(newIndex != null && newIndex != -1){
      _store.bookingHarbourIndex = newIndex;
      _store.bookingHarbourId = _store.harbours[newIndex].id;
      _store.bookingHarbourType = _store.harbours[newIndex].subscriptionType;
      _updateDates(null, null);
      if(_store.bookingHarbourType == 'light' && !_store.bookingShort){
        _admin.showLightDialog();
      }
      _bloc.updateBookingHarbour(_store.bookingHarbourIndex);
      setState(() {});
    }
  }

  void _selectCheckIn() => _selectDate(true);
  void _selectCheckOut() => _selectDate(false);
  void _selectDate(bool start){
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: null,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
        return AppleDateDialog(
          ttl: start ? _lang.selectArrival : _lang.selectDeparture,
          hint: start ? _lang.arrivalNote : _lang.departureNote,
          minDate: start ? _minDateIn : _minDateOut,
          initDate: start ? _initDateIn : _initDateOut,
          onSelect: (DateTime newDate){
            if(newDate != null) {
              start ? _updateDates(newDate, null) : _updateDates(null, newDate);
            }
          },
        );
      },
    );
  }

  void _updateDates(DateTime newIn, DateTime newOut){
    if (_store.bookingHarbourType == 'light'){
      if(DateTime.now().hour < _store.harbours[_store.bookingHarbourIndex].arrival){
        _store.bookingCheckIn = _admin.toArrivalTime(DateTime.now().subtract(Duration(days: 1)));
        _store.bookingCheckOut = _admin.toArrivalTime(DateTime.now());
      } else {
        _store.bookingCheckIn = _admin.toArrivalTime(DateTime.now());
        _store.bookingCheckOut = _admin.toArrivalTime(DateTime.now().add(Duration(days: 1)));
      }
    } else {
      if(newIn != null){
        _store.bookingCheckIn = newIn;
      } else {
        if(_store.bookingCheckIn == null) _store.bookingCheckIn = DateTime.now();
      }
      _minDateIn = DateTime.now();
      _initDateIn = _store.bookingCheckIn;
      _minDateOut = _store.bookingCheckIn.add(Duration(days: 1));
      if(newOut != null){
        _store.bookingCheckOut = newOut;
      } else {
        if(_store.bookingCheckOut == null){
          _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
        } else {
          if(_store.bookingCheckOut.isBefore(_store.bookingCheckIn.add(Duration(days: 1)))){
            _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
          }
        }
      }
      _initDateOut = _store.bookingCheckOut;
    }
    _bloc.updateCheckIn(_store.bookingCheckIn);
    _bloc.updateCheckOut(_store.bookingCheckOut);
  }

  @override
  void initState() {
    super.initState();
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _lang = Language.of(context);
      _bloc = BlocProvider.of(context);
      _admin = new BookingAdmin(context: context);
      _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);
      _dateFormatLight = DateFormat('dd.MM.yyyy,', _lang.lang).add_j();
      _store.bookingEmail = _store.guest ? '' : _store.profile.mail ?? '';
      _store.bookingPhone = _store.guest ? '' : _store.profile.phone ?? '';
      _store.bookingBoatName = _store.guest ? '' : _store.profile.bName ?? '';
      _store.bookingBoatLength = _store.guest ? 0 : _store.profile.bLength;
      _store.bookingBoatWidth = _store.guest ? 0 : _store.profile.bWidth;
      _store.bookingBoatDepth = _store.guest ? 0 : _store.profile.bDepth;
      _mailController.text = _store.bookingEmail;
      _phoneController.text = _store.bookingPhone;
      _bNameController.text = _store.bookingBoatName;
      _updateDates(null, null);
      if(_store.guest) {
        _admin.showGuestDialog();
      } else {
        if(_store.bookingHarbourType == 'light' && !_store.bookingShort) _admin.showLightDialog();
      }
      return true;
    });
  }

  @override
  Widget build(BuildContext context){

    if(_store.bookingHarbourIndex != null && _store.bookingHarbourIndex != -1){
      _campaigns = _store.harbours[_store.bookingHarbourIndex].campaigns;
    }

    return FutureBuilder<bool>(
      future: _initFuture,
      builder: (context, initShot) {
        if(initShot.hasData && initShot.data){
          return Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  padding: const EdgeInsets.only(top: 16, bottom: 20),
                  children: <Widget>[
                    // _shortBooking
                    ShortSwitch(onChange: (bool short){
                      setState(() => _store.bookingShort = short);
                    }),
                    BookingInputRow(
                      icon: IconFont.location,
                      iconSize: 20,
                      widget: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 6, bottom: 5),
                          child: StreamBuilder<Object>(
                            stream: _bloc.bookingHarbour,
                            builder: (context, snapshot) {
                              return Text(
                                snapshot.hasData ?
                                (snapshot.data != -1 ? _store.harbours[snapshot.data].name : _lang.selectHarbour) :
                                (_store.bookingHarbourIndex == -1 ? _lang.selectHarbour : _store.harbours[_store.bookingHarbourIndex].name),
                                style: snapshot.hasData ?
                                (snapshot.data != -1 ? _styles.formInputStyle : _styles.formInputHintStyle) :
                                (_store.bookingHarbourIndex == -1 ? _styles.formInputHintStyle : _styles.formInputStyle),
                                textAlign: TextAlign.left,
                              );
                            }
                          ),
                        ),
                        onTap: _searchHarbour,
                      ),
                    ),

                    // Check-in, Check-out
                    _store.bookingShort ? SizedBox.shrink() : StreamBuilder<DateTime>(
                      stream: _bloc.selectCheckIn,
                      builder: (context, snapshot) {
                        return Column(
                          children: <Widget>[
                            BookingInputRow(
                              icon: IconFont.checkout,
                              iconSize: 20,
                              widget: InkWell(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 6, bottom: 5),
                                  child: Text(
                                    (_store.bookingHarbourType == "light")
                                      ? _dateFormatLight.format(_store.bookingCheckIn)
                                      : _dateFormat.format(_store.bookingCheckIn),
                                    style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
                                      ? _styles.formInputHintStyle
                                      : _styles.formInputStyle,
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light") ? null : _selectCheckIn,
                              )
                            ),
                            SizedBox(width: _spaceBetween),
                            StreamBuilder<DateTime>(
                              stream: _bloc.selectCheckOut,
                              builder: (context, snapOut) {
                                return BookingInputRow(
                                  icon: IconFont.checkin,
                                  iconSize: 20,
                                  widget: InkWell(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 6, bottom: 5),
                                      child: Text(
                                        (_store.bookingHarbourType == "light")
                                          ? _dateFormatLight.format(_store.bookingCheckOut)
                                          : _dateFormat.format(_store.bookingCheckOut),
                                        style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
                                          ? _styles.formInputHintStyle
                                          : _styles.formInputStyle,
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light") ? null : _selectCheckOut
                                  )
                                );
                              }
                            ),
                          ],
                        );
                      }
                    ),

                    _store.bookingShort ? SizedBox.shrink() : SizedBox(height: _spaceBetween),

                    SizedBox(height: _spaceBetween),

                    //Boat name
                    BookingInputRow(
                      icon: IconFont.boat,
                      iconSize: 26,
                      widget: TextFormField(
                        controller: _bNameController,
                        focusNode: _bNameFocusNode,
                        autofocus: false,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                        autocorrect: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: _lang.profileBoatName,
                          hintStyle: _styles.formInputHintStyle,
                          labelStyle: _styles.formInputLabelStyle,
                        ),
                        style: _styles.formInputStyle,
                        onChanged: (String term) => _store.bookingBoatName = _bNameController.text,
                        onFieldSubmitted: (String term){
                          _store.bookingBoatName = _bNameController.text;
                          _bNameFocusNode.unfocus();
                        },
                      ),
                    ),

                    SizedBox(height: _spaceBetween),

                    //Boat Length
                    _store.bookingShort ? SizedBox.shrink() : BookingInputRow(
                      icon: IconFont.sizeh, iconSize: 20,
                      widget: StreamBuilder<Object>(
                        stream: _bloc.boatLength,
                        builder: (context, snapshot) {
                          if(snapshot.hasData) _store.bookingBoatLength = double.parse(snapshot.data.toString());
                          return InkWell(
                            child: Text(
                              snapshot.hasData
                                ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatLength)
                                : (_store.bookingBoatLength != 0.0 ? '${_store.bookingBoatLength.toString()} m' : _lang.profileBoatLength),
                              style: _styles.formInputHintStyle,
                              textAlign: TextAlign.left,
                            ),
                            onTap: null
                          );
                        }
                      )
                    ),

                    _store.bookingShort ? SizedBox.shrink() : SizedBox(height: _spaceBetween),

                    //Boat Width
                    _store.bookingShort ? SizedBox.shrink() : BookingInputRow(
                      icon: IconFont.sizew,
                      iconSize: 20,
                      widget: StreamBuilder<Object>(
                        stream: _bloc.boatWidth,
                        builder: (context, snapshot) {
                          if(snapshot.hasData) _store.bookingBoatWidth = double.parse(snapshot.data.toString());
                          return InkWell(
                            child: Text(
                              snapshot.hasData
                                ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatWidth)
                                : (_store.bookingBoatWidth != 0.0 ? '${_store.bookingBoatWidth.toString()} m' : _lang.profileBoatWidth),
                              style: _styles.formInputHintStyle,
                              textAlign: TextAlign.left,
                            ),
                            onTap: null
                          );
                        }
                      ),
                    ),

                    _store.bookingShort ? SizedBox.shrink() : SizedBox(height: _spaceBetween),

                    //Boat Depth
                    _store.bookingShort ? SizedBox.shrink() : BookingInputRow(
                      icon: IconFont.sized,
                      iconSize: 20,
                      widget: StreamBuilder<Object>(
                        stream: _bloc.boatDepth,
                        builder: (context, snapshot) {
                          if(snapshot.hasData) _store.bookingBoatDepth = double.parse(snapshot.data.toString());
                          return InkWell(
                            child: Text(
                              snapshot.hasData
                                ? (snapshot.data != '0' ? '${snapshot.data} m' : _lang.profileBoatDepth)
                                : (_store.bookingBoatDepth != 0.0 ? '${_store.bookingBoatDepth.toString()} m' : _lang.profileBoatDepth),
                              style: _styles.formInputHintStyle,
                              textAlign: TextAlign.left,
                            ),
                            onTap: null
                          );
                        }
                      ),
                    ),

                    _store.bookingShort ? SizedBox.shrink() : SizedBox(height: _spaceBetween),

                    //Email
                    BookingInputRow(
                      icon: IconFont.email,
                      iconSize: 20,
                      widget: TextFormField(
                        controller: _mailController,
                        focusNode: _mailFocusNode,
                        autofocus: false,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.done,
                        textCapitalization: TextCapitalization.sentences,
                        autocorrect: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: _lang.profileEmail,
                          hintStyle: _styles.formInputHintStyle,
                          labelStyle: _styles.formInputLabelStyle,
                        ),
                        style: _styles.formInputStyle,
                        onChanged: (term) => _store.bookingEmail = _mailController.text,
                        onFieldSubmitted: (term){
                          _store.bookingEmail = _mailController.text;
                          _mailFocusNode.unfocus();
                        },
                      ),
                    ),

                    SizedBox(height: _spaceBetween),

                    //Phone
                    BookingInputRow(
                      icon: IconFont.phone,
                      iconSize: 20,
                      widget: TextFormField(
                        controller: _phoneController,
                        focusNode: _phoneFocusNode,
                        autofocus: false,
                        keyboardType: TextInputType.phone,
                        textInputAction: TextInputAction.done,
                        autocorrect: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: _lang.profilePhone,
                          hintStyle: _styles.formInputHintStyle,
                          labelStyle: _styles.formInputLabelStyle,
                        ),
                        style: _styles.formInputStyle,
                        onChanged: (term) => _store.bookingPhone = _phoneController.text,
                        onFieldSubmitted: (term){
                          _store.bookingPhone = _phoneController.text;
                          _phoneFocusNode.unfocus();
                        },
                      ),
                    ),

                    _campaigns.length > 0 ? SizedBox(height: _spaceBetween) : SizedBox.shrink(),
                    _campaigns.length > 0 ? CampaignItem(
                      w: (MediaQuery.of(context).size.width),
                      campaigns: _campaigns
                    ) : SizedBox.shrink(),

                    _store.bookingShort ? SizedBox(height: 6) : SizedBox.shrink(),
                    _store.bookingShort ? Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Row(
                        children: [
                          Flexible(
                            child: SelectableText(
                              _lang.midnightNote,
                              style: _styles.formInputStyle.copyWith(fontSize: 18, height: 1.4),
                              maxLines: null
                            ),
                          ),
                        ],
                      ),
                    ) : SizedBox.shrink(),
                  ],
                ),
              ),

              SizedBox(height: _spaceBottom),
              // Container(
              //   height: _spaceBottom,
              //   decoration: BoxDecoration(
              //       gradient: LinearGradient(
              //         begin: Alignment.topRight,
              //         end: Alignment.bottomLeft,
              //         colors: [
              //           Colors.blue,
              //           Colors.red,
              //         ],
              //       )
              //   ),
              // ),

              //Next button
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  StreamBuilder(
                    stream: _bloc.loading,
                    builder: (context, snapshot){
                      if(snapshot.hasData && snapshot.data){
                        return Container(
                          width: 48, height: 48,
                          padding: EdgeInsets.all(5),
                          child: CircularProgressIndicator(
                            strokeWidth: 3.5,
                            valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
                          ),
                        );
                      } else {
                        return IconButton(
                          padding: EdgeInsets.all(0),
                          icon: _store.bookingHarbourType == 'super-light'
                            ? SizedBox(
                                width: 46,
                                height: 46,
                                child: SvgPicture.asset('assets/svg/qr_code.svg', color: _styles.commonColor)
                              )
                            : Icon(IconFont.arrow_icon, size: 46, color: _styles.commonColor),
                          onPressed: (){
                            _mailFocusNode.unfocus();
                            _phoneFocusNode.unfocus();
                            _validate(context);
                          }
                        );
                      }
                    },
                  ),
                ],
              ),
              SizedBox(height: 16 + MediaQuery.of(context).padding.bottom),
            ],
          );
        } else {
          return Container();
        }
      }
    );
  }
}
