import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';

class AppleNumberDialog extends StatelessWidget {

  final String ttl;
  final String hint;
  final String type;
  final String initSize;

  final double numBtnHeight = 41.0;
  final double numBtnSpace = 12.0;

  final MyStyles styles = new MyStyles();

  AppleNumberDialog({Key key,
    @required this.ttl,
    this.hint = '',
    @required this.type,
    @required this.initSize}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final bool hinted = (hint != '');
    final double dialogHeight = (hinted ? 400 : 320);
    List<String> numStrings = initSize.split('.');

    int _initIndex1 = int.parse(numStrings[0]);
    int _initIndex2 = int.parse(numStrings[1]);

    int _selectedIndex1;
    int _selectedIndex2;

    void updateValue(){
      if(_selectedIndex1 == null){
        _selectedIndex1 = _initIndex1;
      }
      if(_selectedIndex2 == null){
        _selectedIndex2 = _initIndex2;
      }
      String newValue = '${_selectedIndex1.toString()}.${_selectedIndex2.toString()}';
      switch(type){
        case 'length':
          _bloc.updateBoatLength(newValue);
          break;
        case 'height':
          _bloc.updateBoatWidth(newValue);
          break;
        case 'depth':
          _bloc.updateBoatDepth(newValue);
          break;
        default:
          break;
      }
    }

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          // color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 16),
              Text(ttl, style: styles.pageLineText, textAlign: TextAlign.center),
              SizedBox(height: numBtnSpace),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          //
                        },
                        children: List<Widget>.generate(1, (int index) {
                          return Center(
                            child: Text(''),
                          );
                        }),
                      ),
                    ),
                    Container(
                      width: 40,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          //
                        },
                        children: List<Widget>.generate(1, (int index) {
                          return Center(
                            child: Text(''),
                          );
                        }),
                      ),
                    ),
                    Container(
                      width: 50,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          _selectedIndex1 = index;
                        },
                        children: List<Widget>.generate(41, (int index) {
                          return Center(
                            child: Text(
                              index.toString(),
                              style: styles.appleDateText,
                            ),
                          );
                        }),
                        scrollController: FixedExtentScrollController(initialItem: _initIndex1),
                      ),
                    ),
                    Container(
                      width: 24,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          //
                        },
                        children: List<Widget>.generate(1, (int index) {
                          return Center(
                            child: Icon(
                              IconFont.dot,
                              size: 20,
                              color: styles.commonColor
                            ),
                          );
                        }),
                      ),
                    ),
                    Container(
                      width: 50,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          _selectedIndex2 = index;
                        },
                        children: List<Widget>.generate(10, (int index) {
                          return Center(
                            child: Text(
                              index.toString(),
                              style: styles.appleDateText,
                            ),
                          );
                        }),
                        scrollController: FixedExtentScrollController(initialItem: _initIndex2),
                      ),
                    ),
                    Container(
                      width: 40,
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          //
                        },
                        children: List<Widget>.generate(1, (int index) {
                          return Center(
                            child: Text(
                              'm',
                              style: styles.appleDateText,
                              textAlign: TextAlign.left,
                            ),
                          );
                        }),
                      ),
                    ),
                    Expanded(
                      child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        selectionOverlay: null,
                        itemExtent: 36,
                        onSelectedItemChanged: (int index) {
                          //
                        },
                        children: List<Widget>.generate(1, (int index) {
                          return Center(
                            child: Text(''),
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              ),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              SizedBox(height: (hinted ? 12 : 5)),
              Container(
                height: (hinted ? 80 : 2),
                padding: EdgeInsets.only(left:16, right: 16),
                child: (hinted ? Center(
                  child: SelectableText.rich(
                    TextSpan(
                      style: styles.popupHintText,
                      children: <TextSpan>[
                        TextSpan(text: lang.hint, style: styles.popupHintBoldText),
                        TextSpan(text: hint)
                      ]
                    ),
                    textAlign: TextAlign.center,
                    maxLines: null,
                  ),
                ) : SizedBox(width: 1, height: 1)),
              ),
              SizedBox(height: (hinted ? 12 : 5)),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.exti2,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.check22,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          updateValue();
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}