import 'package:flutter/material.dart';

class DatePickerTheme {
  static Map<int, Color> color =
  {
    50:Color.fromRGBO(136,14,79, .1),
    100:Color.fromRGBO(136,14,79, .2),
    200:Color.fromRGBO(136,14,79, .3),
    300:Color.fromRGBO(136,14,79, .4),
    400:Color.fromRGBO(136,14,79, .5),
    500:Color.fromRGBO(136,14,79, .6),
    600:Color.fromRGBO(136,14,79, .7),
    700:Color.fromRGBO(136,14,79, .8),
    800:Color.fromRGBO(136,14,79, .9),
    900:Color.fromRGBO(136,14,79, 1),
  };
  final dateTheme = ThemeData(
      primarySwatch: MaterialColor(0xFF1A3B62, color),
      accentColor: MaterialColor(0xFF1A3B62, color),
      dialogBackgroundColor: MaterialColor(0xE6FFFFFF, color),
      buttonColor: Color(0xFF1A3B62),
      textTheme: TextTheme(
        // body1
        bodyText2: TextStyle( // Date numbers
          fontSize: 20.0,
          fontFamily: "Neutra2Book",
          color: Color(0xFF1A3B62),//0xFFE0E0E2
          fontWeight: FontWeight.w400
        ),
        caption: TextStyle( // Week days letters
          fontSize: 14.0,
          fontFamily: "Neutra2Book",
          color: Color(0xFF1A3B62),//0xFFE0E0E2
          fontWeight: FontWeight.w400
        ),
        // subhead
        subtitle1: TextStyle( // Month
          fontSize: 18.0,
          fontFamily: "Neutra2Book",
          fontWeight: FontWeight.w400,
          color: Color(0xFF1A3B62)
        ),
        button: TextStyle( // CANCEL and OK buttons
          fontSize: 16.0,
          fontFamily: "Neutra2Book",
          fontWeight: FontWeight.w600,
        ),
      ),
      dialogTheme: DialogTheme(
        elevation: 20,
      ),
      primaryTextTheme: TextTheme(
        // subhead
        subtitle1: TextStyle( // Year in Title
          fontSize: 20.0,
          fontFamily: "Neutra2Book",
          color: Color(0xFFFFFFFF),//0xFFE0E0E2
          fontWeight: FontWeight.w400
        ),
        // display1
        headline4: TextStyle( // The BIG title
          fontSize: 26.0,
          fontFamily: "Neutra2Book",
          color: Color(0xFFFFFFFF),//0xFFE0E0E2
          fontWeight: FontWeight.w400
        ),
      ),
      accentTextTheme: TextTheme(
        // body2
        bodyText1: TextStyle( // Selected Date
          fontSize: 20.0,
          fontFamily: "Neutra2Book",
          color: Color(0xFFFFFFFF),//0xFFE0E0E2
          fontWeight: FontWeight.w400
        ),
      )
  );
  DatePickerTheme();
}