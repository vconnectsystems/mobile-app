// // import 'package:dockside/pages/booking/booking_dates.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:intl/intl.dart';
// import 'dart:collection';
// // import 'dart:convert';
// // import 'package:xml2json/xml2json.dart';
// import 'package:dockside/api/api.dart';
// import 'package:dockside/api/spot.dart';
// // import 'package:dockside/api/drawable_spot.dart';
// import 'package:dockside/api/language.dart';
// import 'package:dockside/api/campaign.dart';
// import 'package:dockside/bloc/bloc.dart';
// import 'package:dockside/bloc/bloc_provider.dart';
// import 'package:dockside/bloc/global_state.dart';
// import 'package:dockside/styles/text_styles.dart';
// import 'package:dockside/pages/common/icon_font_icons.dart';
// import 'package:dockside/pages/common/toast.dart';
// import 'package:dockside/pages/common/booking_input_row.dart';
// import 'package:dockside/pages/common/campaign_item.dart';
// // import 'package:dockside/pages/common/svg_button.dart';
// import 'package:dockside/pages/login/register_name_page.dart';
// import 'apple_date_dialog.dart';
// import 'guest_dialog.dart';
// import 'light_dialog.dart';
// import 'available_dialog.dart';
// import 'selection_dialog.dart';
// // import 'spots_page.dart';
// import 'booking_payment_new.dart';
//
// class QrBookingPage extends StatefulWidget {
//
//   QrBookingPage({Key key}) : super(key: key);
//
//   @override
//   _State createState() => _State();
//
// }
//
// class _State extends State<QrBookingPage> {
//
//   static const double _spaceBetween = 4.0;
//   static const double _spaceBottom = 20.0;
//   final MyStyles _styles = new MyStyles();
//   final GlobalState _store = GlobalState.instance;
//   final TextEditingController _mailController = TextEditingController();
//   final FocusNode _mailFocusNode = FocusNode();
//   final TextEditingController _phoneController = TextEditingController();
//   final FocusNode _phoneFocusNode = FocusNode();
//   final Api _api = new Api();
//   final DateFormat _apiDateFormat = DateFormat('yyyy-MM-dd');
//   DateFormat _dateFormat;
//   DateFormat _dateFormatLight;
//   Language _lang;
//   Bloc _bloc;
//   int _harbourId;
//   int _arrival;
//   bool _isLight = false;
//   Future<bool> _initFuture;
//   DateTime _minDateIn;
//   DateTime _initDateIn;
//   DateTime _minDateOut;
//   DateTime _initDateOut;
//   Spot _spot;
//   String _priceStr;
//   List<Campaign> _campaigns = [];
//
//   void _showToast(String mess, BuildContext context, {bool green = false}){
//     Toast.show(
//         mess, context,
//         backgroundColor: green ? Colors.green : Colors.red,
//         duration: _styles.toastDuration,
//         gravity: _styles.toastGravity
//     );
//   }
//
//   void _showGuestDialog(BuildContext context){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: false,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return GuestDialog(
//           onCancel: (){
//             Navigator.pop(context);
//             Navigator.pop(context);
//           },
//           onConfirm: (){
//             Navigator.push(context, CupertinoPageRoute(builder: (context) => RegisterNamePage()));
//           },
//         );
//       },
//     );
//   }
//
//   void _showLightDialog(BuildContext context){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: false,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,//Colors.black87,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return LightDialog(
//           onOk: (){
//             Navigator.pop(context);
//           },
//         );
//       },
//     );
//   }
//
//   void _showBookedDialog(BuildContext context){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: false,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,//Colors.black87,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return AvailableDialog(
//           message: _lang.alreadyBooked,
//           spotsNum: -1,
//           onCancel: (){
//             Navigator.of(context).pop();
//             Navigator.of(context).pop();
//           },
//           onCall: (){
//             //
//           },
//         );
//       },
//     );
//   }
//
//   DateTime _toArrivalTime(DateTime date){
//     int hour = date.hour;
//     int diff = hour-_arrival;
//     DateTime newTime = date.subtract(Duration(hours: diff));
//     return newTime;
//   }
//
//   Future<void> _updateDates(DateTime newIn, DateTime newOut) async {
//     _isLight = false;
//     if(_store.bookingHarbourIndex != -1) {
//       _store.bookingHarbourType = _store.harbours[_store.bookingHarbourIndex].subscriptionType;
//       _isLight = (_store.bookingHarbourType == 'light');
//       _arrival = _store.harbours[_store.bookingHarbourIndex].arrival;
//     }
//     if (_isLight){
//       if(DateTime.now().hour < _arrival){
//         _store.bookingCheckIn = _toArrivalTime(DateTime.now().subtract(Duration(days: 1)));
//         _store.bookingCheckOut = _toArrivalTime(DateTime.now());
//       } else {
//         _store.bookingCheckIn = _toArrivalTime(DateTime.now());
//         _store.bookingCheckOut = _toArrivalTime(DateTime.now().add(Duration(days: 1)));
//       }
//     } else {
//       if(newIn != null){
//         _store.bookingCheckIn = newIn;
//       } else {
//         if(_store.bookingCheckIn == null){
//           _store.bookingCheckIn = DateTime.now();
//         }
//       }
//       _minDateIn = DateTime.now();
//       _initDateIn = _store.bookingCheckIn;
//       _minDateOut = _store.bookingCheckIn.add(Duration(days: 1));
//       if(newOut != null){
//         _store.bookingCheckOut = newOut;
//       } else {
//         if(_store.bookingCheckOut == null){
//           _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
//         } else {
//           if(_store.bookingCheckOut.isBefore(_store.bookingCheckIn.add(Duration(days: 1)))){
//             _store.bookingCheckOut = _store.bookingCheckIn.add(Duration(days: 1));
//           }
//         }
//       }
//       _initDateOut = _store.bookingCheckOut;
//     }
//     //
//     _bloc.updateCheckIn(_store.bookingCheckIn);
//     _bloc.updateCheckOut(_store.bookingCheckOut);
//     if (!_store.guest) {
//       await _getSpots(context);
//     }
//     return;
//   }
//
//   void _selectCheckIn(){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: true,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return AppleDateDialog(
//           ttl: _lang.selectArrival,
//           hint: _lang.arrivalNote,
//           minDate: _minDateIn,
//           initDate: _initDateIn,
//           onSelect: (DateTime newDate){
//             _updateDates(newDate, null);
//           },
//         );
//       },
//     );
//   }
//
//   void _selectCheckOut() {
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: true,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return AppleDateDialog(
//           ttl: _lang.selectDeparture,
//           hint: _lang.departureNote,
//           minDate: _minDateOut,
//           initDate: _initDateOut,
//           onSelect: (DateTime newDate){
//             if(newDate != null) {
//               _updateDates(null, newDate);
//             }
//           },
//         );
//       },
//     );
//   }
//
//   void _dataError(dynamic error, BuildContext context){
//     _bloc.setLoading(false);
//     print(error.toString());
//     _showToast(error.toString(), context);
//   }
//
//   Future<void> _getSpots(BuildContext context) async {
//     bool _checkPass = false;
//     if(_store.bookingHarbourIndex != -1 && _store.bookingHarbourIndex != null){
//       if(_store.bookingCheckIn != null){
//         if(_store.bookingCheckOut != null){
//           if(_store.bookingBoatLength != 0.0 && _store.bookingBoatLength != null && _store.bookingBoatLength != 0){
//             if(_store.bookingBoatWidth != 0.0 && _store.bookingBoatWidth != null && _store.bookingBoatWidth != 0){
//               if(_store.bookingBoatDepth != 0.0 && _store.bookingBoatDepth != null && _store.bookingBoatDepth != 0){
//                 if(_store.bookingEmail != '' && _store.bookingEmail != null){
//                   if(_phoneController.text != ''){
//                     _bloc.setLoading(true);
//                     _checkPass = true;
//                     _store.bookingPhone = _phoneController.text;
//                     if(_store.profile.phone == null || _store.profile.phone == '' || _store.profile.phone != _store.bookingPhone){
//                       _store.profile.phone = _store.bookingPhone;
//                       await _api.updateProfile(_store.profile, _store.apiToken).then((value){
//                         _checkPass = true;
//                       });
//                     } else {
//                       _checkPass = true;
//                     }
//                   } else {
//                     _showToast(_lang.bookPhoneError, context);
//                   }
//                 } else{
//                   _showToast(_lang.bookEmailError, context);
//                 }
//               } else{
//                 _showToast(_lang.bookDepthError, context);
//               }
//             } else{
//               _showToast(_lang.bookWidthError, context);
//             }
//           } else{
//             _showToast(_lang.bookLengthError, context);
//           }
//         } else{
//           _showToast(_lang.bookDepartureError, context);
//         }
//       } else {
//         _showToast(_lang.bookArrivalError, context);
//       }
//     } else {
//       _showToast(_lang.bookHarbourError, context);
//     }
//     if(_checkPass){
//       String from = _apiDateFormat.format(_store.bookingCheckIn);
//       String to = _apiDateFormat.format(_store.bookingCheckOut);
//       String today;
//       String tomorrow;
//       if(_isLight && DateTime.now().hour < _arrival){
//         today = _apiDateFormat.format(DateTime.now().subtract(Duration(days: 1)));
//       } else {
//         today = _apiDateFormat.format(DateTime.now());
//       }
//       if(_isLight && DateTime.now().hour < _arrival){
//         tomorrow = _apiDateFormat.format(DateTime.now());
//       } else {
//         tomorrow = _apiDateFormat.format(DateTime.now().add(Duration(days: 1)));
//       }
//       _store.bookingIsForToday = (from == today && to == tomorrow);
//       //
//       _harbourId = _store.harbours[_store.bookingHarbourIndex].id;
//       await _api.getSpots(
//           _harbourId,
//           _apiDateFormat.format(_store.bookingCheckIn),
//           _apiDateFormat.format(_store.bookingCheckOut),
//           _store.apiToken
//       ).then((result){
//         _store.serverSpots = result as Map<String, dynamic>;
//         for(int i = 0; i < _store.spotsList.length; i++){
//           if(_store.spotsList[i].id == _store.bookingSpotId){
//             _spot = _store.spotsList[i];
//             _store.bookingSpotCode = _spot.key;
//             break;
//           }
//         }
//         setState(() {
//           _priceStr = _api.priceToString(_spot.price);
//           if(_spot.booked){
//             _showBookedDialog(context);
//           }
//         });
//         _bloc.setLoading(false);
//       }).catchError((error) => _dataError(error, context));
//       return;
//     }
//     return;
//   }
//
//   void _paymentLinkSuccess(value, BuildContext context){
//     String paymentLink = value['payment_link'];
//     print('no_payment_required: ${value["no_payment_required"]}');
//     Navigator.of(context).pop();
//     Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingPaymentPage(paymentLink: paymentLink)));
//   }
//   void _bookingIdSuccess(value, BuildContext context){
//     _store.bookingId = value;
//     Future result = _api.getPaymentLink(value, _store.apiToken);
//     result.then((value) => _paymentLinkSuccess(value, context)).catchError((error) => _dataError(error, context));
//   }
//   void _getBookingId(BuildContext context) async {
//     await _api.addBooking(
//       _apiDateFormat.format(_store.bookingCheckIn),
//       _apiDateFormat.format(_store.bookingCheckOut),
//       _store.bookingSpotId,
//       _store.harbours[_store.bookingHarbourIndex].id,
//       _store.apiToken
//     ).then((value) => _bookingIdSuccess(value, context)
//     ).catchError((error) => _dataError(error, context));
//   }
//   Future<void> _doubleCheckSpot(BuildContext context) async {
//     _bloc.setLoading(true);
//     await _api.getSpots(
//         _store.harbours[_store.bookingHarbourIndex].id,
//         _apiDateFormat.format(_store.bookingCheckIn),
//         _apiDateFormat.format(_store.bookingCheckOut),
//         _store.apiToken
//     ).then((value) {
//       _bloc.setLoading(false);
//       _store.serverSpots = value as LinkedHashMap<String, dynamic>;
//       _spot = _api.getSpotFromKey(_store.bookingSpotCode);
//       if(!_spot.booked) {
//         _getBookingId(context);
//       } else {
//         _showBookedDialog(context);
//       }
//     }).catchError((error){
//       _bloc.setLoading(false);
//       _dataError(error, context);
//     });
//   }
//   void _showSelectionDialog(BuildContext context){
//     showGeneralDialog(
//       context: context,
//       barrierDismissible: false,
//       barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//       barrierColor: null,//Colors.black87,
//       transitionDuration: const Duration(milliseconds: 200),
//       pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
//         return SelectionDialog(
//           ttl: _spot.key,
//           message1: '${_lang.maxSize}${_spot.boatLength} x ${_spot.boatWidth} m',
//           message2: _lang.availableFor,
//           priceStr: _lang.totalPrice,
//           totalPriceStr: _api.priceToString(_spot.price),
//           onConfirm: (){
//             _store.bookingSpotId = _spot.id;
//             Navigator.of(context).pop();
//             _doubleCheckSpot(context);
//           },
//           onCancel: (){
//             Navigator.of(context).pop();
//             // _bloc.clickSpot('-1');
//             // _bloc.showSpotsPopup(0);
//           },
//         );
//       },
//     );
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _initFuture = Future<bool>.delayed(Duration.zero,() async {
//       _lang = Language.of(context);
//       _bloc = BlocProvider.of(context);
//       _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);
//       _dateFormatLight = DateFormat('dd.MM.yyyy,', _lang.lang).add_j();
//       _store.bookingBoatLength = _store.guest ? 0 : _store.profile.bLength;
//       _store.bookingBoatWidth = _store.guest ? 0 : _store.profile.bWidth;
//       _store.bookingBoatDepth = _store.guest ? 0 : _store.profile.bDepth;
//       _mailController.text = _store.guest ? '' : _store.bookingEmail ?? '';
//       _phoneController.text = _store.guest ? '' : (_store.bookingPhone ?? _store.profile.phone ?? '');
//       await _updateDates(null, null);
//       if(_store.guest) {
//         _showGuestDialog(context);
//       } else {
//         if(_store.bookingHarbourType == 'light'){
//           _showLightDialog(context);
//         }
//       }
//       return true;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context){
//
//     if(_store.bookingHarbourIndex != null && _store.bookingHarbourIndex != -1){
//       _campaigns = _store.harbours[_store.bookingHarbourIndex].campaigns;
//     }
//     bool _camp = _campaigns.length > 0;
//
//     // Harbour
//     return FutureBuilder<bool>(
//         future: _initFuture,
//         builder: (context, initShot) {
//           if(initShot.hasData && initShot.data){
//             return Column(
//               children: <Widget>[
//                 Expanded(
//                   child: ListView(
//                     padding: const EdgeInsets.only(top: 16, bottom: 20),
//                     children: <Widget>[
//                       BookingInputRow(
//                         icon: IconFont.location,
//                         iconSize: 20,
//                         widget: Padding(
//                           padding: const EdgeInsets.only(top: 6, bottom: 5),
//                           child: StreamBuilder<Object>(
//                               stream: _bloc.bookingHarbour,
//                               builder: (context, snapshot) {
//                                 return Text(snapshot.hasData
//                                     ? (snapshot.data != -1
//                                     ? _store.bookingSpotCode != null
//                                     ? '#${_store.bookingSpotCode}, ${_store.harbours[snapshot.data].name}'
//                                     : _store.harbours[snapshot.data].name
//                                     : _lang.selectHarbour
//                                 )
//                                     : (_store.bookingHarbourIndex == -1
//                                     ? _lang.selectHarbour
//                                     : _store.bookingSpotCode != null
//                                     ? '#${_store.bookingSpotCode}, ${_store.harbours[_store.bookingHarbourIndex].name}'
//                                     : _store.harbours[_store.bookingHarbourIndex].name
//                                 ),
//                                   style: snapshot.hasData
//                                       ? (snapshot.data != -1
//                                       ? _styles.formInputStyle
//                                       : _styles.formInputHintStyle)
//                                       : (_store.bookingHarbourIndex == -1
//                                       ? _styles.formInputHintStyle
//                                       : _styles.formInputStyle
//                                   ),
//                                   textAlign: TextAlign.left,
//                                 );
//                               }
//                           ),
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       // Check-in, Check-out
//                       StreamBuilder<DateTime>(
//                           stream: _bloc.selectCheckIn,
//                           builder: (context, snapshot) {
//                             return Column(
//                               children: <Widget>[
//                                 BookingInputRow(
//                                     icon: IconFont.checkout,
//                                     iconSize: 20,
//                                     widget: InkWell(
//                                       child: Padding(
//                                         padding: const EdgeInsets.only(top: 6, bottom: 5),
//                                         child: Text((_store.bookingHarbourType == "light")
//                                             ? _dateFormatLight.format(_store.bookingCheckIn)
//                                             : _dateFormat.format(_store.bookingCheckIn),
//                                           style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                               ? _styles.formInputHintStyle
//                                               : _styles.formInputStyle,
//                                           textAlign: TextAlign.left,
//                                         ),
//                                       ),
//                                       onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                           ? null
//                                           : _selectCheckIn,
//                                     )
//                                 ),
//                                 SizedBox(width: _spaceBetween),
//                                 StreamBuilder<DateTime>(
//                                     stream: _bloc.selectCheckOut,
//                                     builder: (context, snapOut) {
//                                       return BookingInputRow(
//                                           icon: IconFont.checkin,
//                                           iconSize: 20,
//                                           widget: InkWell(
//                                               child: Padding(
//                                                 padding: const EdgeInsets.only(top: 6, bottom: 5),
//                                                 child: Text((_store.bookingHarbourType == "light")
//                                                     ? _dateFormatLight.format(_store.bookingCheckOut)
//                                                     : _dateFormat.format(_store.bookingCheckOut),
//                                                   style: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                                       ? _styles.formInputHintStyle
//                                                       : _styles.formInputStyle,
//                                                   textAlign: TextAlign.left,
//                                                 ),
//                                               ),
//                                               onTap: (_store.bookingHarbourIndex == -1 || _store.bookingHarbourType == "light")
//                                                   ? null
//                                                   : _selectCheckOut
//                                           )
//                                       );
//                                     }
//                                 ),
//                               ],
//                             );
//                           }
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Boat Length
//                       BookingInputRow(
//                         icon: IconFont.sizeh,
//                         iconSize: 20,
//                         widget: StreamBuilder<Object>(
//                             stream: _bloc.boatLength,
//                             builder: (context, snapshot) {
//                               if(snapshot.hasData){
//                                 _store.bookingBoatLength = double.parse(snapshot.data.toString());
//                               }
//                               return InkWell(
//                                   child: Text(
//                                     snapshot.hasData
//                                         ? (snapshot.data != '0'
//                                         ? '${snapshot.data} m'
//                                         : _lang.profileBoatLength
//                                     )
//                                         : (_store.bookingBoatLength != 0.0
//                                         ? '${_store.bookingBoatLength.toString()} m'
//                                         : _lang.profileBoatLength
//                                     ),
//                                     style: _styles.formInputHintStyle,
//                                     textAlign: TextAlign.left,
//                                   ),
//                                   onTap: null
//                               );
//                             }
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Boat Width
//                       BookingInputRow(
//                         icon: IconFont.sizew,
//                         iconSize: 20,
//                         widget: StreamBuilder<Object>(
//                             stream: _bloc.boatWidth,
//                             builder: (context, snapshot) {
//                               if(snapshot.hasData){
//                                 _store.bookingBoatWidth = double.parse(snapshot.data.toString());
//                               }
//                               return InkWell(
//                                   child: Text(snapshot.hasData
//                                       ? (snapshot.data != '0'
//                                       ? '${snapshot.data} m'
//                                       : _lang.profileBoatWidth
//                                   )
//                                       : (_store.bookingBoatWidth != 0.0
//                                       ? '${_store.bookingBoatWidth.toString()} m'
//                                       : _lang.profileBoatWidth
//                                   ),
//                                     style: _styles.formInputHintStyle,
//                                     textAlign: TextAlign.left,
//                                   ),
//                                   onTap: null
//                               );
//                             }
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Boat Depth
//                       BookingInputRow(
//                         icon: IconFont.sized,
//                         iconSize: 20,
//                         widget: StreamBuilder<Object>(
//                             stream: _bloc.boatDepth,
//                             builder: (context, snapshot) {
//                               if(snapshot.hasData){
//                                 _store.bookingBoatDepth = double.parse(snapshot.data.toString());
//                               }
//                               return InkWell(
//                                 child: Text(snapshot.hasData
//                                     ? (snapshot.data != '0'
//                                     ? '${snapshot.data} m'
//                                     : _lang.profileBoatDepth
//                                 )
//                                     : (_store.bookingBoatDepth != 0.0
//                                     ? '${_store.bookingBoatDepth.toString()} m'
//                                     : _lang.profileBoatDepth
//                                 ),
//                                   style: _styles.formInputHintStyle,
//                                   textAlign: TextAlign.left,
//                                 ),
//                                 onTap: null,
//                               );
//                             }
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Email
//                       BookingInputRow(
//                         icon: IconFont.email,
//                         iconSize: 20,
//                         widget: TextFormField(
//                           controller: _mailController,
//                           focusNode: _mailFocusNode,
//                           autofocus: false,
//                           keyboardType: TextInputType.emailAddress,
//                           textInputAction: TextInputAction.done,
//                           textCapitalization: TextCapitalization.sentences,
//                           autocorrect: false,
//                           decoration: InputDecoration(
//                             border: InputBorder.none,
//                             hintText: _lang.profileEmail,
//                             hintStyle: _styles.formInputHintStyle,
//                             labelStyle: _styles.formInputLabelStyle,
//                           ),
//                           style: _styles.formInputStyle,
//                           onChanged: (term){
//                             _store.bookingEmail = _mailController.text;
//                           },
//                           onFieldSubmitted: (term){
//                             _store.bookingEmail = _mailController.text;
//                             _mailFocusNode.unfocus();
//                           },
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Phone
//                       BookingInputRow(
//                         icon: IconFont.phone,
//                         iconSize: 20,
//                         widget: TextFormField(
//                           controller: _phoneController,
//                           focusNode: _phoneFocusNode,
//                           autofocus: false,
//                           keyboardType: TextInputType.phone,
//                           textInputAction: TextInputAction.done,
//                           // textCapitalization: TextCapitalization.sentences,
//                           autocorrect: false,
//                           decoration: InputDecoration(
//                             border: InputBorder.none,
//                             hintText: _lang.profilePhone,
//                             hintStyle: _styles.formInputHintStyle,
//                             labelStyle: _styles.formInputLabelStyle,
//                           ),
//                           style: _styles.formInputStyle,
//                           onChanged: (term){
//                             _store.bookingPhone = _phoneController.text;
//                           },
//                           onFieldSubmitted: (term){
//                             _store.bookingPhone = _phoneController.text;
//                             _phoneFocusNode.unfocus();
//                           },
//                         ),
//                       ),
//
//                       SizedBox(height: _spaceBetween),
//
//                       //Price
//                       BookingInputRow(
//                         icon: Icons.local_atm, //credit_card
//                         iconSize: 26,
//                         widget: Text(
//                           _priceStr != null ? 'DKK $_priceStr' : '',
//                           style: _styles.formInputStyle,
//                         ),
//                       ),
//
//                       //
//
//                       _camp ? SizedBox(height: _spaceBetween) : SizedBox.shrink(),
//                       _camp ? CampaignItem(campaigns: _campaigns, w: MediaQuery.of(context).size.width) : SizedBox.shrink(),
//
//                       //
//
//                     ],
//                   ),
//                 ),
//
//                 SizedBox(height: _spaceBottom),
//
//                 //Next button
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     StreamBuilder(
//                       stream: _bloc.loading,
//                       builder: (context, snapshot){
//                         if(snapshot.hasData && snapshot.data){
//                           return Container(
//                             width: 48, height: 48,
//                             padding: EdgeInsets.all(5),
//                             child: CircularProgressIndicator(
//                                 strokeWidth: 3.5,
//                                 valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
//                             ),
//                           );
//                         } else {
//                           return IconButton(
//                               padding: EdgeInsets.all(0),
//                               icon: Icon(IconFont.arrow_icon,
//                                 size: 46,
//                                 color: _styles.commonColor,
//                               ),
//                               onPressed: (){
//                                 _mailFocusNode.unfocus();
//                                 _showSelectionDialog(context);
//                               }
//                           );
//                         }
//                       },
//                     ),
//                   ],
//                 ),
//                 SizedBox(height: 26),
//               ],
//             );
//           } else {
//             return Container(
//               child: Center(
//                 child: Container(
//                   width: 48, height: 48,
//                   padding: EdgeInsets.all(5),
//                   child: CircularProgressIndicator(
//                       strokeWidth: 3.5,
//                       valueColor: new AlwaysStoppedAnimation<Color>(_styles.commonColor)
//                   ),
//                 ),
//               ),
//             );
//           }
//         }
//     );
//   }
// }
