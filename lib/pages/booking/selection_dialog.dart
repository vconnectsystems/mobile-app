import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/dialog_button.dart';

class SelectionDialog extends StatelessWidget {

  final String ttl;
  final String message1;
  final String message2;
  final String priceStr;
  final String totalPriceStr;
  final GestureTapCallback onConfirm;
  final GestureTapCallback onCancel;

  final double dialogHeight = 260;//236
  final double numBtnHeight = 41.0;
  final double numBtnSpace = 18.0;

  final MyStyles styles = new MyStyles();

  SelectionDialog({Key key,
    @required this.ttl,
    @required this.message1,
    @required this.message2,
    @required this.priceStr,
    @required this.totalPriceStr,
    @required this.onConfirm,
    @required this.onCancel
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    String ttl0 = '#';
    String ttl1 = ttl.length == 4 ? ttl.substring(0, 2) : (ttl.length == 3 ? ttl.substring(0, 1) : ttl);
    String ttl2 = ttl.length == 4 ? ttl.substring(2) : (ttl.length == 3 ? ttl.substring(1) : '');

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 20),
                  Text(
                    lang.youSelected,
                    style: styles.pageTextMedium,
                  ),
                  SizedBox(width: 20),
                ],
              ),
              SizedBox(height: 6),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 20),
                  Text(
                    ttl0,
                    style: styles.pageTitleLight.copyWith(fontSize: 26.0),
//                    style: styles.pageTextMediumHeavy,
                  ),
                  Text(
                    ttl1,
                    style: styles.pageTitleBold.copyWith(fontSize: 26.0),
//                    style: styles.pageTextMediumHeavy,
                  ),
                  Text(
                    ttl2,
                    style: styles.pageTitleLight.copyWith(fontSize: 26.0),
//                    style: styles.pageTextMediumHeavy,
                  ),
                  SizedBox(width: 20),
                ],
              ),
              SizedBox(height: 6),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 20),
                  Text(
                    message1,
                    style: styles.pageTextMedium,
                  ),
                  SizedBox(width: 20),
                ],
              ),
              SizedBox(height: 6),
//              Padding(
//                padding: const EdgeInsets.only(left: 20, right: 20),
//                child: SelectableText(
//                  message2,
//                  style: styles.pageTextMedium,
//                  textAlign: TextAlign.left,
//                  maxLines: null,
//                ),
//              ),
//              SizedBox(height: 6),
              Row(
                children: <Widget>[
                  SizedBox(width: 20),
                  Text(
                    priceStr,
                    style: styles.pageTextMedium,
                  ),
                  Text(
                    totalPriceStr,
                    style: styles.pageTextMediumHeavy,
                  ),
                  SizedBox(width: 20),
                ],
              ),
              //SizedBox(height: 35),
              Spacer(),
//              Spacer(),
              Row(
                children: <Widget>[
                  SizedBox(width: numBtnSpace),
                  DialogButton(
                    flex: 3,
                    ttl: lang.goBack,
                    onPressed: onCancel
                  ),
                  Expanded(child: SizedBox(width: 1), flex: 1),
                  DialogButton(
                    flex: 3,
                    ttl: lang.book,
                    onPressed: onConfirm
                  ),
                  SizedBox(width: numBtnSpace),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}