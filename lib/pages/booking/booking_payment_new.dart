import 'dart:io';
import 'package:dockside/pages/common/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'booking_complete.dart';
//import 'booking_page.dart';
import '../../api/api.dart';
import '../../bloc/global_state.dart';
import 'package:dockside/styles/text_styles.dart';

class BookingPaymentPage extends StatefulWidget {

  BookingPaymentPage({Key key}) : super(key: key);

  @override
  _BookingPaymentPage createState() => _BookingPaymentPage();

}

class _BookingPaymentPage extends State<BookingPaymentPage> {

  final Api api = new Api();
  final GlobalState _store = GlobalState.instance;
  final MyStyles _styles = new MyStyles();

  void _showToast(String error, BuildContext context) => Toast.show(
    error.toString(), context,
    backgroundColor: Colors.red,
    duration: _styles.toastDuration,
    gravity: _styles.toastGravity
  );

  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        print('_dataError, SocketException, error.message: ${error.message}');
        _showToast(error.message, context);
      }
    } else {
      print('_dataError, error.message: ${error.message}');
      _showToast(error.toString(), context);
    }
  }

  void _cancelSuccess(value, BuildContext context){
    // print('_cancelSuccess: $value');
    Navigator.of(context).pop();
  }
  void _cancelBooking(BuildContext context){
    Future result = api.cancelBooking(_store.bookingId, _store.apiToken);
    result.then((value) => _cancelSuccess(value, context)).catchError((error) => _dataError(error, context));
  }

  @override
  void initState() {
    super.initState();
    print('_store.bookingPaymentLink: ${_store.bookingPaymentLink}');
    final webView = FlutterWebviewPlugin();
    webView.onUrlChanged.listen((String url) {
      print(":::::::::::::::URL:$url::::::::::::::::::::::::");
      if (url.contains("payment-cancel-callback")) {
        _cancelBooking(context);
      } else {
        if(url.endsWith("api.docksideapp.dk/") || url.endsWith("api.dock.vconnect.systems/")){
          print(":::::::::::::::SUCCESS!!!::::::::::::::::::::::::");
          Navigator.of(context).pop();
          Navigator.push(context, CupertinoPageRoute(builder: (context) => BookingCompletePage()));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context){
    print('_store.bookingPaymentLink: ${_store.bookingPaymentLink}');
    return WebviewScaffold (
      withJavascript: true,
      appCacheEnabled: true,
      url: _store.bookingPaymentLink,
      withLocalStorage: true,
      hidden: true,
      initialChild: Center(
        child: Container(
          width: 48, height: 48,
          padding: EdgeInsets.all(5),
          child: CircularProgressIndicator(
            strokeWidth: 3.5,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1A3B62))
          ),
        ),
      ),
    );
  }
}
