import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../api/harbour.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/icon_font_icons.dart';

class SearchHarbourPage extends StatelessWidget {

  final MyStyles styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final double space = 10.0;
  final double bigSpace = 20.0;
  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();

  SearchHarbourPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);

    List<Harbour> searchResults = [];
    controller.text = '';

    void searchUpdate(){
      searchResults.removeRange(0, searchResults.length);
      String text = controller.text;
      if(text.length > 1){
        for(int i = 0; i<_store.harbours.length; i++){
          Harbour harbour = _store.harbours[i];
          String hName = harbour.name;
          if(hName.toLowerCase().contains(text.toLowerCase())){
            searchResults.add(harbour);
          }
        }
      }
      _bloc.updateSearchResults(searchResults);
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: styles.bgDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: lang.search.toUpperCase(),
                subTitle: lang.harbours.toUpperCase(),
                onPressed: (){
                  focusNode.unfocus();
                  Navigator.pop(context, -1);
                },
              ),
            ),
            SizedBox(height: 16),
            Expanded(
              child: DecoratedBox(
                decoration: styles.pageDecoration,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 6),
                    Row (
                      children: <Widget>[
                        SizedBox(width: bigSpace),
                        Icon(IconFont.search_icon,
                          color: Color(0xFF1A3B62),
                          size: 24
                        ),
                        SizedBox(width: space),
                        Container(width: 1,
                          height: 36,
                          color: Color(0xFF1A3B62)
                        ),
                        SizedBox(width: space),
                        Flexible(
                          child: TextFormField(
                            focusNode: focusNode,
                            onChanged: (text) {
                              searchUpdate();
                            },
                            controller: controller,
                            autofocus: true,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.search,//isPass ? TextInputAction.go : TextInputAction.next,
                            autocorrect: false,
                            onFieldSubmitted: (term){
                              focusNode.unfocus();
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: lang.search.toLowerCase(),
                              hintStyle: styles.textInputHintStyle,
                              labelStyle: styles.textInputLabelStyle,
                            ),
                            style: styles.textInputStyle,
                          ),
                        ),
                        SizedBox(width: space)
                      ],
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 1,
                      decoration: BoxDecoration(
                        color: Color(0x591A3B62), // 59 = 35%
                      ),
                    ),
                    Expanded(
                      child: StreamBuilder<List<Harbour>>(
                        stream: _bloc.searchResults,
                        builder: (context, snapshot) {
                          return ListView.builder(
                            padding: EdgeInsets.all(0),
                            itemCount: snapshot.hasData ? snapshot.data.length : 0,//searchResults.length,
                            itemBuilder: (BuildContext cont, int index) {
                              return InkWell(
                                splashColor: Colors.white70,
                                onTap: (){
                                  focusNode.unfocus();
                                  Navigator.pop(context, snapshot.data[index].ind);
                                  //Navigator.of(context).pop();
                                },
                                child: Container(
                                  height: 57,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(left: 20, right: 20, top: 13, bottom: 13),
                                        child: Text(
                                          snapshot.hasData ? snapshot.data[index].name : '',
                                          style: styles.listItemStyle,
                                        ),
                                      ),
                                      Container(
                                        height: 1,
                                        color: Color(0x291A3B62) // 59 = 35%
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }
                          );
                        }
                      ),
                    )
                  ]
                ),
              )
            ),
          ]
        ),
      )
    );
  }
}
