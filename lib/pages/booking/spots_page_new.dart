// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:intl/intl.dart';
// import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
// import 'dart:ui';
// import '../../api/api.dart';
// import '../../api/language.dart';
// import '../../api/spot.dart';
// import '../../bloc/bloc.dart';
// import '../../bloc/provider.dart';
// import '../../bloc/global_state.dart';
// import '../../styles/text_styles.dart';
// import '../common/icon_font_icons.dart';
// import '../common/progress_dialog.dart';
// import 'zoom_widget.dart';
// import 'selection_dialog.dart';
// import 'available_dialog.dart';
// import 'booking_confirm_page.dart';
//
// class SpotsPage extends StatefulWidget {
//
//   final String svg;
//
//   SpotsPage({Key key, @required this.svg}) : super(key: key);
//
//   @override
//   _SpotsPage createState() => _SpotsPage();
//
// }
//
// class _SpotsPage extends State<SpotsPage> {
//
//   final Api api = new Api();
//   final GlobalState _store = GlobalState.instance;
//   final MyStyles styles = new MyStyles();
//   final DateFormat apiDateFormat = DateFormat('yyyy-MM-dd');
//
//   double _stageW;
//   double _stageH;
//   Bloc _bloc;
//   Future<List<Widget>> _stackFuture;
//   Language lang;
//   DateFormat dateFormat;
//   // Init Popup
//   String initMessage;
//   // Spot Popup
//   String spotPopTtl;
//   String spotPopMess;
//   double spotPopPrice;
//   int spotPopSpotId;
//
//   int clickableButtonsCount = 0;
//
//   void showInitMessage(){
//     if(clickableButtonsCount == 0){
//       initMessage = lang.noAvailable;
//     }else{
//       initMessage = '${lang.spotsForPeriod1}$clickableButtonsCount${lang.spotsForPeriod2}:\n'
//           '${dateFormat.format(_store.bookingCheckIn)} - ${dateFormat.format(_store.bookingCheckOut)}';
//     }
//     _bloc.showSpotsPopup(1);
//   }
//
//   void _doubleCheckSpot(){
//     _bloc.showSpotsPopup(0);
//     Navigator.push(context,
//       CupertinoPageRoute(builder: (context) => BookingConfirmPage()),
//     );
//   }
//
//   void btnClick(int btnIndex, String btnCode, BuildContext context){
//     //
//     Spot sp = api.getSpotFromKey(btnCode);
//     // int hours = _store.bookingCheckOut.difference(_store.bookingCheckIn).inHours;
// //    int days = (hours/24).ceil();
//     //
//     _store.bookingSpotCode = btnCode;
//     _bloc.clickSpot(btnCode);
//     //
//     spotPopTtl = btnCode;
//     spotPopMess = '${lang.maxSize}${sp.boatLength} x ${sp.boatWidth} m';
//     spotPopPrice = sp.price;//double.parse((sp.price).toString()) * days;
//     _store.bookingTotalPrice = spotPopPrice.toString();
//     spotPopSpotId = sp.id;
//     _bloc.showSpotsPopup(2);
//   }
//
//   void makeACall(String phoneNum) async{
//     if(await UrlLauncher.canLaunch(phoneNum)){
//       await UrlLauncher.launch(phoneNum);
//     } else {
//       throw lang.callError;
//     }
//   }
//
//   @override
//   void initState() {
//
//     super.initState();
//
//     _stackFuture = Future<List<Widget>>.delayed(Duration.zero,() {
//       lang = Language.of(context);
//       _bloc = Provider.of(context);
//       dateFormat = DateFormat('dd.MM.yyyy', lang.lang);
//       _stageW = MediaQuery.of(context).size.width;
//       _stageH = MediaQuery.of(context).size.height;
//
//       _bloc.showSpotsPopup(0);
//
//       List<Widget> _stack = [];
//
//       _stack.add(
//           SvgPicture.asset(widget.svg, placeholderBuilder: (BuildContext context) =>
//               Container(
//                   child: Center(
//                       child: Text(
//                         lang.loading,
//                         style: TextStyle(
//                           fontFamily: "AvenirBook",
//                           fontWeight: FontWeight.w300,
//                           color: styles.commonColor,
//                           fontSize: 140.0,
//                         ),
//                       )
//                   )
//               )
//           )
//       );
//
//       for(int p = 0; p<_store.dSpots.length; p++){
//         StreamBuilder<String> streamBuilder = new StreamBuilder<String>(
//             stream: _bloc.spotClick,
//             builder: (context, snapshot) {
//               String svg = _store.dSpots[p].svg;
//               if(snapshot.hasData){
//                 if(snapshot.data == _store.dSpots[p].key){
//                   svg = svg.replaceFirst('rgb(0,150,64)', 'rgb(255,153,0)');
//                 } else if(snapshot.data == '-1'){
//                   svg = svg.replaceFirst('rgb(255,153,0)', 'rgb(0,150,64)');
//                 }
//               }
//               return SvgPicture.string(svg);
//             }
//         );
//         _stack.add(streamBuilder);
//       }
//
//       for(int i = 0; i<_store.dSpots.length; i++){
//         if(_store.dSpots[i].clickable){
//           clickableButtonsCount++;
//           double btnSize = 8;
//           double btnSizeHalf = 4;
//           for(int b = 0; b<_store.dSpots[i].x.length; b++ ){
//             Widget _btn = Positioned(
//               left: (_store.dSpots[i].x[b]-btnSizeHalf) * 6,
//               top: (_store.dSpots[i].y[b]-btnSizeHalf) * 6,
// //              left: (_store.dSpots[i].x-btnSizeHalf) * 6,
// //              top: (_store.dSpots[i].y-btnSizeHalf) * 6,
//               width: btnSize * 6,
//               height: btnSize * 6,
//               child: Material(
//                 color: Colors.transparent,
//                 type: MaterialType.circle,
//                 child: InkWell(
//                   splashColor: Colors.white38,
//                   borderRadius: BorderRadius.circular(btnSizeHalf * 6),
//                   onTap: (){
//                     btnClick(i, _store.dSpots[i].key, context);
//                   },
//                 ),
//               ),
//             );
//             _stack.add(_btn);
//           }
//         }
//       }
//
//       return _stack;
//
//     });
//
//     Future.delayed(Duration(seconds: 1),() {
//       showInitMessage();
//     });
//
//   }
//
//   @override
//   Widget build(BuildContext context){
//
//     return Scaffold(
//       resizeToAvoidBottomInset: true,
//       backgroundColor: Color(0xFFDAEEF3),
//       body: Container(
//         width: _stageW,
//         height: _stageH,
//         decoration: BoxDecoration(
//           color: Color(0xFFDAEEF3),
//         ),
//         child: Material(
//           color: Color(0xFFDAEEF3),
//           type: MaterialType.canvas,
//           child: Stack(
//             children: <Widget>[
//               FutureBuilder<List<Widget>>(
//                   future: _stackFuture,
//                   builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
//                     if (snapshot.hasData) {
//                       return Container(
//                         width: _stageW,
//                         height: _stageH,
//                         child: Zoom(
//                           width: 6300,
//                           height: 7800,
//                           canvasColor: Color(0xFFDAEEF3),
//                           backgroundColor: Color(0xFFDAEEF3),
//                           zoomSensibility: 20,
//                           initZoom: 0,
//                           child: Center(
//                               child: Stack(
//                                   fit: StackFit.expand,
//                                   children: snapshot.data
//                               )
//                           ),
//                         ),
//                       );
//                     } else {
//                       return Container(
//                           child: Center(
//                               child: SizedBox(width: 1, height: 1)
//                           )
//                       );
//                     }
//                   }
//               ),
//               Positioned(
//                 top: 32,
//                 right: 10,
//                 child: IconButton(
//                   icon: Icon(
//                       IconFont.closex,
//                       color: styles.commonColor,
//                       size: 16
//                   ),
//                   onPressed: (){
//                     Navigator.of(context).pop();
//                   },
//                 ),
//               ),
//               FutureBuilder<List<Widget>>(
//                   future: _stackFuture,
//                   builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
//                     if (snapshot.hasData) {
//                       return StreamBuilder<int>(
//                           stream: _bloc.spotsPopupShow,
//                           builder: (context, snapshot) {
//                             if(snapshot.hasData && (snapshot.data > 0) && (snapshot.data < 3)){
//                               return GestureDetector(
//                                 onTap: (){
//                                   if(snapshot.data == 2){
//                                     _bloc.clickSpot('-1');
//                                   }
//                                   _bloc.showSpotsPopup(0);
//                                 },
//                                 child: Container(
//                                   color: Colors.white30,
//                                 ),
//                               );
//                             } else {
//                               return SizedBox(width: 1, height: 1);
//                             }
//                           }
//                       );
//                     } else {
//                       return SizedBox(width: 1, height: 1);
//                     }
//                   }
//               ),
//               FutureBuilder<List<Widget>>(
//                   future: _stackFuture,
//                   builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
//                     if (snapshot.hasData) {
//                       return StreamBuilder<int>(
//                           stream: _bloc.spotsPopupShow,
//                           builder: (context, snapshot) {
//                             if(snapshot.hasData){
//                               if(snapshot.data == 1){
//                                 print('AvailableDialog...');
//                                 return AvailableDialog(
//                                   message: initMessage,
//                                   spotsNum: clickableButtonsCount,
//                                   onCancel: (){
//                                     _bloc.showSpotsPopup(0);
//                                   },
//                                   onCall: (){
//                                     makeACall('tel:${_store.harbours[_store.bookingHarbourIndex].phone}');
//                                   },
//                                 );
//                               } else if(snapshot.data == 2){
//                                 return SelectionDialog(
//                                   ttl: spotPopTtl,
//                                   message1: spotPopMess,
//                                   message2: lang.availableFor,
//                                   priceStr: lang.totalPrice,
//                                   totalPrice: spotPopPrice,
//                                   onConfirm: (){
//                                     print('spotPopSpotId: $spotPopSpotId');
//                                     _store.bookingSpotId = spotPopSpotId;
//                                     _doubleCheckSpot();
//                                   },
//                                   onCancel: (){
//                                     _bloc.clickSpot('-1');
//                                     _bloc.showSpotsPopup(0);
//                                   },
//                                 );
//                               } else if(snapshot.data == 3){
//                                 return ProgressDialog();
//                               } else if(snapshot.data == 4){
//                                 return AvailableDialog(
//                                   message: lang.alreadyBooked,
//                                   spotsNum: -1,
//                                   onCancel: (){
//                                     _bloc.clickSpot('-1');
//                                     _bloc.showSpotsPopup(0);
//                                   },
//                                   onCall: (){
//                                     makeACall('tel:${_store.harbours[_store.bookingHarbourIndex].phone}');
//                                   },
//                                 );
//                               } else {
//                                 return SizedBox(width: 1, height: 1);
//                               }
//                             }else{
//                               return SizedBox(width: 1, height: 1);
//                             }
//                           }
//                       );
//                     } else {
//                       return SizedBox(width: 1, height: 1);
//                     }
//                   }
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
