import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';

class BoatTypeDialog extends StatelessWidget {

  final String ttl;
  final List<String> names;
  final String hint;
  final int initInd;

  final double numBtnHeight = 41.0;
  final double numBtnSpace = 12.0;

  final MyStyles styles = new MyStyles();

  BoatTypeDialog({Key key,
    @required this.ttl,
    @required this.names,
    this.hint = '',
    @required this.initInd}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final bool hinted = (hint != '');
    final double dialogHeight = (hinted ? 300 : 240);
    final List<Center> itemsList = [];

    int _initIndex = initInd;
    int _selectedIndex;

    for(int i=0; i<names.length; i++){
      Text txt = new Text(names[i], style: styles.appleDateText);
      Center center = new Center(child: txt);
      itemsList.add(center);
    }

    void updateValue(){
      if(_selectedIndex == null){
        _selectedIndex = _initIndex;
      }
      int newValue = _selectedIndex;
      _bloc.updateBoatType(newValue);
    }

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 16),
              Text(ttl, style: styles.pageLineText, textAlign: TextAlign.center),
              SizedBox(height: numBtnSpace),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              Expanded(
                child: CupertinoPicker(
                  backgroundColor: Colors.white,
                  itemExtent: 36,
                  onSelectedItemChanged: (int index) {
                    _selectedIndex = index;
                  },
                  children: itemsList,
                  scrollController: FixedExtentScrollController(initialItem: _initIndex),
                ),
              ),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              SizedBox(height: (hinted ? 12 : 5)),
              Container(
                height: (hinted ? 80 : 2),
                padding: EdgeInsets.only(left:16, right: 16),
                child: (hinted ? Center(
                  child: SelectableText.rich(
                    TextSpan(
                        style: styles.popupHintText,
                        children: <TextSpan>[
                          TextSpan(text: lang.hint, style: styles.popupHintBoldText),
                          TextSpan(text: hint)
                        ]
                    ),
                    textAlign: TextAlign.center,
                    maxLines: null,
                  ),
                ) : SizedBox(width: 1, height: 1)),
              ),
              SizedBox(height: (hinted ? 12 : 5)),
              Row(
                children: <Widget>[
                  Expanded(
                      child: Center(
                        child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(IconFont.exti2,
                              size: 36,
                              color: styles.commonColor,
                            ),
                            onPressed: (){
                              Navigator.of(context).pop();
                            }
                        ),
                      )
                  ),
                  Expanded(
                      child: Center(
                        child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(IconFont.check22,
                              size: 36,
                              color: styles.commonColor,
                            ),
                            onPressed: (){
                              updateValue();
                              Navigator.of(context).pop();
                            }
                        ),
                      )
                  ),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}