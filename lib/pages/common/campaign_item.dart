import 'package:flutter/material.dart';
import 'package:dockside/api/campaign.dart';
import 'package:dockside/styles/text_styles.dart';

class CampaignItem extends StatefulWidget {

  final double w;
  final List<Campaign> campaigns;
  final bool lines;

  CampaignItem({Key key,
    @required this.w,
    @required this.campaigns,
    this.lines = true,
  }): super(key:key);

  @override
  _CampaignItemState createState() => _CampaignItemState();
}

class _CampaignItemState extends State<CampaignItem> {

  static const double _hPadding = 16;
  final MyStyles _styles = new MyStyles();
  final PageController _pageController = new PageController();
  final Duration _campDur = new Duration(seconds: 4);
  final Duration _dur = new Duration(milliseconds: 300);
  final Curve _cur = Curves.easeOut;
  final double _vp = 10;
  // final double _h = 130;
  double _h;
  List<Widget> _pages = [];
  int _currentPageIndex = 0;
  bool _disposed = false;

  void _animateToNextPage(){
    if(!_disposed){
      // print('_currentPageIndex: $_currentPageIndex');
      if(_currentPageIndex < _pages.length-1){
        _currentPageIndex++;
      } else {
        _currentPageIndex = 0;
      }
      // if(_currentPageIndex > _pages.length-1){
      //   _currentPageIndex = 0;
      //   _pages = [];
      //   for(int i = 0; i < widget.campaigns.length; i++){
      //     _pages.add(Container(height: _h, child: Image.network(widget.campaigns[i].image, fit: BoxFit.cover)));
      //   }
      //   setState(() {});
      // }
      _pageController.animateToPage(_currentPageIndex, duration: _dur, curve: _cur);
      Future.delayed(_campDur,() => _animateToNextPage());
    }
  }

  void _getCampaigns(){
    _pages = [];
    for(int i = 0; i < widget.campaigns.length; i++){
      _pages.add(Container(height: _h, child: Image.network(widget.campaigns[i].image, fit: BoxFit.cover)));
    }
    if(_pages.length > 1) Future.delayed(_campDur,() => _animateToNextPage());
  }

  @override
  void initState() {
    _h = (widget.w - (_hPadding*2))/3;
    super.initState();
  }

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _getCampaigns();

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: _hPadding, vertical: 8),
      child: Column(
        children: [
          widget.lines ? Container(
            height: 1,
            color: _styles.commonColor.withOpacity(.32),
          ) : SizedBox.shrink(),
          widget.lines ? SizedBox(height: _vp) : SizedBox.shrink(),
          Container(
            height: _h,
            child: PageView(
              controller: _pageController,
              children: _pages,
            ),
          ),
          widget.lines ? SizedBox(height: _vp) : SizedBox.shrink(),
          widget.lines ? Container(
            height: 1,
            color: _styles.commonColor.withOpacity(.32),
          ) : SizedBox.shrink(),
        ],
      ),
    );
  }
}

// class SingleCampaign extends StatelessWidget {
//
//   final Campaign campaign;
//   final double h;
//
//   SingleCampaign({Key key, @required this.campaign, @required this.h}): super(key:key);
//
//   final MyStyles _styles = new MyStyles();
//   final double _p = 8;
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: h,
//       color: Colors.white,//.withOpacity(.4),
//       padding: EdgeInsets.only(left: 12, right: _p, top: _p, bottom: _p),
//       child: Row(
//         children: [
//           Expanded(
//             child: Column(
//               children: [
//                 Row(
//                   children: [
//                     Expanded(
//                       child: Text(
//                         campaign.name,
//                         style: _styles.textPageBodyBold,
//                         overflow: TextOverflow.ellipsis,
//                       )
//                     ),
//                   ],
//                 ),
//                 SizedBox(height: 1),
//                 Expanded(
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Expanded(
//                         child: Text(
//                           campaign.text,
//                           style: _styles.textPageBody,
//                           maxLines: null
//                         )
//                       ),
//                     ],
//                   )
//                 ),
//               ],
//             ),
//           ),
//           SizedBox(width: _p),
//           Container(
//             width: h-(_p*2),
//             height: h-(_p*2),
//             child: Image.network(
//               campaign.image,
//               fit: BoxFit.contain
//             )
//           )
//         ],
//       ),
//     );
//   }
//
// }