import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class FocusKiller extends StatefulWidget {

  final Widget child;

  FocusKiller({this.child});

  @override
  _State createState() => _State();
}

class _State extends State<FocusKiller> with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerUp: (PointerUpEvent pointerUpEvent) {
        RenderBox rb = context.findRenderObject() as RenderBox;
        BoxHitTestResult result = BoxHitTestResult();
        rb.hitTest(result, position: pointerUpEvent.position);
        bool isEditable = result.path.any((HitTestEntry entry) =>
        entry.target.runtimeType == RenderEditable || entry.target.runtimeType == RenderParagraph);
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!isEditable) {
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        }
      },
      child: widget.child,
    );
  }
}
