import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter/cupertino.dart';
import 'dart:ui';
import 'package:dockside/api/language.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/common/dialog_button.dart';

class DeviceTokenDialog extends StatelessWidget {

  final String title;
  final String body;

  final MyStyles styles = new MyStyles();

  DeviceTokenDialog({Key key,
    @required this.title,
    @required this.body,
  }): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);

    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: UnconstrainedBox(
          child: Container(
            width: MediaQuery.of(context).size.width-52,
            decoration: BoxDecoration(
              color: styles.dialogBackgroundColor,
              borderRadius: styles.dialogBorderRadius,
              boxShadow: [styles.dialogShadow],
            ),
            padding: EdgeInsets.only(top: 18, left: 18, right: 18, bottom: 18),
            child: Column(
              children: <Widget>[
                Row(
                  children: [
                    Flexible(child: SelectableText(
                      title,
                      textAlign: TextAlign.left,
                      style: styles.textPageTitle,
                      maxLines: null,
                    ))
                  ],
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    Flexible(child: SelectableText(
                      body,
                      textAlign: TextAlign.left,
                      style: styles.pageTextMedium,
                      maxLines: null,
                    ))
                  ],
                ),
                SizedBox(height: 24),
                Row(
                  children: <Widget>[
                    DialogButton(
                      ttl: 'Copy',
                      onPressed: (){
                        Clipboard.setData(ClipboardData(text: body));
                      }
                    ),
                    Spacer(),
                    DialogButton(
                      ttl: _lang.ok,
                      onPressed: () => Navigator.of(context).pop()
                    ),
                    // Spacer(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
