import 'package:flutter/material.dart';
import '../../api/language.dart';
import 'page_title.dart';

class MessageDialog extends StatelessWidget {

  final StringBuffer stringBuffer;
  final String ttl;
  final TextEditingController controller;
  final Function(String message) onSend;

  MessageDialog({Key key,
    @required this.stringBuffer,
    @required this.ttl,
    @required this.controller,
    @required this.onSend}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 40, bottom: 20),
      child:
      Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 382,//(MediaQuery.of(context).size.height/10)*6,
            //400
            decoration: BoxDecoration(
              color: Colors.white70,
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            child: Column(
              children: <Widget>[
                Material(
                  color: Colors.transparent,
                  type: MaterialType.transparency,
                  child: PageTitle(
                    noX: false,
                    title: '${lang.to}$ttl',
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  type: MaterialType.transparency,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20, top: 16, bottom: 16),
                    child: TextField(
                      //textInputAction: TextInputAction.send,
                      controller: controller,
                      autofocus: true,
                      decoration: null,
                      keyboardType: TextInputType.multiline,
                      maxLines: 12,
                    ),
                  ),
                ),
                Container(
                  constraints: BoxConstraints.expand(height: 1),
                  margin: EdgeInsets.only(top: 1, bottom: 1),
                  decoration: BoxDecoration(
                      color: Color(0xFF1A3B62).withOpacity(.35)
                  ),
                ),
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 16,),
                    Expanded(
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xFF1A3B62),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          type: MaterialType.transparency,
                          child: InkWell(
                            splashColor: Colors.white12,
                            borderRadius: BorderRadius.circular(8),
                            child: Center(
                              child: Text(
                                lang.cancel,
                                style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontFamily: "Neutra2Book",
                                  fontWeight: FontWeight.w300,
                                  fontSize: 18.0
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                    ),
                    Expanded(child: SizedBox(width: 1,),),
                    Expanded(
                      child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xFF1A3B62),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          type: MaterialType.transparency,
                          child: InkWell(
                            splashColor: Colors.white12,
                            borderRadius: BorderRadius.circular(8),
                            child: Center(
                              child: Text(
                                lang.send,
                                style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontFamily: "Neutra2Book",
                                  fontWeight: FontWeight.w300,
                                  fontSize: 18.0
                                ),
                              ),
                            ),
                            onTap: () => onSend(controller.text),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16,),
                  ],
                )
              ],
            )
          ),
        ],
      ),
    );
  }
}