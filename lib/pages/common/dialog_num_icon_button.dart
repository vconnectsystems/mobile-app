import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';

class DialogNumIconButton extends StatelessWidget {

  final double bHeight = 40;
  final double iconSize = 22;
  final IconData icon;
  final GestureTapCallback onPressed;
  final MyStyles styles = new MyStyles();

  DialogNumIconButton({Key key,
    @required this.icon,
    @required this.onPressed}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Expanded(
      child: Container(
        height: bHeight,
        decoration: styles.dialogNumButtonDecoration,
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: InkWell(
            splashColor: Colors.white12,
            borderRadius: BorderRadius.circular(8),
            child: Icon(
                icon,
                size: iconSize,
                color: styles.commonColor
            ),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }
}