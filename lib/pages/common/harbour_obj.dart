import '../harbour/harbour_services_obj.dart';

class HarbourObj {
  int harbourId;
  String title;
  List<String> photos;
  HarbourServicesObj servicesObj;
  double lat;
  double lng;
  String locN;
  String locE;
  String master;
  String address1;
  String address2;
  String country;
  String phone;
}