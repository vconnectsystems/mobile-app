import 'package:flutter/material.dart';
import 'icon_font_icons.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../api/harbour.dart';

class SearchDialog extends StatelessWidget {

  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();
  final Function(int harbourId) onPressed;
  final double space = 10.0;

  SearchDialog({Key key, @required this.onPressed}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final GlobalState _store = GlobalState.instance;

    List<Harbour> searchResults = [];

    void searchUpdate(){
      searchResults.removeRange(0, searchResults.length);
      String text = controller.text;
      if(text.length > 1){
        for(int i = 0; i<_store.harbours.length; i++){
          Harbour harbour = _store.harbours[i];
          String hName = harbour.name;
          if(hName.toLowerCase().contains(text.toLowerCase())){
            searchResults.add(harbour);
          }
        }
      }
      _bloc.updateSearchResults(searchResults);
    }

    controller.addListener(() {
      searchUpdate();
    });

    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 20),
      child: Column(
       children: <Widget>[
         Container(
           width: MediaQuery.of(context).size.width,
           height: 400,
           decoration: BoxDecoration(
             color: Colors.white70,
             borderRadius: BorderRadius.all(Radius.circular(15)),
           ),
           child: Column(
             children: <Widget>[
               SizedBox(height: 6),
               Material(
                 color: Colors.transparent,
                 type: MaterialType.transparency,
                 child: Row (
                   children: <Widget>[
                     SizedBox(width: space),
                     SizedBox(width: space),
                     Icon(IconFont.search_icon,
                       color: Color(0xFF1A3B62),
                       size: 24
                     ),
                     SizedBox(width: space),
                     Container(width: 1,
                       height: 36,
                       color: Color(0xFF1A3B62)
                     ),
                     SizedBox(width: space),
                     Flexible(
                       child: TextFormField(
                         controller: controller,
                         //autofocus: true,
                         keyboardType: TextInputType.text,
                         textInputAction: TextInputAction.done,//isPass ? TextInputAction.go : TextInputAction.next,
                         autocorrect: false,
                         autofocus: true,
                         onFieldSubmitted: (term){
                           focusNode.unfocus();
                         },
                         decoration: InputDecoration(
                           border: InputBorder.none,
                           hintText: lang.search.toLowerCase(),
                           hintStyle: Theme.of(context).textTheme.bodyText2.apply(color: Colors.black26),
                           labelStyle: TextStyle(
                             color: Colors.black87,
                             fontFamily: "Neutra2Text",
                             fontSize: 24.0
                           ),
                         ),
                         style: Theme.of(context).textTheme.bodyText2,
                         //(dataProvider.action == "asdf" ? srt : dataProvider.action),
                       ),
                     ),
                     SizedBox(width: space)
                   ],
                 ),
               ),
               SizedBox(height: 5),
               Container(
                 height: 1,
                 decoration: BoxDecoration(
                   color: Color(0x591A3B62), // 59 = 35%
                 ),
               ),
               Expanded(
                 child: Material(
                   color: Colors.transparent,
                   type: MaterialType.transparency,
                   child: StreamBuilder<List<Harbour>>(
                     stream: _bloc.searchResults,
                     builder: (context, snapshot) {
                       return ListView.builder(
                         padding: EdgeInsets.all(0),
                         itemCount: snapshot.hasData ? snapshot.data.length : 0,//searchResults.length,
                         itemBuilder: (BuildContext cont, int index) {
                           return InkWell(
                             splashColor: Colors.white70,
                             onTap: (){
                               onPressed(snapshot.data[index].ind);
                               Navigator.pop(context);
                             },
                             child: Container(
                               height: 56,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: <Widget>[
                                   Padding(
                                     padding: const EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 14),
                                     child: Text(
                                       snapshot.hasData ? snapshot.data[index].name : '',
                                       style: TextStyle(
                                         color: Color(0xFF1A3B62),
                                         fontFamily: "Neutra2Book",
                                         fontWeight: FontWeight.w300,
                                         fontSize: 22.0
                                       ),
                                     ),
                                   ),
                                   Container(
                                     height: 1,
                                     color: Color(0x291A3B62) // 59 = 35%
                                   )
                                 ],
                               ),
                             ),
                           );
                         }
                       );
                     }
                   ),
                 ),
               )
             ],
           ),
         )
       ],
      )
    );
  }
}
