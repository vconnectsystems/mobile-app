/// Flutter icons IconFont
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  IconFont
///      fonts:
///       - asset: fonts/IconFont.ttf
///
/// 
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
/// * Linecons, Copyright (C) 2013 by Designmodo
///         Author:    Designmodo for Smashing Magazine
///         License:   CC BY ()
///         Homepage:  http://designmodo.com/linecons-free/
/// * Linearicons Free, Copyright (C) Linearicons.com
///         Author:    Perxis
///         License:   CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
///         Homepage:  https://linearicons.com
/// * Entypo, Copyright (C) 2012 by Daniel Bruce
///         Author:    Daniel Bruce
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.entypo.com
///
import 'package:flutter/widgets.dart';

class IconFont {
  IconFont._();

  static const _kFontFam = 'IconFont';
  static const _kFontPkg = null;

  static const IconData mealnew = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData closex = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData panorama_fish_eye = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData lens = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData profile_icon = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData password_icon = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData reply_arrow = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData search_icon = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trash_light = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData checkout = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData calendar = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData washing_machine = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData boat = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sized = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sizeh = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData toilet = IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData electricity = IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData meal = IconData(0xe811, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location = IconData(0xe812, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData arrow_icon = IconData(0xe813, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData gas_station_2 = IconData(0xe814, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData map = IconData(0xe815, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData clock = IconData(0xe816, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData mess_1 = IconData(0xe817, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sizew = IconData(0xe818, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData checkin = IconData(0xe819, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData phone = IconData(0xe81a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData email = IconData(0xe81b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trash = IconData(0xe81c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData new_message2 = IconData(0xe81d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData sent2 = IconData(0xe81e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData settings = IconData(0xe81f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData zoom_out_map = IconData(0xe820, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData location_searching = IconData(0xe821, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData my_location = IconData(0xe822, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData remove_circle_outline = IconData(0xe823, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData control_point = IconData(0xe824, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData minus_circled = IconData(0xe825, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData plus_circled = IconData(0xe826, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData search = IconData(0xe827, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData check22 = IconData(0xe828, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ovalempty = IconData(0xe829, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData oval = IconData(0xe82a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dot = IconData(0xe82b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData exti2 = IconData(0xe82c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData phone3 = IconData(0xe82d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData edit2 = IconData(0xe82e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData goback2 = IconData(0xe82f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData info2 = IconData(0xe830, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData trash2 = IconData(0xe831, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData geography = IconData(0xe832, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData map_marker = IconData(0xe833, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData map_1 = IconData(0xe834, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData unfold_more = IconData(0xe835, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData calendar_full = IconData(0xe836, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dropdown = IconData(0xe837, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData move = IconData(0xe87b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData plus_circle_1 = IconData(0xe881, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData circle_minus = IconData(0xe882, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
