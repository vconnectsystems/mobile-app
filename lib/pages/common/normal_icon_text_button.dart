import 'package:flutter/material.dart';

class NormalIconTextButton extends StatelessWidget {
  final double bHeight;
  final IconData icn;
  final String ttl;
  final double fontSize;
  final GestureTapCallback onPressed;
  final int enabled;
  final double iconSize = 20.0;
  NormalIconTextButton({Key key,
    @required this.bHeight,
    @required this.icn,
    @required this.ttl,
    this.fontSize,
    @required this.onPressed,
    @required this.enabled}) : super (key : key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: bHeight,
        decoration: BoxDecoration(
          color: (enabled == 1)? Color(0xFF1A3B62) : Color(0x801A3B62),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: InkWell(
            splashColor: Colors.white12,
            borderRadius: BorderRadius.circular(8),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                      icn,
                      color: Color(0xFFFFFFFF),
                      size: iconSize
                  ),
                  SizedBox(width: 8),
                  Padding(
                    padding: const EdgeInsets.only(top: 1.0),
                    child: Text(
                      ttl,
                      style: TextStyle(
                          color: Color(0xFFFFFFFF),
                          fontFamily: "Neutra2Book",
                          fontWeight: FontWeight.w300,
                          fontSize: (fontSize != null) ? fontSize : 20.0
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: (enabled == 1)? onPressed : null,
          ),
        ),
      ),
    );
  }
}