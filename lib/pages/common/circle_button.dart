import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';

class CircleButton extends StatelessWidget {
  final IconData icon;
  final GestureTapCallback onPressed;
  final MyStyles styles = new MyStyles();
  CircleButton({Key key, @required this.icon, @required this.onPressed}): super(key:key);
  @override
  Widget build(BuildContext context){
    return IconButton(
        padding: EdgeInsets.all(0),
        icon: Icon(icon, size: 46, color: styles.commonColor),
        onPressed: onPressed
    );
  }
}
