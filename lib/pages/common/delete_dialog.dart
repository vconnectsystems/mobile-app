import 'package:flutter/material.dart';
import '../../api/language.dart';
import 'page_title.dart';

class DeleteDialog extends StatelessWidget {

  final String deleteMessage;
  final GestureTapCallback onPressed;

  DeleteDialog({Key key, @required this.deleteMessage, @required this.onPressed}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.circular(16.0)), //this right here
      child: Container(
        width: MediaQuery.of(context).size.width-50,
        height: 220,//193//(MediaQuery.of(context).size.height/10)*6,
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Column(
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: PageTitle(
                noX: true,
                title: lang.delMess,
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10, left: 20, right: 20),
              child: SelectableText(
                deleteMessage,
                enableInteractiveSelection: true,
                showCursor: false,
                style: TextStyle(
                    color: Color(0xFF1A3B62),
                    fontFamily: "Neutra2Book",
                    fontWeight: FontWeight.w300,
                    fontSize: 18.0
                ),
              ),
            ),
            SizedBox(height: 20,),
            Row(
              children: <Widget>[
                SizedBox(width: 16,),
                Expanded(
                  child: Container(
                    height: 38,
                    decoration: BoxDecoration(
                      color: Color(0xFF1A3B62),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      type: MaterialType.transparency,
                      child: InkWell(
                        splashColor: Colors.white12,
                        borderRadius: BorderRadius.circular(8),
                        child: Center(
                          child: Text(
                            lang.cancel,
                            style: TextStyle(
                                color: Color(0xFFFFFFFF),
                                fontFamily: "Neutra2Book",
                                fontWeight: FontWeight.w300,
                                fontSize: 18.0
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ),
                Expanded(child: SizedBox(width: 1,),),
                Expanded(
                  child: Container(
                    height: 38,
                    decoration: BoxDecoration(
                      color: Color(0xFF1A3B62),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      type: MaterialType.transparency,
                      child: InkWell(
                        splashColor: Colors.white12,
                        borderRadius: BorderRadius.circular(8),
                        child: Center(
                          child: Text(
                            lang.delete,
                            style: TextStyle(
                                color: Color(0xFFFFFFFF),
                                fontFamily: "Neutra2Book",
                                fontWeight: FontWeight.w300,
                                fontSize: 18.0
                            ),
                          ),
                        ),
                        onTap: onPressed,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 16,),
              ],
            )
          ],
        ),
      ),
    );
  }
}