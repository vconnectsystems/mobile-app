import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';

class DialogIconButton extends StatelessWidget {

  final double bHeight = 46;
  final IconData icon;
  final double iconSize = 22;
  final GestureTapCallback onPressed;
  final int flex;

  final MyStyles styles = new MyStyles();

  DialogIconButton({Key key,
    @required this.icon,
    @required this.onPressed, this.flex = -1}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Container(
      height: bHeight,
      decoration: styles.dialogButtonDecoration,
      child: Material(
        color: Colors.transparent,
        type: MaterialType.transparency,
        child: InkWell(
          splashColor: styles.commonColor.withOpacity(.4),//Colors.white12,
          borderRadius: styles.buttonRadius,
          child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                child: Icon(
                  icon,
                  size: iconSize,
                  color: styles.commonColor
                ),
              )
          ),
          onTap: onPressed,
        ),
      ),
    );
  }
}