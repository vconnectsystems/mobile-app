import 'package:flutter/material.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import '../../api/api.dart';
import '../../api/harbour.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/toast.dart';
import '../common/progress_dialog.dart';

class NewMessagePage extends StatelessWidget {

  final Harbour harbour;

  final double bigSpace = 20.0;
  final double space = 10.0;
  final TextEditingController controller = new TextEditingController();
  final FocusNode focusNode = FocusNode();
  final Api _api = new Api();
  final MyStyles styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;

  NewMessagePage({Key key, @required this.harbour}) : super (key : key);

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);

//    final DateTime today = new DateTime.now();
//    String formattedDate = DateFormat('dd.MM.yyyy . kk:mm').format(today);

    controller.text = '';

    void showProgress(){
      showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.white70,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont,
            Animation animation,
            Animation secondaryAnimation){
          return ProgressDialog();
        },
      );
    }

    void _success(String value, BuildContext context){
      Navigator.of(context).pop();
      if(value.contains('error') || value.contains('<html>')){
        _showToast(lang.messSendError, context);
      }else{
        _showToast(lang.messSendSuccess, context, green: true);
        Navigator.pop(context);
      }
    }
    void _error(Error error, BuildContext context){
      _showToast(error.toString(), context);
    }
    void _sendMessage(BuildContext context){
      focusNode.unfocus();
      showProgress();
      Future result = _api.sendMessage(harbour.id, controller.text, _store.apiToken);
      result.then((value) => _success(value, context)).catchError((error) => _error(error, context));
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 7),
          Row (
            children: <Widget>[
              SizedBox(width: bigSpace),
              Expanded(
                child: Text(
                  harbour.name.toUpperCase(),
                  style: styles.pageLineText
                ),
              ),
              SizedBox(width: space),
              Container(
                width: 1,
                height: 46,
              ),
              IconButton(
                  padding: EdgeInsets.all(0),
                  icon: Icon(IconFont.sent2,
                    size: 24,
                    color: styles.commonColor,
                  ),
                  onPressed: (){
                    _sendMessage(context);
                  }
              ),
              SizedBox(width: bigSpace),
            ],
          ),
          SizedBox(height: 4),
          Container(
            height: 1,
            decoration: styles.lineDecoration,
          ),
          Expanded(
            child: ListView(
              padding: const EdgeInsets.all(0),
              children: <Widget>[
                SizedBox(height: bigSpace),
//                Row(
//                  children: <Widget>[
//                    SizedBox(width: bigSpace),
//                    Expanded(
//                      child:
//                      Container(
//                        child: Text(harbour.name.toUpperCase(),
//                          style: styles.messTitleText,
//                        ),
//                      ),
//                    ),
//                    SizedBox(width: bigSpace),
//                  ],
//                ),
                Row(
                  children: <Widget>[
                    SizedBox(width: bigSpace),
                    Text(
                      lang.to,
                      style: styles.messSubTitleText,
                    ),
                    Expanded(
                      child:
                      Container(
                        child: Text(
                          harbour.master.toUpperCase(),
                          style: styles.messTitleText,
                        ),
                      ),
                    ),
                    SizedBox(width: bigSpace),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 16, bottom: 16),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    textCapitalization: TextCapitalization.sentences,
                    controller: controller,
                    focusNode: focusNode,
                    autofocus: true,
                    decoration: new InputDecoration(
                      focusedBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(
                          width: 0.0,
                          color: Color(0x591A3B62)
                        )
                      ),
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(
                          width: 0.0,
                          color: Color(0x591A3B62)
                        )
                      )
                    ),
                    maxLines: null,
                    style: styles.boxInputText,
                  ),
                )
              ],
            ),
          )
        ]
    );
  }
}
