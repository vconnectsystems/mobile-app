import 'package:flutter/material.dart';

class BookingInputRow extends StatelessWidget {

  final IconData icon;
  final double iconSize;
  final Widget widget;
  static const rowHeight = 50.0;

  BookingInputRow({Key key, @required this.icon, @required this.iconSize, @required this.widget}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: iconSize == 20 ? 20 : 17, height: rowHeight),
          Icon(icon, color: Color(0xFF1A3B62), size: iconSize),
          SizedBox(width: iconSize == 20 ? 10 : 7),
          Container(width: 1, height: 36, color: Color(0xFF1A3B62)),
          SizedBox(width: 10),
          Expanded(child: widget),
          SizedBox(width: 12)
        ],
      ),
    );
  }
}
