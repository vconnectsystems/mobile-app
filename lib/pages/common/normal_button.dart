import 'package:flutter/material.dart';

class NormalButton extends StatelessWidget {
  final double bHeight;
  final String ttl;
  final GestureTapCallback onPressed;
  NormalButton({Key key,
    @required this.bHeight,
    @required this.ttl,
    @required this.onPressed}) : super (key : key);
  @override
  Widget build(BuildContext context){
    return Expanded(
      child: Container(
        height: bHeight,
        decoration: BoxDecoration(
          color: Color(0xFF1A3B62),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: InkWell(
            splashColor: Colors.white12,
            borderRadius: BorderRadius.circular(8),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 1.0),
                child: Text(
                  ttl,
                  style: TextStyle(
                      color: Color(0xFFFFFFFF),
                      fontFamily: "Neutra2Book",
                      fontWeight: FontWeight.w300,
                      fontSize: 20.0
                  ),
                ),
              ),
            ),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }
}