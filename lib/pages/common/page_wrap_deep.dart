import 'package:flutter/material.dart';
import 'dart:async';
import 'page_obj.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../profile/profile_page.dart';
import '../dashboard/pages_enum.dart';

class PageWrapDeep extends StatelessWidget {

  final PageObj obj;
  final Widget pageWidget;
  final GestureTapCallback backToDashboard;

  PageWrapDeep({Key key, @required this.pageWidget, @required this.obj, this.backToDashboard}) : super (key : key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);

    Timer _ = new Timer(Duration(milliseconds: 500), () {
      _bloc.changePageLoaded(true);
    });

    final Widget emptyBox = new Column(
      children: <Widget>[
        Expanded(
          child: Container()
        )
      ]
    );
    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: StreamBuilder<Object>(
        stream: _bloc.pageLoaded,
        builder: (context, snapshot) {
          if(snapshot.hasData && snapshot.data){
            if(obj.pageId == Pages.profile){ // Profile
              if(obj.initIndex != null){
                return ProfilePage(backToDashboard: backToDashboard, initIndex: obj.initIndex);
              } else {
                return ProfilePage(backToDashboard: backToDashboard);
              }
            } else {
              return pageWidget;
            }
          } else {
            return emptyBox;
          }
          //return snapshot.hasData ? (snapshot.data == true ? pageWidget(backToDashboard: backToDashboard) : emptyBox) : emptyBox;
        }
      ),
    );
  }
}
