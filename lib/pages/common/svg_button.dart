
class SvgButton {
  final String key;
  final double x;
  final double y;
  SvgButton({this.key, this.x, this.y});
}
