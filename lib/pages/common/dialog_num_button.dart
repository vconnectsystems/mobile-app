import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';

class DialogNumButton extends StatelessWidget {

  final double bHeight = 40;
  final String ttl;
  final GestureTapCallback onPressed;
  final MyStyles styles = new MyStyles();

  DialogNumButton({Key key,
    @required this.ttl,
    @required this.onPressed}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Expanded(
      child: Container(
        height: bHeight,
        decoration: styles.dialogNumButtonDecoration,
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: InkWell(
            splashColor: Colors.white12,
            child: Center(child: Text(ttl, style: styles.dialogButtonText)),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }
}