import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';
import 'page_obj.dart';
import 'header.dart';
import 'page_wrap_deep.dart';

class PageWrapIn extends StatelessWidget {

  final PageObj obj;
  final GestureTapCallback onPressed;
  final MyStyles styles = new MyStyles();

  PageWrapIn({Key key, @required this.obj, @required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context){

    return DecoratedBox(
      decoration: styles.bgDecoration,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'header',
            child: Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(
                xVis: true,
                mainTitle: obj.title,
                subTitle: obj.subTitle,
                onPressed: onPressed,
              ),
            ),
          ),
          SizedBox(height: 16),
          Expanded(
            child: Hero(
              tag: obj.pageId,
              child: DecoratedBox(
                decoration: styles.pageDecoration,
                child: PageWrapDeep(pageWidget:obj.pageWidget, obj: obj, backToDashboard: onPressed),
              )
            )
          ),
        ]
      ),
    );
  }
}