import 'dart:io';

import 'package:dockside/api/country.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';

class NationalityDynamicDialog extends StatefulWidget {

  final String hint;
  final Country initCountry;

  NationalityDynamicDialog({Key key,
    this.hint = '',
    @required this.initCountry}) : super(key: key);

  @override
  _NationalityDynamicDialog createState() => _NationalityDynamicDialog();

}

class _NationalityDynamicDialog extends State<NationalityDynamicDialog> {

  final double numBtnHeight = 41.0;
  final double numBtnSpace = 12.0;

  final GlobalState _store = GlobalState.instance;
  final Api api = new Api();
  final MyStyles styles = new MyStyles();

  bool hinted;
  double dialogHeight;
//  Future<bool> initFuture;
//  Future<bool> loaded;
  Language lang;
  Bloc _bloc;
//  List<Country> countries;
  List<Center> itemsList;
  int _selectedIndex;
  Future<bool> initFuture;

  void updateValue(){
    if(_selectedIndex == null){
      _selectedIndex = widget.initCountry.ind;
    }
    _bloc.updateCountry(_store.countries[_selectedIndex]);
  }

  void _showToast(String error, BuildContext context){
    Toast.show(
        error.toString(), context,
        backgroundColor: Colors.red,
        duration: styles.toastDuration,
        gravity: styles.toastGravity
    );
  }
  void _dataError(dynamic error, BuildContext context){
    if(error.runtimeType == SocketException){
      if(error.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else {
        _showToast(error.message, context);
      }
    } else {
      _showToast(error.toString(), context);
    }
  }
  void _countriesSuccess(List<Country> list, BuildContext context){
//    countries = list;
    _store.countries = list;
    itemsList = [];
    for(int i=0; i<_store.countries.length; i++){
      Text txt = new Text(_store.countries[i].name, style: styles.appleDateText);
      Center center = new Center(child: txt);
      itemsList.add(center);
    }
    print('itemsList.length: ${itemsList.length}');
    _bloc.setLoading(false);
  }

  void getCountries(BuildContext context){
    _bloc.setLoading(true);
//    if(_store.countries.length < 1){
      String languageCode = (lang.lang == 'da') ? 'dk' : lang.lang;
      Future result = api.getCountries(languageCode);
      result.then((value) => _countriesSuccess(value, context)).catchError((error) => _dataError(error, context));
//    } else {
//      itemsList = new List<Center>();
//      for(int i=0; i<_store.countries.length; i++){
//        Text txt = new Text(_store.countries[i].name, style: styles.appleDateText);
//        Center center = new Center(child: txt);
//        itemsList.add(center);
//      }
//      _bloc.setLoading(false);
//    }
  }

  @override
  void initState() {
    super.initState();
    hinted = (widget.hint != '');
    dialogHeight = (hinted ? 420 : 340);//(hinted ? 400 : 320);
    initFuture = Future<bool>.delayed(Duration.zero,() {
      _bloc = BlocProvider.of(context);
      if(_store.countries.length > 0){
        itemsList = [];
        for(int i=0; i<_store.countries.length; i++){
          Text txt = new Text(_store.countries[i].name, style: styles.appleDateText);
          Center center = new Center(child: txt);
          itemsList.add(center);
        }
        _bloc.setLoading(false);
      } else{
        getCountries(context);
      }
      return true;
    });
  }

  @override
  Widget build(BuildContext context){

    lang = Language.of(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width-52,
        height: dialogHeight,
        decoration: BoxDecoration(
          color: styles.dialogBackgroundColor,
          borderRadius: styles.dialogBorderRadius,
          boxShadow: [styles.dialogShadow],
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: Column(
            children: <Widget>[
              SizedBox(height: 16),
              Text(lang.nationality, style: styles.pageLineText, textAlign: TextAlign.center),
              SizedBox(height: numBtnSpace),
              FutureBuilder<bool>(
                future: initFuture,
                builder: (context, snap) {
                  if(snap.hasData && snap.data){
                    return Expanded(
                      child: StreamBuilder<bool>(
                          stream: _bloc.loading,
                          builder: (context, snapshot2) {
                            if(snapshot2.hasData && !snapshot2.data || (_store.countries.length > 0)) {
                              return CupertinoPicker(
                                backgroundColor: Colors.white,
                                itemExtent: 36,
                                onSelectedItemChanged: (int index) {
                                  _selectedIndex = index;
                                },
                                children: itemsList,
                                scrollController: FixedExtentScrollController(
                                  initialItem: widget.initCountry.ind
                                ),
                              );
                            } else {
                              return Container(
                                  child: Center(
                                      child: Container(
                                        width: 48, height: 48,
                                        padding: EdgeInsets.all(5),
                                        child: CircularProgressIndicator(
                                            strokeWidth: 3.5,
                                            valueColor: new AlwaysStoppedAnimation<Color>(styles.commonColor)
                                        ),
                                      )
                                  )
                              );
                            }
                          }
                      ),
                    );
                  } else {
                    return Expanded(
                      child: Center(child: SizedBox(width: 1,),)
                    );
                  }
                }
              ),
//            Container(
//              height: 1,
//              decoration: styles.lineDecoration.copyWith(color: Colors.black26),
//            ),
              SizedBox(height: (hinted ? 12 : 5)),
              Container(
                height: (hinted ? 80 : 2),
                padding: EdgeInsets.only(left:16, right: 16),
                child: (hinted ? Center(
                  child: SelectableText.rich(
                    TextSpan(
                      style: styles.popupHintText,
                      children: <TextSpan>[
                        TextSpan(text: lang.hint, style: styles.popupHintBoldText),
                        TextSpan(text: widget.hint)
                      ]
                    ),
                    textAlign: TextAlign.center,
                    maxLines: null,
                  ),
                ) : SizedBox(width: 1, height: 1)),
              ),
              SizedBox(height: (hinted ? 12 : 5)),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.exti2,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                  Expanded(
                    child: Center(
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.check22,
                          size: 36,
                          color: styles.commonColor,
                        ),
                        onPressed: (){
                          updateValue();
                          Navigator.of(context).pop();
                        }
                      ),
                    )
                  ),
                ],
              ),
              SizedBox(height: numBtnSpace),
            ],
          ),
        ),
      ),
    );
  }
}
