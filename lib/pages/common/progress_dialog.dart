import 'package:flutter/material.dart';

class ProgressDialog extends StatelessWidget {

  ProgressDialog({Key key}): super(key:key);

  @override
  Widget build(BuildContext context){

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Material(
        color: Colors.white54,
        type: MaterialType.canvas,
        child: Center(
          child: Container(
            width: 48, height: 48,
            padding: EdgeInsets.all(5),
            child: CircularProgressIndicator(
              strokeWidth: 3.5,
              valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1A3B62))
            ),
          )
        ),
      )
    );
  }
}