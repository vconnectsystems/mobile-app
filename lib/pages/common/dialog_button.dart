import 'package:flutter/material.dart';
import '../../styles/text_styles.dart';

class DialogButton extends StatelessWidget {

  final double bHeight = 46;
  final String ttl;
  final GestureTapCallback onPressed;
  final int flex;

  final MyStyles styles = new MyStyles();

  DialogButton({Key key,
    @required this.ttl,
    @required this.onPressed, this.flex = -1}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Container(
      height: bHeight,
      decoration: styles.dialogButtonDecoration,
      child: Material(
        color: Colors.transparent,
        type: MaterialType.transparency,
        child: InkWell(
          splashColor: styles.commonColor.withOpacity(.4),//Colors.white12,
          borderRadius: styles.buttonRadius,
          child: Center(
              child: Padding(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text(
                  ttl,
                  style: styles.dialogButtonText,
                ),
              )
          ),
          onTap: onPressed,
        ),
      ),
    );
  }
}