import 'package:flutter/material.dart';
import '../dashboard/pages_enum.dart';

class PageObj {

  final Pages pageId;
  final String title;
  final String subTitle;
  final Widget pageWidget;
  final int initIndex;

  PageObj({
    this.pageId,
    this.title,
    this.subTitle,
    this.pageWidget,
    this.initIndex = 0
  });

}