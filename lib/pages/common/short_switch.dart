import 'package:flutter/material.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/styles/text_styles.dart';

class ShortSwitch extends StatefulWidget {

  final bool short;
  final void Function(bool) onChange;

  ShortSwitch({Key key,
    this.short = false,
    @required this.onChange
  }) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<ShortSwitch> {

  static const double _h = 42;
  static const double _hp = 18;
  static const double _r = 12;
  final MyStyles _styles = new MyStyles();
  Future<bool> _initFuture;
  bool _short;
  Language _lang;

  void _switch(){
    setState(() {
      _short = !_short;
    });
    widget.onChange(_short);
  }

  @override
  void initState() {
    _short = widget.short;
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _lang = Language.of(context);
      return true;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    double _w = MediaQuery.of(context).size.width;
    double _btnW = (_w-(_hp*2))/2;

    return FutureBuilder<bool>(
      future: _initFuture,
      builder: (BuildContext context, AsyncSnapshot initShot) {
        if(initShot.hasData && initShot.data){
          return Container(
            width: _w,
            height: _h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(_r)),//_styles.buttonRadius,
              border: Border.all(color: _styles.commonColor, width: 1)
            ),
            margin: const EdgeInsets.only(left: _hp, right: _hp, top: 4, bottom: 16),
            child: Stack(
              children: [
                AnimatedPositioned(
                  left: _short ? _btnW : 0, right: _short ? 0 : _btnW,
                  top: 0, bottom: 0,
                  duration: Duration(milliseconds: 200),
                  child: Container(
                    decoration: BoxDecoration(
                      color: _styles.commonColor,
                      borderRadius: BorderRadius.all(Radius.circular(_r-2)),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 1.0),
                  child: Row(
                    children: [
                      Expanded(child: Center(
                        child: Text(
                          _lang.overnight,
                          style: _short ? _styles.switchDeSelected : _styles.switchSelected,
                          overflow: TextOverflow.ellipsis,
                        )
                      )),
                      Expanded(child: Center(
                        child: Text(
                          _lang.midnight,
                          style: _short ? _styles.switchSelected : _styles.switchDeSelected,
                          overflow: TextOverflow.ellipsis,
                        )
                      )),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(child: InkWell(onTap: _switch)),
                    Expanded(child: InkWell(onTap: _switch)),
                  ],
                )
              ],
            ),
          );
        } else {
          return Container(height: _h);
        }
      }
    );
  }
}
