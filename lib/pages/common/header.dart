import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'icon_font_icons.dart';
//import 'dart:io' show Platform;
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';

class Header extends StatelessWidget {

  final bool xVis;
  final String mainTitle;
  final String subTitle;
  final GestureTapCallback onPressed;

  final GlobalState _store = GlobalState.instance;
  final MyStyles styles = new MyStyles();

  Header({Key key,
    @required this.xVis,
    @required this.mainTitle,
    @required this.subTitle,
    @required this.onPressed}): super(key:key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);

    return Container(
      height: 130,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 32,
            right: 10,
            child: xVis ? IconButton(
              icon: Icon(IconFont.closex,
                  color: Color(0xFF1A3B62),
                  size: 18
              ),
              onPressed: onPressed,
            ) : SizedBox(width: 1, height: 1),
          ),
          Positioned(
            top: 70,
            left: 26,
            child: Text(
              mainTitle, style: styles.pageTitleLight,
            ),
          ),
          Positioned(
            top: 100,
            left: 26,
            child: StreamBuilder<String>(
              stream: _bloc.nameChange,
              builder: (context, snapshot) {
                return Text(
                  subTitle == 'name' ?
                  (snapshot.hasData ? snapshot.data.toUpperCase() : _store.profile.name.toUpperCase()) : subTitle,
                  style: styles.pageTitleBold,
                );
              }
            ),
          )
        ],
      ),
    );
  }
}