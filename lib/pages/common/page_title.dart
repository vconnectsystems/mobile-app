import 'package:flutter/material.dart';
import 'icon_font_icons.dart';

class PageTitle extends StatelessWidget {
  final String title;
  final GestureTapCallback onPressed;
  final bool noX;
  final IconData icon;
  PageTitle({Key key, @required this.title, @required this.onPressed, @required this.noX, this.icon}): super(key:key);
  @override
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        SizedBox(height: 2),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 20),
            Expanded(
              child: SizedBox(
                height: 50,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 13,//top: 15,
                      child: Row(
                        children: <Widget>[
                          (icon != null) ? Icon(
                            icon,
                            size: 20,
                            color: Color(0xFF1A3B62)
                          ) : SizedBox(width: 0),
                          (icon != null) ? SizedBox(width: 6) : SizedBox(width: 0),
                          Text(
                              title,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Color(0xFF1A3B62),
                                  fontFamily: "Neutra2Book",
                                  fontWeight: FontWeight.w300,
                                  fontSize: 24.0
                              )
                          ),
                        ],
                      ),
                    )
                  ],
                  //alignment: cen,
                ),
              ),
            ),
            !noX? IconButton(
              icon: Icon(IconFont.closex,
                color: Color(0xFF1A3B62),
                size: 16
              ),
              onPressed: onPressed,
            ) : SizedBox(width: 1),
            SizedBox(width: 9),
          ],
        ),
        //SizedBox(height: 20),
        Row(
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    child: Container(
                      constraints: BoxConstraints.expand(height: 1),
                      margin: EdgeInsets.only(top: 1, bottom: 1),
                      decoration: BoxDecoration(
                        color: Color(0xFF1A3B62).withOpacity(.35)
                      ),
                    ),
                  ),
                ]
              )
            ),
          ],
        ),
      ],
    );
  }
}