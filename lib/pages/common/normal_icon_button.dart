import 'package:flutter/material.dart';

class NormalIconButton extends StatelessWidget {
  final double bHeight;
  final IconData icn;
  final GestureTapCallback onPressed;
  final double iconSize;
  NormalIconButton({Key key,
    @required this.bHeight,
    @required this.icn,
    @required this.iconSize,
    @required this.onPressed}) : super (key : key);
  @override
  Widget build(BuildContext context){
    return Expanded(
      child: Container(
        height: bHeight,
        decoration: BoxDecoration(
          color: Color(0xFF1A3B62),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.transparency,
          child: InkWell(
            splashColor: Colors.white12,
            borderRadius: BorderRadius.circular(8),
            child: Icon(
                icn,
                size: iconSize,
                color: Colors.white
            ),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }
}