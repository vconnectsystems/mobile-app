import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/icon_font_icons.dart';
import '../common/normal_icon_button.dart';
import 'master_detail_row.dart';

class MasterDetail extends StatelessWidget {

  final Map<String, dynamic> details;

  final MyStyles _styles = new MyStyles();
  static const double _spaceBetween = 1.0;

  MasterDetail({Key key, @required this.details}) : super(key: key);

  //{id: 24, boat_spot_number: 3109, date_start: 2020-06-11 00:00:00, date_end: 2020-07-19 00:00:00,
  // rent_date_start: 2020-06-12 00:00:00, rent_date_end: 2020-06-17 00:00:00, is_permanent: 1,
  // parent_id: null, note: null, status_id: 1, price: 150.00, total_price: 5700,
  // fixed_price: null, link: null, for_payment: 0, created_at: 2020-06-13 10:48:48,
  // updated_at: 2020-06-13 10:49:17, user_name: Test, user_email: test@mail.bg,
  // user_phone: , user_boat_name: Chaika, user_boat_type: null,
  // user_boat_model: null,
  // user_boat_picture: api.dock.vconnect.systems/storage/boat-pictures/j6QZi4cCi1yG3b7dk0vc.jpeg,
  // user_boat_length: 1, user_boat_width: 1, user_boat_depth: 0, harbour_id: 12, harbour_name: Tuborg}

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);
    const double _lrSpace = 20;
    final double _photoWidth = MediaQuery.of(context).size.width-(_lrSpace*2);
    final double _photoHeight = _photoWidth-(_photoWidth/2.5);
    final DateFormat _dateFormat = DateFormat('dd.MM.yyyy', _lang.lang);

    void _makeACall(String phoneNum) async{
      if(await UrlLauncher.canLaunch(phoneNum)){
        await UrlLauncher.launch(phoneNum);
      } else {
        throw _lang.callError;
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: _styles.bgDecoration,
        child: Column(
          children: <Widget>[
            Header(
              xVis: true,
              mainTitle: details['boat_spot_number'].toString(),
              subTitle: details['user_boat_name'] != null ? (details['user_boat_name']).toString().toUpperCase() : '',
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            SizedBox(height: 16),
            Expanded(
              child: DecoratedBox(
                decoration: _styles.pageDecoration,
                child: ListView(
                  padding: EdgeInsets.all(0),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: _lrSpace, left: _lrSpace, right: _lrSpace, bottom: 8),
                      child: Container(
                        width: _photoWidth,
                        height: _photoHeight,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(11)),
                          child: Image.network('http://${details['user_boat_picture'].toString()}', fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    MasterDetailRow(
                      icon: IconFont.profile_icon,
                      iconSize: 20,
                      widget: Text(details['user_name'], style: _styles.formInputStyle),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.boat,
                      iconSize: 24,
                      widget: Text(
                          details['user_boat_model'] == null ? '' : details['user_boat_model'],
                          style: _styles.formInputStyle),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.sizeh,
                      iconSize: 20,
                      widget: Text(details['user_boat_length'].toString(), style: _styles.formInputStyle),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.sizew,
                      iconSize: 20,
                      widget: Text(details['user_boat_width'].toString(), style: _styles.formInputStyle),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.checkout,
                      iconSize: 20,
                      widget: Text(
                        _dateFormat.format(DateTime.parse(details['date_start'])),
                        style: _styles.formInputStyle
                      ),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.checkin,
                      iconSize: 20,
                      widget: Text(
                        _dateFormat.format(DateTime.parse(details['date_end'])),
                        style: _styles.formInputStyle
                      ),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: Icons.attach_money,
                      iconSize: 20,
                      widget: Text(details['total_price'].toString(), style: _styles.formInputStyle),
                    ),
                    SizedBox(height: _spaceBetween),
                    MasterDetailRow(
                      icon: IconFont.phone,
                      iconSize: 20,
                      widget: Text(
                        details['user_phone'] != null? details['user_phone'].toString() : '',
                        style: _styles.formInputStyle
                      ),
                    ),
                    (details['user_phone'] != null && details['user_phone'] != '') ?
                    SizedBox(height: 8) : SizedBox(height: 0),
                    (details['user_phone'] != null && details['user_phone'] != '') ?
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Spacer(),
                        NormalIconButton(
                          bHeight: 46,
                          icn: IconFont.phone,
                          iconSize: 24,
                          onPressed: (){
                            _makeACall("tel:${details['user_phone']}");
                          }
                        ),
                        Spacer(),
                      ],
                    ) : SizedBox(height: 0),
                    (details['user_phone'] != null && details['user_phone'] != '') ?
                    SizedBox(height: 20) : SizedBox(height: 8),
                  ],
                )
              ),
            ),
          ]
        ),
      )
    );
  }
}
