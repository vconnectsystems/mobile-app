import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../styles/text_styles.dart';

class MasterDetailRow extends StatelessWidget {

  final IconData icon;
  final double iconSize;
  final Widget widget;
  static const rowHeight = 50.0;
  final MyStyles _styles = new MyStyles();

  MasterDetailRow({Key key, @required this.icon, @required this.iconSize, @required this.widget}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: Row (
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: iconSize == 24 ? 20 : 21, height: rowHeight),
          Icon(icon, color: _styles.commonColor, size: iconSize),
          SizedBox(width: iconSize == 20 ? 10 : 7),
          Container(width: 1, height: 36, color: _styles.commonColor),
          SizedBox(width: 10),
          Expanded(child: widget),
          SizedBox(width: 12)
        ],
      ),
    );
  }
}