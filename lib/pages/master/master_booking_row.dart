import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../api/language.dart';
import '../../api/master_booking.dart';
import '../../styles/text_styles.dart';

class MasterBookingRow extends StatelessWidget {

  final MasterBooking booking;
  final void Function(int) onTap;

  static const double _leadIn = 58.0;
  static const double _rowHeight = 52.0;
  static const double _lineHeight = 34.0;
  final MyStyles _styles = new MyStyles();

  MasterBookingRow({Key key, @required this.booking, @required this.onTap}) : super (key : key);

  @override
  Widget build(BuildContext context){

    Language _lang = Language.of(context);
    final DateFormat _dateFormat = DateFormat('dd/M', _lang.lang);

    return Material(
      color: Colors.transparent,
      type: MaterialType.transparency,
      child: InkWell(
        splashColor: Colors.white70,
        onTap: (){
          onTap(booking.id);
        },
        child: Row (
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: _leadIn,
              height: _rowHeight,
              alignment: Alignment.centerRight,
              child: Text(
                booking.boatSpot,
                style: _styles.masterListStyle,
              ),
            ),
            SizedBox(width: 7),
            Container(width: 1, height: _lineHeight, color: _styles.commonColor),
            SizedBox(width: 7),
            Expanded(
              child: Container(
                height: _rowHeight,
                alignment: Alignment.centerLeft,
                child: Text(
                  '${_dateFormat.format(booking.startDate)} - ${booking.boatName}',
                  style: _styles.masterListStyle,
                ),
              )
            ),
            Container(
              width: 80,
              height: _rowHeight,
              alignment: Alignment.centerRight,
              child: Text(
                booking.totalPrice.toString(),
                style: _styles.masterListStyle,
              ),
            ),
//            SizedBox(width: 12),
//            Icon(IconFont.info2, size: 24, color: _styles.commonColor),
            SizedBox(width: 14)
          ],
        ),
      ),
    );
  }
}
