import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../api/master_booking.dart';
import '../../api/api.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/header.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import '../common/progress_dialog.dart';
import 'master_booking_row.dart';
import 'master_detail.dart';

class MasterPage extends StatelessWidget {

  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final double _space = 10.0;
  final double _searchH = 36.0;
  final TextEditingController _controller = new TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final Api _api = new Api();

  MasterPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context){

    final Language _lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    List<MasterBooking> _searchResults = [];

    void _showProgress(){
      showGeneralDialog(
        context: context,
        barrierDismissible: false,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.white12,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont,
            Animation animation,
            Animation secondaryAnimation){
          return ProgressDialog();
        },
      );
    }

    void _showToast(String mess, BuildContext context){
      Toast.show(
        mess,
        context,
        backgroundColor: Colors.red,
        duration: _styles.toastDuration,
        gravity: _styles.toastGravity
      );
    }
    void _dataError(dynamic error, BuildContext context){
      Navigator.of(context).pop();
      if(error.runtimeType == SocketException){
        SocketException _err = error;
        if(_err.message.contains('Failed host lookup')){
          _showToast('Please, check your internet connectivity', context);
        } else {
          _showToast(_err.message, context);
        }
      } else {
        _showToast(error.toString(), context);
      }
    }

    void _detailsSuccess(Map<String, dynamic> details, BuildContext context){
      Navigator.of(context).pop();
      Navigator.of(context).push(
        CupertinoPageRoute(builder: (context) => MasterDetail(details: details)),
      );
    }

    void _getDetails(int bookingId){
      _focusNode.unfocus();
      _showProgress();
      Future result = _api.getMasterBookingDetails(bookingId, _store.apiToken);
      result.then((value) => _detailsSuccess(value, context)).catchError((error) => _dataError(error, context));
    }

    List<Widget> _getBookings (){
      List<Widget> bookings = [];
      if(_controller.text.length > 1){
        for(int i = 0; i<_searchResults.length; i++){
          MasterBookingRow row = new MasterBookingRow(booking: _searchResults[i], onTap: _getDetails);
          bookings.add(row);
        }
      } else {
        for(int i = 0; i<_store.masterBookings.length; i++){
          MasterBookingRow row = new MasterBookingRow(booking: _store.masterBookings[i], onTap: _getDetails);
          bookings.add(row);
        }
      }
      return bookings;
    }

    void _searchUpdate(){
      _searchResults.removeRange(0, _searchResults.length);
      String text = _controller.text;
      if(text.length > 1){
        for(int i = 0; i<_store.masterBookings.length; i++){
          MasterBooking masterBooking = _store.masterBookings[i];
          String spotName = masterBooking.boatSpot;
          String userName = masterBooking.userName;
          if(spotName.toLowerCase().contains(text.toLowerCase()) || userName.toLowerCase().contains(text.toLowerCase())){
            _searchResults.add(masterBooking);
          }
        }
      }
      _bloc.updateMasterSearchResults(true);
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: _styles.bgDecoration,
        child: Column(
          children: <Widget>[
            Header(
              xVis: true,
              mainTitle: _lang.all.toUpperCase(),
              subTitle: _lang.bookings.toUpperCase(),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            SizedBox(height: 16),
            Expanded(
              child: DecoratedBox(
                decoration: _styles.pageDecoration,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 3),
                    Row (
                      children: <Widget>[
                        SizedBox(width: 26),
                        Icon(IconFont.search_icon, color: _styles.commonColor, size: 24),
                        SizedBox(width: 15),
                        Container(width: 1, height: _searchH, color: _styles.commonColor),
                        SizedBox(width: _space),
                        Flexible(
                          child: TextFormField(
                            focusNode: _focusNode,
                            onChanged: (text) {
                              if(_controller.text.length > 0) {
                                _bloc.isSearching(true);
                              } else {
                                _bloc.isSearching(false);
                              }
                              _searchUpdate();
                            },
                            controller: _controller,
                            autofocus: false,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.search,
                            autocorrect: false,
                            onFieldSubmitted: (term){
                              _focusNode.unfocus();
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: _lang.search.toLowerCase(),
                              hintStyle: _styles.textInputHintStyle,
                              labelStyle: _styles.textInputLabelStyle,
                            ),
                            style: _styles.textInputStyle,
                          ),
                        ),
                        StreamBuilder<bool>(
                          stream: _bloc.searching,
                          builder: (context, snapshot) {
                            if(snapshot.hasData && snapshot.data){
                              return InkWell(
                                onTap: (){
                                  _bloc.isSearching(false);
                                  _controller.text = '';
                                  _searchUpdate();
                                },
                                child: Container(
                                  width: 40,
                                  height: _searchH,
                                  alignment: Alignment.center,
                                  child: Icon(IconFont.closex, color: _styles.commonColor, size: 16)
                                )
                              );
                            } else {
                              return SizedBox(width: 0);
                            }
                          }
                        ),
                        SizedBox(width: _space)
                      ],
                    ),
                    SizedBox(height: 2),
                    Container(height: 1, decoration: _styles.lineDecoration),
                    Expanded(
                      child: StreamBuilder<bool>(
                        stream: _bloc.masterSearchResults,
                        builder: (context, snapshot) {
                          return ListView(
                            padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
                            children: _getBookings(),
                          );
                        }
                      ),
                    )
                  ],
                )
              ),
            ),
          ]
        ),
      )
    );
  }
}

