import 'package:flutter/material.dart';
import '../../api/harbour.dart';

class GalleryList extends StatelessWidget {

  final Harbour harbour;
  final Function(int ind) onTap;

  GalleryList({Key key, @required this.harbour, @required this.onTap}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Container(
      height: 70,
      child: ListView.separated(
        separatorBuilder: (context, index) => SizedBox(width: 6),
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(0),
        itemCount: harbour.gallery.length,
        itemBuilder: (BuildContext cont, int index){
          String img = (harbour.gallery[index]);
          return Container(
            width: 120,
            height: 70,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Center(child: CircularProgressIndicator()),
                Image.network(img),
                Material(
                  color: Colors.transparent,
                  type: MaterialType.transparency,
                  child: InkWell(
                    splashColor: Colors.white30,
                    onTap: () => onTap(index),
                  ),
                )
              ],
            ),
          );
        }
      ),
    );
  }
}