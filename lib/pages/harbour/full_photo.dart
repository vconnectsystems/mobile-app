import 'package:flutter/material.dart';

class FullPhoto extends StatelessWidget {

  final String photo;

  FullPhoto({Key key, @required this.photo}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(
            child: CircularProgressIndicator()
          ),
          Image.network(
            photo,
//            headers: {
//              'username' : 'Test',
//              'password' : '123',
////              'Authentication' : _store.apiToken,
//            }
          ),
        ],
      ),
    );
  }
}