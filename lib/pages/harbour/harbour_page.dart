import 'package:dockside/pages/common/header.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../../api/harbour.dart';
import '../../api/language.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../booking/booking_page.dart';
import '../common/icon_font_icons.dart';
import '../common/new_message_page.dart';
import '../common/normal_icon_button.dart';
import '../common/page_obj.dart';
import '../dashboard/pages_enum.dart';
import '../dashboard/page_wrap.dart';
import '../dashboard/dashboard_transition_route.dart';
import 'gallery_list.dart';
import 'gallery_view.dart';

class HarbourPage extends StatelessWidget {

  final PageObj obj;
  final int harbourInd;

  HarbourPage({Key key, @required this.obj, @required this.harbourInd}) : super (key : key);

  static const double _icnSize = 38;
  final GlobalState _store = GlobalState.instance;
  final MyStyles _styles = new MyStyles();

  @override
  Widget build(BuildContext context){

    Language _lang = Language.of(context);
    Harbour _harbour = _store.harbours[harbourInd];

    String _neStr (){
      List<String> list = [];
      if (_harbour.locationNorth != null) list.add(_harbour.locationNorth);
      if (_harbour.locationEast != null) list.add(_harbour.locationEast);
      return list.join('\n');
    }
    String _adrStr () {
      List<String> list = [];
      if (_harbour.address != null) list.add(_harbour.address);
      if (_harbour.address2 != null) list.add(_harbour.address2);
      if (_harbour.country != null) list.add(_harbour.country);
      if (_harbour.phone != null) list.add(_harbour.phone);
      return list.join('\n');
    }

    void _makeACall(String phoneNum) async{
      if(await UrlLauncher.canLaunch(phoneNum)){
        await UrlLauncher.launch(phoneNum);
      } else throw _lang.callError;
    }

    void _showPhoto (int ind) {
      showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black87,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation){
          return GalleryView(list: _harbour.gallery, selIn: ind);
        }
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: DecoratedBox(
        decoration: _styles.bgDecoration,
        child: Column(
          children: <Widget>[
            Material(
              color: Colors.transparent,
              type: MaterialType.transparency,
              child: Header(xVis: true, mainTitle: obj.title, subTitle: obj.subTitle, onPressed: Navigator.of(context).pop),
            ),
            SizedBox(height: 16),
            Expanded(
              child: DecoratedBox(
                decoration: _styles.pageDecoration,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    (_harbour.gallery.length == 0) ? SizedBox(height: 26) : Padding(
                      padding: const EdgeInsets.only(left: 26, right: 26, top: 32, bottom: 12),
                      child: GalleryList(harbour: _harbour, onTap: _showPhoto)
                    ),
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.only(top: 6, left: 26, right: 26, bottom: 20),
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(IconFont.mealnew, size: _icnSize, color: Color(0xFF1A3B62)),
                              SizedBox(width: 14),
                              Icon(IconFont.toilet, size: _icnSize, color: Color(0xFF1A3B62)),
                              SizedBox(width: 16),
                              Icon(IconFont.washing_machine, size: _icnSize, color: Color(0xFF1A3B62)),
                              SizedBox(width: 12),
                              Icon(IconFont.electricity, size: _icnSize, color: Color(0xFF1A3B62)),
                              SizedBox(width: 12),
                              Icon(IconFont.gas_station_2, size: _icnSize, color: Color(0xFF1A3B62)),
                            ],
                          ),
                          SizedBox(height: 20),
                          _neStr() != '' ? SelectableText(_neStr(), style: _styles.pageText) : SizedBox.shrink(),
                          _neStr() != '' ? SizedBox(height: 16) : SizedBox.shrink(),
                          SelectableText(_lang.harbourMaster, style: _styles.pageTextBig),
                          SizedBox(height: 2),
                          SelectableText(_harbour.master, style: _styles.pageText),
                          SizedBox(height: 16),
                          SelectableText(_adrStr(), style: _styles.pageText),
                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(width: 26),
                        NormalIconButton(icn: IconFont.phone, iconSize: 24, bHeight: 44,
                          onPressed: () => _makeACall('tel:${_harbour.phone}')
                        ),
                        SizedBox(width: 8),
                        (_store.guest) ? SizedBox(width: (MediaQuery.of(context).size.width)/4) :
                        NormalIconButton(
                          icn: IconFont.mess_1, iconSize: 24, bHeight: 44,
                          onPressed: () {
                            Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: PageObj(
                              title: _lang.harbours.toUpperCase(),
                              subTitle: _lang.message.toUpperCase(),
                              pageId: Pages.other,
                              pageWidget: NewMessagePage(harbour: _harbour)
                            ))));
                          },
                        ),
                        SizedBox(width: 8),
                        NormalIconButton(
                          icn: IconFont.clock, iconSize: 24, bHeight: 44,
                          onPressed: (){
                            _store.bookingHarbourIndex = _harbour.ind;
                            _store.bookingHarbourType = _harbour.subscriptionType;
                            Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: PageObj(
                              title: _lang.quick.toUpperCase(),
                              subTitle: _lang.booking.toUpperCase(),
                              pageId: Pages.other,
                              pageWidget: BookingPage()
                            ))));
                          },
                        ),
                        SizedBox(width: 26),
                      ],
                    ),
                    SizedBox(height: 32),
                  ]
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
