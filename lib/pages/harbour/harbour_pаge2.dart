// import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
// import '../../api/harbour.dart';
// import '../../api/language.dart';
// import '../../bloc/global_state.dart';
// import '../../styles/text_styles.dart';
// import '../booking/booking_page.dart';
// import '../common/icon_font_icons.dart';
// import '../common/new_message_page.dart';
// import '../common/normal_icon_button.dart';
// import '../common/page_obj.dart';
// import '../dashboard/pages_enum.dart';
// import '../dashboard/page_wrap.dart';
// import '../dashboard/dashboard_transition_route.dart';
// import 'gallery_list.dart';
// import 'gallery_view.dart';
//
// class HarbourPage extends StatelessWidget {
//
//   final int harbourInd;
//   final double icnSize = 38;
//   final double icnSpace = 12;
//   final GlobalState _store = GlobalState.instance;
//
//   HarbourPage({Key key, @required this.harbourInd}) : super (key : key);
//
//   @override
//   Widget build(BuildContext context){
//
//     final Harbour harbour = _store.harbours[harbourInd];
//     final MyStyles styles = new MyStyles();
//     final Language lang = Language.of(context);
//
//     StringBuffer neSb = new StringBuffer();
//     neSb.write(harbour.locationNorth);
//     neSb.write('\n');
//     neSb.write(harbour.locationEast);
//     String neStr = neSb.toString();
//
//     StringBuffer adrSb = new StringBuffer();
//     adrSb.write('${harbour.address}');
//     adrSb.write('\n');
//     adrSb.write('${harbour.address2}');
//     adrSb.write('\n');
//     adrSb.write('${harbour.country}');
//     adrSb.write('\n');
//     adrSb.write('${harbour.phone}');
//     String adrStr = adrSb.toString();
//
//     void makeACall(String phoneNum) async{
//       if(await UrlLauncher.canLaunch(phoneNum)){
//         await UrlLauncher.launch(phoneNum);
//       } else {
//         throw lang.callError;
//       }
//     }
// //    void showMap(){
// //      PageObj pageObj = new PageObj();
// //      pageObj.pageId = Pages.other;
// //      pageObj.title = harbour.name.toUpperCase();
// //      pageObj.subTitle = 'KORT';
// //      pageObj.pageWidget = MapPage(harbourInd: harbourInd);
// //      pageObj.resize = false;
// //      Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
// //    }
//     void showPhoto (ind) {
//       showGeneralDialog(
//         context: context,
//         barrierDismissible: true,
//         barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//         barrierColor: Colors.black87,
//         transitionDuration: const Duration(milliseconds: 200),
//         pageBuilder: (BuildContext context,
//           Animation animation,
//           Animation secondaryAnimation){
//           return GalleryView(
//             list: harbour.gallery,
//             selIn: ind
//           );
//         }
//       );
//     }
// //    void sendMessage(){
// //      showGeneralDialog(
// //        context: context,
// //        barrierDismissible: true,
// //        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
// //        barrierColor: Colors.black87,
// //        transitionDuration: const Duration(milliseconds: 200),
// //        pageBuilder: (BuildContext context,
// //          Animation animation,
// //          Animation secondaryAnimation){
// //          TextEditingController controller = TextEditingController();
// //          controller.addListener(() {
// //            //print(controller.text);
// //          });
// //          return MessageDialog(
// //            ttl: harbour.name,
// //            stringBuffer: new StringBuffer(""),
// //            controller: controller,
// //            onSend: (String mess){
// //              print(mess);
// //            },
// //          );
// //        }
// //      );
// //    }
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         Padding(
//           padding: const EdgeInsets.only(left: 26, right: 26, top: 32, bottom: 12),
//           child: GalleryList(
//             harbour: harbour,
//             onTap: (int ind){
//               showPhoto(ind);
//             },
//           )
//         ),
//         Expanded(
//           child: ListView(
//             padding: const EdgeInsets.only(top: 6, left: 26, right: 26, bottom: 20),
//             children: <Widget>[
//               Row(
//                 children: <Widget>[
//                   Icon(IconFont.mealnew, size: icnSize, color: Color(0xFF1A3B62)),
//                   SizedBox(width: 14),
//                   Icon(IconFont.toilet, size: icnSize, color: Color(0xFF1A3B62)),
//                   SizedBox(width: 16),
//                   Icon(IconFont.washing_machine, size: icnSize, color: Color(0xFF1A3B62)),
//                   SizedBox(width: 12),
//                   Icon(IconFont.electricity, size: icnSize, color: Color(0xFF1A3B62)),
//                   SizedBox(width: 12),
//                   Icon(IconFont.gas_station_2, size: icnSize, color: Color(0xFF1A3B62)),
//                 ],
//               ),
//               SizedBox(height: 20),
//               SelectableText(
//                 '$neStr',
//                 textAlign: TextAlign.left,
//                 style: styles.pageText
//               ),
//               SizedBox(height: 16),
//               SelectableText(
//                 lang.harbourMaster,
//                 textAlign: TextAlign.left,
//                 style: styles.pageTextBig
//               ),
//               SizedBox(height: 2),
//               SelectableText(
//                 harbour.master,
//                 //'Thomas Sørensen',
//                 textAlign: TextAlign.left,
//                 style: styles.pageText
//               ),
//               SizedBox(height: 16),
//               SelectableText(
//                 '$adrStr',
//                 textAlign: TextAlign.left,
//                 style: styles.pageText
//               ),
//             ],
//           ),
//         ),
//         Row(
//           children: <Widget>[
//             SizedBox(width: 26),
//             NormalIconButton(
//               icn: IconFont.phone,
//               iconSize: 24,
//               bHeight: 44,
//               onPressed: (){
//                 makeACall('tel:${harbour.phone}');
//               },
//             ),
//             SizedBox(width: 8),
//             NormalIconButton(
//               icn: IconFont.mess_1,
//               iconSize: 24,
//               bHeight: 44,
//               onPressed: () {
//                 Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: PageObj(
//                   pageId: Pages.other,
//                   title: lang.harbours.toUpperCase(),
//                   subTitle: lang.message.toUpperCase(),
//                   pageWidget: NewMessagePage(harbour: harbour)
//                 ))));
//               },
//             ),
//             SizedBox(width: 8),
//             NormalIconButton(
//               icn: IconFont.clock,
//               iconSize: 24,
//               bHeight: 44,
//               onPressed: (){
//                 _store.bookingHarbourIndex = harbour.ind;
//                 _store.bookingHarbourType = harbour.subscriptionType;
//                 Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: PageObj(
//                   pageId: Pages.other,
//                   title: lang.quick.toUpperCase(),
//                   subTitle: lang.booking.toUpperCase(),
//                   pageWidget: BookingPage()
//                 ))));
//               },
//             ),
//             SizedBox(width: 26),
//           ],
//         ),
//         SizedBox(height: 32),
//       ]
//     );
//   }
// }
