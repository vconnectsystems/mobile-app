import 'package:flutter/material.dart';
import 'full_photo.dart';

class GalleryView extends StatelessWidget {

  final List<String> list;
  final int selIn;

  GalleryView({Key key, @required this.list, @required this.selIn}) : super (key : key);

  @override
  Widget build(BuildContext context){

    List<Widget> pages = [];

    for(int i = 0; i<list.length; i++){
      FullPhoto page = new FullPhoto(photo: list[i]);
      pages.add(page);
    }
    final controller = PageController(
      initialPage: selIn,
    );

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 220,
        child: PageView(
          controller: controller,
          children: pages,
        ),
      ),
    );
  }
}