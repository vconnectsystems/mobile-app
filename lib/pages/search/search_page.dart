// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import '../../api/harbour.dart';
// import '../../api/language.dart';
// import '../../bloc/bloc.dart';
// import '../../bloc/bloc_provider.dart';
// import '../../bloc/global_state.dart';
// import '../../styles/text_styles.dart';
// import '../common/page_obj.dart';
// import '../common/icon_font_icons.dart';
// import '../dashboard/pages_enum.dart';
// //import '../dashboard/page_wrap.dart';
// //import '../dashboard/dashboard_transition_route.dart';
// import '../harbour/harbour_page.dart';
//
// class SearchPage extends StatelessWidget {
//
//   final double space = 10.0;
//   final double bigSpace = 20.0;
//   final TextEditingController controller = new TextEditingController();
//   final FocusNode focusNode = FocusNode();
//   final GlobalState _store = GlobalState.instance;
//   final MyStyles styles = new MyStyles();
//
//   SearchPage({Key key}) : super (key : key);
//
//   @override
//   Widget build(BuildContext context){
//
//     final Language lang = Language.of(context);
//     final Bloc _bloc = BlocProvider.of(context);
//
//     List<Harbour> searchResults = [];
//     controller.text = '';
//
//     void searchUpdate(){
//       searchResults.removeRange(0, searchResults.length);
//       String text = controller.text;
//       if(text.length > 1){
//         for(int i = 0; i<_store.harbours.length; i++){
//           Harbour harbour = _store.harbours[i];
//           String hName = harbour.name;
//           if(hName.toLowerCase().contains(text.toLowerCase())){
//             searchResults.add(harbour);
//           }
//         }
//       }
//       _bloc.updateSearchResults(searchResults);
//     }
//
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         SizedBox(height: 6),
//         Row (
//           children: <Widget>[
//             SizedBox(width: bigSpace),
//             Icon(IconFont.search_icon,
//               color: styles.commonColor,
//               size: 24
//             ),
//             SizedBox(width: space),
//             Container(width: 1,
//               height: 36,
//               color: styles.commonColor
//             ),
//             SizedBox(width: space),
//             Flexible(
//               child: TextFormField(
//                 focusNode: focusNode,
//                 onChanged: (text) {
//                   searchUpdate();
//                 },
//                 controller: controller,
//                 autofocus: false,
//                 keyboardType: TextInputType.text,
//                 textInputAction: TextInputAction.search,//isPass ? TextInputAction.go : TextInputAction.next,
//                 autocorrect: false,
//                 onFieldSubmitted: (term){
//                   focusNode.unfocus();
//                 },
//                 decoration: InputDecoration(
//                   border: InputBorder.none,
//                   hintText: lang.search.toLowerCase(),
//                   hintStyle: styles.textInputHintStyle,
//                   labelStyle: styles.textInputLabelStyle,
//                 ),
//                 style: styles.textInputStyle,
//               ),
//             ),
//             SizedBox(width: space)
//           ],
//         ),
//         SizedBox(height: 5),
//         Container(
//           height: 1,
//           decoration: styles.lineDecoration
//         ),
//         Expanded(
//           child: StreamBuilder<List<Harbour>>(
//             stream: _bloc.searchResults,
//             builder: (context, snapshot) {
//               return ListView.builder(
//                 padding: EdgeInsets.all(0),
//                 itemCount: snapshot.hasData ? snapshot.data.length : searchResults.length,
//                 itemBuilder: (BuildContext cont, int index) {
//                   return InkWell(
//                     splashColor: Colors.white70,
//                     onTap: (){
//                       focusNode.unfocus();
//                       Navigator.of(context).push(CupertinoPageRoute(builder: (context) => HarbourPage(harbourInd: snapshot.data[index].ind, obj: PageObj(
//                         pageId: Pages.other,
//                         title:lang.harbour.toUpperCase(),
//                         subTitle: (snapshot.data[index].name).toUpperCase()
//                       ))));
//                     },
//                     child: Container(
//                       height: 57,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: <Widget>[
//                           Padding(
//                             padding: const EdgeInsets.only(left: 20, right: 20, top: 13, bottom: 13),
//                             child: Text(snapshot.hasData ? snapshot.data[index].name : '', style: styles.listItemStyle),
//                           ),
//                           Container(height: 1, color: Color(0x291A3B62))
//                         ],
//                       ),
//                     ),
//                   );
//                 }
//               );
//             }
//           ),
//         )
//       ]
//     );
//   }
// }
