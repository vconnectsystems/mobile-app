import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:ui' as ui;
import 'dart:typed_data';
import '../../api/harbour.dart';
import '../../bloc/global_state.dart';
import 'map_modal_sheet.dart';

class MapStack extends StatefulWidget {

  final int harbourInd;

  MapStack({Key key, @required this.harbourInd}) : super(key: key);

  @override
  _MapStack createState() => _MapStack();

}

class _MapStack extends State<MapStack> {

  final MapType _mapType = MapType.normal;
  static const LatLng _center = LatLng(56.0523581,10.4732101); //Denmark
  static final CameraPosition _home = CameraPosition(target: _center, zoom: 6.00);
  final GlobalState _store = GlobalState.instance;

  GoogleMapController _controller;
  String _mapStyle;

  showModalInfo(BuildContext context, Harbour harbour){
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      backgroundColor: Colors.transparent,
      builder: (BuildContext context){
        return MapModalSheet(harbourObj: harbour, onMessage: (){});
      }
    );
  }

  Set<Marker> getMarkers(context, BitmapDescriptor icon){
    Set<Marker> markers = new Set<Marker>();
    if(widget.harbourInd == -1){
      for(int i= 0; i<_store.harbours.length; i++){
        int _markerIdCounter = i;
        String markerIdVal = _markerIdCounter.toString();
        MarkerId markerId = MarkerId(markerIdVal);
        Marker marker = Marker(
          markerId: markerId, icon: icon,
          position: new LatLng(_store.harbours[i].latitude, _store.harbours[i].longitude),
          onTap: () => showModalInfo(context, _store.harbours[i]),
        );
        markers.add(marker);
      }
    } else {
      int _markerIdCounter = 0;
      String markerIdVal = _markerIdCounter.toString();
      MarkerId markerId = MarkerId(markerIdVal);
      Marker marker = Marker(
        markerId: markerId, icon: icon,
        position: new LatLng(_store.harbours[widget.harbourInd].latitude, _store.harbours[widget.harbourInd].longitude),
        onTap: () => showModalInfo(context, _store.harbours[widget.harbourInd]),
      );
      markers.add(marker);
    }
    return markers;
  }

  Future<BitmapDescriptor> initMap() async {
    _mapStyle = await rootBundle.loadString('assets/other/map_styles.json');
    ByteData data = await rootBundle.load('assets/images/marker.png');
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: 100);
    ui.FrameInfo fi = await codec.getNextFrame();
    Uint8List markerIconBytes = (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
    return BitmapDescriptor.fromBytes(markerIconBytes);
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        FutureBuilder<BitmapDescriptor>(
          future: initMap(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              return GoogleMap(
                initialCameraPosition: _home,
                mapType: _mapType,
                markers: getMarkers(context, snapshot.data),
                mapToolbarEnabled: true,
                myLocationEnabled: false,
                compassEnabled: true,
                rotateGesturesEnabled: false,
                onMapCreated: (GoogleMapController controller) {
                  _controller = controller;
                  _controller.setMapStyle(_mapStyle);
                },
                onCameraMove: (CameraPosition mapPosition){},
              );
            } else{
              return Center(
                child: Container(
                  width: 48, height: 48,
                  padding: EdgeInsets.all(5),
                  child: CircularProgressIndicator(
                    strokeWidth: 3.5,
                    valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1A3B62))
                  ),
                ),
              );
            }
          }
        )
      ]
    );
  }
}
