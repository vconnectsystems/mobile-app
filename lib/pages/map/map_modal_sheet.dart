import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:flutter/services.dart' show PlatformException;
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dockside/pages/dashboard/dashboard_item_obj.dart';
import 'package:dockside/pages/dashboard/dashboard_data.dart';
import 'package:dockside/api/harbour.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/booking/booking_page.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import 'package:dockside/pages/common/page_obj.dart';
import 'package:dockside/pages/dashboard/dashboard_transition_route.dart';
import 'package:dockside/pages/dashboard/pages_enum.dart';
import 'package:dockside/pages/dashboard/page_wrap.dart';
import 'package:dockside/pages/harbour/harbour_page.dart';

class MapModalSheet extends StatefulWidget {
  final Harbour harbourObj;
  final GestureTapCallback onMessage;
  MapModalSheet({Key key, @required this.harbourObj, @required this.onMessage}): super(key:key);
  @override
  _MapModalSheetState createState() => _MapModalSheetState();
}

class _MapModalSheetState extends State<MapModalSheet> {

  final MyStyles _styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  final DashboardData _dashData = new DashboardData();
  Future<bool> _initFuture;
  Language _lang;
  Bloc _bloc;

  void _makeACall(String phoneNum) async{
    if(await UrlLauncher.canLaunch(phoneNum)){
      await UrlLauncher.launch(phoneNum);
    } else throw _lang.callError;
  }

  Future<void> _book() async {
    _store.bookingHarbourId = widget.harbourObj.id;
    _store.bookingHarbourIndex = widget.harbourObj.ind;
    _store.bookingHarbourType = widget.harbourObj.subscriptionType;
    if(widget.harbourObj.subscriptionType == 'light'){
      String barcodeScanRes;
      try {
        barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#1A3B62', _lang.cancel, false, ScanMode.QR);
        if(barcodeScanRes.contains('DS ')){
          _store.bookingDsCode = barcodeScanRes;
          DashboardItemObj obj = _dashData.boxes[0];
          _bloc.changePage(obj.ind);
          _bloc.changePageLoaded(false);
          PageObj pageObj = new PageObj(
            title: _lang.dashTitles[obj.ind],
            subTitle: _lang.dashSubTitles[obj.ind],
            pageId: obj.pageId,
            pageWidget: obj.pageWidget
          );
          Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: pageObj)));
        }
      } on PlatformException {
        print('Failed to get platform version.');
      }
      if (!mounted) return;
    } else {
      Navigator.of(context).pop();
      Navigator.push(context, DashboardTransitionRoute(widget: PageWrap(obj: PageObj(
        pageId: Pages.other,
        title: _lang.quick.toUpperCase(),
        subTitle: _lang.booking.toUpperCase(),
        pageWidget: BookingPage()
      ))));
    }
  }

  @override
  void initState() {
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _lang = Language.of(context);
      _bloc = BlocProvider.of(context);
      return true;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(height: 240 + MediaQuery.of(context).padding.bottom),
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(26), topRight: Radius.circular(26)),
      ),
      padding: EdgeInsets.only(top: 18, left: 24, right: 24, bottom: 16 + MediaQuery.of(context).padding.bottom),
      child: FutureBuilder<bool>(
        future: _initFuture,
        builder: (BuildContext context, AsyncSnapshot initShot) {
          if(initShot.hasData && initShot.data){
            return IntrinsicWidth(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(child: SelectableText(widget.harbourObj.name.toUpperCase(), style: _styles.modalTitleText, textAlign: TextAlign.left)),
                      IconButton(
                        icon: Icon(IconFont.closex, color: Color(0xFF1A3B62), size: 18),
                        onPressed: Navigator.of(context).pop,
                      )
                    ],
                  ),
                  SizedBox(height: 8),
                  (widget.harbourObj.locationNorth != null && widget.harbourObj.locationEast != null)
                    ? SelectableText('${widget.harbourObj.locationNorth} / ${widget.harbourObj.locationEast}', style: _styles.modalBodyText)
                    : SizedBox.shrink(),
                  (widget.harbourObj.locationNorth != null && widget.harbourObj.locationEast != null) ? SizedBox(height: 4) : SizedBox.shrink(),
                  (widget.harbourObj.master != null) ? SelectableText(_lang.harbourMaster, style: _styles.modalBiggerText) : SizedBox.shrink(),
                  (widget.harbourObj.master != null) ? SizedBox(height: 4) : SizedBox.shrink(),
                  (widget.harbourObj.master != null) ? SelectableText(widget.harbourObj.master, style: _styles.modalBodyText) : SizedBox.shrink(),
                  SizedBox(height: 16),
                  Row(
                    children: <Widget>[
                      SizedBox(width: 0),
                      Expanded(
                        child: Center(
                          child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(IconFont.info2, size: 36, color: _styles.commonColor),
                            onPressed: (){
                              Navigator.of(context).pop();
                              Navigator.of(context).push(CupertinoPageRoute(builder: (context) => HarbourPage(harbourInd: widget.harbourObj.ind, obj: PageObj(
                                pageId: Pages.other,
                                title: _lang.harbour.toUpperCase(),
                                subTitle: (widget.harbourObj.name).toUpperCase()
                              ))));
                            }
                          ),
                        )
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Center(
                          child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: Icon(IconFont.phone3, size: 36, color: _styles.commonColor),
                            onPressed: () => _makeACall("tel:${widget.harbourObj.phone}")
                          ),
                        )
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Center(
                          child: IconButton(
                            padding: EdgeInsets.all(0),
                            icon: widget.harbourObj.subscriptionType == 'super-light'
                              ? SizedBox(
                                  width: 36, height: 36,
                                  child: SvgPicture.asset('assets/svg/qr_code.svg', color: _styles.commonColor)
                                )
                              : Icon(IconFont.clock, size: 36, color: _styles.commonColor),
                            onPressed: _book
                          ),
                        )
                      ),
                    ],
                  ),
                ],
              ),
            );
          } else return Container();
        }
      ),
    );
  }
}