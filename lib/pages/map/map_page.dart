import 'package:flutter/material.dart';
import '../../pages/map/map_stack.dart';

class MapPage extends StatelessWidget {

  final int harbourInd;

  MapPage({Key key, @required this.harbourInd}) : super (key : key);

  @override
  Widget build(BuildContext context){

    return Column(
      children: <Widget>[
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24)),
            child: MapStack(harbourInd: harbourInd)
          ),
        ),
      ],
    );
  }
}
