import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../booking/apple_number_dialog.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import 'register_password_page.dart';

class RegisterBoatPage extends StatelessWidget {

  final TextEditingController modelController = new TextEditingController();
  final FocusNode modelFocusNode = FocusNode();

  final String name;
  final String mail;
  final int countryId;
  final String model;
  final MyStyles styles = new MyStyles();

  RegisterBoatPage({Key key,
    @required this.name,
    @required this.mail,
    @required this.countryId,
    @required this.model
  }) : super(key: key);

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final double space = 8.0;

    double bLength = 0;
    double bWidth = 0;
    double bDepth = 0;

    void _showToast(String mess, BuildContext context, {bool green = false}){
      Toast.show(
        mess,
        context,
        backgroundColor: green ? Colors.green : Colors.red,
        duration: styles.toastDuration,
        gravity: styles.toastGravity
      );
    }

    void _next(BuildContext context){
      if(bLength > 0){
        if(bWidth > 0){
          if(bDepth > 0) {
            Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (context) =>
                  RegisterPasswordPage(
                    name: name,
                    mail: mail,
                    countryId: countryId,
                    model: model,
                    bLength: bLength,
                    bWidth: bWidth,
                    bDepth: bDepth
                  )
              ),
            );
          } else {
            _showToast(lang.boatDepthError, context);
          }
        } else {
          _showToast(lang.boatWidthError, context);
        }
      }else{
        _showToast(lang.boatLengthError, context);
      }
    }

    void showNumPad(String type, String ttl, String initStr){
      showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: null,//Colors.black87,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
          return AppleNumberDialog(
            initSize: initStr,
            ttl: ttl,
            type: type,
            hint: '',
          );
        },
      );
    }

    return Builder(
      builder: (context) => Scaffold(
          resizeToAvoidBottomInset: false,
          body: DecoratedBox(
              decoration: styles.bgDecoration,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 32),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        padding: EdgeInsets.all(0),
                        icon: Icon(IconFont.closex, color: Color(0xFF1A3B62), size: 18),
                        onPressed: (){
                          Navigator.popUntil(context, ModalRoute.withName('/'));
                        },
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                  Spacer(flex: 1),
                  Image(image: AssetImage(lang.loginLogo)),
                  Spacer(flex: 3),
                  SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.only(left: 26, right: 26),
                    child: Container(
                      constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black38,
                            offset: new Offset(0.0, 0.0),
                            blurRadius: 16.0,
                          )
                        ],
                      ),
                      child: Row (
                        children: <Widget>[
                          SizedBox(width: space),
                          SizedBox(width: space),
                          Icon(IconFont.sizeh, color: Color(0xFF1A3B62), size: 18),
                          SizedBox(width: space),
                          Container(width: 1, height: 38, color: Color(0xFF1A3B62)),
                          SizedBox(width: space),
                          Expanded(
                            child: StreamBuilder<String>(
                              stream: _bloc.boatLength,
                              builder: (context, snapshot) {
                                bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (bLength != 0.0 ? false : true);
                                if(snapshot.hasData){
                                  bLength = double.parse(snapshot.data.toString());
                                }
                                return InkWell(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : lang.profileBoatLength)
                                              : (bLength != 0.0 ? '${bLength.toString()} m' : lang.profileBoatLength),
                                          style: isEmpty ? styles.formInputHintStyle : styles.formInputStyle,
                                          textAlign: TextAlign.left,
                                        ),
                                        Spacer(),
                                        Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                                        SizedBox(width: space),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    showNumPad('length', lang.profileBoatLength2, snapshot.hasData ? snapshot.data : bLength.toString());
                                  },
                                );
                              }
                            ),
                          ),
                          SizedBox(width: space)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.only(left: 26, right: 26),
                    child: Container(
                      constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black38,
                            offset: new Offset(0.0, 0.0),
                            blurRadius: 16.0,
                          )
                        ],
                      ),
                      child: Row (
                        children: <Widget>[
                          SizedBox(width: space),
                          SizedBox(width: space),
                          Icon(IconFont.sizew, color: Color(0xFF1A3B62), size: 18),
                          SizedBox(width: space),
                          Container(width: 1, height: 38, color: Color(0xFF1A3B62)),
                          SizedBox(width: space),
                          Expanded(
                            child: StreamBuilder<String>(
                                stream: _bloc.boatWidth,
                                builder: (context, snapshot) {
                                  bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (bWidth != 0.0 ? false : true);
                                  if(snapshot.hasData){
                                    bWidth = double.parse(snapshot.data.toString());
                                  }
                                  return InkWell(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : lang.profileBoatWidth)
                                                : (bWidth != 0.0 ? '${bWidth.toString()} m' : lang.profileBoatWidth),
                                            style: isEmpty ? styles.formInputHintStyle : styles.formInputStyle,
                                            textAlign: TextAlign.left,
                                          ),
                                          Spacer(),
                                          Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                                          SizedBox(width: space),
                                        ],
                                      ),
                                    ),
                                    onTap: (){
                                      showNumPad('height', lang.profileBoatWidth2, snapshot.hasData ? snapshot.data : bWidth.toString());
                                    },
                                  );
                                }
                            ),
                          ),
                          SizedBox(width: space)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.only(left: 26, right: 26),
                    child: Container(
                      constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black38,
                            offset: new Offset(0.0, 0.0),
                            blurRadius: 16.0,
                          )
                        ],
                      ),
                      child: Row (
                        children: <Widget>[
                          SizedBox(width: space),
                          SizedBox(width: space),
                          Icon(IconFont.sized, color: Color(0xFF1A3B62), size: 18),
                          SizedBox(width: space),
                          Container(width: 1, height: 38, color: Color(0xFF1A3B62)),
                          SizedBox(width: space),
                          Expanded(
                            child: StreamBuilder<String>(
                                stream: _bloc.boatDepth,
                                builder: (context, snapshot) {
                                  bool isEmpty = snapshot.hasData ? (snapshot.data != '0' ? false : true) : (bDepth != 0.0 ? false : true);
                                  if(snapshot.hasData){
                                    bDepth = double.parse(snapshot.data.toString());
                                  }
                                  return InkWell(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            snapshot.hasData ? (snapshot.data != '0' ? '${snapshot.data} m' : lang.profileBoatDepth)
                                                : (bDepth != 0.0 ? '${bDepth.toString()} m' : lang.profileBoatDepth),
                                            style: isEmpty ? styles.formInputHintStyle : styles.formInputStyle,
                                            textAlign: TextAlign.left,
                                          ),
                                          Spacer(),
                                          Icon(IconFont.dropdown, color: Color(0xFF1A3B62), size: 20),
                                          SizedBox(width: space),
                                        ],
                                      ),
                                    ),
                                    onTap: (){
                                      showNumPad('depth', lang.profileBoatDepth2, snapshot.hasData ? snapshot.data : bDepth.toString());
                                    },
                                  );
                                }
                            ),
                          ),
                          SizedBox(width: space)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                  StreamBuilder(
                    stream: _bloc.loading,
                    builder: (context, snapshot){
                      if(snapshot.hasData && snapshot.data){
                        return Container(
                          width: 48, height: 48,
                          padding: EdgeInsets.all(5),
                          child: CircularProgressIndicator(
                            strokeWidth: 3.5,
                            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)
                          ),
                        );
                      } else {
                        return IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(IconFont.arrow_icon, size: 46, color: Colors.white70,),
                          onPressed: (){
                            modelFocusNode.unfocus();
                            _next(context);
                          }
                        );
                      }
                    },
                  ),
                  Spacer(flex: 10),
                ]
              )
          )
      ),
    );
  }
}
