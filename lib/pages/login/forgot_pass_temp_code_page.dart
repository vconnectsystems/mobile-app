import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'login_field.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import 'forgot_pass_new_pass_page.dart';

class ForgotPassTempCodePage extends StatelessWidget {

  final TextEditingController codeController = new TextEditingController();
  final FocusNode codeFocusNode = FocusNode();
  final String email;
  final Api _api = new Api();
  final MyStyles styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;

  ForgotPassTempCodePage({Key key, this.email}) : super(key: key);

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);

    void _error(Error error, BuildContext cont){
      print(error.toString());
      _bloc.setLoading(false);
      _showToast(error.toString(), context);
    }
    void _loginSuccess(String value){
      if(value.contains('error') || value.contains('<html>')){
        _bloc.setLoading(false);
        _showToast(lang.incorrectCode, context);
      } else {
        var parsedJson = json.decode(value);
        _store.apiToken = 'Bearer '+parsedJson['api_token'];
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (context) => ForgotPassNewPassPage(email: email)),
        );
      }
    }
    void _checkCode(BuildContext context){
      _bloc.setLoading(true);
      Future result = _api.login(email, codeController.text);
      result.then((value) => _loginSuccess(value)).catchError((error) => _error(error, context));
    }

    return Builder(
      builder: (context) => Scaffold(
          resizeToAvoidBottomInset: false,
          body: DecoratedBox(
              decoration: styles.bgDecoration,
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 32),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(IconFont.closex,
                              color: Color(0xFF1A3B62),
                              size: 18
                          ),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(width: 10),
                      ],
                    ),
                    Spacer(flex: 1),
                    Image(image: AssetImage(lang.loginLogo)),
                    Spacer(flex: 3),
                    Container(
                      padding: EdgeInsets.only(left: 36, right: 36),
                      child: Text(
                        lang.forgotPassMess2,
                        style: styles.loginMediumText,
                        softWrap: true
                      ),
                    ),
                    SizedBox(height: 12),
                    Padding(
                      padding: const EdgeInsets.only(left: 26, right: 26),
                      child: LoginField(
                          icon: IconFont.password_icon,
                          textField: TextFormField(
                            focusNode: codeFocusNode,
                            controller: codeController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                            autocorrect: false,
                            onFieldSubmitted: (term){
                              codeFocusNode.unfocus();
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: lang.passCode,
                              hintStyle: styles.textInputHintStyle,
                              labelStyle: styles.textInputLabelStyle,
                            ),
                            style: styles.textInputStyle,
                          )
                      ),
                    ),
                    SizedBox(height: 40),
                    StreamBuilder(
                      stream: _bloc.loading,
                      builder: (context, snapshot){
                        if(snapshot.hasData && snapshot.data){
                          return Container(
                            width: 48, height: 48,
                            padding: const EdgeInsets.only(left: 8, top: 8, bottom: 0, right: 0),
                            child: CircularProgressIndicator(strokeWidth: 3.5,
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)),
                          );
                        } else {
                          return IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(IconFont.arrow_icon,
                                size: 46,
                                color: Colors.white70,
                              ),
                              onPressed: (){
                                codeFocusNode.unfocus();
                                _checkCode(context);
                              }
                          );
                        }
                      },
                    ),
                    Spacer(flex: 10),
                  ]
              )
          )
      ),
    );
  }
}
