import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import 'login_page.dart';

class RegisterComplete extends StatelessWidget {

  final TextEditingController nameController = new TextEditingController();
  final FocusNode nameFocusNode = FocusNode();
  final double space = 8.0;
  final MyStyles styles = new MyStyles();

  RegisterComplete({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context){

    final lang = Language.of(context);

    StringBuffer sb = new StringBuffer();
    sb.write(lang.profileCreated);
    sb.write('\n');
    sb.write(lang.profileCreated2);
    String messStr = sb.toString();

    void _next(BuildContext context){
      final PageRouteBuilder _homeRoute = new PageRouteBuilder(
        pageBuilder: (BuildContext context, _, __) {
          return LoginPage();
        },
      );
      Navigator.pushAndRemoveUntil(context, _homeRoute, (Route<dynamic> r) => false);
    }

    return Builder(
      builder: (context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(IconFont.closex,
                        color: Color(0xFF1A3B62),
                        size: 18
                    ),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
              Spacer(flex: 1),
              Image(image: AssetImage(lang.loginLogo)),
              Spacer(flex: 3),
              Container(
                margin: EdgeInsets.only(left: 26, right: 26),
                constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                decoration: styles.loginBoxDecoration,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SelectableText(
                      '$messStr',
                      textAlign: TextAlign.center,
                      style: styles.loginMessageText
                    ),
                  ],
                ),
              ),
              SizedBox(height: 40),
              IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(IconFont.arrow_icon,
                  size: 46,
                  color: Colors.white70,
                ),
                onPressed: () => _next(context),
              ),
              Spacer(flex: 10),
            ]
          )
        )
      ),
    );
  }
}
