import 'package:flutter/material.dart';

class LoginField extends StatelessWidget {
  final TextFormField textField;
  final IconData icon;
  final double space = 8.0;
  LoginField({
    Key key,
    @required this.icon,
    @required this.textField,
  }): super(key:key);
  @override
  Widget build(BuildContext context){
    return Container(
      constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          new BoxShadow(
            color: Colors.black38,
            offset: new Offset(0.0, 0.0),
            blurRadius: 16.0,
          )
        ],
      ),
      child: Row (
        children: <Widget>[
          SizedBox(width: space),
          SizedBox(width: space),
          Icon(icon, color: Color(0xFF1A3B62), size: 18),
          SizedBox(width: space),
          Container(width: 1,
            height: 38,
            color: Color(0xFF1A3B62)
          ),
          SizedBox(width: space),
          Flexible(
            child: textField,
          ),
          SizedBox(width: space)
        ],
      ),
    );
  }
}
