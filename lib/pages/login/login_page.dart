import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:provider/provider.dart';
import 'package:dockside/api/api.dart';
import 'package:dockside/api/language.dart';
import 'package:dockside/api/message_listener.dart';
import 'package:dockside/bloc/bloc.dart';
import 'package:dockside/bloc/bloc_provider.dart';
import 'package:dockside/bloc/global_state.dart';
import 'package:dockside/styles/text_styles.dart';
import 'package:dockside/pages/common/toast.dart';
import 'package:dockside/pages/common/icon_font_icons.dart';
import 'package:dockside/pages/login/login_field.dart';
import 'package:dockside/pages/login/register_name_page.dart';
import 'package:dockside/pages/login/forgot_pass_page.dart';

class LoginPage extends StatefulWidget {

  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPage createState() => _LoginPage();

}

class _LoginPage extends State<LoginPage> {

  final TextEditingController _mailController = new TextEditingController();
  final TextEditingController _passController = new TextEditingController();
  final FocusNode _mailFocusNode = FocusNode();
  final FocusNode _passFocusNode = FocusNode();
  final MyStyles _styles = new MyStyles();
  final Api _api = new Api();
  final GlobalState _store = GlobalState.instance;
  SharedPreferences _prefs;
  Future<bool> _initFuture;
  Bloc _bloc;
  Language _lang;

  // void _initDynamicLinks() async {
  //   //docksideapp.page.link
  //   FirebaseDynamicLinks.instance.onLink(
  //     onSuccess: (PendingDynamicLinkData dynamicLink) async {
  //       final Uri deepLink = dynamicLink?.link;
  //       if (deepLink != null) {
  //         Navigator.pushNamed(context, deepLink.path);
  //       }
  //     },
  //     onError: (OnLinkErrorException e) async {
  //       print('onLinkError');
  //       print(e.message);
  //     }
  //   );
  //   final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
  //   final Uri deepLink = data?.link;
  //   if (deepLink != null) {
  //     Navigator.pushNamed(context, deepLink.path);
  //   }
  // }

  void _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(mess, context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: _styles.toastDuration, gravity: _styles.toastGravity
    );
  }

  void _dataError(dynamic error){
    _bloc.setLoading(false);
    if(error.runtimeType == SocketException){
      SocketException _err = error;
      if(_err.message.contains('Failed host lookup')){
        _showToast('Please, check your internet connectivity', context);
      } else _showToast(_err.message, context);
    } else _showToast(error.toString(), context);
  }

  Future<void> _loginValidated() async {
    _bloc.setLoading(true);
    bool loginSuccess = true;
    if(!_store.guest) {
      await _api.login(_mailController.text, _passController.text).then((bool value) async {
        loginSuccess = value;
        if(loginSuccess){
          await _prefs.setString('loginMail', _mailController.text);
          await _prefs.setString('loginPass', _passController.text);
          if(_store.isMaster){
            await _api.getMasterBookings(_store.apiToken);
            _bloc.setLoading(false);
            Navigator.pushNamed(context, '/Master');
          } else {
            await _api.getProfile(_store.apiToken);
            await _api.getNewMessagesCount(_store.apiToken);
          }
        } else {
          _bloc.setLoading(false);
          _showToast(_lang.incorrectLogin, context);
        }
      }).catchError((error) => _dataError(error));
    }
    if(!_store.isMaster && loginSuccess){
      await _api.getHarbours();
      await _api.getCountries((_lang.lang == 'da') ? 'dk' : _lang.lang);
      _bloc.setLoading(false);
      Navigator.pushNamed(context, '/Dashboard');
    }
  }

  Future<void> _login(bool isGuest) async {
    _store.guest = isGuest;
    if(!isGuest){
      String mail = _mailController.text;
      String pass = _passController.text;
      if(mail.length > 6 && mail.isNotEmpty && mail.contains('@') && mail.contains('.')){
        if(pass.length > 4 && pass.isNotEmpty){
          _loginValidated();
        } else _showToast(_lang.invalidPass, context);
      } else _showToast(_lang.invalidEmail, context);
    } else _loginValidated();
  }

  Future<void> _getPrefs() async {
    // DEV
    // chavdardonchev@gmail.com, qwerasdf
    // joedonchev@icloud.com, QwerAsdf
    // Live
    // chavdardonchev@gmail.com, Victory!9
    _prefs = await SharedPreferences.getInstance();
    _mailController.text = _prefs.getString('loginMail') ?? '';
    _passController.text = _prefs.getString('loginPass') ?? '';
  }

  Future<void> _setupNotifications(BuildContext context) async {
    if (Platform.isIOS) await Provider.of<MessageListener>(context, listen: false).getPermission();
    await Provider.of<MessageListener>(context, listen: false).getDeviceToken(_mailController.text);
    Provider.of<MessageListener>(context, listen: false).context = context;
  }

  @override
  void initState() {
    super.initState();
    _initFuture = Future<bool>.delayed(Duration.zero,() async {
      _bloc = BlocProvider.of(context);
      _lang = Language.of(context);
      await _getPrefs();
      await _setupNotifications(context);
      return true;
    });
  }

  @override
  Widget build(BuildContext context){
    return Builder(
      builder: (context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: FutureBuilder<bool>(
          future: _initFuture,
          builder: (BuildContext context, AsyncSnapshot initShot) {
            if(initShot.hasData && initShot.data){
              return DecoratedBox(
                decoration: _styles.bgDecoration,
                child: Padding(
                  padding: EdgeInsets.all(26.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Spacer(flex: 3,),
                      Image(image: AssetImage(_lang.loginLogo)),
                      Spacer(flex: 2,),
                      LoginField(
                        icon: IconFont.profile_icon,
                        textField: TextFormField(
                          focusNode: _mailFocusNode,
                          controller: _mailController,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.next,
                          autocorrect: false,
                          onFieldSubmitted: (term) => _fieldFocusChange(context, _mailFocusNode, _passFocusNode),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: _lang.email,
                            hintStyle: _styles.textInputHintStyle,
                            labelStyle: _styles.textInputLabelStyle,
                          ),
                          style: _styles.textInputStyle,
                        )
                      ),
                      SizedBox(height: 12),
                      LoginField(
                        icon: IconFont.password_icon,
                        textField: TextFormField(
                          focusNode: _passFocusNode,
                          controller: _passController,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          autocorrect: false,
                          obscureText: true,
                          onFieldSubmitted: (term) => _passFocusNode.unfocus(),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: _lang.password,
                            hintStyle: _styles.textInputHintStyle,
                            labelStyle: _styles.textInputLabelStyle,
                          ),
                          style: _styles.textInputStyle,
                        )
                      ),
                      SizedBox(height: 20),
                      StreamBuilder(
                        stream: _bloc.loading,
                        builder: (context, snapshot){
                          if(snapshot.hasData && snapshot.data){
                            return Container(
                              width: 48, height: 48,
                              padding: EdgeInsets.all(5),
                              child: CircularProgressIndicator(
                                strokeWidth: 3.5,
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)
                              ),
                            );
                          } else {
                            return IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(IconFont.arrow_icon, size: 46, color: Colors.white70),
                              onPressed: () => _login(false)
                            );
                          }
                        },
                      ),
                      SizedBox(height: 32),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(_lang.notAMember, style: _styles.loginLittleText),
                          SizedBox(width: 4),
                          InkWell(
                            child: Text(_lang.getProfile, style: _styles.loginLittleLink),
                            onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => RegisterNamePage())),
                          )
                        ]
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            child: Text(_lang.asGuest, style: _styles.loginLittleLink),
                            onTap: () => _login(true),
                          )
                        ]
                      ),
                      Spacer(flex: 4),
                      InkWell(
                        child: Text(_lang.forgotPass, style: _styles.loginLittleLink),
                        onTap: () => Navigator.push(context, CupertinoPageRoute(
                          builder: (context) => ForgotPassPage(email: _mailController.text))
                        )
                      ),
                      SizedBox(height: 10),
                    ]
                  )
                )
              );
            } else return Container(decoration: _styles.bgDecoration);
          }
        )
      ),
    );
  }
}
