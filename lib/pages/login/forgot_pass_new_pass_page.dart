import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import 'login_field.dart';
import 'forgot_pass_complete.dart';

class ForgotPassNewPassPage extends StatelessWidget {

  final TextEditingController pass1Controller = new TextEditingController();
  final TextEditingController pass2Controller = new TextEditingController();
  final FocusNode pass1FocusNode = FocusNode();
  final FocusNode pass2FocusNode = FocusNode();
  final String email;
  final Api _api = new Api();
  final MyStyles styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;

  ForgotPassNewPassPage({Key key, @required this.email}) : super(key: key);

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    SharedPreferences prefs;

    void _getPrefs() async {
      prefs = await SharedPreferences.getInstance();
    }
    bool _validatePass() {
      bool valid = false;
      String pass1 = pass1Controller.text;
      String pass2 = pass2Controller.text;
      if (pass1.length > 7 && pass1.isNotEmpty && pass2.length > 7 && pass2.isNotEmpty) {
        if(pass1 == pass2) {
          valid = true;
        } else {
          _showToast(lang.passMatch, context);
        }
      }else {
        _showToast(lang.pass8, context);
      }
      return valid;
    }

    void _success(String value){
      _bloc.setLoading(false);
      print('_success: $value');
      if(value.contains('error') || value.contains('<html>')){
        _showToast(value, context);
      }else{
        prefs.setString('loginMail', email);
        prefs.setString('loginPass', pass1Controller.text);
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (context) => ForgotPassComplete()),
        );
      }
    }
    void _error(Error error, BuildContext cont){
      print('error: $error');
      _bloc.setLoading(false);
      _showToast(error.toString(), cont);
    }
    void _register(BuildContext context){
      if(_validatePass()){
        _bloc.setLoading(true);
        Future result = _api.changePassword(pass1Controller.text, _store.apiToken);
        result.then((value) => _success(value)).catchError((error) => _error(error, context));
      }
    }

    void _fieldFocusChange(BuildContext context, FocusNode currentFocus,FocusNode nextFocus) {
      currentFocus.unfocus();
      FocusScope.of(context).requestFocus(nextFocus);
    }

    _getPrefs();

    return Builder(
      builder: (context) => Scaffold(
          resizeToAvoidBottomInset: false,
          body: DecoratedBox(
              decoration: styles.bgDecoration,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 32),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(IconFont.closex,
                              color: Color(0xFF1A3B62),
                              size: 18
                          ),
                          onPressed: (){
                            Navigator.popUntil(context, ModalRoute.withName('/'));
                          },
                        ),
                        SizedBox(width: 10),
                      ],
                    ),
                    Spacer(flex: 1),
                    Image(image: AssetImage(lang.loginLogo)),
                    Spacer(flex: 3),
                    Padding(
                      padding: const EdgeInsets.only(left: 26, right: 26),
                      child: LoginField(
                          icon: IconFont.password_icon,
                          textField: TextFormField(
                            focusNode: pass1FocusNode,
                            controller: pass1Controller,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            autocorrect: false,
                            obscureText: true,
                            onFieldSubmitted: (term){
                              _fieldFocusChange(context, pass1FocusNode, pass2FocusNode);
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: lang.enterPass,
                              hintStyle: styles.textInputHintStyle,
                              labelStyle: styles.textInputLabelStyle,
                            ),
                            style: styles.textInputStyle,
                          )
                      ),
                    ),
                    SizedBox(height: 12),
                    Padding(
                      padding: const EdgeInsets.only(left: 26, right: 26),
                      child: LoginField(
                          icon: IconFont.password_icon,
                          textField: TextFormField(
                            focusNode: pass2FocusNode,
                            controller: pass2Controller,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                            autocorrect: false,
                            obscureText: true,
                            onFieldSubmitted: (term){
                              pass2FocusNode.unfocus();
                              //_checkEmail(context);
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: lang.enterPass2,
                              hintStyle: styles.textInputHintStyle,
                              labelStyle: styles.textInputLabelStyle,
                            ),
                            style: styles.textInputStyle,
                          )
                      ),
                    ),
                    SizedBox(height: 40),
                    StreamBuilder(
                      stream: _bloc.loading,
                      builder: (context, snapshot){
                        if(snapshot.hasData && snapshot.data){
                          return Container(
                            width: 48, height: 48,
                            padding: EdgeInsets.all(5),
                            child: CircularProgressIndicator(
                                strokeWidth: 3.5,
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)
                            ),
                          );
                        } else {
                          return IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(IconFont.arrow_icon,
                                size: 46,
                                color: Colors.white70,
                              ),
                              onPressed: (){
                                pass1FocusNode.unfocus();
                                pass2FocusNode.unfocus();
                                _register(context);
                              }
                          );
                        }
                      },
                    ),
                    Spacer(flex: 10),
                  ]
              )
          )
      ),
    );
  }
}
