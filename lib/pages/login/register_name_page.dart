import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../api/api.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../api/language.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import 'login_field.dart';
import 'register_country_page.dart';

class RegisterNamePage extends StatelessWidget {

  final TextEditingController nameController = new TextEditingController();
  final TextEditingController mailController = new TextEditingController();
  final FocusNode nameFocusNode = FocusNode();
  final FocusNode mailFocusNode = FocusNode();
  final MyStyles styles = new MyStyles();
  final double space = 8.0;
  final Api _api = new Api();

  RegisterNamePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context){

    final Bloc _bloc = BlocProvider.of(context);
    final Language lang = Language.of(context);

    void _showToast(String mess, BuildContext context, {bool green = false}){
      Toast.show(
        mess,
        context,
        backgroundColor: green ? Colors.green : Colors.red,
        duration: styles.toastDuration,
        gravity: styles.toastGravity
      );
    }

    bool _validateName(String value) {
      bool valid = false;
      if (value.length > 2 && value.isNotEmpty) {
        valid = true;
      }
      return valid;
    }
    bool _validateMail(String value) {
      bool valid = false;
      if (value.length > 6 && value.isNotEmpty && value.contains('@') && value.contains('.') && !value.contains(' ')) {
        valid = true;
      }
      return valid;
    }

    void _success(String value, BuildContext context){
      _bloc.setLoading(false);
      print('_checkEmail: $value');
      if(value.contains('false')){
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (context) => RegisterCountryPage(name: nameController.text, mail: mailController.text)),
        );
      } else {
        _showToast(lang.alreadyRegistered, context);
      }
    }
    void _error(Error error, BuildContext cont){
      _bloc.setLoading(false);
      _showToast(error.toString(), context);
    }
    void _checkEmail(BuildContext context){
      if(_validateName(nameController.text)){
        if(_validateMail(mailController.text)){
          _bloc.setLoading(true);
          Future result = _api.checkEmail(mailController.text);
          result.then((value) => _success(value, context)).catchError((error) => _error(error, context));
        }else{
          _showToast(lang.invalidEmail, context);
        }
      } else {
        _showToast(lang.atLeast3, context);
      }
    }

    void _launchURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        _showToast('Could not launch $url', context);
        throw 'Could not launch $url';
      }
    }

    return Builder(
      builder: (context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(IconFont.closex,
                        color: Color(0xFF1A3B62),
                        size: 18
                    ),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
              Spacer(flex: 1),
              Image(image: AssetImage(lang.loginLogo)),
              Spacer(flex: 3),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                child: LoginField(
                  icon: IconFont.profile_icon,
                  textField: TextFormField(
                    focusNode: nameFocusNode,
                    controller: nameController,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    textInputAction: TextInputAction.done,
                    autocorrect: false,
                    onFieldSubmitted: (term){
                      nameFocusNode.unfocus();
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: lang.enterName,
                      hintStyle: styles.textInputHintStyle,
                      labelStyle: styles.textInputLabelStyle,
                    ),
                    style: styles.textInputStyle,
                  )
                ),
              ),
              SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                child: LoginField(
                    icon: IconFont.email,
                    textField: TextFormField(
                      focusNode: mailFocusNode,
                      controller: mailController,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.done,
                      autocorrect: false,
                      onFieldSubmitted: (term){
                        mailFocusNode.unfocus();
                      },
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: lang.enterEmail,
                        hintStyle: styles.textInputHintStyle,
                        labelStyle: styles.textInputLabelStyle,
                      ),
                      style: styles.textInputStyle,
                    )
                ),
              ),
//              SizedBox(height: 12),
//              Padding(
//                padding: const EdgeInsets.only(left: 26, right: 26),
//                child: Container(
//                  constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
//                  decoration: BoxDecoration(
//                    color: Colors.white70,
//                    borderRadius: BorderRadius.circular(10),
//                    boxShadow: [
//                      new BoxShadow(
//                        color: Colors.black38,
//                        offset: new Offset(0.0, 0.0),
//                        blurRadius: 16.0,
//                      )
//                    ],
//                  ),
//                  child: Row (
//                    children: <Widget>[
//                      SizedBox(width: space),
//                      SizedBox(width: space),
//                      Icon(IconFont.geography, color: Color(0xFF1A3B62), size: 18),
//                      SizedBox(width: space),
//                      Container(width: 1,
//                          height: 38,
//                          color: Color(0xFF1A3B62)
//                      ),
//                      SizedBox(width: space),
//                      Expanded(
//                        child: InkWell(
//                          child: StreamBuilder<Country>(
//                            stream: _bloc.countryUpdate,
//                            builder: (context, snapshot) {
//                              if(snapshot.hasData){
//                                country = snapshot.data;
//                              }
//                              return Text(
//                                snapshot.hasData ?
//                                (country.id != -1 ? country.name : lang.nationalityHint) :
//                                (country.id == -1 ? lang.nationalityHint : country.name),
//                                style: snapshot.hasData ?
//                                (country.id != -1 ? styles.textInputStyle : styles.textInputHintStyle) :
//                                (country.id == -1 ? styles.textInputHintStyle : styles.textInputStyle),
//                                textAlign: TextAlign.left,
//                              );
//                            }
//                          ),
//                          onTap: (){
//                            nameFocusNode.unfocus();
//                            selectNationality();
//                          },
//                        ),
//                      ),
//                      SizedBox(width: space)
//                    ],
//                  ),
//                ),
//              ),

              SizedBox(height: 40),
              IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(IconFont.arrow_icon,
                  size: 46,
                  color: Colors.white70,
                ),
                onPressed: () => _checkEmail(context),
              ),
              SizedBox(height: 32),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(lang.conditionsText1, style: styles.loginLittleText),
                  ]
              ),
              SizedBox(height: 4),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(lang.conditionsText2, style: styles.loginLittleText),
                    SizedBox(width: 4),
                    InkWell(
                      child: Text(lang.conditionsBtn, style: styles.loginLittleLink),
                      onTap: (){
                        _launchURL(lang.conditionsLink);
                      },
                    )
                  ]
              ),
              Spacer(flex: 10),
            ]
          )
        )
      ),
    );
  }
}
