import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'login_field.dart';
import '../../api/api.dart';
import '../../api/language.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/global_state.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import 'forgot_pass_temp_code_page.dart';

class ForgotPassPage extends StatefulWidget {

  final String email;

  ForgotPassPage({Key key, this.email}) : super(key: key);

  @override
  _ForgotPassPageState createState() => _ForgotPassPageState();
}

class _ForgotPassPageState extends State<ForgotPassPage> {

  final TextEditingController mailController = new TextEditingController();
  final FocusNode mailFocusNode = FocusNode();
  final Api _api = new Api();
  final MyStyles styles = new MyStyles();
  final GlobalState _store = GlobalState.instance;
  Language _lang;
  Bloc _bloc;

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  bool _validateMail(String value) {
    bool valid = false;
    if (value.length > 6 && value.isNotEmpty && value.contains('@') && value.contains('.')) {
      valid = true;
    }
    return valid;
  }

  void _error(Error error, BuildContext cont){
    _bloc.setLoading(false);
    _showToast(error.toString(), cont);
  }

  void _resetSuccess(String value){
    print('_resetSuccess: $value');
    _bloc.setLoading(false);
    if (value.contains('success')){
      _store.needNewPass = true;
      Navigator.push(
        context,
        CupertinoPageRoute(builder: (context) => ForgotPassTempCodePage(email: widget.email)),
      );
    } else {
      _showToast(_lang.resetProblem, context);
    }
  }

  void _emailExists(String value){
    if(value.contains('<html>')){
      _bloc.setLoading(false);
      _showToast(_lang.invalidEmail, context);
    } else if (value.contains('false')){
      _bloc.setLoading(false);
      _showToast(_lang.noEmail, context);
    }else {
      Future result = _api.resetPassword(mailController.text);
      result.then((value) => _resetSuccess(value)).catchError((error) => _error(error, context));
//        _resetSuccess('success');
    }
  }

  void _checkEmail(BuildContext context){
    if(_validateMail(mailController.text)){
      _bloc.setLoading(true);
      Future result = _api.checkEmail(mailController.text);
      result.then((value) => _emailExists(value)).catchError((error) => _error(error, context));
    }else{
      _showToast(_lang.invalidEmail, context);
    }
  }

  @override
  Widget build(BuildContext context){

    _lang = Language.of(context);
    _bloc = BlocProvider.of(context);
    mailController.text = widget.email;

    return Builder(
      builder: (context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(IconFont.closex,
                      color: Color(0xFF1A3B62),
                      size: 18
                    ),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
              Spacer(flex: 1),
              Image(image: AssetImage(_lang.loginLogo)),
              Spacer(flex: 3),
              Container(
                padding: EdgeInsets.only(left: 36, right: 36),
                child: Text(
                  _lang.forgotPassMess1,
                  style: styles.loginMediumText,
                  softWrap: true
                ),
              ),
              SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                child: LoginField(
                  icon: IconFont.email,
                  textField: TextFormField(
                    focusNode: mailFocusNode,
                    controller: mailController,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.done,
                    autocorrect: false,
                    onFieldSubmitted: (term){
                      mailFocusNode.unfocus();
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: _lang.enterEmail2,
                      hintStyle: styles.textInputHintStyle,
                      labelStyle: styles.textInputLabelStyle,
                    ),
                    style: styles.textInputStyle,
                  )
                ),
              ),
              SizedBox(height: 40),
              StreamBuilder(
                stream: _bloc.loading,
                builder: (context, snapshot){
                  if(snapshot.hasData && snapshot.data){
                    return Container(
                      width: 48, height: 48,
                      padding: const EdgeInsets.only(left: 8, top: 8, bottom: 0, right: 0),
                      child: CircularProgressIndicator(strokeWidth: 3.5,
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)),
                    );
                  } else {
                    return IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Icon(IconFont.arrow_icon,
                        size: 46,
                        color: Colors.white70,
                      ),
                      onPressed: (){
                        mailFocusNode.unfocus();
                        _checkEmail(context);
                      }
                    );
                  }
                },
              ),
              Spacer(flex: 10),
            ]
          )
        )
      ),
    );
  }
}
