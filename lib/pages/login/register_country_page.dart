import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../api/language.dart';
import '../../api/country.dart';
import '../../bloc/bloc.dart';
import '../../bloc/bloc_provider.dart';
import '../../styles/text_styles.dart';
import '../common/icon_font_icons.dart';
import '../common/toast.dart';
import '../common/nationality_dynamic_dialog.dart';
import 'register_boat_page.dart';

class RegisterCountryPage extends StatelessWidget {

  final TextEditingController modelController = new TextEditingController();
  final FocusNode modelFocusNode = FocusNode();

  final String name;
  final String mail;
  final MyStyles styles = new MyStyles();

  RegisterCountryPage({Key key, @required this.name, @required this.mail}) : super(key: key);

  void _showToast(String mess, BuildContext context, {bool green = false}){
    Toast.show(
      mess,
      context,
      backgroundColor: green ? Colors.green : Colors.red,
      duration: styles.toastDuration,
      gravity: styles.toastGravity
    );
  }

  @override
  Widget build(BuildContext context){

    final Language lang = Language.of(context);
    final Bloc _bloc = BlocProvider.of(context);
    final double space = 8.0;

    Country country = new Country(id: -1, iso: '', name: '', ind: 0);

    bool _validateModel(String value) {
      bool valid = false;
      if (value.length > 2 && value.isNotEmpty) {
        valid = true;
      }
      return valid;
    }

    void _next(BuildContext context){
      if(_validateModel(modelController.text)){
        if(country.id != -1){
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (context) => RegisterBoatPage(
                name: name,
                mail: mail,
                countryId: country.id,
                model: modelController.text
              )
            ),
          );
        } else {
          _showToast(lang.nationalityError, context);
        }
      }else{
        _showToast(lang.modelError, context);
      }
    }

    void selectNationality(){
      showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: null,//Colors.black87,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext cont, Animation animation, Animation secondaryAnimation){
          return NationalityDynamicDialog(
            initCountry: country,
            hint: '',
          );
        },
      );
    }

    return Builder(
      builder: (context) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: DecoratedBox(
          decoration: styles.bgDecoration,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.all(0),
                    icon: Icon(IconFont.closex,
                      color: Color(0xFF1A3B62),
                      size: 18
                    ),
                    onPressed: (){
                      Navigator.popUntil(context, ModalRoute.withName('/'));
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
              Spacer(flex: 1),
              Image(image: AssetImage(lang.loginLogo)),
              Spacer(flex: 3),
              SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                child: Container(
                  constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.black38,
                        offset: new Offset(0.0, 0.0),
                        blurRadius: 16.0,
                      )
                    ],
                  ),
                  child: Row (
                    children: <Widget>[
                      SizedBox(width: space),
                      SizedBox(width: space),
                      Icon(IconFont.geography, color: Color(0xFF1A3B62), size: 18),
                      SizedBox(width: space),
                      Container(width: 1,
                          height: 38,
                          color: Color(0xFF1A3B62)
                      ),
                      SizedBox(width: space),
                      Expanded(
                        child: InkWell(
                          child: StreamBuilder<Country>(
                              stream: _bloc.countryUpdate,
                              builder: (context, snapshot) {
                                if(snapshot.hasData){
                                  country = snapshot.data;
                                }
                                return Padding(
                                  padding: const EdgeInsets.only(top: 4.0),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        snapshot.hasData ?
                                        (country.id != -1 ? country.name : lang.nationalityHint) :
                                        (country.id == -1 ? lang.nationalityHint : country.name),
                                        style: snapshot.hasData ?
                                        (country.id != -1 ? styles.textInputStyle : styles.textInputHintStyle) :
                                        (country.id == -1 ? styles.textInputHintStyle : styles.textInputStyle),
                                        textAlign: TextAlign.left,
                                      ),
                                      Spacer(),
                                      Icon(IconFont.dropdown,
                                          color: Color(0xFF1A3B62),
                                          size: 20
                                      ),
                                      SizedBox(width: space),
                                    ],
                                  ),
                                );
                              }
                          ),
                          onTap: (){
                            modelFocusNode.unfocus();
                            selectNationality();
                          },
                        ),
                      ),
                      SizedBox(width: space)
                    ],
                  ),
                ),
              ),
              SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.only(left: 26, right: 26),
                 child: Container(
                    constraints: BoxConstraints(minWidth: 120.0, maxWidth: 480.0, minHeight: 60.0, maxHeight: 60.0),
                    decoration: BoxDecoration(
                      color: Colors.white70,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        new BoxShadow(
                          color: Colors.black38,
                          offset: new Offset(0.0, 0.0),
                          blurRadius: 16.0,
                        )
                      ],
                    ),
                    child: Row (
                      children: <Widget>[
                        SizedBox(width: space),
                        SizedBox(width: space-3),
                        Icon(IconFont.boat, color: Color(0xFF1A3B62), size: 24),
                        SizedBox(width: space-3),
                        Container(width: 1,
                            height: 38,
                            color: Color(0xFF1A3B62)
                        ),
                        SizedBox(width: space),
                        Flexible(
                          child: TextFormField(
                            focusNode: modelFocusNode,
                            controller: modelController,
                            keyboardType: TextInputType.emailAddress,
                            textCapitalization: TextCapitalization.words,
                            textInputAction: TextInputAction.done,
                            autocorrect: false,
                            onFieldSubmitted: (term){
                              modelFocusNode.unfocus();
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: lang.profileBoatModel,
                              hintStyle: styles.textInputHintStyle,
                              labelStyle: styles.textInputLabelStyle,
                            ),
                            style: styles.textInputStyle,
                          ),
                        ),
                        SizedBox(width: space)
                      ],
                    ),
                  )
              ),
              SizedBox(height: 40),
              StreamBuilder(
                stream: _bloc.loading,
                builder: (context, snapshot){
                  if(snapshot.hasData && snapshot.data){
                    return Container(
                      width: 48, height: 48,
                      padding: EdgeInsets.all(5),
                      child: CircularProgressIndicator(
                        strokeWidth: 3.5,
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white70)
                      ),
                    );
                  } else {
                    return IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Icon(IconFont.arrow_icon,
                        size: 46,
                        color: Colors.white70,
                      ),
                      onPressed: (){
                        modelFocusNode.unfocus();
                        _next(context);
                      }
                    );
                  }
                },
              ),
              Spacer(flex: 10),
            ]
          )
        )
      ),
    );
  }
}
